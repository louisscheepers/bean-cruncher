//initialize chart and set
let chart = new Chartist.Line('#chart1', {
    labels:['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
    series:[[50, 70, 55, 75, 60, 80],
            [30, 38, 50, 80, 95, 20]]
}, {
    low: 0,
    high: 100,
    showArea: true,
    showPoint: false,
    fullWidth: true
    }
)

//draw lines
chart.on('draw', function(data){
    if(data.type == 'line' || data.type == 'area')
    {
        data.element.animate({
            d:{
                begin: 0 * data.index,
                dur: 2000,
                from: data.path.clone().scale(1,0).translate(0, 
                    data.chartRect.height()).stringify(), 
                to: data.path.clone().stringify(), 
                easing: Chartist.Svg.Easing.easeOutQuint 
            }
        });
    }
});
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once 'core/init.php';

if(Session::exists('home')){
    echo '<p>' . Session::flash('home') . ' </p>';
}

$user = new User();
if($user->isLoggedIn()){
    ?>
        <p>Hello <a href="profile.php?user=<?php echo escape($user->data()->username); ?>"><?php echo escape($user->data()->name); ?></p>
        <ul>
            <li><a href="profile.php">Profile</a></i>
            <li><a href="changepassword.php">Change Password</a></i>
            <li><a href="logout.php">Log out</a></i>
        </ul>
    <?php

    if($user->hasPermission('admin')){
        echo '<p>you are an administrator!';
    }


}else{
    echo '<p>You need to <a href="login.php">Login</a> or <a href="register.php">Register</a>';
}
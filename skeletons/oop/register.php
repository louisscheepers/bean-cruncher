<?php
require_once 'core/init.php';

if(Input::exists()){
    if(Token::check(Input::get('token'))){
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'username' => array(
                'required' => true,
                'min' => 2,
                'max' => 20,
                'unique' => 'users',
                'numeric' => false
            ),
            'password' => array(
                'required' => true,
                'min' => 6,
            ),
            'password_again' => array(
                'required' => true,
                'min' => 6,
                'matches' => 'password'
            ),
            'name' => array(
                'required' => true,
                'min' => 2,
                'max' => 50
            )
        ));
    
        if($validate->passed()){
            $user = new User();
            $salt = Hash::salt(16);
        
            try {
                $user->create(array(
                    'username' => Input::get('username'),
                    'password' => Hash::make(Input::get('password'), $salt),
                    'salt' => $salt,
                    'name' => Input::get('name'),
                    'joined' => date('Y-m-d H:i:s'),
                    'groups' => 1
                ));
                
                Session::flash('home', 'You Registered Successfully!');
                Redirect::to('index.php');
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
            
        }else{
            foreach($validate->errors() as $error){
                echo $error, '<br>';
            }
        }
    }
}
        

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OOP - Register</title>
</head>
<body>
    <form action="" method="POST">
        <div class="field">
            <label for="username">Username</label>
            <input type="text" name="username" id="username" value="<?php echo escape(Input::get('username'));?>" autocomplete="off">
        </div>
        <div class="password">
            <label for="password">Choose a Password</label>
            <input type="password" name="password" id="password" value="<?php echo escape(Input::get('password'));?>">
        </div>
        <div class="password_again">
            <label for="password_again">Enter your password again</label>
            <input type="password" name="password_again" id="password_again" value="<?php echo escape(Input::get('password_again'));?>">
        </div>
        <div class="name">
            <label for="name">Enter your name</label>
            <input type="text" name="name" id="name" value="<?php echo escape(Input::get('name'));?>">
        </div>

        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
        <input type="submit" value="Register">


    </form>
</body>
</html>
console.log("JS is up!");

var daysCount = new Array();

function setGraphA(){
    // Initialize x = days
    var x = document.getElementById("daysCountA").value;
    // Initialize y = series
    var lines = document.getElementById("seriesCountA").value;

    var seriesCount = new Array();

    // Fill array with days
    document.getElementById("daysDisplayA").innerHTML = "";     //clear
    for(var i = 1; i <= x; i++){
        daysCount[i] = i;
        document.getElementById("daysDisplayA").innerHTML = x;
    }

    var temp = [20, 50, 80, 70, 100];
    // Initialize arrays in seriesCount
    document.getElementById("seriesDisplayA").innerHTML = '';   //clear
    for(i = 0; i < lines; i++){
        document.getElementById("seriesDisplayA").innerHTML += '<br/><span class="font-weight-bold">Bar values for day ' + (i+1) + ':</span> ';
        seriesCount.push([]);
        for(j = 0; j < x; j++){
            seriesCount[i].push(Math.floor(Math.random() * 100));
            document.getElementById("seriesDisplayA").innerHTML += seriesCount[i][j] + ' ';
        }
    }
    
    // Initialize chart 1
    new Chartist.Bar('#chart1', {
        labels: daysCount,
        series: seriesCount
    }); 
}

function setGraphB(){
    // Initialize x = days
    var x = document.getElementById("daysCountB").value;
    // Initialize y = series
    var lines = document.getElementById("seriesCountB").value;

    var seriesCount = new Array();

    // Fill array with days
    document.getElementById("daysDisplayB").innerHTML = "";     //clear
    for(var i = 0; i < x; i++){
        daysCount[i] = i;
        document.getElementById("daysDisplayB").innerHTML = x;
    }

    // Initialize arrays in seriesCount
    document.getElementById("seriesDisplayB").innerHTML = '';   //clear
    for(i = 0; i < lines; i++){
        document.getElementById("seriesDisplayB").innerHTML += '<br/><span class="font-weight-bold">Line values ' + (i+1) + ': </span>';
        seriesCount.push([]);
        for(j = 0; j < x; j++){
            seriesCount[i].push(Math.floor(Math.random() * 100)) ;
            document.getElementById("seriesDisplayB").innerHTML += seriesCount[i][j] + ' ';
        }
    }

    // Initialize chart 1
    new Chartist.Line('#chart2', {
        labels: daysCount,
        series: seriesCount
    }, {
        // Remove this configuration to see that chart rendered with cardinal spline interpolation
        // Sometimes, on large jumps in data values, it's better to use simple smoothing.
        lineSmooth: Chartist.Interpolation.simple({
          divisor: 2
        }),
        fullWidth: true,
        chartPadding: {
          right: 20
        },
        low: 0
      });
}
function setGraphC(){
    // Initialize x = days
    var x = document.getElementById("valuesCountC").value;

    var valuesCount = new Array();

    document.getElementById("valueDisplayC").innerHTML += '<br/>';
    // Initialize valuesCount array
    for(i = 0; i < x; i++){
        valuesCount[i] = Math.floor(Math.random() * 100);
        document.getElementById("valueDisplayC").innerHTML += 'R' + valuesCount[i] + ' 000 <br/>';
    }

    // Initialize chart 2
    new Chartist.Pie('#chart1', {
        series: [20, 10, 30, 40]
      }, {
        donut: true,
        donutWidth: 60,
        startAngle: 270,
        total: 200,
        showLabel: false
      });
}
function setGraphD(){
    // Initialize x = days
    var x = document.getElementById("valuesCountD").value;

    var valuesCount = new Array();

    document.getElementById("valueDisplayD").innerHTML = '<br/>';
    // Initialize valuesCount array
    for(i = 0; i < x; i++){
        valuesCount[i] = Math.floor(Math.random() * 1000);
        document.getElementById("valueDisplayD").innerHTML += 'R' + valuesCount[i] + ' 000 <br/>';
    }

    // Initialize chart 2
    var data = {
        series: valuesCount
      };
      
      var sum = function(a, b) { return a + b };
      
      new Chartist.Pie('#chart2', data, {
        labelInterpolationFnc: function(value) {
          return Math.round(value / data.series.reduce(sum) * 100) + '%';
        }
      });
}

<?php
    require_once 'core/init.php';
    if(Input::exists()){
        if(Token::check(Input::get('token'))){

            $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'email' => array('required' => true),
                'password' => array('required' => true)
            ));

            if($validate->passed()){
                //Login
                $user = new User();
                $remember = (Input::get('remember') === 'on') ? true : false;
                $login = $user->login(Input::get('email'), Input::get('password'), $remember);

                if($login){
                    Redirect::to('index.php');
                }else{
                    echo '<p>Sorry Login Failed, check your credentials and try again</p>';
                }
            }else{  
                foreach($validate->errors() as $error){
                    echo $error, '<br>';
                }
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

        <title>BC | Sign in</title>
    </head>
    <body id="grad">
        <div class="container b-black-1 mt-100px w-400px">
            <div class="row text-center">
                <!-- Logo -->
                <div class="col-12 my-2">
                    <a href="signin.html"><img class="" src="img/logo.png" alt="logo"></a>
                </div> 
                <!-- Heading -->
                <div class="col-4 my-4">
                    <hr>
                </div>
                <div class="col-4 my-4">
                    <p class="font-weight-bold text-black">Sign in</p>
                    <?php
                    if(Session::exists('login')){
                        echo '<p> ' . escape(Session::flash('login')) .  '</p>';
                    } 
                    ?>
                </div>
                <div class="col-4 my-4">
                    <hr>
                </div>
            </div>
            
        
            <form action="" method="POST">
                <div class="row text-center">
                    <!-- Email -->
                    <div class="input-group bg-white rounded my-2">
                        <span class="input-group-addon"><i class="fas fa-user m-3 text-light-grey"></i></span>
                        <input id="email" name="email" type="text" class="form-control b-0 my-auto" placeholder="Email" autocomplete="off">
                    </div>
                    <!-- Password -->
                    <div class="input-group bg-white rounded my-2">
                        <span class="input-group-addon"><i class="fas fa-lock m-3 text-light-grey"></i></span>
                        <input name="password" id="password" type="password" class="form-control b-0 my-auto" placeholder="Password" autocomplete="off">
                    </div>
                    
                    <!-- Forgot Password? -->
                    <div class="col-12 mt-3 mb-4">
                        <a href="recovery.html" class="text-black fs-14 float-right">Forgot password?</a>
                    </div>
                    <!-- Sign In Button-->
                    <div class="col-12">
                        <input type="submit" class="btn bg-blue text-white w-400px ml-n3" value="Sign in">
                    </div>
                    <!-- Don't have an account? -->
                    <div class="col-12 text-black fs-14 my-2">
                        You don't have an account? <a href="register.php" class="text-black text-decoration-underline">Sign up</a>
                    </div>
                </div>
                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
            </form>
        </div>

        


        
        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>

        <!-- My js -->
        <script src="js/script.js"></script>
    </body>
</html>
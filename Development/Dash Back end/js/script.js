console.log("JS is up!");

// function setLabels(){
//   if(document.getElementById('radioDaily').checked) { 
//     console.log("DAILY radio button checked");
//   } 
//   else if(document.getElementById('radioWeekly').checked) { 
//     console.log("WEEKLY radio button checked");
//   } 
//   else if(document.getElementById('radioLastYear').checked) { 
//     console.log("Last year radio button checked");
//   } 
//   else if(document.getElementById('radioAllYears').checked) { 
//     console.log("All years radio button checked");
    
//     x = labelx;
//     ser = seriesData;
//   } 
//   else { 
//     alert("Please select a date range!");
//   }
// }

// console.log(labelx);
// console.log(data);

function setGraph(){
  //setLabels();
  console.log('Set graph');
  document.getElementById('chart1').innerHTML = "<h2>Bank Transactions</h2>";
    new Chartist.Line('#chart1', {
        labels: labelx,
        series: [
          data
        ]
      }, {
        fullWidth: true,
        chartPadding: {
          right: 40
        }
      },{
      lineSmooth: Chartist.Interpolation.simple({
        divisor: 2
      }),
      fullWidth: true,
      chartPadding: {
        right: 20
      },
      low: 0
    });
}
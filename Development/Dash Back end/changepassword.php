<?php
require_once 'core/init.php';

$user = new User();

if(!$user->isLoggedIn()){
    Redirect::to('index.php');
}

if(Input::exists()){
    if(Token::check(Input::get('token'))){
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'password_current' => array(
                'required'  => true,
                'min' => 6
            ),
            'password_new' => array(
                'required'  => true,
                'min' => 6
            ),
            'password_new_again' => array(
                'required'  => true,
                'min' => 6,
                'matches' => 'password_new'
            )
        ));

        if($validate->passed()){
            if(Hash::make(Input::get('password_current'), $user->data()->salt) !== $user->data()->password){
                echo "your current password is wrong";
            }else{
                $salt = Hash::salt(16);
                $user->update(array(
                    'password' => Hash::make(Input::get('password_new'), $salt),
                    'salt' => $salt
                ));

                Session::flash('login', 'Your Password has been reset');
                Redirect::to('login.php');
            }
            
        }else{
            foreach($validate->errors() as $error){
                echo $error, '<br>';
            }
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OOP - Change Password</title>
</head>
<body>
    <form action="" method="POST">
        <div class="field">
            <label for="password_current">Current Password</label>
            <input type="password" name="password_current" id="password_current" autocomplete="off">
        </div>
        <div class="field">
            <label for="password_new">Choose a Password</label>
            <input type="password" name="password_new" id="password_new" autocomplete="off">
        </div>
        <div class="field">
            <label for="password_new_again">Retype new Password</label>
            <input type="password" name="password_new_again" id="password_new_again" autocomplete="off">
         </div>

        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
        <input type="submit" value="Change Password">


    </form>
</body>
</html>
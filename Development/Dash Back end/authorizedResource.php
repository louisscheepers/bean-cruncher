<?php
  ini_set('display_errors', 'On');
  require __DIR__ . '/vendor/autoload.php';
  require_once('const/const.php');
  require_once('core/init.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">

        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>

        <title>Graphs Demo</title>

    </head>
    

    <body id="#grad">
        <div class="row">
            <!-- Sidenav Left -->
            <div class="col-2 mt-5 bg-blue p-5">
                <h2>Line Graph</h2>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="radioRange" id="radRange" value="radRange">
                    <label class="form-check-label" for="radRange">
                      Range
                    </label>
                  </div>
                  <div class="form-group">
                    <input type="date" class="form-control" id="from" placeholder="From:" name="from_date">
                    <input type="date" class="form-control" id="to" placeholder="To:" name="to_date">
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="radioRange" id="radioYear" value="radSelectYear">
                    <label class="form-check-label" for="radSelectYear">
                      Year
                    </label>
                  </div>
                  <input id="selectYear" type="number" min="1900" max="2099" value="2019" class="form-control" name="year_selected" placeholder="Year">

                  <button id="drawGMR" type="button" onclick="" class="btn btn-info my-3">Draw Graph</button>
                  <br>
            </div>
            

            <!-- Main Content -->
            <div class="col-6 mt-5">
                <div class="container h-400px w-820px">
                    <div class="h-100 w-100" id="chart1"></div>
                </div>
            </div>
        </div>
        
        <!-- Script -->
        <script src="js/script.js"></script>
        
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
          
      


        <script>
            $( document ).ready(function() {
            });

            $("#drawGMR"). click(function(){
              $("#chart1").html("Loading...").slidedown;
              var radioValue = $("input[name='radioRange']:checked"). val();
              console.log("Radio Value: " + radioValue);
              if(radioValue === "radSelectYear"){
                // console.log('Rad Year selected' + $('#selectYear').val());
                drawGMR(false);
              }
              if(radioValue === "radRange"){
                // console.log('Range: ');
                // console.log('From: ' + $('#from').val() + 'To: ' + $('#to').val());
                drawGMR(true);
              }
            });

            function drawGMR(range){
              let from_date = null;
              let to_date = null;
                if(range){
                  from_date = $('#from').val();
                  to_date = $('#to').val();
                }
                else{
                  from_date = $('#selectYear').val() + '-01-01';
                  to_date = $('#selectYear').val() + '-12-31';
                }
                
                let html = 'Loading...';
                $('.chart1').html(html).slidedown;
                $.getJSON('test.php?from_date=' + from_date + '&to_date=' + to_date, function(result){
                    setGraph(result.labels, result.data);
                });
            }

            function setGraph(labelx, data){
            //setLabels();
            console.log('Set graph');
            document.getElementById('chart1').innerHTML = "<h2>Graph</h2>";
                new Chartist.Line('#chart1', {
                    labels: labelx,
                    series: [
                    data
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                    right: 40
                    }
                },{
                lineSmooth: Chartist.Interpolation.simple({
                    divisor: 2
                }),
                fullWidth: true,
                chartPadding: {
                    right: 20
                },
                low: 0
                });
            }
        </script>
    </body>
</html>
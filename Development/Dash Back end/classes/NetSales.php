<?php

class NetSales{
    private $_netSales,
            $_apiObject;

    public function __construct($apiObject){
        $this->_apiObject = $apiObject;
        $this->setNetSales();
    }

    public function setNetSales(){

        $reports = $this->_apiObject->getReports()[0];
        $rows = $reports->getRows();
        for($i = 1; $i < count($rows); $i++){
            $innerRows = $rows[$i]->getRows();
        
            foreach($innerRows as $innerrow){
                $title = $innerrow->getCells()[0]->getValue() ;
                $value = $innerrow->getCells()[1]->getValue() ;
                if($title == 'Sales'){
                    $this->_netSales = $value;
                }
            }
        }
    }
    public function getNetSales(){
        return $this->_netSales;
    }
}
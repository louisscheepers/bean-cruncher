<?php

class SeriesData{
    private static $_data = array();

    private static function setData($fromDate, $toDate){
        $gmr = new GrossMarginRatio();
        
        DateLabels::setDateRange($fromDate, $toDate);
        $months = DateLabels::getMonthRange();
        $years = DateLabels::getYearRange();

        for($i = 0; $i < count($months); $i++){
            $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));
            $toDate = date('Y-m-d', mktime(0, 0, 0, $months[$i], 31, $years[$i]));
  
            $gmratio = $gmr->getGMR($fromDate, $toDate);

            if($gmratio == 0){
              array_push(self::$_data, null);
            }
            else{
              array_push(self::$_data, $gmratio);
            }
        }
    }
    public static function getData($fromDate, $toDate){
        self::setData($fromDate, $toDate);
        return self::$_data;
    }
}
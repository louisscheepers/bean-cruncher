<?php

class GrossMarginRatio{
    private $_grossProfit,
            $_netSales,
            $_gmr,
            $_apiObject,
            $_instance,
            $_xero;

    public function __construct(){
        $this->_xero = Xero::getInstance();
        $this->_instance = $this->_xero->getApiInstance();
    }
    
    private function setApiObject($fromDate, $toDate){
        $this->_apiObject = $this->_instance->getReportProfitAndLoss($this->_xero->getXeroTenantId(), $fromDate, $toDate);
    }

    // Set methods
    private function setGrossProfit(){
        $grossProfit = new GrossProfit($this->_apiObject);
        $this->_grossProfit = $grossProfit->getGrossProfit();
    }
    private function setNetSales(){
        $netSales = new NetSales($this->_apiObject);
        $this->_netSales = $netSales->getNetSales();
    }
    private function setGMR($fromDate, $toDate){
        $this->setApiObject($fromDate, $toDate);
        $this->setGrossProfit();
        $this->setNetSales();
        if($this->_netSales != 0){
            $this->_gmr = $this->_grossProfit / $this->_netSales;
        }
        else{
            $this->_gmr = 0;
        }
    }

    // Get methods
    public function getGrossProfit(){
        return $this->_grossProfit;
    }
    public function getNetSales(){
        return $this->_netSales;
    }
    public function getGMR($fromDate, $toDate){
        $this->setGMR($fromDate, $toDate);
        return $this->_gmr;
    }
}
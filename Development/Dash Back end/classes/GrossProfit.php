<?php

class GrossProfit{
    private $_grossProfit,
            $_apiObject;

    public function __construct($apiObject){
        $this->_apiObject = $apiObject;
        $this->setGrossProfit();
    }

    public function setGrossProfit(){

        $reports = $this->_apiObject->getReports()[0];
        $rows = $reports->getRows();
        for($i = 1; $i < count($rows); $i++){
            $innerRows = $rows[$i]->getRows();
        
            foreach($innerRows as $innerrow){
                $title = $innerrow->getCells()[0]->getValue() ;
                $value = $innerrow->getCells()[1]->getValue() ;
                if($title == 'Gross Profit'){
                $this->_grossProfit = $value;
                }
            }
        }
    }
      
    
    public function getGrossProfit(){
        return $this->_grossProfit;
    }
}
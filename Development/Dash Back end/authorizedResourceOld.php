<?php
  ini_set('display_errors', 'On');
  require __DIR__ . '/vendor/autoload.php';
  require_once('storage.php');
  require_once('const/const.php');
  // Use this class to deserialize error caught
  use XeroAPI\XeroPHP\AccountingObjectSerializer;

  // Storage Classe uses sessions for storing token > extend to your DB of choice
  $storage = new StorageClass();
  $xeroTenantId = (string)$storage->getSession()['tenant_id'];

  if ($storage->getHasExpired()) {
    $provider = new \League\OAuth2\Client\Provider\GenericProvider([
        'clientId'                =>  __CLIENT_ID__,   
        'clientSecret'            =>  __SECRET__,
        'redirectUri'             =>  __REDIRECT_URI__, 
        'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
        'urlAccessToken'          => 'https://identity.xero.com/connect/token',
        'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
    ]);

    $newAccessToken = $provider->getAccessToken('refresh_token', [
      'refresh_token' => $storage->getRefreshToken()
    ]);
    
    // Save my token, expiration and refresh token
    $storage->setToken(
        $newAccessToken->getToken(),
        $newAccessToken->getExpires(), 
        $xeroTenantId,
        $newAccessToken->getRefreshToken(),
        $newAccessToken->getValues()["id_token"] );
  }

  $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$storage->getSession()['token'] );
  $config->setHost("https://api.xero.com/api.xro/2.0");        

  $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
      new GuzzleHttp\Client(),
      $config
  );





  $apiResponse = $apiInstance->getOrganisations($xeroTenantId); 
  $myCompany = $apiResponse->getOrganisations()[0];
  
  $message = "no API calls";
  if (isset($_GET['action'])) { 
    if ($_GET["action"] == 1) {
        // Get Organisation details
        
        $apiAccounts = $apiInstance->getAccounts($xeroTenantId);
        $accounts = $apiAccounts->getAccounts();
        $message = '<table>';
        $message .= '<tr>
        <th>Name</th>
        <th>Status</th>
        <th>Type</th>
        <th>Class</th>
        <th>Description</th>
      </tr>';

        foreach($accounts as $account){
          $message .= '<tr style="min-height: 40px;">';
          $message .= '<td style="width: 200px;">' . $account->getName() . '</td>';
          $message .= '<td style="width: 200px;">' . $account->getStatus() . '</td>';
          $message .= '<td style="width: 200px;">' . $account->getType() . '</td>';
          $message .= '<td style="width: 200px;">' . $account->getClass() . '</td>';
          $message .= '<td style="padding-left:40px;width: 900px;">' . $account->getDescription() . '</td>';
          $message .= '</tr>';
        }
        $message .= '</table>';
    
    } else if ($_GET["action"] == 2) {
        
      $apiTransactions = $apiInstance->getBankTransactions($xeroTenantId);
      $transactions = $apiTransactions->getBankTransactions();

     
        $message = '<table>';
        $message .= '<tr>
        <th>Name</th>
        <th>Status</th>
        <th>Subtotal</th>
        <th>Tax</th>
        <th>Total</th>
        <th>Date</th>
        <th>Type</th>
        <th>IsReconciled?</th>
      </tr>';
      
        foreach($transactions as $item){
          $message .= '<tr style="min-height: 40px;">';
          $message .= '<td style="text-align: center;  width: 200px;">' . $item->getBankAccount()->getName() . '</td>';
          $message .= '<td style="text-align: center;  width: 200px;">' . $item->getStatus() . '</td>';
          $message .= '<td style="text-align: right;  width: 200px;">' . $item->getSubTotal() . '</td>';
          $message .= '<td style="text-align: right;  width: 200px;">' . $item->getTotalTax() . '</td>';
          $message .= '<td style="text-align: right;  width: 200px;">' . $item->getTotal() . '</td>';
          $message .= '<td style="text-align: center;  width: 200px;">' . $item->getDate()->format('Y-m-d') . '</td>';
          $message .= '<td style="text-align: center;  width: 200px;">' . $item->getType() . '</td>';
          $message .= '<td style="text-align: center;  width: 200px;">' . (($item->getIsReconciled() == 1) ? 'YES' : 'NO') . '</td>';
          $message .= '</tr>';
        }
        $message .= '</table>';
      
    
    } else if ($_GET["action"] == 3) {
      $apiItems = $apiInstance->getItems($xeroTenantId);
      $items = $apiItems->getItems();

     
        $message = '<table>';
        $message .= '<tr>
        <th>CODE</th>
        <th>NAME</th>
        <th>Tracked as Inventory</th>
        <th>Price</th>
        <th>Account Code</th>
        <th>TaxType</th>
        <th>Sold</th>
        <th>Purchased</th>
      </tr>';
      
        foreach($items as $item){
          $message .= '<tr style="min-height: 40px;">';
          $message .= '<td style="text-align: left;  width: 100px;">' . $item->getCode(). '</td>';
          $message .= '<td style="text-align: left;  width: 400px; padding-left:20px;">' . $item->getName() . '</td>';
          $message .= '<td style="text-align: right;  width: 200px;">' . (($item->getIsTrackedAsInventory() == 1) ? 'YES' : '-') . '</td>';
          $message .= '<td style="text-align: right;  width: 200px;">' . $item->getSalesDetails()->getUnitPrice() . '</td>';
          $message .= '<td style="text-align: right;  width: 200px;">' . $item->getSalesDetails()->getAccountCode() . '</td>';
          $message .= '<td style="text-align: center;  width: 200px;">' . $item->getSalesDetails()->getTaxType() . '</td>';
          $message .= '<td style="text-align: center;  width: 200px;">' . (($item->getIsSold() == 1) ? 'YES' : '-') . '</td>';
          $message .= '<td style="text-align: center;  width: 200px;">' . (($item->getIsPurchased() == 1) ? 'YES' : '-') . '</td>';
          $message .= '</tr>';
        }
        $message .= '</table>';
      
    } else if ($_GET["action"] == 4) {
        $companys = $apiResponse->getOrganisations();

        $message = '<table>';
        $message .= '<tr>
        <th>Name</th>
        <th>Pays Taxes</th>
        <th>Type</th>
        <th>Country Code</th>
        <th>Tax Number</th>
      </tr>';

        foreach($companys as $company){
          $address = $company->getAddresses();
          $message .= '<tr style="min-height: 40px;">';
          $message .= '<td style="width: 200px;">' . $company->getName() . '</td>';
          $message .= '<td style="text-align: center; width: 200px;">' . (($company->getPaysTax() == 1) ? 'YES' : '-') . '</td>';
          $message .= '<td style="text-align: center; width: 200px;">' . $company->getOrganisationType() . '</td>';
          $message .= '<td style="text-align: center; width: 200px;">' . $company->getCountryCode() . '</td>';
          $message .= '<td style="padding-left:40px;width: 200px;">' . $company->getTaxNumber() . '</td>';
          $message .= '</tr>';
        }
        $message .= '</table>';
        
    } else if ($_GET["action"] == 5){
      $apiContacts = $apiInstance->getContacts($xeroTenantId);
      $contacts = $apiContacts->getContacts();

      $message = '<table>';
        $message .= '<tr>
        <th>FullName</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>1st Address type</th>
        <th>2nd Address type</th>

      </tr>';

      foreach($contacts as $contact){
        $address = $contact->getAddresses();
        $message .= '<tr style="min-height: 40px;">';
        $message .= '<td style="width: 200px;">' . $contact->getName() . '</td>';
        $message .= '<td style="width: 200px;">' . $contact->getFirstName() . '</td>';
        $message .= '<td style="width: 200px;">' . $contact->getLastName() . '</td>';
        $message .= '<td style="width: 200px;">' . $address[0]->getAddressType() . '</td>';
        $message .= '<td style="width: 200px;">' . $address[1]->getAddressType() . '</td>';
        $message .= '</tr>';
      }
      $message .= '</table>';

    }else if ($_GET["action"] == 6){
      $apiBankTransactions = $apiInstance->getBankTransactions($xeroTenantId);
      $transactions = $apiBankTransactions->getBankTransactions();

      $message = '<table>';
        $message .= '<tr>
        <th>Bank Transactions</th>

      </tr>';
      
      $all_totals = array();

      foreach($transactions as $transaction){
        array_push($all_totals, $transaction->getTotal());
        $message .= '<td>';
        $message .= $transaction->getTotal();
        $message .= '</td>';
        $message .= '</tr>';
        
      }
      $message .= '</table>'; 

    }else if($_GET["action"] == 7){
      $apiAccounts = $apiInstance->getAccounts($xeroTenantId);
      $accs = $apiAccounts->getAccounts();

      $message = '<table>';
      $message .= '<tr>';
      $message .= '<th> Account Name </th>';
      $message .= '<th> Account Type </th>';
      $message .= '</tr>';
      foreach($accs as $acc){
        // if($acc->getClass() == 'REVENUE'){
          $message .= '<tr>';
          $message .= '<td>' . $acc->getName() . '</td>';
          $message .= '<td>' . $acc->getClass() . '</td>';
          $message .= '</tr>';
        // }
        
      }
      $message .= '</table>';
    }
  }
?>


<script type="text/javascript">
  var all_totals = <?php echo json_encode($all_totals) ?>;
</script>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">

        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>

        <title>Graphs Demo</title>

    </head>
    <div>
      <h2>
        <?php echo 'Hello ' . $myCompany->getName();  ?>
      </h2>
    </div>
    

    <body id="#grad">
        <div class="row">
            <!-- Sidenav Left -->
            <div class="col-2 mt-5 bg-blue p-5">
                <h2>Line Graph</h2>
                <form>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="radioRange" id="radioDaily" value="option1">
                    <label class="form-check-label" for="radioDaily">
                      Daily
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="radioRange" id="radioWeekly" value="option2">
                    <label class="form-check-label" for="radioWeekly">
                      Weekly
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="radioRange" id="radioMonthly" value="option3">
                    <label class="form-check-label" for="radioMonthly">
                      Monthly
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="radioRange" id="radioYearly" value="option4">
                    <label class="form-check-label" for="radioYearly">
                      Yearly
                    </label>
                  </div>

                    <button type="button" onclick="setGraph()" class="btn btn-info my-3">Draw Transaction Graph</button>
                    
                    <br/>
                </form>
            </div>
            

            <!-- Main Content -->
            <div class="col-6 mt-5">
                <div class="container h-400px w-670px">A
                    <div class="h-100 w-100" id="chart1"></div>
                </div>
                <div class="container h-400px w-670px">B
                    <div class="h-100 w-100" id="chart2"></div>
                </div>
            </div>

            <!-- Sidenav Right -->
            <div class="col-4 mt-5 bg-light-red p-5">
              <ul>
                <li><a href="authorizedResource.php?action=1">Get All Accounts</a></li>
                <li><a href="authorizedResource.php?action=2">Get All Transactions</a></li>
                <li><a href="authorizedResource.php?action=3">Get All Items</a></li>
                <li><a href="authorizedResource.php?action=4">Get All Organizations</a></li>
                <li><a href="authorizedResource.php?action=5">Get All Contacts</a></li>
                <li><a href="authorizedResource.php?action=6">Get Bank Transactions</a></li>
                <li><a href="authorizedResource.php?action=7">Get Expense Accounts</a></li>
              </ul>
              <?php
                echo($message);
              ?>
            </div>
        </div>


        
          

        

        <!-- Script -->
        <script src="js/script.js"></script>

        <!-- Bootstrap -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
    </body>
</html>
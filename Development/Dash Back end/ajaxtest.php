
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font roboto -->
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

        <title></title>
    </head>

    <body>

        <div>
            <button onclick="drawGMR();">Draw</button>
        </div>
        <div id="test" class="test">


        </div>

        <script>
            $( document ).ready(function() {
            });

            function drawGMR(){
                let from_date = '2019-10-12';
                let to_date = '2020-02-01';
                let html = 'Loading...';
                $('.test').html(html).slidedown;
                $.getJSON('test.php?from_date=' + from_date + '&to_date=' + to_date, function(result){
                    setGraph(result.labels, result.data);
                });
            }


            function setGraph(labelx, data){
            //setLabels();
            console.log('Set graph');
            document.getElementById('test').innerHTML = "<h2>Bank Transactions</h2>";
                new Chartist.Line('#test', {
                    labels: labelx,
                    series: [
                    data
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                    right: 40
                    }
                },{
                lineSmooth: Chartist.Interpolation.simple({
                    divisor: 2
                }),
                fullWidth: true,
                chartPadding: {
                    right: 20
                },
                low: 0
                });
            }
        </script>

        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>

        <!-- My js -->
        <script src="js/charts.js"></script>
        <!-- <script src="js/arrow-switcher.js"></script> -->

        <!-- Date Pick -->
        <script src="moment.min.js"></script>
        <script src="daterangepicker.js"></script>
    </body>
</html>

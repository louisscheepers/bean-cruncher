<?php
require_once 'core/init.php';

if(Input::exists()){
    if(Token::check(Input::get('token'))){
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'email' => array(
                'required' => true,
                'min' => 2,
                'max' => 20,
                'unique' => 'users'
            ),
            'password' => array(
                'required' => true,
                'min' => 6,
            ),
            'password_again' => array(
                'required' => true,
                'min' => 6,
                'matches' => 'password'
            ),
            'name' => array(
                'required' => true,
                'min' => 2,
                'max' => 50
            )
        ));
    
        if($validate->passed()){
            $user = new User();
            $salt = Hash::salt(16);
        
            try {
                $user->create(array(
                    'email' => Input::get('email'),
                    'password' => Hash::make(Input::get('password'), $salt),
                    'salt' => $salt,
                    'name' => Input::get('name'),
                    'joined' => date('Y-m-d H:i:s'),
                    'groups' => 1
                ));
                
                Session::flash('login', 'You Registered Successfully!');
                Redirect::to('verify.html');
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
            
        }else{
            foreach($validate->errors() as $error){
                echo $error, '<br>';
            }
        }
    }
}
        

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

        <title>BC | Sign up</title>
    </head>
    <body id="grad">
        <div class="container b-black-1 mt-100px w-400px">
            <div class="row text-center">
                <!-- Logo -->
                <div class="col-12 my-2">
                    <a href="signin.html"><img class="" src="img/logo.png" alt="logo"></a>
                </div> 
                <!-- Heading -->
                <div class="col-4 my-4">
                    <hr>
                </div>
                <div class="col-4 my-4">
                    <p class="font-weight-bold text-black">Sign up</p>
                </div>
                <div class="col-4 my-4">
                    <hr>
                </div>
            </div>
            
        
            <form action="" method="POST">
                <div class="row text-center">
                    <!-- Name -->
                    <div class="input-group bg-white rounded my-2">
                        <span class="input-group-addon"><i class="fas fa-user m-3 text-light-grey"></i></span>
                        <input id="name" type="text" autocomplete="off" class="form-control b-0 my-auto" name="name" placeholder="Name" value="<?php echo escape(Input::get('name'));?>">
                    </div>
                    <!-- Surname -->
                    <div class="input-group bg-white rounded my-2">
                        <span class="input-group-addon"><i class="fas fa-user m-3 text-light-grey"></i></span>
                        <input id="surname" type="text" autocomplete="off" class="form-control b-0 my-auto" name="surname" placeholder="Surname">
                    </div>
                    <!-- Email -->
                    <div class="input-group bg-white rounded my-2">
                        <span class="input-group-addon"><i class="fas fa-user m-3 text-light-grey"></i></span>
                        <input id="email" type="text" autocomplete="off" class="form-control b-0 my-auto" name="email" placeholder="Email" value="<?php echo escape(Input::get('email'));?>">
                    </div>
                    <!-- Tel number -->
                    <div class="input-group bg-white rounded my-2">
                        <span class="input-group-addon"><i class="fas fa-phone m-3 text-light-grey"></i></i></span>
                        <input id="number" type="text" autocomplete="off" class="form-control b-0 my-auto" name="number" placeholder="Tel number">
                    </div>
                    <!-- Password --> 
                    <div class="input-group bg-white rounded my-2">
                        <span class="input-group-addon"><i class="fas fa-lock m-3 text-light-grey"></i></span>
                        <input id="password" type="password" autocomplete="off" class="form-control b-0 my-auto" name="password" placeholder="Password" value="<?php echo escape(Input::get('password'));?>">
                    </div>
                    <!-- Password confirm -->
                    <div class="input-group bg-white rounded my-2">
                        <span class="input-group-addon"><i class="fas fa-lock m-3 text-light-grey"></i></span>
                        <input id="password_again" type="password" autocomplete="off" class="form-control b-0 my-auto" name="password_again" placeholder="Confirm password" value="<?php echo escape(Input::get('password_again'));?>">
                    </div>
                    
                    <!-- Agree to the Terms of Use -->
                    <div class="col-12 mt-3 mb-4 fs-20 text-black">
                        <label class="container-check">I agree to the <a href="#">Terms of Use</a>
                            <input type="checkbox">
                            <span class="checkmark rounded mt-1"></span>
                        </label>
                    </div>
                    <!-- Sign up Button-->
                    <div class="col-12">
                        <input type="submit" class="btn bg-blue text-white w-400px ml-n3" value="Sign up">
                    </div>
                    <!-- Already have an account? -->
                    <div class="col-12 text-black fs-14 my-2">
                        Already have an account? <a href="login.php" class="text-black text-decoration-underline">Sign in</a>
                    </div>
                </div>
                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
            </form>
        </div>

        


        
        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>

        <!-- My js -->
        <script src="js/script.js"></script>
    </body>
</html>
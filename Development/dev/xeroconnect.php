<?php
  ini_set('display_errors', 'On');
  require_once('const/const.php');
  require_once(__DIR__ . '/core/init.php');
  // Storage Class uses sessions for storing access token (demo only)
  // you'll need to extend to your Database for a scalable solution
  $storage = new ApiStorage();

  $user = new User();
  //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
  if($user->isLoggedIn()){
    //$logger->debug('xeroconnect.php - User is logged in!');
  }
  else{
    //$logger->debug('xeroconnect.php - User NOT logged in!');
  }

  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
  
  $provider = new \League\OAuth2\Client\Provider\GenericProvider([
    'clientId'                =>  __XERO_CLIENT_ID__,   
    'clientSecret'            =>  __XERO_SECRET__,
    'redirectUri'             =>  __REDIRECT_URI__, 
    'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
    'urlAccessToken'          => 'https://identity.xero.com/connect/token',
    'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
  ]);

  // Scope defines the data your app has permission to access.
  // Learn more about scopes at https://developer.xero.com/documentation/oauth2/scopes
  $options = [
      'scope' => ['openid email profile offline_access accounting.settings accounting.transactions accounting.contacts accounting.journals.read accounting.reports.read accounting.attachments']
  ];

  // This returns the authorizeUrl with necessary parameters applied (e.g. state).
  $authorizationUrl = $provider->getAuthorizationUrl($options);

  // Save the state generated for you and store it to the session.
  // For security, on callback we compare the saved state with the one returned to ensure they match.
  // $_SESSION['oauth2state'] = $provider->getState();
  Session::put('oauth2state', $provider->getState());
  
  // Redirect the user to the authorization URL.
  header('Location: ' . $authorizationUrl);
  exit();
?>
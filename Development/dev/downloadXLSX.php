<?php
    ini_set('display_errors', 'On');
    require_once('const/const.php');
    require_once(__DIR__ . '/core/init.php');

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Style\Fill;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    
    $spreadsheet = new Spreadsheet();
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(11);
    $sheetCount = 0;

    for($j = 1; $j <= 8; $j++){
 
        if(isset($_SESSION['dataBox' . $j]) && $_SESSION['dataBox' . $j] != null){
            
            if($sheetCount != 0){
                $spreadsheet->createSheet();
            }
            $sheet = $spreadsheet->setActiveSheetIndex($sheetCount);

            $mod = $_SESSION['dataBox' . $j][0];
            $fromDate = $_SESSION['dataBox' . $j][1];
            $toDate = $_SESSION['dataBox' . $j][2];
            $modData = PullRange::getData($mod, $fromDate, $toDate);
            unset($modData['last_updated']);
            $result = array();

            switch ($mod) {
                case 'cico':

                    $heading = 'Cash In vs Cash Out';
                    array_push($result, [$heading]);
                    $headers = getDateRangeFormat($fromDate, count($modData['cash_in']));
                    array_push($result, $headers);
                    $cashIn = array('Cash In');
                    $cashOut = array('Cash Out');

                    for($i = 0; $i < count($modData['cash_in']); $i++){
                        array_push($cashIn, strval($modData['cash_in'][$i]));
                        array_push($cashOut, strval($modData['cash_out'][$i]));
                    }

                    array_push($result, $cashIn);
                    array_push($result, $cashOut);

                    $sheet = createSheet($sheet, 'Cash In vs Cash Out', $result, count($modData['cash_in']) + 1, 4);
                    
                break;
                case 'ino':

                    $heading = 'Income vs Expenses';
                    array_push($result, [$heading]);
                    $headers = getDateRangeFormat($fromDate, count($modData['income']));
                    array_push($result, $headers);

                    $income = array('Income');
                    $expenses = array('Expenses');

                    for($i = 0; $i < count($modData['income']); $i++){
                        array_push($income, strval($modData['income'][$i]));
                        array_push($expenses, strval($modData['expenses'][$i]));
                    }

                    array_push($result, $income);
                    array_push($result, $expenses);

                    $sheet = createSheet($sheet, 'Income vs Expenses', $result, count($modData['income']) + 1, 4);

                break;
                case 'avd':

                    $heading = 'Asset vs Debt';
                    array_push($result, [$heading]);
                    $headers = ['', $toDate];
                    array_push($result, $headers);

                    $assets = array('Assets', $modData['assets']);
                    $debt = array('Debt', $modData['debt']);

                    array_push($result, $assets);
                    array_push($result, $debt);

                    $sheet = createSheet($sheet, 'Asset vs Debt', $result, 2, 4);

                break;
                case 'pl':

                    $result = getProfitAndLossAccounts($modData, $fromDate);
                    $sheet = createSheet($sheet, 'Profit and Loss', $result, 3, count($result));

                break;
                case 'bs':

                    $result = getBalanceSheetAccounts($modData, $fromDate);
                    $sheet = createSheet($sheet, 'Balance Sheet', $result, 2, count($result));

                break;
                case 'ar':

                    $heading = 'Accounts Receivable';
                    $result = getInvoiceAccounts($heading, $modData);
                    $sheet = createSheet($sheet, 'Accounts Receivable', $result, 7, count($result));

                break;
                case 'ddo':

                    $heading = 'Debtor days outstanding';
                    $result = getInvoiceAccounts($heading, $modData);
                    $sheet = createSheet($sheet, 'Debtor Days Outstanding', $result, 7, count($result));

                break;
                case 'tcs':

                    $heading = 'Top Customers by Balance';
                    $result = getInvoiceAccounts($heading, $modData);
                    $sheet = createSheet($sheet, 'Top Customers by Balance', $result, 7, count($result));

                break;
                case 'ret':

                    $heading = 'Retention';
                    array_push($result, [$heading]);
                    $headers = getDateRangeFormat($fromDate, count($modData['retention']));
                    array_push($result, $headers);

                    $retention = array('Retention');
                    for($i = 0; $i < count($modData['retention']); $i++){
                        array_push($retention, strval($modData['retention'][$i]));
                    }

                    array_push($result, $retention);

                    $sheet = createSheet($sheet, 'Retention', $result, count($modData['retention']) + 1, 3);

                break;
                case 'ap':

                    $heading = 'Accounts Payable';
                    array_push($result, [$heading]);
                    $result = getInvoiceAccounts($heading, $modData);
                    $sheet = createSheet($sheet, 'Accounts Payable', $result, 7, count($result));

                break;
                case 'cdo':

                    $heading = 'Creditor days outstanding';
                    array_push($result, [$heading]);
                    $result = getInvoiceAccounts($heading, $modData);
                    $sheet = createSheet($sheet, 'Creditor Days Outstanding', $result, 7, count($result));

                break;
                case 'tsp':

                    $heading = 'Top Suppliers by Balance';
                    array_push($result, [$heading]);
                    $result = getInvoiceAccounts($heading, $modData);
                    $sheet = createSheet($sheet, 'Top Suppliers by Balance', $result, 7, count($result));

                break;
                case 'im':

                    $heading = 'Inventory Management';
                    $result = getInventoryQuantity($heading, $modData);
                    $sheet = createSheet($sheet, 'Inventory Management', $result, 4, count($result));

                break;
                case 'inas':

                    $heading = 'Top Inventory by Sales';
                    array_push($result, [$heading]);
                    $headers = ['Item Code', 'Total Sales'];
                    array_push($result, $headers);

                    for($i = 0; $i < count($modData); $i++){
                        $code = $modData[$i]['item_code'];
                        $sold = $modData[$i]['total_sales'];

                        $item = [$code, $sold];
                        array_push($result, $item);
                    }
                    $sheet = createSheet($sheet, 'Top Inventory by Sales', $result, 2, count($result));

                break;
                case 'inaq':

                    $heading = 'Top Inventory by Quantity';
                    $result = getInventoryQuantity($heading, $modData);
                    $sheet = createSheet($sheet, 'Top Inventory by Quantity', $result, 4, count($result));

                break;
                case 'plr':

                    $heading = 'Profit and Loss Ratios';
                    array_push($result, [$heading]);
                    $headers = getDateRangeFormat($fromDate, count($modData['gross_margin']));
                    array_push($result, $headers);

                    $gm = array('Gross Margin');
                    $om = array('Operating Margin');
                    $nm = array('Net Margin');

                    for($i = 0; $i < count($modData['gross_margin']); $i++){
                        array_push($gm, strval($modData['gross_margin'][$i]));
                        array_push($om, strval($modData['operating_margin'][$i]));
                        array_push($nm, strval($modData['net_margin'][$i]));
                    }

                    array_push($result, $gm);
                    array_push($result, $om);
                    array_push($result, $nm);

                    $sheet = createSheet($sheet, 'Profit and Loss Ratios', $result, count($modData['gross_margin']) + 1, 5);

                // break;
                case 'bsr':

                    $heading = 'Balance Sheet Ratios';
                    array_push($result, [$heading]);
                    $headers = getDateRangeFormat($fromDate, count($modData['current_ratio']));
                    array_push($result, $headers);

                    $cr = array('Current Ratio');
                    $qr = array('Quick Ratio');
                    $de = array('Debt to Equity');

                    for($i = 0; $i < count($modData['current_ratio']); $i++){
                        array_push($cr, strval($modData['current_ratio'][$i]));
                        array_push($qr, strval($modData['quick_ratio'][$i]));
                        array_push($de, strval($modData['debt_to_equity'][$i]));
                    }

                    array_push($result, $cr);
                    array_push($result, $qr);
                    array_push($result, $de);

                    $sheet = createSheet($sheet, 'Balance Sheet Ratios', $result, count($modData['current_ratio']) + 1, 5);

                break;
            }

            $sheetCount++;
        }

    }

    $sheet = $spreadsheet->setActiveSheetIndex(0);
    downloadXLSX($spreadsheet);

    // ***********
    // * Methods *
    // ***********

    function createSheet($sheet, $title, $data, $xCells, $yCells){
        $sheet->setTitle($title);
        $sheet->getRowDimension('1')->setRowHeight(35);
        
        // format table
        $alphabet = range('A', 'Z');
        $lastColumn = $alphabet[$xCells - 1];

        // align first col left
        $sheet->getStyle('A2:A' . $yCells)->applyFromArray(styleAlignment('left'));
        
        // align data right
        $sheet->getStyle('B2:' . $lastColumn . $yCells)->applyFromArray(styleAlignment('right'));

        // add border to table column 1
        $sheet->getStyle('A2:A' . $yCells)->applyFromArray(styleBorders('thin', 'thin', 'thin', 'thin'));
        
        // add border to table headers
        $sheet->getStyle('A2:' . $lastColumn . '2')->applyFromArray(styleBorders('thin', 'thin', 'thin', 'thin'));
        $sheet->getStyle('A2:' . $lastColumn . '2')->applyFromArray(styleFill('E9E9E9'));

        // add border to table 
        $sheet->getStyle('A2:' . $lastColumn . $yCells)->applyFromArray(styleBorders('thin', 'thin', 'thin', 'thin'));

        if($title == 'Profit and Loss' || $title == 'Balance Sheet'){
            $data = formatSubheadings($sheet, $data, $xCells);
        }

        setAutoWidth($sheet);
        formatRange($sheet, 'A1:' . $alphabet[$xCells - 1] . '1');
        
        $sheet->fromArray($data, NULL, 'A1');
    }

    function formatRange($sheet, $range, $type = 'main'){
        
        $styleRange = $sheet->getStyle($range);

        if($type == 'main'){
            $sheet->mergeCells($range);
            $styleRange->applyFromArray(styleBold(true));
            $styleRange->applyFromArray(styleAlignment('center'));
            $styleRange->applyFromArray(styleBorders('thick', 'thick', 'thick', 'thick'));
            $styleRange->applyFromArray(styleFill());
        }
        else if($type == 'sub'){
            $sheet->mergeCells($range);
            $styleRange->applyFromArray(styleAlignment('center'));
            $styleRange->applyFromArray(styleBorders('thin', 'thin', 'thin', 'thin'));
            $styleRange->applyFromArray(styleFill('E9E9E9'));
        }
        else if($type == 'subtotal'){
            $styleRange->applyFromArray(styleBorders('thin', 'thick', 'thin', 'thin'));
            $styleRange->applyFromArray(styleFill('F0F0F0'));
        }
        else if($type == 'total'){
            $styleRange->applyFromArray(styleBorders('thin', 'thick', 'thin', 'thin'));
            $styleRange->applyFromArray(styleFill('F3F3F3'));
        }
    }

    function formatSubheadings($sheet, $data, $xCells){

        $alphabet = range('A', 'Z');
        $lastColumn = $alphabet[$xCells - 1];

        for($i = 1; $i <= count($data); $i++){
            if(isset($data[$i][3])){
                $range = 'A' . ($i + 1) . ':' . $lastColumn . ($i + 1);
                
                if($data[$i][3] == '#subheading#'){
                    formatRange($sheet, $range, 'sub');
                }
                if($data[$i][3] == '#subtotal#'){
                    formatRange($sheet, $range, 'subtotal');
                }
                if($data[$i][3] == '#total#'){
                    formatRange($sheet, $range, 'total');
                }

                unset($data[$i][3]);
            }
        }

        return $data;
    }

    function setAutoWidth($sheet){
        foreach (range('A','Z') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);  
        }
    }

    function styleBold($isBold = true){
        $styleArray = array();
        $styleArray['font']['bold'] = $isBold;

        return $styleArray;
    }

    function styleAlignment($horizontalAlignment = 'left', $verticalAlignment = 'center'){
        $styleArray = array();
        $styleArray['alignment']['vertical'] = $verticalAlignment;
        $styleArray['alignment']['horizontal'] = $horizontalAlignment;

        return $styleArray;
    }

    function styleBorders($borderTop = null, $borderBottom = null, $borderLeft = null, $borderRight = null){
        $styleArray = array();
        $styleArray['borders']['top']['borderStyle'] = $borderTop;
        $styleArray['borders']['bottom']['borderStyle'] = $borderBottom;
        $styleArray['borders']['left']['borderStyle'] = $borderLeft;
        $styleArray['borders']['right']['borderStyle'] = $borderRight;

        return $styleArray;
    }

    function styleFill($color = 'C0C0C0'){
        $styleArray = array();
        // $styleArray['fill']['fillType'] = Fill::FILL_GRADIENT_LINEAR;
        // $styleArray['fill']['startColor']['argb'] = $color;
        // $styleArray['fill']['endColor']['argb'] = 'FFFFFF';
        $styleArray['fill']['fillType'] = Fill::FILL_SOLID;
        $styleArray['fill']['startColor']['argb'] = $color;

        return $styleArray;
    }

    function generateXLSXName(){
        $now = new DateTime();
        $today = $now->format('Y-m-d');
        $companyName = Companies::getActiveCompanyName();

        return $companyName . ' - ' . $today;
    }

    function downloadXLSX($spreadsheet){
        
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . generateXLSXName() . '.xlsx"');
        $writer->save('php://output');
        exit();
    }

    function getDateRangeFormat($date, $count){

        $dates = array();
        $dt = new DateTime($date);

        array_push($dates, '');

        for($i = 0; $i < $count; $i++){
            if($i != 0){
                $dt->modify('first day of next month');
            }

            array_push($dates, $dt->format('Y-m'));
        }

        return $dates;
    }

    function getInvoiceAccounts($heading, $data){
        
        $array = array();

        array_push($array, [$heading]);
        $headers = ['Name', '120+ days', '90-120 days', '60-90 days', '30-60 days', '0-30 days', 'Total'];
        array_push($array, $headers);

        for($i = 0; $i < count($data['accounts']); $i++){
            $name = $data['accounts'][$i]['name'];
            $_0 = $data['accounts'][$i]['0-30'];
            if(!$_0)
                $_0 = '0';
            $_30 = $data['accounts'][$i]['30-60'];
            if(!$_30)
                $_30 = '0';
            $_60 = $data['accounts'][$i]['60-90'];
            if(!$_60)
                $_60 = '0';
            $_90 = $data['accounts'][$i]['90-120'];
            if(!$_90)
                $_90 = '0';
            $_120 = $data['accounts'][$i]['120+'];
            if(!$_120)
                $_120 = '0';
            $total = $data['accounts'][$i]['total'];

            if($total != 0){
                $account = [$name, $_0, $_30, $_60, $_90, $_120, $total];
                array_push($array, $account);
            }
        }

        return $array;
    }

    function getInventoryQuantity($heading, $data){
        
        $array = array();

        array_push($array, [$heading]);
        $headers = ['Item Code', 'Quantity on Hand', 'Selling Price', 'Purchase Price'];
        array_push($array, $headers);

        for($i = 0; $i < count($data['item']); $i++){
            $code = $data['item'][$i]['name'];
            $qoh = $data['item'][$i]['quantity_on_hand'];
            if(!$qoh){
                $qoh = '0';
            }
            $sell = $data['item'][$i]['sell_price'];
            if(!$sell){
                $sell = '0';
            }
            $purchase = $data['item'][$i]['purchase_price'];
            if(!$purchase){
                $purchase = '0';
            }

            $item = [$code, $qoh, $sell, $purchase];
            if($heading == 'Top Inventory by Quantity'){
                if($qoh != 0){
                    array_push($array, $item);
                }
            }
            else{
                array_push($array, $item);
            }
        }

        return $array;
    }

    function getProfitAndLossAccounts($data, $date){

        $array = array();    

        $headers = ['Profit and Loss as on ' . $date];
        $subheaders = ['Account', 'Amount', 'Year to date'];
        array_push($array, $headers);
        array_push($array, $subheaders);

        if(!empty($data['sales'])){
            array_push($array, ['Sales', '', '', '#subheading#']);
            foreach($data['sales'] as $account){
                array_push($array, [$account['account'], $account['amount'][0], getYTD($account)]);
            }
            array_push($array, ['Total Sales', $data['total_sales'][0]['amount'], $data['total_sales'][0]['ytd'], '#subtotal#']);
            array_push($array, ['']);
        }
        if(!empty($data['cost_of_sales'])){
            array_push($array, ['Cost of Sales', '', '', '#subheading#']);
            foreach($data['cost_of_sales'] as $account){
                array_push($array, [$account['account'], $account['amount'][0], getYTD($account)]);
            }
            array_push($array, ['Total Cost of Sales', $data['total_cost_of_sales'][0]['amount'], $data['total_cost_of_sales'][0]['ytd'], '#subtotal#']);
            array_push($array, ['']);
        }
        array_push($array, ['Gross Profit', $data['gross_profit'][0]['amount'], $data['gross_profit'][0]['ytd'], '#total#']);
        array_push($array, ['']);
        if(!empty($data['other_income'])){
            array_push($array, ['Other Income', '', '', '#subheading#']);
            foreach($data['other_income'] as $account){
                array_push($array, [$account['account'], $account['amount'][0], getYTD($account)]);
            }
            array_push($array, ['Total Other Income', $data['total_other_income'][0]['amount'], $data['total_other_income'][0]['ytd'], '#subtotal#']);
            array_push($array, ['']);
        }
        if(!empty($data['expenses'])){
            array_push($array, ['Expenses', '', '', '#subheading#']);
            foreach($data['expenses'] as $account){
                array_push($array, [$account['account'], $account['amount'][0], getYTD($account)]);
            }
            array_push($array, ['Total Expenses', $data['total_expenses'][0]['amount'], $data['total_expenses'][0]['ytd'], '#subtotal#']);
            array_push($array, ['']);
        }
        array_push($array, ['Net Profit Before Tax', $data['net_profit_before_tax'][0]['amount'], $data['net_profit_before_tax'][0]['ytd'], '#total#']);
        array_push($array, ['']);
        if(!empty($data['tax'])){
            array_push($array, ['Tax', '', '', '#subheading#']);
            foreach($data['tax'] as $account){
                array_push($array, [$account['account'], $account['amount'][0], getYTD($account)]);
            }
            array_push($array, ['Total Tax', $data['total_tax'][0]['amount'], $data['total_tax'][0]['ytd'], '#subtotal#']);
            array_push($array, ['']);
            array_push($array, ['Net Profit After Tax', $data['net_profit_before_tax'][0]['amount'], $data['net_profit_before_tax'][0]['ytd'], '#total#']);
        }

        return $array;
    }

    function getYTD($ytd){
        if(isset($ytd['ytd'])){
            return $ytd['ytd'];
        }
        return 0;
    }

    function getBalanceSheetAccounts($data, $date){

        if(strlen($date) < 8){
            $date = $date . '-01';
        }
        $array = array();

        $headers = ['Balance Sheet as on ' . $date];
        $subheaders = ['Account', 'Amount'];
        array_push($array, $headers);
        array_push($array, $subheaders);

        if(!empty($data['current_assets'])){
            array_push($array, ['Current Assets', '', '', '#subheading#']);
            foreach($data['current_assets'] as $account){
                array_push($array, [$account['account'], $account['data'][0]['amount']]);
            }
            array_push($array, ['Total Current Assets', $data['total_current_assets'][$date], '', '#subtotal#']);
            array_push($array, ['']);
        }
        if(!empty($data['non_current_assets'])){
            array_push($array, ['Non-Current Assets', '', '', '#subheading#']);
            foreach($data['non_current_assets'] as $account){
                array_push($array, [$account['account'], $account['data'][0]['amount']]);
            }
            array_push($array, ['Total Non-Current Assets', $data['total_non_current_assets'][$date], '', '#subtotal#']);
            array_push($array, ['']);
        }
        array_push($array, ['Total Assets', $data['total_assets'][$date], '', '#total#']);
        array_push($array, ['']);
        
        if(!empty($data['current_liabilities'])){
            array_push($array, ['Current Liabilities',  '', '', '#subheading#']);
            foreach($data['current_liabilities'] as $account){
                array_push($array, [$account['account'], $account['data'][0]['amount']]);
            }
            array_push($array, ['Total Current Liabilities', $data['total_current_liabilities'][$date], '', '#subtotal#']);
            array_push($array, ['']);
        }
        if(!empty($data['non_current_liabilities'])){
            array_push($array, ['Non-Current Liabilities', '', '', '#subheading#']);
            foreach($data['non_current_liabilities'] as $account){
                array_push($array, [$account['account'], $account['data'][0]['amount']]);
            }
            array_push($array, ['Total Non-Current Liabilities', $data['total_non_current_liabilities'][$date], '', '#subtotal#']);
            array_push($array, ['']);
        }
        array_push($array, ['Total Liabilities', $data['total_liabilities'][$date], '', '#total#']);
        array_push($array, ['']);

        if(!empty($data['equity'])){
            array_push($array, ['Equity', '', '', '#subheading#']);
            foreach($data['equity'] as $account){
                array_push($array, [$account['account'], $account['data'][0]['amount']]);
            }
            array_push($array, ['Total Equity', $data['total_equity'][$date], '', '#subtotal#']);
            array_push($array, ['']);
        }
        array_push($array, ['Total Liabilities', $data['total_liabilities'][$date], '', '#total#']);
        array_push($array, ['']);
        array_push($array, ['Total Equity and Liabilities', $data['total_equity_and_liabilities'][$date], '', '#total#']);

        return $array;

    }
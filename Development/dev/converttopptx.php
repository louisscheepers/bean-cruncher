<?php
require_once(__DIR__ . '/core/init.php');
//$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');

//$logger->debug("Checking if there is POST input for converting a pdf to PPTX");
if(Input::exists()){
    //$logger->info("Retrieving Post data");
    $base64 = Input::get('pdf');
    $filename = Input::get('name');
    //$logger->info("Filename -> " . $filename);
    $rndname =  uniqid(rand(), true);
    //$logger->info("Temp Name -> " . $rndname);
    # Decode the Base64 string, making sure that it contains only valid characters
    $bin = base64_decode($base64, true);
    //$logger->info("Decoding PDF");
    # Perform a basic validation to make sure that the result is a valid PDF file
    # Be aware! The magic number (file signature) is not 100% reliable solution to validate PDF files
    # Moreover, if you get Base64 from an untrusted source, you must sanitize the PDF contents
    if (strpos($bin, '%PDF') !== 0) {
        //$logger->error("The file uploaded for conversion is not a pdf file");
        throw new Exception('Missing the PDF file signature');
    }

    # Write the PDF contents to a local file
    //$logger->info("Saving File to tmp/pdf ");
    file_put_contents('tmp/pdf/' . $rndname . '.pdf', $bin);
    //$logger->info("Starting PPTX Conversion");
    convertToPptx($rndname, $filename);
    //$logger->info("Conversion complete");
}




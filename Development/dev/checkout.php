<?php
require_once __DIR__ . '/core/init.php';
$user = new User();

if($user->isLoggedIn() && Input::exists("GET")){

    $package = Input::get("package");
    $amount = Input::get("amount");

    $pay = new PeachPay('ZAR', 'DB');

    $fullname = new stdClass();
    $fullname->name = $user->data()->user_name;
    $fullname->surname = $user->data()->user_surname;
    $pay->checkout($amount,PeachTransactions::generateID($user->data()->user_id, 'IN'), $fullname);

    $response = $pay->getCheckoutResponse();
    $pay->setCheckoutId($response->id);
    
    $db = DB::getInstance();

    $packageTypes = $db->get('package_types', array('package_id', ">", 0));

    if($packageTypes->count()){
        $packageTypes = $packageTypes->results();
    }

    foreach($packageTypes as $pkg){
        if($package == $pkg->package_id){
            $package = $pkg;
        }
    }

    ?>


    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Beancruncher</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Beancruncher">
        <!-- Begin loading animation -->
        <link href="css/loaders/loader-pulse.css" rel="stylesheet" type="text/css" media="all" />
        <!-- End loading animation -->
        <link href="css/marketing-theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/marketing-style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Muli:400,700,800,900&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/ce368f4420.js" crossorigin="anonymous"></script>
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
        <script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=<?php echo $pay->getCheckoutId(); ?>"></script>

    </head>
    <body style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
    <div class="loader">
        <div class="loading-animation"></div>
    </div>
    
    <!--    Content-->
    <div class="container" style="min-height: 100vh;">
        <div class="row d-flex justify-content-center mb-6 mt-6">
            <div class="col-11 col-sm-12 col-md-10 col-lg-6 col-xl-6 card rounded-0-1 my-auto custom-card white-boxShadow">
                <!-- Logo -->
                <div class="row d-flex justify-content-center">
                    <div class="col-8 col-sm-6 col-md-6 col-lg-6 col-xl-6 logo-size-int mb-2">
                        <img src="img/logo.png" class="img-fluid" alt="logo">
                    </div>
                </div>
                <!-- Heading -->
                <div class="row d-flex justify-content-center verify-b my-2">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                        <h5 class="font-thin main-purple">Checkout to Continue</h5>
                    </div>
                </div>
                <!-- Lock -->
                <div class="row d-flex justify-content-center kwl-icon">
                    <div class="col-3 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-0">
                        <img src="img/payment.png" class="img-fluid" alt="logo">
                    </div>
                </div>
                <!-- Brand text -->
                <div class="row d-flex justify-content-center">
                    <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center we-sent-row">
                            <p class="verify-txt-color fs-13">
                                Checkout is currently in TEST mode, please choose VISA with card number <b>4111 1111 1111 1111</b>
                                <br>Select <b>Successfull</b> when prompted for card return code
                           </p>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-center verify-b">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                        <h5 class="main-purple"><?php echo htmlentities($package->description) ?> subscription for $<?php echo htmlentities($amount) ?> p/m</h5>
                    </div>
                </div>
                <div class="col-12">
                    <form action="<?php echo $pay->getReturnUrl() . '?id=' . $pay->getCheckoutId(); ?>" class="paymentWidgets" data-brands="VISA MASTER AMEX"></form>

                    <div class=" float-left verify-b peach-logo">
                        <img class="img-fluid" src="img/Peach.png">
                    </div>
                    <script>
                        var wpwlOptions = {
                            showCVVHint: true,
                        }
                    </script>
                </div>
                <div class="row d-flex justify-content-center int-btn-b">
                    <div class="col-5 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <button onclick="history.back()" class="btn btn-block the-border main-purple py-2 fs-12" >Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon fmr-m" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>

    
    
    
    <!--    BACK TO TOP BTN-->
    <a href="#top" class="btn rounded-circle btn-back-to-top btt-btn" data-smooth-scroll data-aos="fade-up" data-aos-offset="2000" data-aos-mirror="true" data-aos-once="false">
        <img src="img/icons/interface/icon-arrow-up.svg" alt="Icon" class="icon bg-white" data-inject-svg>
    </a>
    <!-- Required vendor scripts (Do not remove) -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- Smooth scroll (animation to links in-page)-->
    <script type="text/javascript" src="js/smooth-scroll.polyfills.min.js"></script>
    <!-- SVGInjector (replaces img tags with SVG code to allow easy inclusion of SVGs with the benefit of inheriting colors and styles)-->
    <script type="text/javascript" src="js/svg-injector.umd.production.js"></script>
    <!-- Required theme scripts (Do not remove) -->
    <script type="text/javascript" src="js/theme.js"></script>
    <!-- Removes page load animation when window is finished loading -->
    <script type="text/javascript">
        window.addEventListener("load",function(){document.querySelector('body').classList.add('loaded');});
    </script>

    <!--
    <script src='https://github.com/mattbryson/TouchSwipe-Jquery-Plugin/blob/master/jquery.touchSwipe.min.js'></script>
    <script  src="assets/reviews3d-slider/dist/script.js"></script>
    -->

    </body>
    </html>
    <?php
}else{
    Redirect::to('index.php');
}
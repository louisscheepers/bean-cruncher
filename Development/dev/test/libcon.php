<?php
require_once '../vendor/autoload.php';
require'../core/init.php';
$pdf_file = 'tmp.pdf';
$inDir = __DIR__ . '/tmp/pdf/'. $pdf_file;
$pptx_file = 'tmp.pptx';
$outDir = __DIR__ . '/tmp/pptx/';
$outFile = 'tmp.pptx';
$converter = new OfficeConverter($inDir, $outDir);
$converter->convertTo($outFile); //generates pdf file in same directory as test-file.docx
//to specify output directory, specify it as the second argument to the constructor
Redirect::to('/tmp/pptx/' . $outFile );
<?php
  ini_set('display_errors', 'On');
  require_once('const/const.php');
  require_once('core/init.php');
  $user = new User();
  if($user->isLoggedIn()){
  }
  else{
    Redirect::to('index.php');
  }
  
  if(Companies::getActiveCompanyId() == null){
    $companies = Companies::getCompanies();
    if($companies == null){
        Redirect::to('integration.php');
    }
    else{
        Companies::setActiveCompany($comapnies[0]['id']);
    }
  }

  if(isset($_GET['exists'])){
    if($_GET['exists'] == ''){
      Redirect::to('company-settings.php');
    }
    $registeredComps = explode(',', $_GET['exists']);
  }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=1200" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Company Settings</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

        <link href="css/custom.css" rel="stylesheet" />
    </head>

    <body class="content-bg">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
               
                <nav class="sb-topnav navbar navbar-expand navbar-dark p-0 my-3">    
                    <div class="container nav-size">
                        <div class="row d-content no-gutters">
                            <!-- First Colum with nested colums -->
                            <div class="col-2 col-lg-5">
                                <div class="row">
                                    <div class="col-12 col-lg-2 align-self-center">
                                        <a type="button" class=" btn-lg d-content" href="index.php">
                                            <img class="img-fluid arrow-icon-size pl-1" src="img/arrow-left-icon.png">
                                        </a>
                                    </div>

                                    <div class="col-lg-10 d-none d-lg-block"></div>
                                </div>
                            </div>

                            <!-- Second Colum with nested colums -->    
                            <div class="col-8 col-lg-2 text-center">
                                <h5 class="nav-settings d-content txt-color">
                                  Company Settings
                                </h5>
                            </div>

                            <!-- Third Colum with nested colums -->
                            <div class="col-2 col-lg-5">
                                <div class="row">
                                    <div class="col-lg-1 d-none d-lg-block">
                                        <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0 "></div>
                                    </div>
                                    
                                    <div class="col-lg-9 txt-align-right align-self-center d-none d-lg-block">
                                        <h5 id="company-heading" class="nav-settings d-content">
                                            <?php echo Companies::getActiveCompanyName() ?>
                                        </h5>
                                    </div>
                                    <div class="col-12 col-lg-2 align-self-center">
                                        <div id="comp-icon" class="float-right">
                                          <?php
                                            if(Companies::getActiveCompanyBrand() == null){
                                                echo '<img id="comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
                                            }
                                            else{
                                                echo '<img id="comp-img" src="data:image/jpeg;base64,' . base64_encode(Companies::getActiveCompanyBrand()) . '" class="rounded-circle icon-size" alt="Profile Icon">';
                                            }
                                          ?>                                        </div>
                                      </div>
                                </div>
                             </div>
                        </div>
                    </div>
                </nav>

                <main>
                  <div class="container my-5 p-0">
                    <div class="card">
                      <div class="card-body profile-card">
                        <div class="row mt-3">
                            <div class="col-sm-1 align-self-center">
                              <div class="text-right">
                                <img src="img/companySettings.png" class="img-fluid company-icon-size">
                              </div>
                            </div>
                            <div class="col-sm-8 align-self-center">
                              <div class="txt-inline">
                                <h5 class="font-weight-normal align-middle txt-color mt-m5">Company Settings</h5>
                              </div>
                            </div>
                            <div class="col-sm-3">
                            <div class="text-center ">
                                  <a class="tooltip-hover" onclick="linkDisabledTooltip();" onmouseover="onLinkMouseOver();" onmouseout="onLinkMouseOverOut();">
                                    <span id="linkTooltip" class="tooltiptext">Upgrade to Continue</span>
                                    <button id="linkcompanyBtnDisabled"  type="button" class="btn d-none btn-cross-platform my-3 px-4">Link another company</button>
                                  </a>
                                  <a id="linkcompanyBtnActive" href="integration.php">
                                      <button type="button" class="btn btn-cross-platform my-3 px-4">Link another company</button>
                                    </a>
                                  </div>
                            </div>
                        </div>

                        <div class="dropdown-divider"></div>

                        <div class="row my-lg-5 pb-4">
                          <div class="col-sm-12 mt-lg-1">
                            <div class="text-center my-6">
                              <img src="img/xero-2.png" class="img-fluid brand-height-xero" alt="Xero">
                              <!-- <h5 class="font-weight-200 align-middle txt-color">Companies</h5>
                              <p class="txt-color font-weight-bold font-size-16">Xero</p>-->
                            </div>
                            <div class="row sm-blue-card mx-3">
                              <div class="col-sm-5 align-self-center">
                                <h5 class="font-weight-normal txt-color-w font-size-16 mb-0">Company <a id="sort-tag-xero" onclick="sortCompanies()"><i id="sort-xero-down" class="fas fa-sort filter-icon" style="width: 18px;"></i></a></h5>
                              </div>
                              <div class="col-sm-2 align-self-center">
                              <h5 class="font-weight-normal txt-color-w text-center font-size-16 mb-0">Industry</h5>
                              </div>
                              <div class="col-sm-3"></div>
                              <div class="col-sm-2 align-self-center">
                              </div>
                            </div>
                            <div id="companyRowsXero" class="row mt-4"> 
                              <div id="loader" class="my-auto mx-auto"><img src="img/BC-preloader-gif.gif" style="height: 40px;"></div>
                            </div>
                            <?php
                              if(isset($_GET['exists'])){
                                $registeredComps = explode(',', $_GET['exists']);
                                echo '<div class="row my-lg-4 text-center">';
                                if(count($registeredComps) == 1){
                                  echo '<div class="col-sm-12 col-12 txt-color-orange fs-14 mt-3">';
                                  echo '<div class="container w-50">';
                                  echo '<b>' . $registeredComps[0] . '</b> has already been registered to Beancruncher';
                                }
                                else{
                                  echo '<div class="col-sm-12 col-12 txt-color-orange text-left fs-14 mt-3">';
                                  echo '<div class="container w-50">';
                                  echo '<span>The following companies are already registered to Beancruncher: </span><br>';
                                  for($i = 0; $i < count($registeredComps); $i++){
                                    echo '<span class="float-left"> • ' . $registeredComps[$i] . '</span><br>';
                                  }
                                }
                                echo '</div></div></div>';
                              }
                            ?>
                          </div>
                        </div>
                            
                        <div class="row my-lg-5 py-4">
                          <div class="col-sm-12 mt-lg-1">
                            <div class="text-center my-6">
                            <img src="img/sage-2.png" class="img-fluid brand-height-sage" alt="Sage">
                            </div>
                            <div class="row sm-blue-card mx-3">
                              <div class="col-sm-5 align-self-center">
                                <h5 class="font-weight-normal txt-color-w font-size-16 mb-0">Company<a id="sort-tag-sage" onclick="sortCompanies()"><i id="sort-sage-down" class="fas fa-sort filter-icon" style="width: 18px;"></i></a> </h5>
                              </div>
                              <div class="col-sm-2 align-self-center">
                              <h5 class="font-weight-normal txt-color-w text-center font-size-16 mb-0">Industry</h5>
                              </div>
                              <div class="col-sm-3"></div>
                              <div class="col-sm-2 align-self-center">
                              </div>
                            </div>
                              <div id="companyRowsSage" class="row mt-4"> 
                                <div id="loader" class="my-auto mx-auto"><img src="img/BC-preloader-gif.gif" style="height: 40px;"></div>
                              </div>
                          </div>
                        </div>
                            
                        <div class="row my-lg-5 py-4">
                          <div class="col-sm-12 mt-lg-1">
                            <div class="text-center my-6">
                              <img src="img/quickbooks-2.png" class="img-fluid brand-height-q" alt="Quickbooks">
                            </div>
                            <div class="row sm-blue-card mx-3">
                              <div class="col-sm-5 align-self-center">
                                <h5 class="font-weight-normal txt-color-w font-size-16 mb-0">Company<a id="sort-tag-qb" onclick="sortCompanies()"><i id="sort-qb-down" class="fas fa-sort filter-icon" style="width: 18px;"></i></a> </h5>
                              </div>
                              <div class="col-sm-2 align-self-center">
                              <h5 class="font-weight-normal txt-color-w text-center font-size-16 mb-0">Industry</h5>
                              </div>
                              <div class="col-sm-3"></div>
                              <div class="col-sm-2 align-self-center">
                              </div>
                            </div>
                              <div id="companyRowsQuickbooks" class="row mt-4"> 
                                <div id="loader" class="my-auto mx-auto"><img src="img/BC-preloader-gif.gif" style="height: 40px;"></div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </main>
            </div>
        </div>

        <!-- Delete Company Modal -->
        <div class="modal fade" id="confirm-delete" role="dialog">
          <div class="modal-dialog modal-dialog-centered w-35">
            <div class="modal-content txt-color">
            <div class="modal-header">
            <button type="button" class="close z-index-1" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
              <div class="modal-body">
                <div class="container">
                  <div class="row justify-content-center">
                    <img src="img/Delete company 1.png" class="img-fluid modal-icon-size">
                  </div>
                  <h5 class="mt-4 modal-title text-center font-weight-bold font-size-16">Delete Company</h5>
                  <br>
                  <p class="font-weight-normal font-size-14 text-center">By confirming this action, this company and its data will be removed from Beancruncher.</p>
                  <p class="font-weight-normal font-size-14 text-center">Do you want to continue?</p>

                  <div class="row mb-4">
                    <div class="col-6 text-right">
                      <div id="confirm-btn"></div>
                    </div>
                    <div class="col-6 text-left">
                      <button type="button" class="mt-3 btn btn-cross-platform btn-w align-self-center" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>


        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Company Settings Script -->
        <script src="js/company-settings.js"></script>

        <!-- Simple Search Script -->
    <script src="js/simpleSearch.js"></script>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>

    </body>
</html>

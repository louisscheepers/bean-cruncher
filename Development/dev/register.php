
<?php
require_once __DIR__ . '/core/init.php';

$user = new User();

//$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
if($user->isLoggedIn()){
    //$logger->debug('Register - User is logged in');
}
else{
    //$logger->debug('Register - NOT LOGGED IN!');
}

$msg = '';
$tos = '';
$isPending = false;

if(Input::exists()){
    $db = DB::getInstance();

    if(isset($_POST['sign-up'])){
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'email' => array(
                'required' => true,
                'min' => 2,
                'max' => 32
            ),
            'password' => array(
                'required' => true,
                'min' => 6,
            ),
            'password-again' => array(
                'required' => true,
                'matches' => 'password'
            ),
            'name' => array(
                'required' => true,
                'min' => 2,
                'max' => 50
            ),
            'surname' => array(
                'required' => true,
                'min' => 2,
                'max' => 50
            ),
            'number' => array(
                'required' => true,
                'min' => 10,
                'max' => 10
            )
        ));

        if($validate->passed() && isset($_POST['tos-agreement'])){

            $exist = $db->get('user', array(
                'user_email' => array('operator' => '=', 'value' => $_POST['email'])
            ));
            if($exist->results()){
                $msg .= 'User already registered';
            }
            else{
                $user = new User();
                $salt = Hash::salt(16);

                $pending = $db->get('temp_pending_user', array(
                    'user_email' => array('operator' => '=', 'value' => $_POST['email'])
                ));
                if($pending->results()){
                    $isPending = true;
                    $companies = array();
                    $pendingUserId = $pending->first()->pending_user_id;

                    $dbPendingUserCompDesc = $db->get('temp_pending_user_company_desc', array(
                        'pending_user_id' => array('operator' => '=', 'value' => $pendingUserId)
                    ));
                    if($dbPendingUserCompDesc->results()){
                        $data = $dbPendingUserCompDesc->results();

                        for($i = 0; $i < count((array)$data); $i++){
                            $pendingUserCompId = $data[$i]->pending_user_company_id;

                            $dbPendingUserComp = $db->get('temp_pending_user_company', array(
                                'pending_user_company_id' => array('operator' => '=', 'value' => $pendingUserCompId)
                            ));
                            if($dbPendingUserComp->results()){
                                $dbCompanyId = $dbPendingUserComp->first()->company_id;
                                $companies[$i] = $dbCompanyId;

                                $deleteUserComp = $db->delete('temp_pending_user_company', array(
                                    'pending_user_company_id', '=', $pendingUserCompId
                                ));
                                $deleteUserCompDesc = $db->delete('temp_pending_user_company_desc', array(
                                    'pending_user_company_id', '=', $pendingUserCompId
                                ));
                            }
                        }
                    }
                }
                try {

                    $package = 0;
                    $expiry = 0;
                    if($isPending){
                        $package = 2;
                        $expiry = 4764960000;
                    }

                    $user->create(array(
                        'user_email' => Input::get('email'),
                        'user_password' => Hash::make(Input::get('password'), $salt),
                        'user_salt' => $salt,
                        'user_name' => Input::get('name'),
                        'user_surname' => Input::get('surname'),
                        'user_number' => Input::get('number'),
                        'user_joined' => date('Y-m-d H:i:s'),
                        'package_id' => $package,
                        'package_expiry' => $expiry,
                        'verified' => 0
                    ));
                    $deletePendingUser = $db->delete('temp_pending_user', array(
                        'user_email', '=', $_POST['email']
                    ));

                    if($companies != null){
                        for($i = 0; $i < count((array)$companies); $i++){
                            Companies::addNewUser($_POST['email'], $companies[$i], false);
                        }
                    }
                    $login = $user->login(Input::get('email'), Input::get('password'), $remember);

                    if($login){
                        Redirect::to('index.php');
                    }
                    else{
                        Redirect::to('login.php');
                    }
                } 
                catch (Exception $e) {
                    die($e->getMessage());
                }
            }
        }
        else{
            for($i = 0; $i < count($validate->errors()); $i++){
                if($validate->errors()[$i] == 'password must match password-again'){
                    $msg .= 'Passwords don\'t match<br>';
                }
                else if($validate->errors()[$i] == 'password must be a minimum of 6 characters'){
                    $msg .= 'Password must be a minimum of 6 characters<br>';
                }else if($validate->errors()[$i] == 'number must be a minimum of 10 characters'){
                    $msg .= 'Phone number is invalid <br>';
                }else{
                    $msg .= $validate->errors()[$i] . '<br>';
                }
                
                // echo $validate->errors()[$i] . '<br>';
            }
            if(!isset($_POST['tos-agreement'])){
                $tos = 'You must accept our Terms of Service <br>';
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

        <title>BC | Sign up</title>
    </head>
    <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
       <div class="container" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-6 mt-6">
                <div class="col-11 col-sm-11 col-md-11 col-lg-6 col-xl-6 card custom-card my-auto rounded-0-1 white-boxShadow">
                    <!-- Logo -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-7 col-sm-7 col-md-7 col-lg-7 col-xl-7 logo-size-pos">
                            <img src="img/logo.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <!-- Heading -->
                    <div class="row d-flex justify-content-center org-setup-sign">    
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <span>Sign up</span>
                        </div>
                    </div>
                    <?php
                        if($msg != ''){
                            echo '<div class="text-color-red fs-13 text-center mb-4">' . $msg . '</div>';
                        }
                    ?>
                    <!-- Form -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-11 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                            <form action="" method="POST" class="needs-validation">
                    <!-- Name -->
                                <div class="input-group shadow-custom-sm org-name g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Guy icon.svg" class="img-fluid email_icon_width" alt=""/></span>
                                    </div>
                                    <input id="name" name="name" type="text" class="form-control border-left-0 ml-1 pl-1 rounded-right-border field-text-color fs-14 my-auto" placeholder="Name" value="<?php echo Input::get('name') ?>" required>
                                </div>
                    <!-- Surname -->
                                <div class="input-group shadow-custom-sm org-name g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Guy icon.svg" class="img-fluid email_icon_width" alt=""/></span>
                                    </div>
                                    <input id="surname" name="surname" type="text" class="form-control border-left-0 ml-1 pl-1 rounded-right-border field-text-color fs-14 my-auto" placeholder="Surname" value="<?php echo Input::get('surname') ?>" required>
                                </div>
                    <!-- Email -->
                                <div class="input-group shadow-custom-sm org-name g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border pr-2"><img src="img/Email icon.svg" class="img-fluid email_icon_width" alt=""/></span>
                                    </div>
                                    <input id="email" name="email" type="email" class="form-control border-left-0 ml-1 pl-1 rounded-right-border field-text-color fs-14 my-auto" placeholder="Email address" value="<?php echo Input::get('email') ?>" required>
                                </div>
                    <!-- Number -->
                                <div class="input-group shadow-custom-sm org-name g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Phone icon.svg" class="img-fluid email_icon_width" alt=""/></span>
                                    </div>
                                    <input id="number" name="number" type="text" class="form-control border-left-0 ml-1 pl-1 rounded-right-border field-text-color fs-14 my-auto" placeholder="Tel number" value="<?php echo Input::get('number') ?>" required>
                                </div>
                    <!-- Password -->
                                <div class="input-group shadow-custom-sm org-name g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Lock icon.svg" class="img-fluid email_icon_width" alt=""/></span>
                                    </div>
                                    <input id="password" name="password" type="password" class="form-control border-left-1 ml-1 pl-1 rounded-right-border field-text-color fs-14 my-auto" placeholder="Password" value="<?php echo Input::get('password') ?>" required>
                                    <div id="revealPassword" class="input-group-append">
                                        <span class="input-group-text bg-white rounded-right-border"><span id="eye-icon" class="eye-style"><i class="far fa-eye"></i></span></span>
                                    </div>
                                </div>
                    <!-- Password Again -->
                                <div class="input-group shadow-custom-sm org-name-up g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Lock icon.svg" class="img-fluid email_icon_width" alt=""/></span>
                                    </div> 
                                    <input id="passwordAgain" name="password-again" type="password" class="form-control border-left-0 ml-1 pl-1 rounded-right-border field-text-color fs-14 my-auto" placeholder="Confirm password" value="<?php echo Input::get('password-again') ?>" required>
                                    <div id="revealPasswordAgain" class="input-group-append">
                                        <span class="input-group-text bg-white rounded-right-border"><span id="eye-icon1" class="eye-style"><i class="far fa-eye"></i></span></span>
                                    </div>
                                </div>
                    <!-- Agree to the Terms of Use -->
                                <div class="bluegrey agree-b">
                                    <label class="container-check mb-0"><span class="fs-12">I agree to the </span><a href="assets/terms-of-use/Terms of Use.pdf" target="_blank" class="bluegrey fs-12"><b>Terms of Use</b></a>
                                        <input id="tos-agreement" name="tos-agreement" type="checkbox" <?php if(Input::get('tos-agreement') == 'on'){ echo 'checked';} ?> >
                                        <span class="checkmark rounded "></span>
                                    </label>
                                </div>
                                <?php
                                    
                                    if($tos != ''){
                                        echo '<div class="text-color-red fs-13 mt-n3 mb-4">' . $tos . '</div>';
                                    }
                                ?>
                    <!-- Sign up -->
                                <div class="row d-flex justify-content-center">
                                    <div class="col-6 col-sm-6 col-md-5 col-lg-5 col-xl-5">
                                        <input name="sign-up" type="submit" class="btn btn-block nav-free-but the-border main-purple py-2 fs-12" value="Sign up">
                                    </div>
                                </div>
                    <!-- Already have an account? -->
                                <div class="row bluegrey d-flex justify-content-center mb-4 mt-2">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                                        <span class="fs-12">Already have an account? <a href="login.php" class="bluegrey"><b>Sign in</b></a></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>
       
        <script>
            (function() {
            'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            // form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();

            $('#revealPassword').on('click', function (){
                var inputType = $('#password').attr('type');
                if(inputType === 'password'){
                    $('#password').get(0).type = 'text';
                    $('#eye-icon').html('<i class="far fa-eye-slash"></i>');
                }else{
                    $('#password').get(0).type = 'password';
                    $('#eye-icon').html('<i class="far fa-eye"></i>');
                }
            });

            $('#revealPasswordAgain').on('click', function (){
                var inputType = $('#passwordAgain').attr('type');
                if(inputType === 'password'){
                    $('#passwordAgain').get(0).type = 'text';
                    $('#eye-icon1').html('<i class="far fa-eye-slash"></i>');
                }else{
                    $('#passwordAgain').get(0).type = 'password';
                    $('#eye-icon1').html('<i class="far fa-eye"></i>');
                }
            });
        </script>

        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    </body>
</html>
<?php
require_once __DIR__ . '/core/init.php';
$user = new User();

if($user->isLoggedIn()){

    $pay = new PeachPay('ZAR', 'DB');

    $fullname = null;
    $fullname->name = $user->data()->user_name;
    $fullname->surname = $user->data()->user_surname;
    $pay->checkout('120.00',PeachTransactions::generateID($user->data()->user_id, 'IN'), $fullname);

    $response = $pay->getCheckoutResponse();
    $pay->setCheckoutId($response->id);

    ?>


    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Beancruncher</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Beancruncher">
        <!-- Begin loading animation -->
        <link href="css/loaders/loader-pulse.css" rel="stylesheet" type="text/css" media="all" />
        <!-- End loading animation -->
        <link href="css/marketing-theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/marketing-style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Muli:400,700,800,900&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/ce368f4420.js" crossorigin="anonymous"></script>
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
        <script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=<?php echo $pay->getCheckoutId(); ?>"></script>
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

    </head>
    <body style="overflow-x: auto; min-width: 100%;">
    <div class="loader">
        <div class="loading-animation"></div>
    </div>
    <div class="navbar-container">
        <nav class="navbar navbar-expand-lg navbar-light shadow-custom" data-sticky="top">
            <a class="navbar-brand" href="index.html">
                <img src="img/Slices/new/Nav_logo.png" alt="" class="nav-logo">
            </a>
            <div class="">
                <button aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target=".navbar-collapse" data-toggle="collapse" type="button">
                    <img alt="Navbar Toggler Open Icon" class="navbar-toggler-open icon icon-sm main-purple" data-inject-svg src="img/icons/interface/icon-menu.svg">
                    <img alt="Navbar Toggler Close Icon" class="navbar-toggler-close icon icon-sm main-purple" data-inject-svg src="img/icons/interface/icon-x.svg">
                </button>
            </div>
            <div class="collapse navbar-collapse order-3 order-lg-2 justify-content-lg-end" id="navigation-menu">
                <ul class="navbar-nav navbar-center">
                    <li class="nav-item mx-lg-3 mobi-nav-style secret-shadow">
                        <div class="dropdown">
                            <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item nav-active ml-3 ml-lg-0 mobi-nav-style-1" data-toggle="dropdown-grid" href="/pages/index.html" role="button">Home</a>
                        </div>
                    </li>
                    <li class="nav-item mx-lg-3 mobi-nav-style">
                        <div class="dropdown nav-act">
                            <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item nav-weight ml-3 ml-lg-0 mobi-nav-style-1" data-toggle="dropdown-grid" href="/pages/features.html" role="button">Features</a>
                        </div>
                    </li>
                    <li class="nav-item mx-lg-3 mobi-nav-style">
                        <div class="dropdown nav-act">
                            <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item nav-weight ml-3 ml-lg-0 mobi-nav-style-1" data-toggle="dropdown-grid" href="/pages/pricing.html" role="button">Pricing</a>
                        </div>
                    </li>
                    <li class="nav-item mx-lg-3 mobi-nav-style">
                        <div class="dropdown nav-act">
                            <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item nav-weight ml-3 ml-lg-0 mobi-nav-style-1" data-toggle="dropdown-grid" href="/pages/demo.html" role="button">Demo</a>
                        </div>
                    </li>
                    <li class="nav-item mx-lg-3 mobi-nav-style">
                        <div class="dropdown nav-act">
                            <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item nav-weight ml-3 ml-lg-0 mobi-nav-style-1" data-toggle="dropdown-grid" href="/pages/contact.html" role="button">Contact</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="collapse navbar-collapse order-3 order-lg-2 justify-content-end" id="navigation-menu">
                <div>
                    <ul class="navbar-nav">
                        <li class="nav-item mx-lg-3 pr-lg-1 mobi-nav-style">
                            <div class="dropdown nav-act">
                                <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item nav-weight ml-3 ml-lg-0 mobi-nav-style-1 nav-login-orange" data-toggle="dropdown-grid" href="#" role="button">Login</a>
                            </div>
                        </li>
                        <li class="nav-item d-none d-sm-none d-md-none d-lg-block">
                            <div class="dropdown">
                                <a aria-expanded="false" aria-haspopup="true" class="btn nav-free-but ml-3 ml-lg-0" data-toggle="dropdown-grid" href="pricing.html" role="button">Free Trial</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
<!--    Content-->
    <div class="container">
        <div class="row d-flex justify-content-center mb-6 mt-6">
            <div class="col-11 col-sm-12 col-md-10 col-lg-6 col-xl-6 card rounded-0-1 my-auto custom-card white-boxShadow">
                <!-- Logo -->
                <div class="row d-flex justify-content-center">
                    <div class="col-8 col-sm-6 col-md-6 col-lg-6 col-xl-6 logo-size-int mb-2">
                        <a href=""><img src="img/logo.png" class="img-fluid" alt="logo"></a>
                    </div>
                </div>
                <!-- Heading -->
                <div class="row d-flex justify-content-center verify-b">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                        <h5 class="font-thin main-purple">Checkout to Continue</h5>
                    </div>
                </div>
                <!-- Lock -->
                <div class="row d-flex justify-content-center kwl-icon">
                    <div class="col-3 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-0">
                        <a href=""><img src="img/payment.png" class="img-fluid" alt="logo"></a>
                    </div>
                </div>
                <!-- Brand text -->
                <div class="row d-flex justify-content-center">
                    <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center we-sent-row">
                            <p class="verify-txt-color fs-13">Checkout is currently in TEST mode, please choose VISA with card number 4200 0000 0000 0000</b></p>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-center verify-b">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                        <h5 class="main-purple">Pro subscription for R120 p/m</h5>
                    </div>
                </div>
                <div class="col-12">
                    <form action="<?php echo $pay->getReturnUrl() . '?id=' . $pay->getCheckoutId(); ?>" class="paymentWidgets" data-brands="VISA MASTER AMEX"></form>

                    <div class=" float-left verify-b peach-logo">
                        <img class="img-fluid" src="img/Peach.png">
                    </div>
                    <script>
                        var wpwlOptions = {
                            showCVVHint: true
                        }
                    </script>
                </div>
                <div class="row d-flex justify-content-center int-btn-b">
                    <div class="col-5 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <a href="#" onclick="history.back()" class="btn btn-block the-border main-purple py-2 fs-12" >Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--    Footer -->
    <footer class="py-0 shadow-lg footer-height d-none d-lg-block">
        <div class="container-fluid">
            <div class="row">
                <div class="col-2 d-none d-sm-block d-md-none d-lg-block p-0 mt-auto">
                    <img alt="BeanCruncher" class="img-fluid" src="img/Slices/Home/Footer%20Left%20Side.png">
                </div>
                <div class="col-lg-2 d-none d-sm-block d-md-none d-lg-block">
                    <ul class="nav col-md-12 pt-lg-3 pl-5 pl-lg-4 ml-5 pr-5 mb-lg-4">
                        <li class="nav-item">
                            <a href="index.html" class="nav-link main-purple footer-menu pb-lg-0 pr-lg-5">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="features.html" class="nav-link pt-0 main-purple footer-menu not-bold pb-lg-0 pr-lg-5"><div class="the-purple">Features</div></a>
                        </li>
                        <li class="nav-item">
                            <a href="pricing.html" class="nav-link pt-0 main-purple footer-menu not-bold pb-lg-0 pr-lg-5"><div class="the-purple">Pricing</div></a>
                        </li>
                        <li class="nav-item">
                            <a href="demo.html" class="nav-link pt-0 main-purple footer-menu not-bold pb-lg-0 pr-lg-5"><div class="the-purple">Demo</div></a>
                        </li>
                        <li class="nav-item">
                            <a href="contact.html" class="nav-link main-purple pt-0 footer-menu not-bold pb-lg-0 pr-lg-5"><div class="the-purple">Contact</div></a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-7 pt-sm-0 px-5">
                    <h6 class="main-purple text-center mt-md-4 pb-1 pb-sm-0">Subscribe to our newsletter</h6>
                    <div class="row">
                        <div class="col-lg-12 col-11 col-md-8 px-0">
                            <form data-form-email novalidate action="/forms/mailchimp.php">
                                <div class="d-flex flex-sm-row form-group">
                                    <input style="height: 35px" class="form-control flex-grow-1 the-border-field custom-f-font" name="email" placeholder="Email Address" type="email" required>
                                    <button type="submit" class="submit-btn-pink btn btn-loading btn-sm ml-2" data-loading-text="Sending">
                                        <!-- Icon for loading animation -->
                                        <svg class="icon bg-white" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <title>Icon For Loading</title>
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g>
                                                    <polygon points="0 0 24 0 24 24 0 24" opacity="0"></polygon>
                                                </g>
                                                <path d="M12,4 L12,6 C8.6862915,6 6,8.6862915 6,12 C6,15.3137085 8.6862915,18 12,18 C15.3137085,18 18,15.3137085 18,12 C18,10.9603196 17.7360885,9.96126435 17.2402578,9.07513926 L18.9856052,8.09853149 C19.6473536,9.28117708 20,10.6161442 20,12 C20,16.418278 16.418278,20 12,20 C7.581722,20 4,16.418278 4,12 C4,7.581722 7.581722,4 12,4 Z"
                                                      fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 12.000000) scale(-1, 1) translate(-12.000000, -12.000000) ">
                                                </path>
                                            </g>
                                        </svg>
                                        <span class="">Submit</span>
                                    </button>
                                </div>
                                <div data-recaptcha data-sitekey="INSERT_YOUR_RECAPTCHA_V2_SITEKEY_HERE" data-size="invisible" data-badge="bottomleft"></div>
                                <div class="d-none alert alert-success w-100" role="alert" data-success-message>
                                    Thanks, a member of our team will be in touch shortly.
                                </div>
                                <div class="d-none alert alert-danger w-100" role="alert" data-error-message>
                                    Please fill all fields correctly.
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6 col-md-3 ml-2 ml-sm-0">
                    <h6 class="main-purple mb-1 mt-md-4 pl-3">Contact Us:</h6>
                    <p class="light-purple not-bold mb-0 pl-3">+27 (0) 72 253 1794 / +27 (0) 61 519 8817</p>
                    <p class="light-purple not-bold mb-0 pl-3">3802 Jan Frederick Ave, Randpark Ridge, 2156</p>
                </div>
                <div class="col-lg-1 col-5 col-md-2 pl-4">
                    <h6 class="main-purple mb-1 mt-md-4">Follow us:</h6>
                    <a href="#"><i class="fab fa-facebook-f main-purple mr-1 hov-ora"></i></a>
                    <a href="#"><i class="fab fa-twitter main-purple mr-1 hov-ora"></i></a>
                    <a href="#"><i class="fab fa-instagram main-purple hov-ora"></i></a>
                </div>
                <div class="col-1 d-none d-sm-block d-md-none d-lg-block p-0 mt-auto">
                    <img alt="BeanCruncher" class="img-fluid" src="img/Slices/Home/Footer%20Right%20Side.png">
                </div>
            </div>
        </div>
    </footer>
    <footer class="py-0 d-none d-lg-block">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 footer-purple py-3 small">
                    <p class="m-0 text-center footer-small-font text-white not-bold">Copyright©2020 |
                        <a class="footer-small-font text-white not-bold">Designed by </a>
                        <a class="footer-small-font text-white not-bold" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <footer class="py-0 d-block d-sm-none shadow-lg">
        <div class="container-fluid">
            <div class="row pt-5">
                <div class="col-10">
                    <img src="img/Slices/Footer Mobile/Mobile logo.png" alt="BeanCruncher" class="img-fluid pl-4">
                </div>
            </div>
            <div class="row my-5 pl-3">
                <div class="col-4">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="index.html" class="nav-link main-purple footer-menu pb-0">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="features.html" class="nav-link main-purple footer-menu not-bold pb-0">Features</a>
                        </li>
                        <li class="nav-item">
                            <a href="pricing.html" class="nav-link main-purple footer-menu not-bold pb-0">Pricing</a>
                        </li>
                        <li class="nav-item">
                            <a href="demo.html" class="nav-link main-purple footer-menu not-bold pb-0">Demo</a>
                        </li>
                        <li class="nav-item">
                            <a href="contact.html" class="nav-link main-purple footer-menu not-bold pb-0">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
            <h6 class="main-purple col-11 my-4 ml-3 d-none d-sm-block">Subscribe to our newsletter</h6>
            <form data-form-email novalidate action="/forms/mailchimp.php">
                <input style="height: 40px" class="form-control flex-grow-1 the-border-field custom-f-font col-8 mx-2 mb-4 ml-4 d-none d-sm-block" name="email" placeholder="Email Address" type="email" required>
                <button type="submit" class="btn btn-loading submit-btn-pink btn-sm ml-2 col-4 btn-lg mb-5 ml-4 d-none d-sm-block" data-loading-text="Sending">
                    Icon for loading animation
                    <svg class="icon bg-white" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <title>Icon For Loading</title>
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g>
                                <polygon points="0 0 24 0 24 24 0 24" opacity="0"></polygon>
                            </g>
                            <path d="M12,4 L12,6 C8.6862915,6 6,8.6862915 6,12 C6,15.3137085 8.6862915,18 12,18 C15.3137085,18 18,15.3137085 18,12 C18,10.9603196 17.7360885,9.96126435 17.2402578,9.07513926 L18.9856052,8.09853149 C19.6473536,9.28117708 20,10.6161442 20,12 C20,16.418278 16.418278,20 12,20 C7.581722,20 4,16.418278 4,12 C4,7.581722 7.581722,4 12,4 Z"
                                  fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 12.000000) scale(-1, 1) translate(-12.000000, -12.000000) ">
                            </path>
                        </g>
                    </svg>
                    <span class="text-white">Submit</span>
                </button>
                <div data-recaptcha data-sitekey="INSERT_YOUR_RECAPTCHA_V2_SITEKEY_HERE" data-size="invisible" data-badge="bottomleft"></div>
                <div class="d-none alert alert-success w-100" role="alert" data-success-message>
                    Thanks, a member of our team will be in touch shortly.
                </div>
                <div class="d-none alert alert-danger w-100" role="alert" data-error-message>
                    Please fill all fields correctly.
                </div>
            </form>
            <div class="col-12 ml-3">
                <h6 class="main-purple">Contact Us:</h6>
                <p class="light-purple not-bold mb-0">+27 (0) 72 253 1794 / +27 (0) 61 519 8817</p>
                <p class="light-purple not-bold mb-0">3802 Jan Frederick Ave, Randpark Ridge, 2156</p>
            </div>
            <div class="row">
                <div class="col-5 my-5">
                    <h6 class="main-purple ml-3 pl-2">Follow us:</h6>
                    <a href="#"><i class="fab fa-facebook-f main-purple mr-2 ml-3 pl-2 hov-ora"></i></a>
                    <a href="#"><i class="fab fa-twitter main-purple mr-2 hov-ora"></i></a>
                    <a href="#"><i class="fab fa-instagram main-purple hov-ora"></i></a>
                </div>
                <div class="col-7 px-0 mt-auto">
                    <img alt="BeanCruncher" class="img-fluid mx-auto float-right" src="img/Slices/Footer/Image_bottom.png">
                </div>
            </div>
            <div class="row">
                <div class="col-12 footer-purple py-3 small">
                    <p class="m-0 text-center footer-small-font text-white">Copyright©2020 |
                        <a class="footer-small-font text-white">Designed by </a>
                        <a class="footer-small-font text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <footer class="py-0 d-none d-md-block d-lg-none shadow-lg">
        <div class="container-fluid">
            <div class="row mt-lg-5 pt-5">
                <div class="row">
                    <div class="col-5 ml-5 px-0">
                        <img alt="BeanCruncher" src="img/Slices/Footer Tablet/Tablet Logo.png" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2 px-0 pt-5">
                    <ul class="nav pl-4">
                        <li class="nav-item pl-2">
                            <a href="index.html" class="nav-link main-purple footer-menu pb-lg-0 pt-0">Home</a>
                        </li>
                        <li class="nav-item pl-2">
                            <a href="features.html" class="nav-link pt-0 main-purple footer-menu not-bold pb-lg-0"><div class="the-purple">Features</div></a>
                        </li>
                        <li class="nav-item pl-2">
                            <a href="pricing.html" class="nav-link pt-0 main-purple footer-menu not-bold pb-lg-0"><div class="the-purple">Pricing</div></a>
                        </li>
                        <li class="nav-item pl-2">
                            <a href="demo.html" class="nav-link pt-0 main-purple footer-menu not-bold pb-lg-0"><div class="the-purple">Demo</div></a>
                        </li>
                        <li class="nav-item pl-2">
                            <a href="contact.html" class="nav-link main-purple pt-0 footer-menu not-bold pb-lg-0"><div class="the-purple">Contact</div></a>
                        </li>
                    </ul>
                </div>
                <div class="col-5 pl-4 pt-5">
                    <h6 class="main-purple">Contact Us:</h6>
                    <p class="light-purple not-bold mb-0">+27 (0) 72 253 1794 / +27 (0) 61 519 8817</p>
                    <p class="light-purple not-bold mb-0">3802 Jan Frederick Ave, Randpark Ridge, 2156</p>
                </div>
                <div class="col-2 pt-5">
                    <h6 class="main-purple ml-3 pl-2">Follow us:</h6>
                    <a href="#"><i class="fab fa-facebook-f main-purple mr-2 ml-3 pl-2 hov-ora"></i></a>
                    <a href="#"><i class="fab fa-twitter main-purple mr-2 hov-ora"></i></a>
                    <a href="#"><i class="fab fa-instagram main-purple hov-ora"></i></a>
                </div>
                <div class="col-3 px-0 mt-auto">
                    <img alt="BeanCruncher" class="img-fluid mx-auto float-right" src="img/Slices/Home/Footer Right Side.png">
                </div>
            </div>
            <div class="row">
                <div class="col-12 footer-purple py-3 small">
                    <p class="m-0 text-center footer-small-font text-white">Copyright©2021 |
                        <a class="footer-small-font text-white">Designed by </a>
                        <a class="footer-small-font text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!--    BACK TO TOP BTN-->
    <a href="#top" class="btn rounded-circle btn-back-to-top btt-btn" data-smooth-scroll data-aos="fade-up" data-aos-offset="2000" data-aos-mirror="true" data-aos-once="false">
        <img src="img/icons/interface/icon-arrow-up.svg" alt="Icon" class="icon bg-white" data-inject-svg>
    </a>
    <!-- Required vendor scripts (Do not remove) -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- Smooth scroll (animation to links in-page)-->
    <script type="text/javascript" src="js/smooth-scroll.polyfills.min.js"></script>
    <!-- SVGInjector (replaces img tags with SVG code to allow easy inclusion of SVGs with the benefit of inheriting colors and styles)-->
    <script type="text/javascript" src="js/svg-injector.umd.production.js"></script>
    <!-- Required theme scripts (Do not remove) -->
    <script type="text/javascript" src="js/theme.js"></script>
    <!-- Removes page load animation when window is finished loading -->
    <script type="text/javascript">
        window.addEventListener("load",function(){document.querySelector('body').classList.add('loaded');});
    </script>


    <!--
    <script src='https://github.com/mattbryson/TouchSwipe-Jquery-Plugin/blob/master/jquery.touchSwipe.min.js'></script>
    <script  src="assets/reviews3d-slider/dist/script.js"></script>
    -->

    </body>
    </html>
    <?php
}else{
    Redirect::to('index.php');
}
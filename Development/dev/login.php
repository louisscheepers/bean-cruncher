<?php
require_once __DIR__ . '/core/init.php';

$loginFailed = false;
$msg = '';

if(Input::exists()){
    if(isset($_POST['login'])){

        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'email' => array('required' => true),
            'password' => array('required' => true)
        ));

        if($validate->passed()){
            //Login
            $user = new User();
            $remember = (Input::get('remember') === 'on') ? true : false;
           
            $login = $user->login(Input::get('email'), Input::get('password'), $remember);

            if($login){
                if($user->data()->verified == 0){
                    Redirect::to('verify.php');
                }
                else{
                    Redirect::to('index.php');
                }
            }
            else{
                $loginFailed = true;
            }
        }
        else{  
            foreach($validate->errors() as $error){
                echo $error, '<br>';
            }
        }
    }

    if(isset($_GET['redirect'])){
        $msg = '<p class="verify-txt-color fs-13">Please enter your details again.</p>';
    }

    //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
    $user = new User();
    if($user->isLoggedIn()){
        //$logger->debug('Login - User is logged in');
    }
    else{
        //$logger->debug('Login - NOT LOGGED IN!');
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
        <title>BC | Sign in</title>
    </head>
    <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
    <div class="loader">
        <div class="loading-animation"></div>
    </div>
        <div class="container" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-6 mt-6">
                <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-6 card custom-card rounded-0-1 my-auto white-boxShadow">
                   <!--BC Logo -->
                    <div class="row d-flex justify-content-center bc-logo">
                        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-0">
                            <img src="img/Logo_BC.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <!-- Logo -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-sm-7 col-md-7 col-lg7 col-xl-7 logo-size-pos-sign">
                            <img src="img/logo.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <!-- Heading -->
                    <div class="row d-flex justify-content-center org-setup-sign">    
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <span>Sign in</span>
                        </div>
                    </div>
                    <!-- Incorrect -->
                    <?php
                        if($loginFailed){
                            echo 
                            '<div class="row d-flex justify-content-center">
                                <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center we-sent-row">
                                        <p class="verify-txt-color fs-13">Your details are <b class="bean-orange">incorrect</b>. Please try again.</p>
                                    </div>
                                </div>
                            </div>';
                        }
                        
                        if(isset($_GET['reenter'])){
                            echo 
                            '<div class="row d-flex justify-content-center">
                                <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center we-sent-row">
                                        <p class="verify-txt-color fs-13">Please enter your details again to continue.</p>
                                    </div>
                                </div>
                            </div>';
                        }
                    ?>
                    
                    <!-- Form -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-11 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                            <form action="" method="POST" class="needs-validation">
                    <!-- Email -->
                                <div class="input-group shadow-custom-sm org-name g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Email icon.svg" class="img-fluid email_icon_width" alt=""/></span>
                                    </div>
                                    <input id="email" name="email" type="email" class="form-control border-left-0 pl-1 rounded-right-border field-text-color fs-14 my-auto" placeholder="Email" required>
                                </div>
                    <!-- Password -->
                                <div class="input-group shadow-custom-sm g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Lock icon.svg" class="img-fluid email_icon_width pl-1" alt=""/></span>
                                    </div>
                                    <input id="password" name="password" type="password" class="form-control border-left-0 border-right-0 ml-1 pl-1 rounded-right-border field-text-color fs-14 my-auto" placeholder="Password"  required>
                                    <div id="revealPassword" class="input-group-append">
                                        <span class="input-group-text bg-white rounded-right-border"><span id="eye-icon" class="eye-style"><i class="far fa-eye"></i></span></span>
                                    </div>
                                </div>
                    <!-- Forgot Password & Sign in -->
                                <div class="row d-flex justify-content-between fp-row">
                    <!-- Forgot Password? -->
                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center text-sm-left pt-2 my-auto">
                                        <a href="recovery.php" class="bluegrey fs-12 my-4">Forgot password?</a>
                                    </div>
                    <!-- Token  -->
                    <!-- <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">-->
                    <!-- Sign In Button-->
                                    <div class="col-6 col-sm-5 col-md-5 col-lg-5 col-xl-5">
                                        <input name="login" type="submit" class="btn btn-block nav-free-but the-border main-purple py-2 fs-12 my-4" value="Sign in">
                                    </div>
                                </div>
                    <!-- Remember me & Sign up -->
                                <div class="row rem-row">
                    <!-- Remember Me -->
                                    <div class="col-2 col-sm-2 col-lg-2 col-xl-4 pr-0" style="visibility: hidden;">
                                        <div class="bluegrey agree-b">
                                    <label class="container-check2 mb-0"><span class="fs-12"> Remember me </span>
                                        <input name="remember" id="remember" type="checkbox">
                                        <span class="checkmark2 rounded"></span>
                                    </label>
                                </div>
                                    </div>
<!--
                                    <div class="col-5 col-sm-5 col-md-5 col-lg-4 col-xl-4 pr-0">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input bluegrey" name="remember" id="remember">
                                            <label class="form-check-label fs-13 bluegrey" for="rememberCheck">Remember me</label>
                                        </div>
                                    </div>
-->
                    <!-- Don't have an account? -->
                                    <div class="col-10 col-sm-10 col-lg-10 col-xl-8">
                                        <div class="bluegrey float-right text-right">
                                            <span class="fs-12">You don't have an account? <a href="register.php" class="bluegrey"><b>Sign up</b></a></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>
        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        
        <script>
        
            if ( window.history.replaceState ) {
                window.history.replaceState( null, null, window.location.href );
            }
            (function() {
            'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            // form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();

            $('#revealPassword').on('click', function (){
                var inputType = $('#password').attr('type');
                if(inputType === 'password'){
                    $('#password').get(0).type = 'text';
                    $('#eye-icon').html('<i class="far fa-eye-slash"></i>');
                }else{
                    $('#password').get(0).type = 'password';
                    $('#eye-icon').html('<i class="far fa-eye"></i>');
                }
            });
        </script>
    </body>
</html>
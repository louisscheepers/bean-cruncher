<?php
require_once __DIR__ . '/core/init.php';

//$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
$user = new User();
if($user->isLoggedIn()){
    //$logger->debug('Verify - User is logged in');
}
else{
    //$logger->debug('Verify - NOT LOGGED IN!');
    redirect::to('index.php');
}

$msg = '';
if(Input::exists('GET')){
    if(Input::get('invalid_hash') == 'true'){
        //$logger->debug('Verify - Invalid hash msg');
        $msg = 'Invalid hash, please try again.';
    }
}

// echo 'No input exists';
$email = $user->data()->user_email;
Email::sendPreset($email, 'verify');
    

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

        <title>BC | Verify</title>
    </head>
    <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
        <div class="container" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-5 mt-6">
                <div class="col-11 col-sm-12 col-md-10 col-lg-5 col-xl-5 card rounded-0-1 my-auto verify-card white-boxShadow">
            <!-- Logo -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-10 col-sm-7 col-md-7 col-lg-8 col-xl-8 logo-size-register">
                            <img src="img/logo.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
            <!-- Heading -->
                    <div class="row d-flex justify-content-center verify-b">    
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <span>Verify</span>
                        </div>
                    </div>
            <!-- Lock -->
                    <div class="row d-flex justify-content-center we-sent-row">
                        <div class="col-3 col-sm-2 col-md-2 col-lg-3 col-xl-3 px-lg-1">
                            <img src="img/Verify icon.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
            <!-- Brand text -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center we-sent-row">
                                <p class="verify-txt-color"><b class="fs-13">We sent you an email.</b><br/><span class="fs-12">Please click on the link in the email to verify your account.</span></p>
                            </div>

                            <?php 
                                if($msg != ''){
                                    echo '<div class="text-center text-color-red fs-13 mb-4">' . $msg . '</div>';
                                }
                            ?>
            <!-- Back -->
                            <div class="row d-flex justify-content-center btn-back-bottom">
                                <div class="col-5 col-sm-4 col-md-5 col-lg-6 col-xl-6">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center we-sent-row">
                                        <p class="verify-txt-color fs-13 we-sent-row">Didn't receive an email?</p>
                                    </div>
                                    <form action="" method="POST">
                                        <input name="send-again" type="submit" class="btn btn-block nav-free-but the-border main-purple py-2 fs-12" value="Send again">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>

        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    </body>
</html>
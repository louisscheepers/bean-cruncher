<?php
require_once __DIR__ . '/core/init.php';
//$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');



if(Input::exists('GET')){
    if(isset($_GET['email'])){
        $user = new User($_GET['email']);
        
        if($user->isLoggedIn()){
            //$logger->debug('verifyAccount - User is logged in');
        }
        else{
            //$logger->debug('verifyAccount - User NOT logged in, forcing login');
            $user->login();
        }

        $getHash = $_GET['hash'];
        //$logger->debug('verifyAccount - Get-Hash: ' . $getHash);
        $email = $_GET['email'];
        //$logger->debug('verifyAccount - Get-Email: ' . $email);
        
        $dbHash = $user->data()->user_salt;
        //$logger->debug('verifyAccount - DB-Hash: ' . $dbHash);
        if($dbHash == $getHash){

            $verified = DB::getInstance()->update('user', 'user_id', $user->data()->user_id, array(
                'verified' => 1
            ));
            if($verified){
                if($user->isLoggedIn()){
                    //$logger->debug('verifyAccount - User logged in after verified');
                }
                else{
                    //$logger->debug('verifyAccount - User NOT logged in after verified');
                }

                //$logger->debug('verifyAccount - User verified, redirecting to index');
                Redirect::to('index.php');
            }
            else{
                //$logger->debug('verifyAccount - Verification failed to update');
            }
        }
        else{
            //$logger->debug('verifyAccount - Invalid has, redirecting to verify.php?invalid_hash');
            Redirect::to('verify.php?invalid_hash=true');
        }
    }
}
else{
    //$logger->debug('verifyAccount - No Input, redirecting to index');
    Redirect::to('index.php');
}
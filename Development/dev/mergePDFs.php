<?php
require_once (__DIR__ . '/core/init.php');

//$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
$pdf = new \Clegginabox\PDFMerger\PDFMerger();

if(Input::exists()){
    //$logger->info("Retrieving pdf files from POST");
    $method = Input::get('method');
    $firstSection64 = Input::get('firstSection');
    $dashboard64 = Input::get('dashboard');
    $annex64 = Input::get('annextures');
    $name = Input::get('name');
    //$logger->debug("Building Binaries");
    $firstBin = decodeb64($firstSection64);
    $dashBin = decodeb64($dashboard64);
    $annexBin = decodeb64($annex64);

    $firstSecName = "first" . uniqid(rand(), true);
    $dashName = "dash" . uniqid(rand(), true);
    $annexName = "annex" . uniqid(rand(), true);
    $mergedName = "merged" . uniqid(rand(), true);

    //$logger->debug("Looking For PDF Signature");
    if(checkIfPDF($firstBin) || checkIfPDF($dashBin) || checkIfPDF($annexBin)){
        echo "NOT PDFS";
        //$logger->error("The file uploaded for conversion is not a pdf file");
        throw new Exception('Missing the PDF file signature');
    }

    //$logger->info("Saving temporary files to merge");

    saveAsPDF($firstBin, $firstSecName);
    saveAsPDF($dashBin, $dashName);
    saveAsPDF($annexBin, $annexName);

    $pdf = mergePDFs($pdf, $firstSecName, $dashName, $annexName);


    switch ($method){
        case "pdf":{
            clearstatcache();
            if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/tmp/pdf/' . $name . '.pdf')){
                unlink($_SERVER['DOCUMENT_ROOT'] . '/tmp/pdf/' . $name . '.pdf');
            }
            $pdf->merge('file', 'tmp/pdf/' . $name . '.pdf', 'P');
            removePdfsForMerge($firstSecName, $dashName, $annexName);
            //$logger->info("PDF Merged - Ready for Download");

            // //$logger->info("File being sent to downloadPDF.php");
            // Redirect::to('downloadPDF.php?name='.$name);

            break;
        }
        case "pptx":{
            $pdf->merge('file', 'tmp/pdf/' . $mergedName . '.pdf', 'P');
            //$logger->info("PDF Merged - Ready for pptx conversion");
            convertToPptx($mergedName, $name);
            removePdfsForMerge($firstSecName, $dashName, $annexName);

            echo "PPTX Ready";
            break;
        }
        case "email":{
            $emails = Input::get('email');
            file_put_contents('attachment/pdf/' . $name . '.pdf', $mergedName);
            if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/attachment/pdf/' . $name . '.pdf')){
                unlink($_SERVER['DOCUMENT_ROOT'] . '/attachment/pdf/' . $name . '.pdf');
            }

            $pdf->merge('file', 'attachment/pdf/' . $name . '.pdf', 'P');
            //$logger->info("PDF Merged - Ready for email");
            removePdfsForMerge($firstSecName, $dashName, $annexName);
            sendEmails($emails,$name);
            
            echo "Emails Sent";
            break;
        }
    }
}

    function decodeb64($b64){
        return base64_decode($b64, true);
    }

    function checkIfPDF($bin){
        return strpos($bin, '%PDF') !== 0;
    }

    function saveAsPDF($bin, $name){
        file_put_contents('tmp/pdf/' . $name . '.pdf', $bin);
    }

    function mergePDFs($pdf, $first, $second, $third){
        $pdf->addPDF('tmp/pdf/' . $first . '.pdf', 'all', 'P');
        $pdf->addPDF('tmp/pdf/' . $second . '.pdf', 'all', 'P');
        $pdf->addPDF('tmp/pdf/' . $third . '.pdf', 'all', 'P');

        return $pdf;
    }

    function convertToPptx($rndname, $realname){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
        //$logger->info("Converting " . $rndname . ".pdf -> " . $realname . ".pptx ");
        $outDir = __DIR__ . '/tmp/pptx/';
        //$logger->debug("Outdir -> " . "outDir");
        $source = __DIR__ . '/tmp/pdf/' . $rndname . '.pdf';
        //$logger->debug("Source -> " . $source);
        $pdf = __DIR__ . '/tmp/pdf/' . $rndname . '.pdf';
        //$logger->debug("PDF -> " . $pdf);
        $rndpptx = __DIR__ . '/tmp/pptx/' . $rndname . '.pptx';
        //$logger->debug("rndPPTx -> " . $rndpptx);

        $newpptx = __DIR__ . '/tmp/pptx/' . $realname . '.pptx';
        //$logger->debug("New Pptx -> " . $newpptx);

        //$logger->info("Starting conversion ...");
        echo shell_exec('export HOME=/tmp && soffice --infilter="impress_pdf_import" --convert-to pptx --outdir ' . $outDir. ' ' . $source);
        clearstatcache();

        //$logger->info("Conversion complete running housekeeping");
        if(file_exists($rndpptx)){
            //$logger->info("rndpptx file exists");
            if(file_exists($newpptx)){
                //$logger->warning("new pptx file already exists");
                //$logger->warning("deleting " . $newpptx);
                unlink($newpptx);
            }

            //$logger->info("Renaming pptx file : " . $rndname . ".pptx -> " . " " . $realname . ".pptx");
            if(rename($rndpptx, $newpptx)){
                //$logger->info("Rename succesfull deleting the pdf file");
                unlink($pdf);
            }else{
                //$logger->error("Could not rename the file");
            }

            clearstatcache();

            if(file_exists($newpptx)){
                //$logger->info("The PPTX file is ready for download - Conversion complete");
            }else{
                //$logger->error("Conversion failed -> the new pptx file is missing");
            }
        }

    }

    function sendEmails($emails, $filename){
        $filePath = 'https://app.beancruncheranalytics.com/attachment/pdf/' . $filename . '.pdf';

        $emailArr = explode (',', $emails);
        for($i = 0; $i < count($emailArr); $i++){
            Email::sendPreset($emailArr[$i], 'report', $filePath);
        }
    }

    function removePDF($filename){
        unlink(__DIR__. '/tmp/pdf/' . $filename . '.pdf');
    }

    function removePdfsForMerge($first, $second, $third){
        removePDF($first); removePDF($second); removePDF($third);
    }
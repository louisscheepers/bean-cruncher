<?php
  ini_set('display_errors', 'On');
  require_once('const/const.php');
  require_once(__DIR__ . '/core/init.php');
  $user = new User();
  if($user->isLoggedIn()){
    $role = Session::get('active_role_id');
  }
  else{
    Redirect::to('index.php');
  }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Scheduled Emails</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

        
        <link href="css/custom.css" rel="stylesheet" />
    </head>
    <body class="content-bg">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
               
            <nav class="sb-topnav navbar navbar-expand navbar-dark p-0 my-3">    
                    <div class="container nav-size">
                        <div class="row d-content no-gutters">
                            <!-- First Colum with nested colums -->
                            <div class="col-sm-5">
                                <div class="row">
                                <div class="col-sm-2">
                                        <a type="button" class=" btn-lg d-content" href="index.php">
                                            <img class="img-fluid arrow-icon-size pl-1" src="img/arrow-left-icon.png">
                                        </a>
                                    </div>

                                    <div class="col-sm-10 align-self-center">
                                    <div class="dropdown d-none d-md-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                            <input class="search-element" type="text" id="companySearch" autocomplete="off" onkeyup="search()" placeholder="Search..." title="List of Companies" data-toggle="dropdown">
                            <div id="companiesDropDown" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <?php
                                $companies = Companies::getCompanies();
                                for($i = 0; $i < count((array)$companies); $i++){
                                  if($companies[$i]['status'] == 1){
                                  ?>
                                  <li>
                                    <a id="active-company-dropdown" class="dropdown-item" onclick="loadCompany('<?php echo str_replace('\'', '\\\'', $companies[$i]['name'])  ?>')"><?php echo $companies[$i]['name']; ?></a>
                                  </li>
                                  <?php
                                  }
                                }
                              ?> 
                            </div>
                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Second Colum with nested colums -->    
                            <div class="col-sm-2">
                                <h5 class="nav-settings d-content txt-color">
                                Scheduled Emails
                                </h5>
                            </div>

                            <!-- Third Colum with nested colums -->
                            <div class="col-sm-5">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0 "></div>
                                    </div>
                                    
                                    <div class="col-sm-4 txt-align-right align-self-center">
                                        <div class="dropdown d-none d-md-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                                            <button class="btn btn-transparent dropdown-toggle" type="button" id="companyDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?php echo Companies::getActiveCompanyName() ?>
                                            </button>
                                            <div id="companiesDropDown" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <?php
                                                    $companies = Companies::getCompanies();
                                                    for($i = 0; $i < count((array)$companies); $i++){
                                                    if($companies[$i]['status'] == 1){
                                                    ?>
                                                    <a id="active-company-dropdown" class="dropdown-item" onclick="loadCompany('<?php echo str_replace('\'', '\\\'', $companies[$i]['name']) ?>')"><?php echo $companies[$i]['name']; ?></a>
                                                    <?php
                                                    }
                                                    }
                                                ?> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 align-self-center">
                                        <div id="comp-icon">
                                            <img id="comp-img" src="img/profile-icon-orange.png" class="rounded-circle icon-size" alt="Profile Icon">
                                        </div>
                                      </div>
                                </div>
                             </div>
                        </div>
                    </div>
                </nav>
                
                <main>
                    <div class="container my-5 p-0">
                        <div class="row mt-lg-1">
                            <div class="col-12 mt-lg-1">
                               <div class="card">
                                  <div class="card-body profile-card">

                                        <div class="row no-gutters blue-card mb-4">
                                          <div class="col-sm-9 align-self-center">
                                            <div class="row no-gutters">
                                              <div class="col-sm-1">
                                                <img src="img/mail-white.png" class="img-fluid mail-icon-size mb-small">
                                              </div>
                                              <div class="col-sm-5 ml-m20">
                                                <h5 class="font-weight-light align-middle txt-color-w m-0 ">Scheduled Emails</h5>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-sm-3">
                                           <button class="btn float-right txt-color-w  font-weight-200" onclick="deleteFavourites()">
                                            <i class="fas fa-times mx-3"></i> Remove selected
                                            </button>
                                          </div>
                                        </div>
                                        <div class="p-4 mt-n2">
                                          <h5 class="txt-color font-weight-bold"></h5>
                                          <div id="favourites"></div>
                                        </div>
                                  </div>
                                </div>
                           </div>
                       </div>
                    </div>
                </main>
          
            </div>
        </div>


        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Company Settings Script -->
        <script src="js/scheduled-emails.js"></script>
        <!-- Simple Search Script -->
        <script src="js/simpleSearch.js"></script>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    </body>
</html>

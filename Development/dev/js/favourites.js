$(document).ready(function(){
    fetchFavourites();
});

function fetchFavourites(){
    var action = "fetch";
    
    $.ajax({
        url:"json.php",
        method:"POST",
        data:{favourite:action},
        success:function(data){
            let obj = jQuery.parseJSON(data);

            let message = '<div class="row p-3">';
            if(obj != null){
                for(let i = 0; i < obj.length; i++){
                    message +=
                    '<div class="col-3">' +
                        '<div class="txt-color">' +
                            '<label class="container-box" style="width: max-content;" for="fav-' + obj[i] + '">' + obj[i] + 
                                '<input name="type" type="checkbox" class="form-check-input" id="fav-' + obj[i] + '" value="' + obj[i] + '">' +
                                '<span class="checkmark"></span>'+
                            '</label>' +
                        '</div>' +
                    '</div>';
                }
                message += '</div>';
                document.getElementById('favourites').innerHTML = message;
                document.getElementById('loader').hidden = true;
            }
            else{
                document.getElementById('loader').hidden = true;
                document.getElementById('favourites').innerHTML = '<div class="py-3 txt-color">No favourites has been added</div>';
            }
        }
    });
}

function deleteFavourites(){
    document.getElementById('loader').hidden = false;
    var action = "delete";
    let arr = new Array();

    $("input:checkbox[name=type]:checked").each(function(){
        arr.push($(this).val());
    });

    $.ajax({
        url:"json.php",
        method:"POST",
        data:{favourite:action,names:arr},
        success:function(data){
            document.getElementById('loader').hidden = true;
            fetchFavourites();
        }
    });
}

function fetchCompany(){
    document.getElementById('loader').hidden = false;
    var action = "fetch";

    $.ajax({
        url:"json.php",
        method:"POST",
        data:{company:action},
        success:function(data){
            let obj = jQuery.parseJSON(data);
            document.getElementById('comp-name').value = obj['company_name'];
            console.log(obj['company_brand']);
            if(obj['company_brand'] != '<img id="comp-img" src="data:image/jpeg;base64," class="rounded-circle icon-size" alt="Company Brand">'){
                document.getElementById('comp-icon').innerHTML = obj['company_brand'];
            }
            else{
                document.getElementById('comp-icon').innerHTML = '<img id="nav-comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
            }
            document.getElementById('loader').hidden = true;
        }
    });
}

function loadCompany(name, id){

    $.getJSON('json.php?aComp=' + id, function(){
        document.getElementById('comp-name').innerHTML = name;
        fetchCompany();
    });
}
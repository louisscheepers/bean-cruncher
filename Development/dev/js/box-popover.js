$(function () {
    $('[data-toggle="popover"]').popover()
  })

  $('[data-toggle="popover"]').popover({ 
        trigger: "manual" ,
        html: true,
        animation:true,
        container: 'body',
        html: true,
        placement: 'top',
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
        sanitize: false,
        content: function () {
            if ($("#popoverBox1:hover").length) {
                return $("#popoverBoxContent1").html();
            } 
            else if ($("#popoverBox2:hover").length) {
                return $("#popoverBoxContent2").html();
            } 
            else if ($("#popoverBox3:hover").length) {
                return $("#popoverBoxContent3").html();
            } 
            else if ($("#popoverBox4:hover").length) {
                return $("#popoverBoxContent4").html();
            } 
            else if ($("#popoverBox5:hover").length) {
                return $("#popoverBoxContent5").html();
            } 
            else if ($("#popoverBox6:hover").length) {
                return $("#popoverBoxContent6").html();
            } 
            else if ($("#popoverBox7:hover").length) {
                return $("#popoverBoxContent7").html();
            } 
            else if ($("#popoverBox8:hover").length) {
                return $("#popoverBoxContent8").html();
            } 
        }
    })
    .on("mouseenter", function () {
        var _this = this;
        $(this).popover("hide");
        $(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 150);
});
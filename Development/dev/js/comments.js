function openComments(btnNo){
    //find box id from button
    let boxes = document.querySelectorAll('[id^="box'+ btnNo +'"]');

    //get item from first box's id
    boxModule = boxes[0].id.substring(4);

    document.getElementById('comment-modal-header').innerHTML = '<b>' + getModuleName(boxModule) + ' Comments:</b>';

    displayComments();

    //change id
    document.getElementById('commentsModal').id = 'commentsModal' + boxModule;
    $('#commentsModal' + boxModule).modal();

    document.getElementById('comment-error').hidden = true;
}

function editComment(commentId){
    document.getElementById('comment-error').hidden = true;
    console.log('Edit comment ' + commentId);

    let cancelEdit = document.getElementById('cancelEdit');
    cancelEdit.hidden = false;

    let comment = document.getElementById('comment' + commentId).innerText;
    comment = comment.slice(1, -1); 

    let form = document.getElementById('commentForm');
    form.focus();
    form.value = comment;

    
    let button = document.getElementById('setCommentBtn');
    if(!button){
        button = document.getElementById('editCommentBtn');
    }
    button.innerText = 'Edit';
    button.id = 'editCommentBtn';
    
    document.getElementById('editCommentBtn').onclick = function() {
        console.log('Editing..');

        form.innerHTML = '';
        let newComment = document.getElementById('commentForm').value;

        if(newComment.length > 250){
            document.getElementById('comment-error').hidden = false;
        }
        else{
            $.ajax({
                type: "GET",
                url: "json.php",
                data: {eCom:commentId,com:newComment},
                success: displayComments()
            });
            document.getElementById('commentForm').value = '';
            document.getElementById('setCommentBtn').removeAttribute('onclick');
            document.getElementById('setCommentBtn').setAttribute("onClick", "setComment()");
            document.getElementById('comment-error').hidden = true;
        }

        document.getElementById('cancelEdit').hidden = true;
    }
}

function cancelEdit(){
    let button = document.getElementById('editCommentBtn');
    if(button){
        button.innerText = 'Submit';
        button.id = 'setCommentBtn';
        button.setAttribute("onClick", "setComment()");
    }

    document.getElementById('cancelEdit').hidden = true;
    document.getElementById('setCommentBtn').setAttribute("onClick", "setComment()");

    let form = document.getElementById('commentForm');
    form.value = '';

    document.getElementById('comment-error').hidden = true;
    // let commentPar = document.getElementById('p-comment' + commentID);
    // commentPar.style.backgroundColor = '#fff';
}

function deleteComment(commentId){
    let modal = document.querySelectorAll('[id^="commentsModal"]');
    boxModule = modal[0].id.substring(13);

    $.ajax({
        type: "GET",
        url: "json.php",
        data: {dCom:commentId},
        success: displayComments()
    });
}

function setComment(){
    console.log('Set comment');
    
    //find modal id
    let modal = document.querySelectorAll('[id^="commentsModal"]');
    let module = modal[0].id.substring(13);

    if(module.length == 0){
        console.log("No data in box!");
    }
    else{
        let commentField = document.getElementById('commentForm');
        if(commentField && commentField.value){
            let newComment = commentField.value
            if(newComment.length > 250){
                document.getElementById('comment-error').hidden = false;
            }
            else{
                $.ajax({
                    type: 'GET',
                    url: 'json.php',
                    data: {sCom:boxModule.substring(),com:newComment},
                    success: displayComments()
                });
                document.getElementById('commentForm').value = '';
                document.getElementById('comment-error').hidden = true;
            }
        }
    }
}

function displayComments(){
    let setBtn = document.getElementById('setCommentBtn');
    let editBtn = document.getElementById('editCommentBtn');
    let button;
    if(setBtn){
        button = setBtn;
    }
    else{
        button = editBtn;
    }
    
    button.id = 'setCommentBtn';
    button.innerText = 'Submit';

    commentSection = document.getElementById('commentSection');
    
    commentSection.innerHTML = '<div class="text-center my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';

    let role;
    let id;

    $.when(
        $.when(
            $.getJSON('json.php?aRole=' + null, function(userRole){
                role = userRole;
            })
        ).then(function(){
            $.getJSON('json.php?gaUserId=' + null, function(userId){
                id = userId;
            })
        })
    ).then(function(){
        $.getJSON('json.php?gCom=' + boxModule, function(result){
            commentSection.innerHTML = '';
            let comments = "";

            if(result != '' || result.length != 0){
                for(let i = 0; i < result.length; i++){
                    comments += '<div class="row py-2 no-gutters">';
                    comments += '<div class="col-7">';
                    comments += '<p id="p-comment' + result[i]['id'] + '" class="my-auto font-size-12 text-left txt-color rounded">';
                    if(result[i]['user'] == ''){
                        result[i]['user'] = '<span class="text-muted">Deleted user</span>';
                    }
                    comments += '<b>' + result[i]['user'] + '</b> - <i id="comment' + result[i]['id'] + '" class="text-break">"' + result[i]['comment'] + '"</i> </div>' +
                    '<div class="col-5"><span class="float-right small font-size-10 txt-color-light-grey">' + result[i]['date_time'];
                    if(id == result[i]['user_id']){
                        comments += '<br class="comments-break"><a class="comments-icon-sm" onclick="editComment(' + result[i]['id'] + ')" id="editComment' + result[i]['id'] + '"><i class="fas fa-cog settings-icon-size-small"></i></a>';
                    }
                    else{
                        comments += '<br class="comments-break"><a class="comments-icon-sm disabled" id="editComment"><i class="fas fa-cog settings-icon-size-small-disabled"></i></a>';
                        
                    }
                    if(role == 1 || role == 2 || id == result[i]['user_id']){
                        comments += '<a onclick="deleteComment(' + result[i]['id'] + ')" id="deleteComment' + result[i]['id'] + '" class=""><i class="far fa-trash-alt remove-icon-size-small"></i></a>';
                    }
                    else{
                        comments += '<a class="disabled"><i class="far fa-trash-alt remove-icon-size-small-disabled"></i></a>';
                    }
                    comments += '</span></p>';
                    comments += '</div></div>';
                }
            }
            else{
                comments = '<p class="p-2 font-size-12 text-center">No comments</p>';
            }
            commentSection.innerHTML = comments;
        })
    });
}

$('#commentsModal').on('hidden.bs.modal', function () {
    let modals = document.querySelectorAll('[id^="commentsModal"]');
    let module =  modals[0].id.substr(13);
    document.getElementById('comment-error').hidden = true;

    for(let i = 1; i <= 8; i++){
        let boxModules = document.querySelectorAll('[id^="box'+ (i) +'"]');
        let boxModule = boxModules[0].id.substr(4);

        if(module === boxModule){
            let div = document.getElementById('modal-data' + i);
            let data = div.data;

            let annexure = new Annexure(i, module, data);
            annexure.buildAnnexure();
        }
    }
    
    modals[0].id = 'commentsModal';
});

function focusComment(){
    document.getElementById('setCommentBtn').style.color = '#fff';
    document.getElementById('setCommentBtn').style.backgroundColor = '#FF9300';
    document.getElementById('setCommentBtn').style.borderColor = '#FF9300';
}
function blurComment(){
    document.getElementById('setCommentBtn').style.color = '#100D4C';
    document.getElementById('setCommentBtn').style.backgroundColor = '#fff';
    document.getElementById('setCommentBtn').style.borderColor = '#100D4C';
}
function search() {
    var input, filter, div, li, a, i, txtValue;
    input = document.getElementById("companySearch");
    filter = input.value.toUpperCase();
    div = document.getElementById("companiesDropDown");
    li = div.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
            div.style.display = "";
        } else {
            div.style.display = "none";
            li[i].style.display = "none";
        }
    }
    if (input.value.trim() >= ''){
        div.classList.add('show');
    }
}


function mobileSearchShow() {
    document.getElementById("mbSearch").classList.toggle("show");
}

function mobileSearchHide() {
        if( document.getElementById("mbSearch").classList.contains("show")){
            document.getElementById("mbSearch").classList.remove("show");
        }
} 

function mobileSearch() {
    var input, filter, div, li, a, i, txtValue;
    input = document.getElementById("companySearchM");
    filter = input.value.toUpperCase();
    div = document.getElementById("companiesDropDownM");
    li = div.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
            div.style.display = "";
        } else {
            div.style.display = "none";
            li[i].style.display = "none";
        }
    }
    if (input.value.trim() >= ''){
        div.classList.add('show');
    }
  }
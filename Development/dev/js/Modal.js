class Modal{
    
    setModal(number, data){
        let div = document.getElementById('modal-data' + number);
        div.data = data;
        // console.log('Setting Modal: ' + number);
    }

    drawModal(number, module){
        let div = document.getElementById('modal-data' + number);
        let data = div.data;
        // console.log('Getting Modal: ' + number);

        document.getElementById('modal-body' + number);

        let names = new Array();
        let totals = new Array();
        let items = new Array();
        let sales = new Array();
        let qoh = new Array();
        let labelx;
        let seriesData;
        let seriesData2;
        let seriesData3;
        let seriesData4;
        let header = '<button type="button" class="close z-index-1" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>';
        let body = "";
        let graphZone = '#modal-graph' +number;
        let graphId = 'modal-graph' + number;
        let chartId = 'modal-graph' + number + "canvas";
        let chart;
        let canvas;
        let ctx;
        let days = ['0-30', '30-60', '60-90', '90-120', '120+'];
        let _0_30;
        let _30_60;
        let _60_90;
        let _90_120;
        let _120;
        let description;
        let graphs;
        
        

        switch(module){
        //Cash In VS Cash Out
            case 'cico':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-6 p-5 my-auto txt-color">' +
                        '<h5><strong>Overview -</strong> Cash In v Cash Out</h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                    '<div class="col-6 p-5 my-auto">' +
                        '<div class="row">' +
                            '<div class="" id="modal-graph' + number + '" style="position: absolute; z-index: 100;">' +
                        '</div>' +
                    '</div>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    let cashIn = data.data['cash_in'];
                    let cashOut = data.data['cash_out'];
                    
                    let diff = new Array();
                    for(let i = 0; i < cashIn.length; i++){
                        diff[i] = cashIn[i] - cashOut[i];
                    }
                    chart = new BeanChart(chartId, ctx, data.labels, cashIn, cashOut, diff)
                    chart.drawMixedGroupBarLine(["Cash In", "Cash Out", "Movement"], true);

                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Income v Expenses
            case 'ino' :

                body = 
                '<div class="row h-100">' +
                    '<div class="col-6 p-5 my-auto">' +
                        '<div class="row">' +
                            '<div class="" id="' + graphId + '" style="position: absolute; z-index: 100;"></div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-6 p-5 my-auto txt-color">' +
                        '<h5><strong>Overview -</strong> Income v Expenses</h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {

                    chart = new BeanChart(chartId, ctx, data.labels, data.data['income'], data.data['expenses'], data.data['net_profit'])
                    chart.drawMixedGroupBarLine(["Income", "Expenses", "Net Profit"], true);

                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;
                
        //Asset v Debt
            case 'avd' :

                body = 
                '<div class="row h-100">' +
                    '<div class="col-6 p-5 my-auto txt-color">' +
                        '<h5><strong>Overview -</strong> Asset v Debt</h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                    '<div class="col-6 p-5">' +
                        '<div class="row">' +
                            '<div class="" id="'+ graphId +'"></div>' + 
                        '</div>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    let asset_debt = [data.data['assets'], data.data['debt']];
                    chart = new BeanChart(chartId, ctx, ['Assets', 'Debt'], asset_debt);
                    chart.drawDoughnut([true,'right']);

                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });
            
                break;
        
        //Profit & Loss
            case 'pl':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-12 p-5 my-auto txt-color">' + 
                        '<h5><strong>Financial Management -</strong> Profit & Loss </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14" style="height: 440px; overflow-y:auto;"></p>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Balance Sheet
            case 'bs':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-12 p-5 my-auto txt-color" >' + 
                        '<h5><strong>Financial Management -</strong> Balance Sheet</h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14" style="height: 440px; overflow-y:auto;"></p>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Accounts Receivable
            case 'ar' :
              
                body = 
                '<div class="row h-100">' +
                    '<div class="col-12 p-5 my-auto txt-color">' +
                        '<h5><strong>Customers -</strong> Accounts Receivable </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });
            
            break;

        //Debitors days outstanding
            case 'ddo':

                body = 
                '<div class="row h-200">' +
                    '<div class="col-6 p-5 my-auto txt-color">' +
                        '<h5><strong>Customers -</strong> Debtors Days Outstanding </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                    '<div class="col-6 p-5">' +
                        '<div class="row">' +
                            '<div class="" id="'+ graphId +'"></div>' + 
                        '</div>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    _0_30 = 0;
                    _30_60 = 0;
                    _60_90 = 0;
                    _90_120 = 0;
                    _120 = 0;
                    for(let i = 0; i < data.data['accounts'].length; i++){
                        if(data.data['accounts'][i]['0-30'] != 0){
                            _0_30 += data.data['accounts'][i]['0-30'];
                        }
                        if(data.data['accounts'][i]['30-60'] != 0){
                            _30_60 += data.data['accounts'][i]['30-60'];
                        }
                        if(data.data['accounts'][i]['60-90'] != 0){
                            _60_90 += data.data['accounts'][i]['60-90'];
                        }
                        if(data.data['accounts'][i]['90-120'] != 0){
                            _90_120 += data.data['accounts'][i]['90-120'];
                        }
                        if(data.data['accounts'][i]['120+'] != 0){
                            _120 += data.data['accounts'][i]['120+'];
                        }
                    }
                    seriesData = [_0_30, _30_60, _60_90, _90_120, _120];

                    chart = new BeanChart(chartId, ctx, days, seriesData);
                    chart.drawSingleBar();

                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Top Customers by Balance Outstandin
            case 'tcs':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-6 p-5 my-auto txt-color">' +
                        '<h5><strong>Customers -</strong> Top Customers by Balance Outstanding </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14 txt-color"></p>' +
                    '</div>' +
                    '<div class="col-6 p-5 my-auto">' +
                        '<div class="row">' +
                            '<div class="" id="'+ graphId +'"></div>' + 
                        '</div>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    for(let i = 0; i < data.data['accounts'].length; i++){
                        names[i] = data.data['accounts'][i]['name'];
                        totals[i] = data.data['accounts'][i]['total'];
                        if(i == 5){
                            break;
                        }
                    }

                    chart = new BeanChart(chartId, ctx, names, totals);
                    chart.drawHorizontalBar(false, "Balance Outstanding", true, false);

                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Retention
            case 'ret':
                
                body = 
                '<div class="row h-100">' +
                    '<div class="col-6 p-5 my-auto">' +
                        '<div class="row">' +
                            '<div class="" id="' + graphId + '" style="position: absolute; z-index: 100;"></div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-6 p-5 my-auto txt-color">' +
                        '<h5><strong>Customers -</strong> Retention</h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    let ret = new Array();
                    for(let i = 0; i < data.data['retention'].length; i++){
                        ret[i] = data.data['retention'][i];
                    }

                    chart = new BeanChart(chartId, ctx, data.labels, ret, ret);
                    chart.drawBarLine(["",""], false, "%");
                    
                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Accounts Payable
            case 'ap':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-12 p-5 my-auto txt-color">' +
                        '<h5><strong>Suppliers -</strong> Accounts Payable </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

                break;

        //Creditors Days Outstanding
            case 'cdo':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-6 p-5 my-auto txt-color">' +
                        '<h5><strong>Suppliers -</strong> Creditors Days Outstanding</h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                    '<div class="col-6 p-5">' +
                        '<div class="row">' +
                            '<div class="" id="'+ graphId +'"></div>' + 
                        '</div>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    _0_30 = 0;
                    _30_60 = 0;
                    _60_90 = 0;
                    _90_120 = 0;
                    _120 = 0;
                    for(let i = 0; i < data.data['accounts'].length; i++){
                        if(data.data['accounts'][i]['0-30'] != 0){
                            _0_30 += data.data['accounts'][i]['0-30'];
                        }
                        if(data.data['accounts'][i]['30-60'] != 0){
                            _30_60 += data.data['accounts'][i]['30-60'];
                        }
                        if(data.data['accounts'][i]['60-90'] != 0){
                            _60_90 += data.data['accounts'][i]['60-90'];
                        }
                        if(data.data['accounts'][i]['90-120'] != 0){
                            _90_120 += data.data['accounts'][i]['90-120'];
                        }
                        if(data.data['accounts'][i]['120+'] != 0){
                            _120 += data.data['accounts'][i]['120+'];
                        }
                    }
                    seriesData = [_0_30, _30_60, _60_90, _90_120, _120];

                    chart = new BeanChart(chartId, ctx, days, seriesData);
                    chart.drawSingleBar();

                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Top Suppliers by Purchases
            case 'tsp':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-6 p-5 my-auto txt-color">' +
                        '<h5><strong>Suppliers -</strong> Top Suppliers by Balance Outstanding</h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                    '<div class="col-6 p-5 my-auto">' +
                    '<div class="row">' +
                        '<div class="" id="'+ graphId +'"></div>' + 
                    '</div>' +
                '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    for(let i = 0; i < data.data['accounts'].length; i++){
                        names[i] = data.data['accounts'][i]['name'];
                        totals[i] = data.data['accounts'][i]['total'];
                        if(i == 5){
                            break;
                        }
                    }

                    chart = new BeanChart(chartId, ctx, names, totals);
                    chart.drawHorizontalBar(false, "Balance Outstanding", true, false);


                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;
            
        //Inventory Management
            case 'im':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-12 p-5 my-auto txt-color">' +
                        '<h5><strong>Inventory -</strong> Inventory Management </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Inventory Analysis - Sales
            case 'inas':
            
                body = 
                '<div class="row h-100">' + 
                    '<div class="col-6 p-5 my-auto txt-color">' + 
                        '<h5><strong>Inventory -</strong> Top Items by Sales </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                        // '<span class="small float-right mr-n5">Jan </span><br>' +  
                        // '<span class="color-red small float-right mr-n5">Feb</span><br>' +  
                    '</div>' + 
                    '<div class="col-6 p-5">' +
                    '<div class="row">' +
                        '<div class="" id="'+ graphId +'"></div>' + 
                    '</div>' +
                '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    for(let i = 0; i < Object.keys(data.data).length; i++){
                        if(typeof data.data[i] === 'undefined'){
                            break;
                        }
                        else{
                            items[i] = data.data[i]['item_code'];
                            sales[i] = data.data[i]['total_sales'];
                        }
                    }

                    chart = new BeanChart(chartId, ctx, items, sales);
                    chart.drawSingleBar();

                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Inventory Analysis - Quantity
            case 'inaq':
                
                body = 
                '<div class="row h-100">' + 
                    '<div class="col-6 p-5 my-auto txt-color">' + 
                        '<h5><strong>Inventory -</strong> Top Items by Quantity </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' + 
                    '</div>' + 
                    '<div class="col-6 p-5">' +
                    '<div class="row">' +
                        '<div class="" id="'+ graphId +'"></div>' + 
                    '</div>' +
                '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    data.data['item'].sort(function(a,b){
                        return  b['quantity_on_hand'] - a['quantity_on_hand'];
                    })
    
                    for(let i = 0; i < Object.keys(data.data).length; i++){
                        if(typeof data.data['item'][i] === 'undefined'){
                            break;
                        }
                        else{
                            names[i] = data.data['item'][i]['name'];
                            qoh[i] = data.data['item'][i]['quantity_on_hand'];
                        }
                    }

                    chart = new BeanChart(chartId, ctx, names, qoh);
                    chart.drawSingleBar();

                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

        //Profit and Loss Ratios
            case 'plr':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-12 p-5 my-auto txt-color">' +
                        '<h5><strong>Analysis -</strong> Profit & Loss Ratios </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });
                
            break;

        //Balance Sheet Ratios
            case 'bsr':

                body = 
                '<div class="row h-100">' +
                    '<div class="col-12 p-5 my-auto txt-color">' +
                        '<h5><strong>Analysis -</strong> Balance Sheet Ratios </h5>' +
                        '<br>' +
                        '<p id="modal-description' + number + '" class="font-size-14"></p>' +
                    '</div>' +
                '</div>';

                $('#modal-header' + number).html(header).slidedown;
                $('#modal-body' + number).html(body).slidedown;
                 

                $('#popupModal' + number).on('shown.bs.modal', function (e) {
                    description = new Description(number, module, data);
                    $('#modal-description' + number).html(description.getDescription()).slidedown;
                });

            break;

            default:

            break;
        }
    }
}

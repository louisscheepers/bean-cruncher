
$(document).ready(function(){
    populateCompanies();
});

function populateCompanies(ascending = true){
    let types;
    let dropdown = '';
    
    $.when(
        $.getJSON('json.php?cType=get', function(result){
            let types = result.sort();

            console.log(types);
            for(let i = 0; i < types.length; i++){
                dropdown += '<option value="' + types[i] + '">' + types[i] + '</option>';
            }
            dropdown += '<option value="None selected" hidden>None selected</option>';
        })
    ).then(function(){
        $.getJSON('json.php?gComp=get', function(result){
            console.log(result);

            let xeroCount = 0;
            let qbCount = 0;
            let sageCount = 0;
            let xeroMsg = '';
            let sageMsg = '';
            let quickMsg = '';
            let message = '';
            let disabled = '';
            let ownedCount = 0;
            
            
            if(ascending){
                result.sort(function(a, b) {
                    var textA = a['name'].toUpperCase();
                    var textB = b['name'].toUpperCase();
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
            }
            else{
                result.sort(function(b, a) {
                    var textA = a['name'].toUpperCase();
                    var textB = b['name'].toUpperCase();
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
            }

            if(result.length !== 0){
                for(let i = 0; i < result.length; i++){

                    console.log("user is owner" + " " + result[i]['userisowner']);
                        if(result[i]['userisowner']){
                            ownedCount++;
                        }
                        
                    message = '';
                    if(result[i]['status'] == 1){
                        let showUsers = false;
                        if(result[i]['package'] > 2){
                            showUsers = true;
                        }

                        if(result[i]['role'] == 3){
                            disabled = 'disabled';
                        }
                        else{
                            disabled = '';
                        }
        
                        type = result[i]['type'];
                        message += 
                        '<div class="row w-100 pl-7">' +
                            '<div class="col-sm-4 ml-3 mr-2 my-auto txt-color font-weight-bold">' +
                                result[i]['name'] +
                            '</div>' +
                            '<div class="col-sm-3 mx-2 my-auto">';
                            
                        if(type != null){
                            message += 
                            '<select class="custom-select m-0 my-auto dp-file-label" style="overflow: hidden !important;text-overflow: ellipsis !important;" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['name'] + '\', ' + result[i]['id'] + ')" ' + disabled + '>' +
                                dropdown +
                                '<option value="" selected hidden>' + type + '</option>' +
                            '</select>';
                        }
                        else{
                            message += 
                            '<select class="custom-select m-0 my-auto dp-file-label text-overfl" style="overflow: hidden !important;text-overflow: ellipsis !important;" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['name'] + '\', ' + result[i]['id'] + ')" ' + disabled + '>' +
                                '<option value="" selected disabled hidden>Choose industry type</option>' +
                                dropdown + 
                            '</select>';
                        }
                        
                        message += 
                        '</div>' +
                            '<div class="col-sm-2 mx-2 my-auto font-weight-light">';
                            if(result[i]['role'] == 3){
                                message +=
                                '<span class="text-muted">Users</span>';
                            }
                            else if(showUsers){
                                message +=
                                '<a href="user-management.php" class="txt-color" onclick="loadCompany(\'' + result[i]['name'] + '\', ' + result[i]['id'] + ')">Users</a>';
                            }
                        message +=
                            '</div>' +
                            '<div class="col-sm-1 my-auto font-weight-light txt-color">';
                                if(result[i]['role'] == 3){
                                    message +=
                                    '<span class="text-muted">Delete</span>';
                                }
                                else{
                                    message +=
                                    '<a class="txt-color" onclick="confirmDelete(' + result[i]['id'] + ')" href="javascript:void(0)">Delete</a>';
                                }
                                message +=
                            '</div>' +
                            '<div class="col-sm-1 my-auto">' + 
                                '<div id="loader' + result[i]['id'] + '" class=""></div>' +
                            '</div>' +
                        '</div>';
        
                        if(i != result.length - 1){
                            message += 
                            '<div class="col-12 w-75"><hr class="mx-7"></div>';
                        }
                    }
                   
                    switch (result[i]['api']) {
                        case '1':
                            xeroMsg += message;
                            xeroCount++;
                        break;
                        case '2':
                           sageMsg += message;
                            sageCount++;
                        break;
                        case '3':
                           quickMsg += message;
                            qbCount++;
                            break;
                        }
                    }

                    document.getElementById('companyRowsXero').innerHTML = xeroMsg;
                    document.getElementById('companyRowsSage').innerHTML = sageMsg;
                    document.getElementById('companyRowsQuickbooks').innerHTML = quickMsg;
                }

            
            
            if(result.length != 0){
                checkLinkCompanyPermissions(result[0]["userPackage"], ownedCount);    
            }

            if(xeroCount == 0){
                document.getElementById('companyRowsXero').innerHTML = '<div class="row pl-7 mt-4 font-weight-light txt-color"><p class="ml-3">No companies</p></div>';
            }
            if(sageCount == 0){
                document.getElementById('companyRowsSage').innerHTML = '<div class="row pl-7 mt-4 font-weight-light txt-color"><p class="ml-3">No companies</p></div>';
            }
            if(qbCount == 0){
                document.getElementById('companyRowsQuickbooks').innerHTML = '<div class="row pl-7 mt-4 font-weight-light txt-color"><p class="ml-3">No companies</p></div>';
            }
        })
    });
}

function linkDisabledTooltip() {
    var linktooltip = document.getElementById("linkTooltip");
    if (linktooltip.style.display === "none") {
        linktooltip.style.display = "block";
    } else {
        linktooltip.style.display = "none";
    }
}

function onLinkMouseOver() {
    document.getElementById('linkTooltip').style.display='block';
}

function onLinkMouseOverOut() {  
    document.getElementById('linkTooltip').style.display='none';
}

function checkLinkCompanyPermissions(package, count){
    console.log("Checking Permissions");
    console.log(package + " " + count);
    if(package <= 3 && count > 0){  
        console.log("disable multiple companies");
        $("#linkcompanyBtnActive").addClass('d-none');
        $("#linkcompanyBtnDisabled").removeClass("d-none");
    }

    if(package > 3){
        console.log("enable multiple companies");
        $("#linkcompanyBtnActive").removeClass('d-none');
        $("#linkcompanyBtnDisabled").addClass("d-none");
    }
}

function sortCompanies(){
    let sortTag = document.getElementById('sort-tag-xero');
    if(document.getElementById('sort-xero-down')){
        sortTag.innerHTML = '<i id="sort-xero-up" class="fas fa-sort filter-icon" style="width: 18px;"></i>';
        populateCompanies(false);
    }
    else{
        sortTag.innerHTML = '<i id="sort-xero-down" class="fas fa-sort filter-icon" style="width: 18px;"></i>';
        populateCompanies(true);
    }
    
}

function selectChange(name, id){
    console.log('Changed company ' + name + ' ID: ' + id + '\'s type to ' + document.getElementById('select' + id).value);

    let newType = document.getElementById('select' + id).value;
    
    $.ajax({
        type: "GET",
        url: 'json.php',
        data: {uComp:id, type:newType},
        success: function(){
            // populateCompanies();
            console.log('Successfully updated!');
        }
    });
}


function confirmDelete(id){
    $("#confirm-delete").modal();
    document.getElementById('confirm-btn').innerHTML = '<button type="button" onclick="deleteCompany(\'' + id + '\')" class="mt-3 btn btn-cross-platform btn-w" data-dismiss="modal">Confirm</button>'
}

function deleteCompany(id){
    document.getElementById('loader' + id).innerHTML = '<img src="img/BC-preloader-gif.gif" style="height: 25px;">';

    $.ajax({
        type: "GET",
        url: 'json.php',
        data: {dComp:id},
        success: function(){

            console.log("DELETED COMPANY");
            location.reload();
            return false;
        }
    });
    
}

// function updateDB(name){
//     document.getElementById('loader' + id).innerHTML = '<img src="img/BC-preloader-gif.gif" style="height: 25px;">';

//     let action = 'update';
//     let mods = 'all';
//     let interval = 12;
//     $.ajax({
//         type: "GET",
//         url: 'json.php',
//         data: {q:action,mod:mods,int:interval},
//         success: function(){
//             populateCompanies();
//         }
//     });
//     // $.getJSON('json.php?aComp=' + name, function(){
//     //     document.getElementById('updateLoader').style.display = 'none';
//     // });
// }

function loadCompany(name, id){
    $.getJSON('json.php?aComp=' + id, function(){});
    document.getElementById('companyDropDown').innerHTML = name;
}
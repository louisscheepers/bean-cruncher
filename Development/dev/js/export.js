//create default div wrappers beforehand
console.log('Creating default wrappers...');

let wrapperCompanyIcon = document.createElement('div');
wrapperCompanyIcon.innerHTML = '<img id="def-comp-img" src="./img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Company Brand">';
let brandIcon = wrapperCompanyIcon.firstChild;

let wrapperUserIcon = document.createElement('div');
wrapperUserIcon.innerHTML = '<img id="def-user-img" src="./img/person-blue.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
let usrIcon = wrapperUserIcon.firstChild;

function makePDF(onlyPdf, callback) {

    document.getElementById('nav-progress').hidden = false;
    document.getElementById('nav-progress').value = 5;
    document.getElementById('nav-progress-text').hidden = false;
    document.getElementById('nav-progress-text').innerText = 'Retrieving data...';
    action = 'fetch';
    let pdfArray = [];
    $.ajax({
        type: "POST",
        url: 'json.php',
        data: {company:action},
        success: function(result){
            document.getElementById('nav-progress').value = 10;
            document.getElementById('nav-progress-text').innerText = 'Getting Company Info ...';
            let today = new Date();
            let month;
            let day;
            if((today.getMonth() + 1).toString().length == 1){
                month = '0' + (today.getMonth() + 1);
            }
            else{
                month = today.getMonth() + 1;
            }
            if((today.getDate()).toString().length == 1){
                day = '0' + (today.getDate());
            }
            else{
                day = today.getDate();
            }
            let date = today.getFullYear()+'-'+month+'-'+day;
            let obj = jQuery.parseJSON(result);
            
            document.getElementById('nav-progress').value = 20;
            document.getElementById('nav-progress-text').innerText = 'Creating Front Page';

            var firstSection = createFirstSection(obj, date, brandIcon, usrIcon);
            
            //Add a table of contents to first section before adding to array

            pdfArray.push(btoa(firstSection.output()));
            console.log("Pushed to array !!!");

            console.log("Updating Nav progress !!!");
            document.getElementById('nav-progress').value = 60;
            document.getElementById('nav-progress-text').innerText = 'Inserting Charts...';

            //Page 3 Dashboard content
            let annexures = new Array();
            for(let j = 1; j <= 8; j++){
                if($('#annex' + j).height() == 600){
                    annexures.push(j);
                }
            }
            $.when(
                console.log('showing pdfDash'),
                $('#pdfDash').show(),
                $('#pdfDashTitle').show(),
                $('#pdfAnnexure').show()
            ).then(function(){
                console.log('launching set')
                $.when(
                    console.log('preparation check')
                ).then(function(){
                    console.log('starting kendo process')
                    getKendoPDF("pdfDash", false, 0.5 ,function (dbContent){
                        console.log('kendo1 callback');
                        pdfArray.push(dbContent);
                        document.getElementById('nav-progress').value = 80;
                        document.getElementById('nav-progress-text').innerText = 'Inserting Annexures...';
                        for(let i = 1; i <= 8; i ++ ){
                            if(isHtmlEmpty('annex' + i) && i > 1){
                                $('#annex' + i).removeClass('break');
                            }
                            else{
                                expand(i, true,true,true);
                            }
                        }

                        for(let i = 8; i >= 1; i -- ){
                             if(!isHtmlEmpty('pdfAnnbox' + i)){
                                $('#pageBreak' + i).removeClass('page-break');
                                console.log("removing page break: " + i);
                                break;
                             }
                        }
        
                        getKendoPDF("pdfAnnexure", false, 0.6,function (annexPDF){
                            console.log('kendo2 callback');
                            pdfArray.push(annexPDF);
                            document.getElementById('nav-progress').value = 100;
                            document.getElementById('nav-progress-text').innerText = 'Building Pdf...';
                            for(let i = 1; i <= 8; i ++ ){
                                if(!isHtmlEmpty('annex' + i)){
                                    expand(i, false,true,true);
                                } else{
                                    $('#annex' + i).removeClass('break');
                                }
                            }
        
                            document.getElementById('pdfDash').hidden = true;
                            document.getElementById('pdfDashTitle').hidden = true;
                            document.getElementById('pdfAnnexure').hidden = true;
                            callback(pdfArray,"" + obj['company_name'] + " - " + date);
                        })
                    });
                });
            });
        }
    });
}

function createFirstSection(obj, date, defaultBrandIcon, defaultUserIcon){
    let name = obj['company_name'];
    let img = obj['company_brand'];

    let username = obj['username'];
    let usersurname = obj['surname'];
    let userimg = obj['userimage'];

    var firstSection = new jsPDF({orientation:'p', unit:'pt', format:'a4', compress:true});

    let firstLinePos = 120;
    let nextLinePos = 0;

    //Page 1 + 2
    firstSection.setTextColor('#100D4C');
    if(img == '<img id="comp-img" src="data:image/jpeg;base64," class="rounded-circle icon-size" alt="Company Brand">'){
        firstSection.addImage(defaultBrandIcon, 'JPEG', 200, 260, 200, 200, undefined, 'FAST');
    }
    else{
        let brandWrapper = document.createElement('div');
        brandWrapper.innerHTML = img;
        let brand = brandWrapper.firstChild;
        firstSection.addImage(brand, 'JPEG', 200, 240, 200, 200, undefined, 'FAST');
    }
    // throw new Error("END");

    firstSection.setFont("arial", "bold");
    firstSection.setFontSize(22);
    nextLinePos = 750;
    firstSection.text(30, nextLinePos, "" + name + " " + " ");

    firstSection.setFont("arial", "normal");
    firstSection.setFontSize(16);
    nextLinePos = 780;
    firstSection.text(30, nextLinePos, "" + date + " " + " ");
    

    firstSection.setTextColor("#A5AFB7");
    firstSection.setFont("arial", "normal");
    firstSection.setFontSize(10);
    firstSection.text(30, 805, "Compiled by:" + " " + " ");

    if(userimg == '<img id="user-img" src="data:image/jpeg;base64," class="rounded-circle icon-size" alt="Profile Icon">'){
        firstSection.addImage(defaultUserIcon, 'JPEG', 93, 792, 20, 20, undefined, 'FAST');
    }
    else{
        let userWrapper = document.createElement('div');
        userWrapper.innerHTML = userimg;
        let usrIcon = userWrapper.firstChild;
        firstSection.addImage(usrIcon, 'JPEG', 93, 792, 20, 20, undefined, 'FAST');
    }

    firstSection.setTextColor("#A5AFB7");
    firstSection.setFont("arial", "normal");
    firstSection.setFontSize(10);
    firstSection.text(120, 805, username + " " + usersurname + " " + " ");

    //Page 2 TOC
    firstSection.addPage('a4','p');
    firstSection.setFont("arial", "bold");
    firstSection.setFontSize(22);
    firstSection.setTextColor('#100D4C');
    firstSection.text(30, 70, "Table of Contents");
    firstSection.setFont("arial", "normal");
    firstSection.setFontSize(16);

    firstLinePos = 120;
    nextLinePos = 0;
    for(let i = 1; i <=8 ; i ++){
        if(!isHtmlEmpty('annex' + i)){
            firstSection.text(30,firstLinePos + nextLinePos , $('#annex' + i + ' h5').text());
            nextLinePos += 30;
        }
    }

    return firstSection;
}

function getKendoPDF(id, landscape, scale, callback){
    console.log('kendo running');
    kendo.drawing
        .drawDOM("#" + id,
            {
                forcePageBreak: ".page-break",
                paperSize: "A4",
                scale: scale,
                landscape: landscape
            })
        .then(function(group){
            console.log('kendo then');
            kendo.drawing.pdf.toDataURL(group, function(dataURL){

            dataURL = dataURL.replace('data:application/pdf;base64,', '');
            callback(dataURL);
        });
    })
}

function uploadPDFtoServer(pdfArray, name, method, email, callback) {
    document.getElementById('nav-progress').value = 80;
    document.getElementById('nav-progress-text').innerText = 'Uploading...';
    switch (method){
        case "pdf":{
            $.post("mergePDFs.php",
                {
                    firstSection: "" + pdfArray[0],
                    dashboard: "" + pdfArray[1],
                    annextures: "" + pdfArray[2],
                    name : "" + name,
                    method: method
                },
                function(response, textStatus)
                {
                    // callback(response);
                });
            // break;
        }
        case "email":{
            $.post("mergePDFs.php",
                {
                    firstSection: "" + pdfArray[0],
                    dashboard: "" + pdfArray[1],
                    annextures: "" + pdfArray[2],
                    email: email,
                    name : "" + name,
                    method: method
                },
                function(response, textStatus)
                {
                    callback(response);
                });
            break;
        }
        case "pptx":{
            $.post("mergePDFs.php",
                {
                    firstSection: "" + pdfArray[0],
                    dashboard: "" + pdfArray[1],
                    annextures: "" + pdfArray[2],
                    name : "" + name,
                    method: method
                },
                function(response, textStatus)
                {
                    callback(response);
                });
            break;
        }
    }

}

function exportFile(extention){
    for(i = 1; i <= 8; i++){
        boxes = document.querySelectorAll('[id^="box'+ (i) +'"]');
        box = boxes[0].id;
        if(box.length != 4){
            switch (extention) {
                //CSV
                case 'csv':
                    window.location = 'downloadCSV.php';
                break;
                //XLSX
                case 'xlsx':
                    window.location = 'downloadXLSX.php';
                break;
                //PDF
                case 'pdf':

                    document.getElementById('pdfDash').hidden = false;
                    document.getElementById('pdfDashTitle').hidden = false;
                    document.getElementById('pdfAnnexure').hidden = false;
                    $('#export-modal').modal('show');

                    makePDF(true,function (pdf,filename){
                        uploadPDFtoServer(pdf, filename, "pdf", null, function (data){
                            document.getElementById('nav-progress').value = 100;
                            console.log("Response from server: " + data);
                            downloadFile('tmp/pdf/' + filename + '.pdf', filename);
                            document.getElementById('nav-progress').hidden = true;
                            document.getElementById('nav-progress').value = 0;
                            document.getElementById('nav-progress-text').hidden = true;
                            document.getElementById('nav-progress-text').innerText = '';

                            $('#export-modal').modal('hide');
                        });
                    })

                break;
                //POWERPOINT
                case 'pptx':

                    document.getElementById('pdfDash').hidden = false;
                    document.getElementById('pdfDashTitle').hidden = false;
                    document.getElementById('pdfAnnexure').hidden = false;
                    $('#export-modal').modal('show');

                    makePDF(false,function (pdf,filename){
                        console.log("PDF callback success");

                        uploadPDFtoServer(pdf, filename, "pptx", null, function (data){
                            document.getElementById('nav-progress').value = 100;
                            console.log("Response from server: " + data);
                            window.location = 'tmp/pptx/' + filename + '.pptx';
                            document.getElementById('nav-progress').hidden = true;
                            document.getElementById('nav-progress').value = 0;
                            document.getElementById('nav-progress-text').hidden = true;
                            document.getElementById('nav-progress-text').innerText = '';
                            
                            $('#export-modal').modal('hide');
                        });
                    });

                break;
            }
            break;
        }
    }
}

function downloadFile(tab, filename){
    console.log('Downloading PDF...');
    let a = document.createElement('a');
    a.href = tab;
    a.download = filename;
    a.click();
}

function isHtmlEmpty(selector){
    if($.trim($("#" + selector).html())==''){
        return true
    }
    return false;
}

function prepareEmail(){
    console.log('Preparing email');

    let addresses = document.getElementById('addresses').value;
    document.getElementById('addresses').value = '';

    $("#email-modal").modal('hide');
    $('#export-modal').modal('show');

    console.log('Starting makePDF');
    document.getElementById('pdfDash').hidden = false;
    document.getElementById('pdfDashTitle').hidden = false;
    document.getElementById('pdfAnnexure').hidden = false;
    makePDF(false,function (pdf,filename){
        console.log("PDF callback success");

        uploadPDFtoServer(pdf, filename, "email", addresses, function (data){
            console.log('Upload to server callback success');
            document.getElementById('nav-progress').value = 100;
            console.log("Response from server: " + data);
            document.getElementById('nav-progress').hidden = true;
            document.getElementById('nav-progress').value = 0;
            document.getElementById('nav-progress-text').hidden = true;
            document.getElementById('nav-progress-text').innerText = '';

            $.when(
                $('#export-modal').modal('hide')
            ).then(
                $("#email-sent-modal").modal('show')
            )
        });
    });
}
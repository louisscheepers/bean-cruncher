class Annexure{

    constructor(scrollNumber, module, data){
        this.scrollNumber = scrollNumber;
        this.module = module;
        this.data = data;
        // console.log('Creating annexure of ' + this.module + ' at number ' + this.scrollNumber);
    }

    pdfAnnBox(scrollNumber, cat, title, pdfGraphZone, pdfchartId){
        let html =
            '<div class="row">' +
            '<div class="col-10"><h5 class="text-left p-2 mt-3 txt-color">' + scrollNumber + '.&nbsp;</span>' + '<strong>' + cat +' -</strong> ' + title + '</h5></div>' +
            '<div class="col-1"></div>' +
            '<div class="col-1"></div>' +
            '<div class="col-8">' +
                '<div class="mt-5 pfd-graph-w" id="pdfAnnex' + scrollNumber + 'graphZone" style="position: absolute; z-index: 101;"></div>' +
            '</div>' +
            '<div class="col-3"></div>' +
            '<div class="col-1"></div>' +
            '<div class="col-10"><p id="pdfDescription' + scrollNumber + '" class="font-size-14 mb-3 mt-5 mobile-fs-11 txt-color"></p></div>' +
            '<div class="col-1"></div>' +
            '<div class="col-1"></div>' +
            '<div class="col-10 mb-5">' +
                '<p class="col-12 my-3" id="pdfComments' + scrollNumber + '"' + 
            '<div class="col-1"></div>' +
            '<div id="pageBreak' + scrollNumber + '" class="page-break"></div>' +
            '</div>';
            
        $('#pdfAnnbox' + scrollNumber).html(html).slidedown;
        $(pdfGraphZone).html('<canvas id="' + pdfchartId + '"></canvas>').slidedown;

        $(pdfGraphZone).css('position', 'relative');
        $(pdfGraphZone).css('margin', 'auto');
        $(pdfGraphZone).css('width', '90%');
    }

    buildAnnexure(){

        document.getElementById('annex' + this.scrollNumber).style.height = "600px";

        let names = new Array();
        let totals = new Array();
        let items = new Array();
        let sales = new Array();
        let qoh = new Array();
        let html = '';
        let descriptionId = 'description' + this.scrollNumber;
        let pdfDescriptionId = 'pdfDescription' + this.scrollNumber;
        let graphZone = '#annexure-graph' + this.scrollNumber;
        let pdfAnnexGraphZone = '#pdfAnnex' + this.scrollNumber + 'graphZone';
        let commentZone = 'annex-comments' + this.scrollNumber;
        let pdfCommentZone = 'pdfComments' + this.scrollNumber;

        let graphId;
        let chartId = 'annex' + this.scrollNumber + "canvas";
        let pdfchartId = 'pdf' + this.scrollNumber + 'canvas';
        let pdfAnnexchart;
        let pdfcanvas;
        let chart;
        let days = ['0-30', '30-60', '60-90', '90-120', '120+'];
        let canvas;
        let ctx;
        let pdfctx;
        let _0_30;
        let _30_60;
        let _60_90;
        let _90_120;
        let _120;
        let data;
        let modal;
        let description;
        let commentElement;
        let pdfCommentElement;

        switch(this.module){

        //Cash In v Cash Out
            case 'cico':

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 col-lg-5 p-3 my-auto">' +
                        '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">' + '<span nowrap class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Overview -</strong>&nbsp;Cash In v Cash Out </h5>' +
                        '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '<br>' +
                        '<a  class="k-hide float-left border-0 bg-view-comments mt-n1 txt-color mobile-fs-14 font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')"><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                    '</div>' +
                    '<div class="col-5 p-3 my-auto d-none d-lg-block">' +
                        '<div class="row">' +
                            '<div class="" id="annexure-graph' + this.scrollNumber + '" style="position: absolute; z-index: 100;"></div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' + 
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;" >' +
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Overview', 'Cash in vs Cash out', pdfAnnexGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + commentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                let cashIn = this.data.data['cash_in'];
                let cashOut = this.data.data['cash_out'];
                let diff = new Array();
                for(let i = 0; i < cashIn.length; i++){
                    diff[i] = cashIn[i] - cashOut[i];
                }

                chart = new BeanChart(chartId, ctx, this.data.labels, cashIn, cashOut, diff)
                chart.drawMixedGroupBarLine(["Cash In", "Cash Out", "Movement"], true);

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, this.data.labels, cashIn, cashOut, diff)
                pdfAnnexchart.drawMixedGroupBarLine(["Cash In", "Cash Out", "Movement"], true);

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);
               
            break;

        //Income v Expenses
            case 'ino':

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-5 p-3 my-auto d-none d-lg-block">' +
                        '<div class="row">' +
                            '<div class="h-85 w-85" id="annexure-graph' + this.scrollNumber + '" style="position: absolute; z-index: 100;"></div>' +
                            '<div class="h-85 w-85" id="annexure-graph' + this.scrollNumber + 'Lap" style="position: absolute; z-index: 101;"></div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-10 col-lg-5 p-3 my-auto">' +
                        '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Overview -</strong>&nbsp;Income v Expenses</h5>' +
                        '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '<br>' +
                        '<a  class="k-hide float-left border-0 bg-view-comments txt-color mobile-fs-14 mt-n1 font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Overview', 'Income v Expenses', pdfAnnexGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                chart = new BeanChart(chartId, ctx, this.data.labels, this.data.data['income'], this.data.data['expenses'], this.data.data['net_profit'])
                chart.drawMixedGroupBarLine(["Income", "Expenses", "Net Profit"], true);

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, this.data.labels, this.data.data['income'], this.data.data['expenses'], this.data.data['net_profit'])
                pdfAnnexchart.drawMixedGroupBarLine(["Income", "Expenses", "Net Profit"], true);

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;
                
        //Asset v Debt
            case 'avd' :

                descriptionId = 'description' + this.scrollNumber;
                graphId = 'annexure-graph' + this.scrollNumber;

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 col-lg-5 p-3 my-auto">' +
                        '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Overview -</strong>&nbsp;Asset v Debt</h5>' +
                        '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '<br>' +
                        '<a  class="k-hide float-left border-0 bg-view-comments mt-n1 txt-color mobile-fs-14 font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                    '</div>' +
                    '<div class="col-5 p-5 my-auto d-none d-lg-block" id="annexure-graph' + this.scrollNumber + '"></div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Overview', 'Asset v Debt', pdfAnnexGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });
                
                let asset_debt = [this.data.data['assets'], this.data.data['debt']];
                chart = new BeanChart(chartId, ctx, ['Assets', 'Debt'], asset_debt);
                chart.drawDoughnut([true,'right']);

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, ['Assets', 'Debt'], asset_debt)
                pdfAnnexchart.drawDoughnut([true,'right']);

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);
            
            break;
        
        //Profit & Loss
            case 'pl':

                descriptionId = 'description' + this.scrollNumber;

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 p-3 my-4">' +
                        '<div class="row no-gutters">' +
                            '<div class="col-12 col-lg-6 txt-color">' +
                                '<h5 class="txt-color mobile-fs-14 mb-6 mb-sm-3" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Financial Management -</strong>&nbsp;Profit & Loss  </h5>' +
                            '</div>' +
                            '<div class="col-12 col-lg-6 txt-color font-weight-semi-bold border-0 annex-text-left txt-align-right">' +
                                '<a  class="mobile-fs-14 k-hide border-0 bg-view-comments mt-n1 mb-sm-3" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                            '</div>' +
                        '</div>' +
                        '<br>' +
                        '<div id="annex-row' + this.scrollNumber + '" style="height: 440px; overflow-y: auto; overflow-x: hidden;">' +
                            '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 w-100 txt-color"></p>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"><i class="fas fa-angle-double-down"></i></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Financial Management', 'Profit & Loss');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Balance Sheet
            case 'bs':
 
            descriptionId = 'description' + this.scrollNumber;
            console.log('Showing BS Ann');
 
            html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 p-3 my-auto">' +
                        '<div class="row no-gutters">' +
                            '<div class="col-12 col-lg-6 txt-color">' +
                                '<h5 class="txt-color mobile-fs-14 mb-6 mb-sm-3" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Financial Management -</strong>&nbsp;Balance Sheet</h5>' +
                            '</div>' +
                            '<div class="col-12 col-lg-6 mb-sm-3 annex-text-left txt-color font-weight-semi-bold border-0 txt-align-right">' +
                                '<a  class="k-hide border-0 bg-view-comments mobile-fs-14 mt-n1" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                            '</div>' +
                        '</div>' +
                        '<div id="annex-row' + this.scrollNumber + '" style="height: 440px; overflow-y: auto; overflow-x: hidden;">' +
                            '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 w-100 txt-color"></p>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mx-auto mb-3"><i class="fas fa-angle-double-down"></i></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;

                 //pdfBox
                 this.pdfAnnBox(this.scrollNumber,'Financial Management', 'Balance Sheet');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Accounts Receivable
            case 'ar' :

                descriptionId = 'description' + this.scrollNumber;

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 p-3 my-auto">' +
                    '<div class="row no-gutters">' +
                            '<div class="col-12 col-lg-6 txt-color">' +
                                '<h5 class="txt-color mobile-fs-14 mb-6 mb-sm-3" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Customers -</strong>&nbsp;Accounts Receivable </h5>' +
                            '</div>' +
                            '<div class="col-12 col-lg-6 txt-color font-weight-semi-bold mb-sm-3 border-0 annex-text-left txt-align-right">' +
                                '<a  class="k-hide border-0 bg-view-comments mobile-fs-14 mt-n1 txt-color font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                            '</div>' +
                        '</div>' +
                        '<div id="annex-row' + this.scrollNumber + '" style="height: 440px; overflow-y: auto; overflow-x: hidden;">' +
                            '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"><i class="fas fa-angle-double-down"></i></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' +  
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                
                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Customers', 'Accounts Receivable');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();
                
                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);
            
            break;

        //Debtors days outstanding
            case 'ddo':

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 col-lg-5 p-3 my-auto">' +
                        '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">'  + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Customers -</strong>&nbsp;Debtors Days Outstanding </h5>' +
                            '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                            '<br>' +
                            '<a  class="k-hide mobile-fs-14 float-left border-0 bg-view-comments mt-n1 txt-color font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                    '</div>' +
                    '<div class="col-5 p-5 my-auto d-none d-lg-block" id="annexure-graph' + this.scrollNumber+ '"></div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Customers', 'Debtors Days Outstanding', pdfAnnexGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });
                
                _0_30 = 0;
                _30_60 = 0;
                _60_90 = 0;
                _90_120 = 0;
                _120 = 0;
                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    if(this.data.data['accounts'][i]['0-30'] != 0){
                        _0_30 += this.data.data['accounts'][i]['0-30'];
                    }
                    if(this.data.data['accounts'][i]['30-60'] != 0){
                        _30_60 += this.data.data['accounts'][i]['30-60'];
                    }
                    if(this.data.data['accounts'][i]['60-90'] != 0){
                        _60_90 += this.data.data['accounts'][i]['60-90'];
                    }
                    if(this.data.data['accounts'][i]['90-120'] != 0){
                        _90_120 += this.data.data['accounts'][i]['90-120'];
                    }
                    if(this.data.data['accounts'][i]['120+'] != 0){
                        _120 += this.data.data['accounts'][i]['120+'];
                    }
                }
                data = [_0_30, _30_60, _60_90, _90_120, _120]

                chart = new BeanChart(chartId, ctx, days, data);
                chart.drawSingleBar();

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, days, data)
                pdfAnnexchart.drawSingleBar();

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Top Customers by Balance Outstanding
            case 'tcs':

                descriptionId = 'description' + this.scrollNumber;
                graphId = 'annexure-graph' + this.scrollNumber;

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 col-lg-6 p-3 my-auto">' +
                    '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Customers -</strong>&nbsp;Top Customers by Balance Outstanding</h5>' +
                        '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '<br>' +
                        '<a  class="k-hide float-left mobile-fs-14 border-0 bg-view-comments mt-n1 txt-color font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                    '</div>' +
                    '<div class="col-4 p-3 my-auto d-none d-lg-block" id="annexure-graph' + this.scrollNumber+ '"></div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' +  
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                 //pdfBox
                 this.pdfAnnBox(this.scrollNumber,'Customers', 'Top Customers by Balance Outstanding', pdfAnnexGraphZone, pdfchartId);
                 pdfcanvas = document.getElementById(pdfchartId)
                 pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    names[i] = this.data.data['accounts'][i]['name'];
                    totals[i] = this.data.data['accounts'][i]['total'];
                    if(i == 5){
                        break;
                    }
                }

                chart = new BeanChart(chartId, ctx, names, totals);
                chart.drawHorizontalBar(false, "Balance Outstanding", true, false);

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, names, totals)
                pdfAnnexchart.drawHorizontalBar(false, "Balance Outstanding" , false);

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Retention
            case 'ret':

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-5 p-3 my-auto d-none d-lg-block">' +
                            '<div class="h-85 w-85" id="annexure-graph' + this.scrollNumber + '" style="position: absolute; z-index: 100;"></div>' +
                    '</div>' +
                    '<div class="col-10 col-lg-5 p-3 my-auto">' +
                        '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Customers -</strong>&nbsp;Retention </h5>' +
                        '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '<br>' +
                        '<a  class="k-hide float-left mobile-fs-14 border-0 bg-view-comments mt-n1 txt-color font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Customers', 'Retention', pdfAnnexGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                let ret = new Array();
                for(let i = 0; i < this.data.data['retention'].length; i++){
                    ret[i] = this.data.data['retention'][i];
                }

                chart = new BeanChart(chartId, ctx, this.data.labels, ret, ret);
                chart.drawBarLine(["",""], false, "%");

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, this.data.labels, ret, ret)
                pdfAnnexchart.drawBarLine(["",""], false, "%");

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Accounts Payable
            case 'ap':

                descriptionId = 'description' + this.scrollNumber;
                graphId = 'annexure-graph' + this.scrollNumber;

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 p-3 my-auto">' +
                        '<div class="row no-gutters">' +
                            '<div class="col-12 col-lg-6 txt-color">' +
                                '<h5 class="txt-color mobile-fs-14 mb-6 mb-sm-3" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Suppliers -</strong>&nbsp;Accounts Payable </h5>' +
                            '</div>' +
                            '<div class="col-12 col-lg-6 txt-color mb-sm-3 font-weight-semi-bold border-0 annex-text-left txt-align-right">' +
                                '<a  class="k-hide mobile-fs-14 border-0 bg-view-comments  mt-n1" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                            '</div>' +
                        '</div>' +
                        '<div id="annex-row' + this.scrollNumber + '" style="height: 440px; overflow-y: auto; overflow-x: hidden;">' +
                            '<p id="description' + this.scrollNumber + '" class="font-size-14 txt-color"></p>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"><i class="fas fa-angle-double-down"></i></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' +  
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                
                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Suppliers', 'Accounts Payable');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });
                
                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Creditors Days Outstanding
            case 'cdo':

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 col-lg-5 p-3 my-auto">' +
                        '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Suppliers -</strong>&nbsp;Creditors Days Outstanding </h5>' +
                        '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '<br>' +
                        '<a  class="k-hide mobile-fs-14 float-left border-0 bg-view-comments mt-n1 txt-color font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                    '</div>' +
                    '<div class="col-5 p-5 d-none d-lg-block" id="annexure-graph' + this.scrollNumber+ '"></div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Suppliers', 'Creditors Days Outstanding', pdfAnnexGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });
                
                _0_30 = 0;
                _30_60 = 0;
                _60_90 = 0;
                _90_120 = 0;
                _120 = 0;
                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    if(this.data.data['accounts'][i]['0-30'] != 0){
                        _0_30 += this.data.data['accounts'][i]['0-30'];
                    }
                    if(this.data.data['accounts'][i]['30-60'] != 0){
                        _30_60 += this.data.data['accounts'][i]['30-60'];
                    }
                    if(this.data.data['accounts'][i]['60-90'] != 0){
                        _60_90 += this.data.data['accounts'][i]['60-90'];
                    }
                    if(this.data.data['accounts'][i]['90-120'] != 0){
                        _90_120 += this.data.data['accounts'][i]['90-120'];
                    }
                    if(this.data.data['accounts'][i]['120+'] != 0){
                        _120 += this.data.data['accounts'][i]['120+'];
                    }
                }
                data = [_0_30, _30_60, _60_90, _90_120, _120]

                chart = new BeanChart(chartId, ctx, days, data);
                chart.drawSingleBar();

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, days, data)
                pdfAnnexchart.drawSingleBar();

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Top Suppliers by Purchases
            case 'tsp':

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 col-lg-5 p-3 my-auto">' +
                    '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Suppliers -</strong>&nbsp;Top Suppliers by Balance Outstanding</h5>' +
                        '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '<br>' +
                        '<a  class="k-hide float-left border-0 mobile-fs-14 bg-view-comments mt-n1 txt-color font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                    '</div>' +
                    '<div class="col-5 p-5 d-none d-lg-block" id="annexure-graph' + this.scrollNumber+ '"></div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Suppliers', 'Top Suppliers by Balance Outstanding', pdfAnnexGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    names[i] = this.data.data['accounts'][i]['name'];
                    totals[i] = this.data.data['accounts'][i]['total'];
                    if(i == 5){
                        break;
                    }
                }
                chart = new BeanChart(chartId, ctx, names, totals);
                chart.drawHorizontalBar(false, "Balance Outstanding", true, false);

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, names, totals)
                pdfAnnexchart.drawHorizontalBar(false, "Balance Outstanding" , false);

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;
            
        //Inventory Management
            case 'im':

                descriptionId = 'description' + this.scrollNumber;

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 p-3 my-auto">' +
                        '<div class="row no-gutters">' +
                            '<div class="col-12 col-lg-6 txt-color">' +
                                '<h5 class="txt-color mobile-fs-14 mb-6 mb-sm-3" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Inventory -</strong>&nbsp;Inventory Management </h5>' +
                            '</div>' +
                            '<div class="col-12 col-lg-6 txt-color font-weight-semi-bold mb-sm-3 annex-text-left border-0 txt-align-right">' +
                                '<a  class="k-hide mobile-fs-14 border-0 bg-view-comments mt-n1" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                            '</div>' +
                        '</div>' +
                        '<br>' +
                        '<div id="annex-row' + this.scrollNumber + '" style="height: 440px; overflow-y: auto; overflow-x: hidden;">' +
                            '<p id="description' + this.scrollNumber + '" class="font-size-14 txt-color"></p>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"><i class="fas fa-angle-double-down"></i></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                
                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Financial Management', 'Inventory Management');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Inventory Analysis - Sales
            case 'inas':

                html = 
                '<div class="row h-100 no-gutters">' + 
                    '<div class="col-1"></div>' + 
                        '<div class="col-10 col-lg-5 p-3 my-auto">' + 
                        '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Inventory -</strong>&nbsp;Top Items by Sales </h5>' +
                            '<div class="row">' + 
                                '<span class="small my-auto mobile-fs-11 ml-4 txt-color">Sales </span>' +
                                '<label class="switch my-auto ml-1">' +
                                    '<input type="checkbox" checked=checked onclick="checkQuantity(\'box' + this.scrollNumber + '\', \'inas\')">' +
                                    '<span class="slider"></span>' +
                                '</label><br>' +
                                '<span class="small my-auto mobile-fs-11 ml-3 txt-color">Quantity </span>' +
                                '<label class="switch my-auto ml-1">' +
                                    '<input type="checkbox" onclick="checkQuantity(\'box' + this.scrollNumber + '\', \'inas\')">' +
                                    '<span class="slider"></span>' +
                                '</label>' +
                            '</div>' +
                            '<br>' +
                        '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '<br>' +
                        '<a  class="k-hide float-left border-0 bg-view-comments mt-n1 txt-color font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                        // '<span class="small float-right mr-n5">Jan </span><br>' +  
                        // '<span class="color-red small float-right mr-n5">Feb</span><br>' +  
                    '</div>' + 
                    '<div class="col-5 p-5 my-auto d-none d-lg-block" id="annexure-graph' + this.scrollNumber+ '"></div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');

                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Inventory', 'Top Items by Sales', pdfAnnexGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });
                
                for(let i = 0; i < Object.keys(this.data.data).length; i++){
                    if(typeof this.data.data[i] === 'undefined'){
                        break;
                    }
                    else{
                        items[i] = this.data.data[i]['item_code'];
                        sales[i] = this.data.data[i]['total_sales'];
                    }
                }


                chart = new BeanChart(chartId, ctx, items, sales);
                chart.drawSingleBar();

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, items, sales)
                pdfAnnexchart.drawSingleBar();


                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Inventory Analysis - Quantity
            case 'inaq':
                
                html = 
                '<div class="row h-100 no-gutters">' + 
                    '<div class="col-1"></div>' + 
                        '<div class="p-3 col-10 col-lg-5 my-auto">' + 
                        '<h5 class="txt-color mobile-fs-14 mb-6" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Inventory -</strong>&nbsp;Top Items by Quantity </h5>' +
                                '<div class="row">' + 
                                    '<span class="small mobile-fs-11 my-auto ml-4 txt-color">Sales </span>' +
                                    '<label class="switch my-auto ml-1">' +
                                        '<input type="checkbox" onclick="checkSales(\'box' + this.scrollNumber + '\', \'inas\')">' +
                                        '<span class="slider"></span>' +
                                    '</label>' +
                                    '<span class="small mobile-fs-11 my-auto ml-3 txt-color">Quantity </span>' +
                                    '<label class="switch my-auto ml-1">' +
                                        '<input type="checkbox" checked=checked onclick="checkSales(\'box' + this.scrollNumber + '\', \'inas\')">' +
                                        '<span class="slider"></span>' +
                                    '</label>' +
                                '</div>' +
                                '<br>' +
                        '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' + 
                        '<br>' +
                        '<a  class="k-hide float-left border-0 bg-view-comments mt-n1 txt-color font-weight-semi-bold" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                    '</div>' + 
                    '<div class="col-5 p-5 d-none d-lg-block" id="annexure-graph' + this.scrollNumber+ '"></div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                canvas = document.getElementById(chartId);
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Inventory', 'Top Items by Quantity', pdfAnnexGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                this.data.data['item'].sort(function(a,b){
                    return  b['quantity_on_hand'] - a['quantity_on_hand'];
                })

                for(let i = 0; i < Object.keys(this.data.data).length; i++){
                    if(typeof this.data.data['item'][i] === 'undefined'){
                        break;
                    }
                    else{
                        names[i] = this.data.data['item'][i]['name'];
                        qoh[i] = this.data.data['item'][i]['quantity_on_hand'];
                    }
                }

                chart = new BeanChart(chartId, ctx, names, qoh);
                chart.drawSingleBar();

                pdfAnnexchart = new BeanChart(pdfchartId, pdfctx, names, qoh)
                pdfAnnexchart.drawSingleBar();

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);
                
            break;
        
        //Profit Loss Ratios
            case 'plr':

                descriptionId = 'description' + this.scrollNumber;
                graphId = 'annexure-graph' + this.scrollNumber;

                html = 
                '<div class="row h-100 no-gutters">' +
                    '<div class="col-1"></div>' +
                    // '<div class="col-5 p-5">' +
                    //     '<div class="h-85 w-85" id="annexure-graph' + this.scrollNumber + '" style="position: absolute; z-index: 100;"></div>' +
                    //     '<div class="h-85 w-85" id="annexure-graph' + this.scrollNumber + 'Lap" style="position: absolute; z-index: 101;"></div>' +
                    // '</div>' +S
                    '<div class="col-10 p-3 my-auto">' +
                        // '<div class="row">' +
                        '<div class="row no-gutters">' +
                            '<div class="col-12 col-lg-6 txt-color">' +
                                '<h5 class="txt-color mobile-fs-14 mb-6 mb-sm-3" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Analysis -</strong>&nbsp;Profit & Loss Ratios </h5>' +
                            '</div>' +
                            '<div class="col-12 col-lg-6 txt-color font-weight-semi-bold mb-sm-3 border-0 annex-text-left  txt-align-right">' +
                                '<a  class="k-hide border-0 mobile-fs-14 bg-view-comments  mt-n1" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                            '</div>' +
                        '</div>' +
                        '<div id="annex-row' + this.scrollNumber + '" style="height: 440px;">' +
                            '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '</div>' +
                        // '</div>' +
                        // '<div class="row">' +
                        //     '<div class="col-12 small text-center my-auto">' +
                        //         '<span class="">Gross Margin</span><br>' +
                        //         '<span class="color-red">Operating Margin</span><br>' +
                        //         '<span class="color-yellow">Net Margin</span>' +
                        //     '</div>' +
                        // '</div>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-3 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' +  
                    '<div class="col-1"></div>' +  
                '</div>';
                $('#annex' + this.scrollNumber).html(html).slidedown;

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Analysis', 'Profit & Loss Ratios');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });

                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

        //Balance Sheet Ratios
            case 'bsr':
                
                descriptionId = 'description' + this.scrollNumber;
                graphId = 'annexure-graph' + this.scrollNumber;

                html = 
                '<div class="row h-100 p-3 no-gutters">' +
                    '<div class="col-1"></div>' +
                    '<div class="col-10 p-3 my-auto">' +
                        '<div class="row no-gutters">' +
                            '<div class="col-12 col-lg-6 txt-color">' +
                                '<h5 class="txt-color mobile-fs-14 mb-6 mb-sm-3" style="display:flex;">' + '<span class="d-none d-lg-block">' + this.scrollNumber + '.&nbsp;</span>' + '<strong>Analysis -</strong>&nbsp;Balance Sheet Ratios </h5>' +
                            '</div>' +
                            '<div class="col-12 col-lg-6 txt-color mb-sm-3 font-weight-semi-bold border-0 annex-text-left txt-align-right">' +
                                '<a  class="k-hide mobile-fs-14 border-0 bg-view-comments  mt-n1" onclick="openComments(' + this.scrollNumber + ')" ><img class="img-fluid comment-icon-size comment-icon-align" src="img/commentBubble.png"> Comments</a>' +
                            '</div>' +
                        '</div>' +
                        '<div id="annex-row' + this.scrollNumber + '" style="height: 440px;">' +
                            '<p id="description' + this.scrollNumber + '" class="font-size-14 mobile-fs-11 txt-color"></p>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-1 d-flex align-items-end flex-column">' +
                        '<a style="visibility: hidden;" id="annex-expander' + this.scrollNumber + '" onclick="expand(' + this.scrollNumber + ', true, false, false)" class="mt-auto mr-3 mb-5 w-25"></a>' +
                    '</div>' +
                '</div>' +
                '<div class="row rounded m-0">' +
                    '<div class="col-1"></div>' + 
                    '<div class="col-10 mb-5" id="annex-comments' + this.scrollNumber + '" style="visibility: hidden;">' + 
                        '<h6 class="txt-color">Comments:</h6>' +
                    '</div>' + 
                    '<div class="col-1"></div>' +  
                '</div>'
                $('#annex' + this.scrollNumber).html(html).slidedown;

                //pdfBox
                this.pdfAnnBox(this.scrollNumber,'Analysis', 'Balance Sheet Ratios');

                description = new Description(this.scrollNumber, this.module, this.data);
                document.getElementById(descriptionId).innerHTML = description.getDescription();

                document.getElementById(pdfDescriptionId).innerHTML = description.getDescription();
                pdfCommentElement = document.getElementById(pdfCommentZone);

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        for(let i = 0; i < result.length; i++){
                            $('#' + commentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        commentElement = document.getElementById(commentZone);
                        if(commentElement){
                            commentElement.style.visibility = 'hidden';
                        }
                    }
                    else{
                        $('#' + commentZone).html('');
                    }
                });

                $.getJSON('json.php?gCom=' + this.module, function(result){
                    if(result.length != 0){
                        $('#' + pdfCommentZone).append('<h6 class="txt-color">Comments:</h6>');
                        for(let i = 0; i < result.length; i++){
                            $('#' + pdfCommentZone).append(' <p class="my-2 pr-3 pl-3 font-size-12 text-left rounded txt-color"><b>' + result[i]['user'] + '</b><br><i class="w-break">"' + result[i]['comment'] + '"</i><span class="float-right my-auto small font-size-10">' + result[i]['date_time'] + '</span></p>');
                        }
                        pdfCommentElement = document.getElementById(pdfCommentZone);
                        // if(pdfCommentElement){
                        //     pdfCommentElement.style.visibility = 'hidden';
                        // }
                    }
                    else{
                        $('#' + pdfCommentZone).html('');
                    }
                });
                
                modal = new Modal();
                modal.setModal(this.scrollNumber, this.data);

            break;

            default:

                console.log('Incorrect module called');
                
            break;

        }
    }
}
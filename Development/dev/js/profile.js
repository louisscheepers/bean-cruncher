$(document).ready(function(){
    fetchProfile();
});

function fetchProfile(){
    var action = "fetch";

    $.ajax({
        url:"json.php",
        method:"POST",
        data:{profile:action},
        success:function(data){
            let obj = jQuery.parseJSON(data);
            document.getElementById('name').value = obj['user_name'];
            document.getElementById('surname').value = obj['user_surname'];
            document.getElementById('number').value = obj['user_number'];
            document.getElementById('email').value = obj['user_email'];
            if(obj['user_img'] == '<img id="user-img" src="data:image/jpeg;base64," class="rounded-circle icon-size" alt="Profile Icon">'){
                document.getElementById('user-icon').innerHTML = '<img id="user-img" src="img/profile-icon-pink.png" class="rounded-circle icon-size" alt="Profile Icon">';
            }
            else{
                document.getElementById('user-icon').innerHTML = obj['user_img'];
            }
        }
    });
}

function updateProfile(){

    var action = "update";

    let name = document.getElementById('name').value;
    let surname = document.getElementById('surname').value;
    let number = document.getElementById('number').value;
    let email = document.getElementById('email').value;

    $.ajax({
        type: 'POST',
        url: 'json.php',
        data: {profile:action,name:name,surname:surname,num:number,email:email},
        success: function(){
            // alert('Profile updated');
        }
    });
}

function readURL(input, isUser) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#user-img')
                .attr('src', e.target.result)
                .width(115)
                .height(115);
            // document.getElementById('user-file').innerHTML = filename;
        
        };

        reader.readAsDataURL(input.files[0]);
    }
}

// function loadCompany(name){
//     document.getElementById('company-settings').hidden = true;
//     document.getElementById('err').hidden = true;    
//     // document.getElementById('loader').innerHTML = '<div id="loader" class="loader"></div>';


//     $.getJSON('json.php?aComp=' + name, function(){});

//     document.getElementById('companyDropDown').innerHTML = name;
//     $.getJSON('json.php?aRole=' + name, function(role){
//         if(role == 3){
//             document.getElementById('err').hidden = false;
//         }
//         else{
//             fetchCompany();
//             document.getElementById('company-settings').hidden = false;
//         }
//     });
//     document.getElementById('companyTitle').innerHTML = name;
// }
/*!
    * Start Bootstrap - SB Admin v6.0.0 (https://startbootstrap.com/templates/sb-admin)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-sb-admin/blob/master/LICENSE)
    */
   (function($) {
    "use strict";

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
    $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function() {
        if (this.href === path) {
            $(this).addClass("active");
        }
    });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
    });
})(jQuery);

$(document).ready(function(){
    
    // $(document).click(function() {
    //     if($(".dropdown-menu").hasClass('show')){
    //         $(".dropdown-menu").removeClass('show');
    //     }
    // });

    isMobile = false;
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
        isMobile = true;
    }

    document.getElementById('pdfDash').hidden = true;
    document.getElementById('pdfAnnexure').hidden = true;
    if($("#updatemodal").length != 0){
        $("#updatemodal").modal("show");
    }
    if($("#first-entry-modal").length != 0){
        $("#first-entry-modal").modal("show");
    }
    
    $.ajax({
        type: "GET",
        url: 'json.php',
        data: {gaComp:true},
        success: function(result){
            // console.log(result);

            if(result != ''){
                let obj = jQuery.parseJSON(result);
                loadCompany(obj['company_name'], obj['company_id'], false, obj['package']);
            }
        },
        complete: function(result){
            setLayout('SESSION_LAYOUT');
        }
    });


});
for(let i = 1; i <= 8; i++){
    $(document).on('click', '#dateDropDown' + i, function (e) {
        e.stopPropagation();
    });

    // $('#dropDownBoxMenu' + i).on('hidden.bs.dropdown', function () {
    //     console.log('hidden');
    //     $('#dateDropDown' + i).prev().dropdown('toggle');
    //     // $('#dateDropDown' + i).dropdown('hide');
    // });
    $('#dropDownBoxMenu' + i).on('show.bs.dropdown', function (e) {
        e.stopPropagation();
        if( $('#dateDropDown' + i).hasClass('show')){
            $('#dateDropDown' + i).removeClass('show');
        }
    });

    document.getElementById("dates" + i).addEventListener('click', function (event) { 
        document.getElementById("dateDropDown"  + i).classList.toggle("show");
        event.stopPropagation(); 
    });
}


function importDataPrompt(){
    let ele = document.getElementsByName('update-interval');
    let interval;
    for(i = 0; i < ele.length; i++) {
        if(ele[i].checked){
            console.log(ele[i].value);
            interval = ele[i].value;
        }
    }
    let time = '';

    document.getElementById('import-modal-body').innerHTML =
    '<img src="img/BC-preloader-gif.gif" class="img-fluid modal-icon-size mb-4">' +
    '<br>' +
    '<b>Data is being imported to dashboard.</b>' +
    '<p class="mt-3">This might take a few mins.<br> We will notify you when it is completed.</p>';
    // '<button type="button" class="mt-3 mb-3 btn btn-cross-platform btn-custom-card-w" data-dismiss="modal">Done</button>';

    importData(interval);
}

function updateWarning(state, name){
    if(state == 0){
        location.reload();
    }
    else if(state == 1){
        location.reload();
    }
}

function importData(interval){
    //update everything
    let action = 'update';
    let mod = 'all';

    $(document).ready(function(){
        $.ajax({
            type: "GET",
            url: 'json.php',
            data: {q:action, mod:mod, int:interval},
            success: function(result){
                // console.log('Finished importing ' + interval + ' months');
                // alert('Finished importing ' + interval + ' months');

                clearBox('0');
                
                $.getJSON('json.php?hasLogged', function(result){
                    document.getElementById('dashboard-content').hidden = false;
                    $('#updatemodal').modal('hide');
                    if(result == 0){
                        console.log('Has logged: ' + result);
                        location.reload();
                    }
                });

            }
        });
    });
}


function sendEmail(pdf){
    $.ajax({
        type: 'GET',
        url: 'json.php',
        data: {data:pdf},
        success: function(result){
            console.log(result);
        }
    });
}

function setLayout(name, isFavourite){

    if(typeof isFavourite == 'undefined'){
        isFavourite = false;
    }
    console.log(isFavourite);

    console.log('Setting layout for: ' + name);

    if(isFavourite){
        console.log(name + ' is a favourite');
        clearBox('0');
    }
    
    let layout;
    $.when(
        $.getJSON('json.php?gLay=' + name, function(result){
            layout = result;
        })
    ).then(function(layout){
        for(let i = 0; i < 8; i++){
            if(layout[i]['module'] != null){
                let boxid = 'box' + (i + 1);
                let module = layout[i]['module'].toString();
                let from = layout[i]['from_date'];
                let to = layout[i]['to_date'];
                displayModule(boxid, module, from, to);
            }
        }
    });
}

function deleteFavourite(name){
    console.log('Deleting favourite: ' + name);
    $.ajax({
        type: 'GET',
        url: 'json.php',
        data: {dLay:name},
        success: function(){
            console.log('Deleted');
            populateFavourites();
        }
    });
}
function saveFavourite(){
    document.getElementById('btnAddFavourite').disabled = true;

    name = document.getElementById('favouriteName').value;
    let uniqueCheck = document.getElementById('favourite-' + name);
    let dataOnDash = false;
    let validCharCount = false;

    if(name.length < 23){
        validCharCount = true;
    }


    for(i = 0; i < 8; i++){
        boxes = document.querySelectorAll('[id^="box'+ (i + 1) +'"]');
        box = boxes[0].id;
        mod = box.substr(4);

        if(mod.length !== 0){
            dataOnDash = true;
        }
    }
    if(!dataOnDash){
        document.getElementById('favourite-error').hidden = false;
        document.getElementById('favourite-error').innerText = 'No data on dashboard';
        document.getElementById('btnAddFavourite').disabled = false;
    }
    else if(!document.getElementById('favouriteName').value){
        document.getElementById('favourite-error').hidden = false;
        document.getElementById('favourite-error').innerText = 'Enter a name';
        document.getElementById('btnAddFavourite').disabled = false;
    }
    else if(uniqueCheck){
        document.getElementById('favourite-error').hidden = false;
        document.getElementById('favourite-error').innerText = 'Name already exists';
        document.getElementById('btnAddFavourite').disabled = false;
        name.value = '';
    }else if(!validCharCount){
        console.log('Exceeded char count');
        document.getElementById('favourite-error').hidden = false;
        document.getElementById('favourite-error').innerText = 'Maximum of 22 chars (' + name.length + ')';
        document.getElementById('btnAddFavourite').disabled = false;
    }
    else{
        modules = new Array();
    
        for(i = 0; i < 8; i++){
            boxes = document.querySelectorAll('[id^="box'+ (i + 1) +'"]');
            box = boxes[0].id;
            mod = box.substr(4);
    
            if(mod.length != 0){

                let fromYearE = document.getElementById('fromY' + (i + 1));
                let fromYear = fromYearE.options[fromYearE.selectedIndex].value;
                let fromMonthE = document.getElementById('fromM' + (i + 1));
                let fromMonth = fromMonthE.options[fromMonthE.selectedIndex].value;

                if(fromMonth.length == 1)
                    fromMonth = '0'+fromMonth;


                let toYearE = document.getElementById('toY' + (i + 1));
                let toYear = toYearE.options[toYearE.selectedIndex].value;
                let toMonthE = document.getElementById('toM' + (i + 1));
                let toMonth = toMonthE.options[toMonthE.selectedIndex].value;
                if(toMonth.length == 1)
                    toMonth = '0'+toMonth;
    
                let fromDate = fromYear + '-' + fromMonth;
                let toDate = toYear + '-' + toMonth;
    
                if(toDate == '' || toDate == null){
                    toDate = '2020-07';
                }

                modules += mod + ',';
                modules += fromDate + ',' + toDate + ',';

            }
            else{
                modules += 0 + ',' + null + ',' + null + ',';
            }
        }
        modules = modules.substring(0, modules.length - 1);
    
        console.log(modules);
        $.ajax({
            type: 'GET',
            url: 'json.php',
            data: {sLay:modules,name:name},
            success: function(){
                document.getElementById('btnAddFavourite').disabled = false;
                $("#favourite-modal").modal("hide");
                document.getElementById('favourite-error').hidden = true;
                populateFavourites();
            }
        });
    }

}

$('#favourite-modal').on('hidden.bs.modal', function () {
    document.getElementById('favouriteName').value = '';
    document.getElementById('favourite-error').hidden = true;
    document.getElementById('btnAddFavourite').disabled = false;
});

$('#email-sent-modal').on('hidden.bs.modal', function () {
    $('#export-modal').modal('hide')
});

function populateFavourites(){

    var select = document.getElementById('favouritesDropDown');
    select.innerHTML = '';

    $.getJSON('json.php?fav=get', function(result){
        if(result != null){
            for(let i = 0; i < result.length; i++){
                console.log(result[i]);
                let name = result[i];
                select.innerHTML +=
                '<div id="favourite-' + name + '" class="container p-0">' +
                    '<div class="row">' +
                        '<div class="col-10">'+
                            '<a class="txt-color font-weight-normal font-size-16" onclick="setLayout(\'' + name + '\', true)">' +
                            '<span class="float-left ml-2 my-2">' + name + '</span></a>' +
                        '</div>' +
                        '<div class="col-2">' +
                            '<a class="btn float-right" onclick="deleteFavourite(\'' + name + '\')">' +
                                '<i class="far fa-trash-alt"></i>' +
                            '</a>' +
                        '</div>' +
                    '</div><hr class="hr-custom">' +
                '</div>';
            }
        }
        select.innerHTML += '<a class="dropdown-item txt-color font-weight-bold font-size-16 my-2" data-toggle="modal" data-target="#favourite-modal"><i class="fas fa-plus"></i> Add</a>';
    });
}

function loadCompany(name, id, clicked, package){

    if(clicked){
        console.log('Loading company ' + name + ' from company switch...');
    }
    else{
        console.log('Loading company ' + name + ' from page reload...');
    }

    document.getElementById('chart-of-accounts').hidden = true;
    
    console.log('Company loaded successfully');
    console.log('1. Company name: ' + name);
    console.log('2. Company ID: ' + id);

    // if(name != activeComp){
        document.getElementById('company-loader').hidden = false;
        modules = new Array();

        if(clicked){
            for(i = 0; i < 8; i++){
                boxes = document.querySelectorAll('[id^="box'+ (i + 1) +'"]');
                box = boxes[0].id;
                mod = box.substr(4);
                modules[i] = mod;
            }
            clearBox('0');
        }
        
        $.when(
            console.log('Doin when= ' + id),
            $.ajax({
                url:"json.php",
                method:"GET",
                data:{aComp:id},
                success:function(data){
                    console.log('Company ID: ' + id);
                    console.log('Company activated')
                    $.getJSON('json.php?aRole=' + null, function(userRole){
                        hideRoleButtons(userRole);
                        checkForUpdate(clicked)
                    })
                }
            })
        ).then(function(){
            let action = 'fetch';
            $.ajax({
                url:"json.php",
                method:"POST",
                data:{company:action, id:id},
                success:function(data){
                    let obj = jQuery.parseJSON(data);

                    console.log(obj);
    
                    console.log('*** Setting company icon ***');
                    if(obj['company_brand'] != '<img id="comp-img" src="data:image/jpeg;base64," class="rounded-circle icon-size" alt="Company Brand">'){
                        console.log('Preset company icon');
                        document.getElementById('comp-icon').innerHTML = obj['company_brand'];
                    }
                    else{
                        console.log('Default company icon');
                        document.getElementById('comp-icon').innerHTML = '<img id="comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Company Brand">';
                    }
                    console.log('*** Finished setting company icon ***');
                    document.getElementById('companyDropDown').innerHTML = name;
                    document.getElementById('company-loader').hidden = true
                }
            });
        });
    // }
}

function checkPermissions(package){
    if(package <= 2 ){

        $("#userpermission").addClass('d-none');
        $("#favspermission").addClass('d-none');
        $("#exportPP").addClass('d-none');
        $("#exportExcell").addClass('d-none');
        $("#favspermission").addClass('d-none');
        $("#favmenupermission").addClass('d-none');
        $("#emailreportpermission").addClass('d-none');
        for(let i = 1; i <= 8; i++){
            $("#com" + i).parent('div').addClass('d-none');
        }
    }
    
    if(package >= 3){

        $("#userpermission").removeClass('d-none');
        $("#favspermission").removeClass('d-none');
        $("#exportPP").removeClass('d-none');
        $("#exportExcell").removeClass('d-none');
        $("#favspermission").removeClass('d-none');
        $("#favmenupermission").removeClass('d-none');
        $("#emailreportpermission").removeClass('d-none');
        $("#com1").removeClass('d-none');
        for(let i = 1; i <= 8; i++){
            $("#com" + i).parent('div').removeClass('d-none');
        }
    }
}
function hideRoleButtons(role){
    console.log('Role: ' + role);
    if(role == 3){
        document.getElementById('chart-of-accounts').hidden = true;
        document.getElementById('user-management').style.visibility = 'hidden';
        document.getElementById('user-management-list-item').style.visibility = 'hidden';
    }
    else{
        document.getElementById('chart-of-accounts').hidden = false;
        document.getElementById('user-management').style.visibility = 'visible';
        document.getElementById('user-management-list-item').style.visibility = 'visible';
    }
}

function checkForUpdate(allowCheck){
    if(allowCheck){
        console.log('Checking for company updates');
    
        $.getJSON('json.php?isCompUpdated=' + name, function(result){
            let isUpdated = result;
            if(isUpdated == '0'){
                console.log('isUpdated: 0');
                updateWarning(isUpdated, name);
            }
            else if(isUpdated == '1'){
                console.log('isUpdated: 1');
                updateWarning(isUpdated, name);
            }
            else{
                console.log('isUpdated: 2');
                document.getElementById('dashboard-content').hidden = false;
    
                for(let i = 0; i < modules.length; i++){
                    let boxid = 'box' + (i + 1);
                    if(modules[i] != ''){
                        displayModule(boxid, modules[i]);
                    }
                }
            }
        });
    }
    else{
        console.log('Skipping company update check');
    }
}

function expand(number, expand, clearBox, showComments){

    let annexure = document.getElementById('annex' + number);
    let expander = document.getElementById('annex-expander' + number);
    let annexRow = document.getElementById('annex-row' + number);
    let comments = document.getElementById('annex-comments' + number);

    if(expand){
        if(expander){
            expander.removeAttribute('onclick');
            expander.setAttribute('onClick', 'expand(' + number + ', false)');
            expander.innerHTML = '<i class="fas fa-angle-double-up"></i>';
        }

        if(annexure){
            annexure.style.removeProperty('height');
            annexure.style.minHeight = '600px';
        }

        if(annexRow){
            annexRow.style.removeProperty('height');
            annexRow.style.removeProperty('overflow-auto');
            annexRow.style.minHeight = '440px';
        }

        if(comments){
            if(showComments){
                comments.style.visibility = 'visible';
            }
        }

        if(!clearBox){
            comments.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
        }
    }
    else{
        if(expander){
            expander.removeAttribute('onclick');
            expander.setAttribute('onClick', 'expand(' + number + ', true)');
            expander.innerHTML = '<i class="fas fa-angle-double-down"></i>';
        }

        if(annexure){
            annexure.style.removeProperty('min-height');
            annexure.style.height = '600px';
        }

        if(annexRow){
            annexRow.style.removeProperty('min-height');
            annexRow.setAttribute = 'overflow-auto';
            annexRow.style.height = '440px';
        }

        if(comments){
            comments.style.visibility = 'hidden';
        }
        if(!clearBox){
            annexure.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
        }
    }


}

function getModuleName(modIndicator){
    let names = {
        'cico' : 'Cash In v Cash Out',
        'ino' : 'Income v Expenses',
        'avd' : 'Asset v Debt',
        'pl' : 'Proft and Loss',
        'bs' : 'Balance Sheet',
        'ar' : 'Accounts Receivable',
        'ddo' : 'Debtors Days Outstanding',
        'tcs' : 'Top Customers by Sales',
        'ret' : 'Retention',
        'ap' : 'Accounts Payable',
        'cdo' : 'Creditors Days Outstanding',
        'tsp' : 'Top Suppliers by Balance Outstanding',
        'im' : 'Inventory Management',
        'inas' : 'Inventory Analysis - Sales',
        'inaq' : 'Inventory Analysis - Quantity',
        'plr' : 'Profit and Loss Ratios',
        'bsr' : 'Balance Sheet Ratios'
    };
    return names[modIndicator];
}
function calculateVarPercentage(m2, m1, upIsGreen, round){
    let per;
    let upColor;
    let downColor;

    if(upIsGreen){
        upColor = 'green';
        downColor = 'red';
    }
    else{
        upColor = 'red';
        downColor = 'green';
    }

    if(m1 == 0 && m2 != 0){
        per = '100%<img src="img/' + upColor + '-arrow-up.png" class="mx-2" style="width: 16px;">';
    }
    else if(m1 == m2){
        per = '<span style="padding-right:34px">0%</span>';
    }
    else if(m1 == 0 && m2 == 0){
        per = '<span style="padding-right:34px">0%</span>';
    }
    else if(m1 == null && m2 == null){
        per = '<span style="padding-right:34px">0%</span>';
    }
    else{
        if(typeof round === 'undefined'){
            per = Math.round((m2 - m1) / m1 * 100);
        }
        else{
            per = ((m2 - m1) / m1 * 100).toFixed(round);
        }
        if(per < 1 && per > 0){
            per = '<1%<img src="img/' + upColor + '-arrow-up.png" class="mx-2" style="width: 16px;">';
        }
        else if(per > 999){
            per = '>999%<img src="img/' + upColor + '-arrow-up.png" class="mx-2" style="width: 16px;">';
        }
        else if(per < -999){
            per = '<-999%<img src="img/' + downColor + '-arrow-down.png" class="mx-2" style="width: 16px;">';
        }
        else if(per > 0){
            per = per + '%<img src="img/' + upColor + '-arrow-up.png" class="mx-2" style="width: 16px;">';
        }
        else if(per < 0){
            per = '(' + Math.abs(per) + '%)<img src="img/' + downColor + '-arrow-down.png" class="mx-2" style="width: 16px;">';
        }
        else if(per == 0 && m1 != m2){
            per = '<span style="padding-right:34px">0%</span>';
        }
    }
    return per;
}

function calculateVar(m2, m1, upIsGreen, currency, round, currencyToLeft){
    let upColor;
    let downColor;

    if(typeof currency === 'undefined'){
        currency = 'R ';
    }

    if(upIsGreen){
        upColor = 'green';
        downColor = 'red';
    }
    else{
        upColor = 'red';
        downColor = 'green';
    }

    let variance = m2 - m1;
    if(m1 != null && m2 != null){
        if(variance > 0){
            per = formatCurrency(variance, currency, round, currencyToLeft) + '<img src="img/' + upColor + '-arrow-up.png" class="mx-2" style="width: 16px;">';
            return per;
        }
        else if(variance < 0){
            per = '(' + formatCurrency(-variance, currency, round, currencyToLeft) + ')<img src="img/' + downColor + '-arrow-down.png" class="mx-2" style="width: 16px;">';
            return per;
        }
        else{
            return '<span style="padding-right:34px">' + formatCurrency(variance, currency, round, currencyToLeft) + '</span>';
        }
    }
    else{
        return '<span style="padding-right:34px">' + currency + ' 0</span>';
    }
}

function formatCurrency(item, currency, round, currencyToLeft){

    if(typeof currencyToLeft == 'undefined'){
        currencyToLeft = true;
    }
    
    let amount;

    if(typeof round === 'undefined'){
        amount = parseInt(item);
    }
    else{
        amount = parseFloat(item).toFixed(round);
    }

    if(typeof currency === 'undefined'){
        currency = '';
    }

    if(currencyToLeft){
        if(amount < 0){
            return '(' + currency + String(Math.abs(amount)).replace(/(.)(?=(\d{3})+$)/g,'$1 ') + ')';
        }
        else{
            return currency + String(amount).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
        }
    }
    return  String(amount).replace(/(.)(?=(\d{3})+$)/g,'$1 ') + currency;
}

function formatDateRange(dates){
    // console.log(dates);
    let formattedDates = new Array();

    for(let i = 0; i < dates.length; i++){
        if(i == 0 || i == (dates.length - 1)){
            formattedDates[i] = dates[i];
        }
        else{
            let year = dates[i].substr(dates[i].length - 4);
            let lastMonthYear = dates[i-1].substr(dates[i].length - 4);

            if(year != lastMonthYear){
                formattedDates[i] = dates[i];
            }
            else{
                formattedDates[i] = dates[i].substring(0, dates[i].length - 4);
            }
        }

        formattedDates[i] = formattedDates[i].trim();
    }
    return formattedDates;
}

/* Mobile Side NavBar responsiveness */ 
function openNav() {
    var element = document.getElementById("layoutSidenav_nav");
    element.classList.toggle("w-double");
  
    let menuids = ['overview', 'finman', 'customers', 'suppliers', 'inventory', 'analysis', 'cSettings'];
  
    for (let i = 0; i < menuids.length; i++){
      document.getElementById(menuids[i]).classList.toggle("fs-14");
    }
}
  
let shouldCollapse = true;

$("#layoutSidenav_nav").on("click", "a", function() {
    shouldCollapse = false;
    if(!$("#layoutSidenav_nav").hasClass("w-double")){
        openNav();
    } 
});
  
$("#layoutSidenav_nav").on("click", function() {
    if(shouldCollapse){
        openNav();
        collapseMenus();
    }
  
  
    shouldCollapse = true;
});

function collapseMenus(){
    let collapseids = ['collapseOverview', 'collapseFinancialManagement', 'collapseCustomers', 'collapseSuppliers', 'collapseInventory', 'collapseAnalysis', 'collapseSettings'];

    for (let i = 0; i < collapseids.length; i++){
        let isExpanded = $("#" + collapseids[i]).hasClass('show');
        console.log(isExpanded);
        if (isExpanded){
            $("#" + collapseids[i]).removeClass('show');
            console.log(collapseids[i] + "IsOpen Closing");
        }
    }
}

/* Top Nav Settings Tooltip */ 
function tooltipExp() {
    var expTooltip = document.getElementById("exportTooltip");
    if (expTooltip.style.display === "none") {
        expTooltip.style.display = "block";
    } else {
        expTooltip.style.display = "none";
    }
}

function tooltipFav() {
    var favTooltip = document.getElementById("favouritesTooltip");
    if (favTooltip.style.display === "none") {
        favTooltip.style.display = "block";
    } else {
        favTooltip.style.display = "none";
    }
}

function onExpTooltipMouseOver() {
    document.getElementById('exportTooltip').style.display='block';
}

function onExpTooltipMouseOutHover() {
    document.getElementById('exportTooltip').style.display='none';
}

function onFavTooltipMouseOver() {
    document.getElementById('favouritesTooltip').style.display='block';
}

function onFavTooltipMouseOutHover() {
    document.getElementById('favouritesTooltip').style.display='none';
}

/* End of Top Nav Settings Tooltip */

/* Sosatie Menu Tooltip */ 

function tooltipCalender1() {
    var expTooltip = document.getElementById("calenderTooltip1");
    if (expTooltip.style.display === "none") {
        expTooltip.style.display = "block";
    } else {
        expTooltip.style.display = "none";
    }
}

function tooltipCalender2() {
    var expTooltip = document.getElementById("calenderTooltip2");
    if (expTooltip.style.display === "none") {
        expTooltip.style.display = "block";
    } else {
        expTooltip.style.display = "none";
    }
}

function tooltipCalender3() {
    var expTooltip = document.getElementById("calenderTooltip3");
    if (expTooltip.style.display === "none") {
        expTooltip.style.display = "block";
    } else {
        expTooltip.style.display = "none";
    }
}

function tooltipCalender4() {
    var expTooltip = document.getElementById("calenderTooltip4");
    if (expTooltip.style.display === "none") {
        expTooltip.style.display = "block";
    } else {
        expTooltip.style.display = "none";
    }
}

function tooltipCalender5() {
    var expTooltip = document.getElementById("calenderTooltip5");
    if (expTooltip.style.display === "none") {
        expTooltip.style.display = "block";
    } else {
        expTooltip.style.display = "none";
    }
}

function tooltipCalender6() {
    var expTooltip = document.getElementById("calenderTooltip6");
    if (expTooltip.style.display === "none") {
        expTooltip.style.display = "block";
    } else {
        expTooltip.style.display = "none";
    }
}

function tooltipCalender7() {
    var expTooltip = document.getElementById("calenderTooltip7");
    if (expTooltip.style.display === "none") {
        expTooltip.style.display = "block";
    } else {
        expTooltip.style.display = "none";
    }
}

function tooltipCalender8() {
    var expTooltip = document.getElementById("calenderTooltip8");
    if (expTooltip.style.display === "none") {
        expTooltip.style.display = "block";
    } else {
        expTooltip.style.display = "none";
    }
}

function onCalenderTooltipMouseOver() {
    for(let i = 1; i <= 8; i++){
        document.getElementById('calenderTooltip' + i).style.display='block';
    }
}

function onCalenderTooltipMouseOutHover() {
    for(let i = 1; i <= 8; i++){
        document.getElementById('calenderTooltip' + i).style.display='none';
    }
}

function onMenuTooltipMouseOver() {
    for(let i = 1; i <= 8; i++){
        document.getElementById('commentTooltip' + i).style.display='block';
        document.getElementById('refreshTooltip' + i).style.display='block';
        document.getElementById('popupTooltip' + i).style.display='block';
        document.getElementById('annexTooltip' + i).style.display='block';
    }
}

function onMenuTooltipMouseOutHover() {
    for(let i = 1; i <= 8; i++){
        document.getElementById('commentTooltip' + i).style.display='none';
        document.getElementById('refreshTooltip' + i).style.display='none';
        document.getElementById('popupTooltip' + i).style.display='none';
        document.getElementById('annexTooltip' + i).style.display='none';
    }
}

/* End of Sosatie Menu Tooltip */
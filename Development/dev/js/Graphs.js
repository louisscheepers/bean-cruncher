class Graphs{

    xAxis;
    yAxis;

    constructor(labelx, seriesData, seriesData2, seriesData3, seriesData4, boxid){
        this.labelx = labelx;
        this.seriesData = seriesData;
        this.seriesData2 = seriesData2;
        this.seriesData3 = seriesData3;
        this.seriesData4 = seriesData4;
        this.boxid = boxid;
    }

    // ***
    // Draw and animate graphs in boxid
    // ***

    // setID(){
    //     if(this.isDoubleGraph){
    //         this.boxid = this.boxid + 'Overlap';
    //     }
    // }

    // Single series bar chart
    setBarGraph(){
        var chart = new Chartist.Bar('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData
            ]
        }
        );
        chart.on('draw', function(data) {
            if(data.type == 'bar') {
                data.element.animate({
                    y2: {
                        dur: '0.7s',
                        from: data.y1,
                        to: data.y2
                    }
                });
            }
        });
    }

    // Double series bar chart
    setDoubleBarGraph(){
        var chart = new Chartist.Bar('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData, this.seriesData2
            ]
        });
        chart.on('draw', function(data) {
            if(data.type == 'bar') {
                data.element.animate({
                    y2: {
                        dur: '0.7s',
                        from: data.y1,
                        to: data.y2
                    }
                });
            }
        });
    }

    // Triple series bar chart
    setTripleBarGraph(){
        var chart = new Chartist.Bar('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData, this.seriesData2, this.seriesData3
            ]
        });
        chart.on('draw', function(data) {
            if(data.type == 'bar') {
                data.element.animate({
                    y2: {
                        dur: '0.7s',
                        from: data.y1,
                        to: data.y2
                    }
                });
            }
        });
    }

    // Double horizontal series bar chart
    setHorizontalBarGraph(){
        new Chartist.Bar('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData
            ]
        }, {
            seriesBarDistance: 10,
            reverseData: true,
            horizontalBars: true,
            axisY: {
            offset: 70
            }
        });
    }

    // Pie chart
    setPieGraph(){
        let lbAD = ['Assets', 'Debt'];

        new Chartist.Pie('#' + this.boxid, {
            labels: this.labelx,
            series: this.seriesData
          }, {
            donut: true,
            donutWidth: 30,
            donutSolid: true,
            startAngle: 270,
            showLabel: true
          });
          //animation stuffs
    //       chart.on('draw', function(data) {
    //         if(data.type === 'slice') {
    //           // Get the total path length in order to use for dash array animation
    //           var pathLength = data.element._node.getTotalLength();
          
    //           // Set a dasharray that matches the path length as prerequisite to animate dashoffset
    //           data.element.attr({
    //             'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
    //           });
          
    //           // Create animation definition while also assigning an ID to the animation for later sync usage
    //           var animationDefinition = {
    //             'stroke-dashoffset': {
    //               id: 'anim' + data.index,
    //               dur: 1000,
    //               from: -pathLength + 'px',
    //               to:  '0px',
    //               easing: Chartist.Svg.Easing.easeOutQuint,
    //               // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
    //               fill: 'freeze'
    //             }
    //           };
          
    //           // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
    //           if(data.index !== 0) {
    //             animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
    //           }
          
    //           // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
    //           data.element.attr({
    //             'stroke-dashoffset': -pathLength + 'px'
    //           });
          
    //           // We can't use guided mode as the animations need to rely on setting begin manually
    //           // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
    //           data.element.animate(animationDefinition, false);
    //         }
    //       });
    }

    //Double series line chart
    setDoubleLineGraph(){
        var chart = new Chartist.Line('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData, this.seriesData2
            ]
        }, {
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 2
            }),
            // high: 2,
            // low: 0,
            // showLine: false,
            // showArea: true,
            showPoint: true,
            fullWidth: true,
            chartPadding: {
                right: 40,
            }
        });

        

        // Let's put a sequence number aside so we can use it in the event callbacks
        var seq = 0,
        delays = 50,
        durations = 800;

        // Once the chart is fully created we reset the sequence
        chart.on('created', function() {
        seq = 0;
        });

            // On each drawn element by Chartist we use the Chartist.Svg API to trigger SMIL animations
            chart.on('draw', function(data) {
            seq++;

            if(data.type === 'line') {
                // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
                data.element.animate({
                opacity: {
                    // The delay when we like to start the animation
                    begin: seq * delays + 400,
                    // Duration of the animation
                    dur: durations,
                    // The value where the animation should start
                    from: 0,
                    // The value where it should end
                    to: 1
                }
                });
            } else if(data.type === 'label' && data.axis === 'x') {
                data.element.animate({
                y: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.y + 100,
                    to: data.y,
                    // We can specify an easing function from Chartist.Svg.Easing
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'label' && data.axis === 'y') {
                data.element.animate({
                x: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 100,
                    to: data.x,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'point') {
                data.element.animate({
                x1: {
                    begin: seq * delays,
                    dur: durations - (durations * 0.2),
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                x2: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                opacity: {
                    begin: seq * delays,
                    dur: durations,
                    from: 0,
                    to: 1,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'grid') {
                // Using data.axis we get x or y which we can use to construct our animation definition objects
                var pos1Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '1'] - 30,
                to: data[data.axis.units.pos + '1'],
                easing: 'easeOutQuart'
                };

                var pos2Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '2'] - 100,
                to: data[data.axis.units.pos + '2'],
                easing: 'easeOutQuart'
                };

                var animations = {};
                animations[data.axis.units.pos + '1'] = pos1Animation;
                animations[data.axis.units.pos + '2'] = pos2Animation;
                animations['opacity'] = {
                begin: seq * delays,
                dur: durations,
                from: 0,
                to: 1,
                easing: 'easeOutQuart'
                };

                data.element.animate(animations);
            }
        });
    }

    //Triple series line chart
    setTripleLineGraph(){
        var chart = new Chartist.Line('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData, this.seriesData2, this.seriesData3
            ]
        }, {
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 2
            }),
            // high: 2,
            // low: 0,
            // showLine: false,
            // showArea: true,
            showPoint: true,
            fullWidth: true,
            chartPadding: {
                right: 40,
            }
        });

        

        // Let's put a sequence number aside so we can use it in the event callbacks
        var seq = 0,
        delays = 50,
        durations = 800;

        // Once the chart is fully created we reset the sequence
        chart.on('created', function() {
        seq = 0;
        });

            // On each drawn element by Chartist we use the Chartist.Svg API to trigger SMIL animations
            chart.on('draw', function(data) {
            seq++;

            if(data.type === 'line') {
                // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
                data.element.animate({
                opacity: {
                    // The delay when we like to start the animation
                    begin: seq * delays + 400,
                    // Duration of the animation
                    dur: durations,
                    // The value where the animation should start
                    from: 0,
                    // The value where it should end
                    to: 1
                }
                });
            } else if(data.type === 'label' && data.axis === 'x') {
                data.element.animate({
                y: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.y + 100,
                    to: data.y,
                    // We can specify an easing function from Chartist.Svg.Easing
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'label' && data.axis === 'y') {
                data.element.animate({
                x: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 100,
                    to: data.x,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'point') {
                data.element.animate({
                x1: {
                    begin: seq * delays,
                    dur: durations - (durations * 0.2),
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                x2: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                opacity: {
                    begin: seq * delays,
                    dur: durations,
                    from: 0,
                    to: 1,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'grid') {
                // Using data.axis we get x or y which we can use to construct our animation definition objects
                var pos1Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '1'] - 30,
                to: data[data.axis.units.pos + '1'],
                easing: 'easeOutQuart'
                };

                var pos2Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '2'] - 100,
                to: data[data.axis.units.pos + '2'],
                easing: 'easeOutQuart'
                };

                var animations = {};
                animations[data.axis.units.pos + '1'] = pos1Animation;
                animations[data.axis.units.pos + '2'] = pos2Animation;
                animations['opacity'] = {
                begin: seq * delays,
                dur: durations,
                from: 0,
                to: 1,
                easing: 'easeOutQuart'
                };

                data.element.animate(animations);
            }
        });
    }

    // Single series line chart
    setSingleLineGraph(){
        var chart = new Chartist.Line('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData
            ]
        }, {
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 2
            }),
            // high: 2,
            // low: 0,
            // showLine: false,
            // showArea: true,
            showPoint: true,
            fullWidth: true,
            chartPadding: {
                right: 40,
            }
        });


        // Let's put a sequence number aside so we can use it in the event callbacks
        var seq = 0,
        delays = 50,
        durations = 800;

        // Once the chart is fully created we reset the sequence
        chart.on('created', function() {
        seq = 0;
        });

            // On each drawn element by Chartist we use the Chartist.Svg API to trigger SMIL animations
            chart.on('draw', function(data) {
            seq++;

            if(data.type === 'line') {
                // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
                data.element.animate({
                opacity: {
                    // The delay when we like to start the animation
                    begin: seq * delays + 400,
                    // Duration of the animation
                    dur: durations,
                    // The value where the animation should start
                    from: 0,
                    // The value where it should end
                    to: 1
                }
                });
            } else if(data.type === 'label' && data.axis === 'x') {
                data.element.animate({
                y: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.y + 100,
                    to: data.y,
                    // We can specify an easing function from Chartist.Svg.Easing
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'label' && data.axis === 'y') {
                data.element.animate({
                x: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 100,
                    to: data.x,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'point') {
                data.element.animate({
                x1: {
                    begin: seq * delays,
                    dur: durations - (durations * 0.2),
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                x2: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                opacity: {
                    begin: seq * delays,
                    dur: durations,
                    from: 0,
                    to: 1,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'grid') {
                // Using data.axis we get x or y which we can use to construct our animation definition objects
                var pos1Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '1'] - 30,
                to: data[data.axis.units.pos + '1'],
                easing: 'easeOutQuart'
                };

                var pos2Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '2'] - 100,
                to: data[data.axis.units.pos + '2'],
                easing: 'easeOutQuart'
                };

                var animations = {};
                animations[data.axis.units.pos + '1'] = pos1Animation;
                animations[data.axis.units.pos + '2'] = pos2Animation;
                animations['opacity'] = {
                begin: seq * delays,
                dur: durations,
                from: 0,
                to: 1,
                easing: 'easeOutQuart'
                };

                data.element.animate(animations);
            }
        });
    }

    setQuadrupleLineGraph(){
        var chart = new Chartist.Line('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData, this.seriesData2, this.seriesData3, this.seriesData4
            ]
        }, {
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 2
            }),
            // high: 2,
            // low: 0,
            // showLine: false,
            // showArea: true,
            showPoint: true,
            fullWidth: true,
            chartPadding: {
                right: 40,
            }
        });


        // Let's put a sequence number aside so we can use it in the event callbacks
        var seq = 0,
        delays = 50,
        durations = 800;

        // Once the chart is fully created we reset the sequence
        chart.on('created', function() {
        seq = 0;
        });

            // On each drawn element by Chartist we use the Chartist.Svg API to trigger SMIL animations
            chart.on('draw', function(data) {
            seq++;

            if(data.type === 'line') {
                // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
                data.element.animate({
                opacity: {
                    // The delay when we like to start the animation
                    begin: seq * delays + 400,
                    // Duration of the animation
                    dur: durations,
                    // The value where the animation should start
                    from: 0,
                    // The value where it should end
                    to: 1
                }
                });
            } else if(data.type === 'label' && data.axis === 'x') {
                data.element.animate({
                y: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.y + 100,
                    to: data.y,
                    // We can specify an easing function from Chartist.Svg.Easing
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'label' && data.axis === 'y') {
                data.element.animate({
                x: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 100,
                    to: data.x,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'point') {
                data.element.animate({
                x1: {
                    begin: seq * delays,
                    dur: durations - (durations * 0.2),
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                x2: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                opacity: {
                    begin: seq * delays,
                    dur: durations,
                    from: 0,
                    to: 1,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'grid') {
                // Using data.axis we get x or y which we can use to construct our animation definition objects
                var pos1Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '1'] - 30,
                to: data[data.axis.units.pos + '1'],
                easing: 'easeOutQuart'
                };

                var pos2Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '2'] - 100,
                to: data[data.axis.units.pos + '2'],
                easing: 'easeOutQuart'
                };

                var animations = {};
                animations[data.axis.units.pos + '1'] = pos1Animation;
                animations[data.axis.units.pos + '2'] = pos2Animation;
                animations['opacity'] = {
                begin: seq * delays,
                dur: durations,
                from: 0,
                to: 1,
                easing: 'easeOutQuart'
                };

                data.element.animate(animations);
            }
        });
    }

    // Single lapping series line chart
    setLappingSingleLineGraph(){
        var chart = new Chartist.Line('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData
            ]
        }, {
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 2
            }),
            // high: 2,
            // low: 0,
            // showLine: false,
            showArea: false,
            showPoint: false,
            fullWidth: true,
            chartPadding: {
                right: 40,
            },
            axisX: {
                showGrid: false, 
                showLabel: false
            },
            axisY: {
                showGrid: false, 
                showLabel: false
            }
        });


        // Let's put a sequence number aside so we can use it in the event callbacks
        var seq = 0,
        delays = 50,
        durations = 800;

        // Once the chart is fully created we reset the sequence
        chart.on('created', function() {
        seq = 0;
        });

            // On each drawn element by Chartist we use the Chartist.Svg API to trigger SMIL animations
            chart.on('draw', function(data) {
            seq++;

            if(data.type === 'line') {
                // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
                data.element.animate({
                opacity: {
                    // The delay when we like to start the animation
                    begin: seq * delays + 400,
                    // Duration of the animation
                    dur: durations,
                    // The value where the animation should start
                    from: 0,
                    // The value where it should end
                    to: 1
                }
                });
            } else if(data.type === 'label' && data.axis === 'x') {
                data.element.animate({
                y: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.y + 100,
                    to: data.y,
                    // We can specify an easing function from Chartist.Svg.Easing
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'label' && data.axis === 'y') {
                data.element.animate({
                x: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 100,
                    to: data.x,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'point') {
                data.element.animate({
                x1: {
                    begin: seq * delays,
                    dur: durations - (durations * 0.2),
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                x2: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                opacity: {
                    begin: seq * delays,
                    dur: durations,
                    from: 0,
                    to: 1,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'grid') {
                // Using data.axis we get x or y which we can use to construct our animation definition objects
                var pos1Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '1'] - 30,
                to: data[data.axis.units.pos + '1'],
                easing: 'easeOutQuart'
                };

                var pos2Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '2'] - 100,
                to: data[data.axis.units.pos + '2'],
                easing: 'easeOutQuart'
                };

                var animations = {};
                animations[data.axis.units.pos + '1'] = pos1Animation;
                animations[data.axis.units.pos + '2'] = pos2Animation;
                animations['opacity'] = {
                begin: seq * delays,
                dur: durations,
                from: 0,
                to: 1,
                easing: 'easeOutQuart'
                };

                data.element.animate(animations);
            }
        });
    }

    // Tripple lapping series line chart
    setLappingTrippleLineGraph(){
        var chart = new Chartist.Line('#' + this.boxid, {
            labels: this.labelx,
            series: [
                this.seriesData, this.seriesData2, this.seriesData3
            ]
        }, {
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 2
            }),
            // high: 2,
            // low: 0,
            // showLine: false,
            showArea: false,
            showPoint: false,
            fullWidth: true,
            chartPadding: {
                right: 40,
            },
            axisX: {
                showGrid: false, 
                showLabel: false
            },
            axisY: {
                showGrid: false, 
                showLabel: false
            }
        });


        // Let's put a sequence number aside so we can use it in the event callbacks
        var seq = 0,
        delays = 50,
        durations = 800;

        // Once the chart is fully created we reset the sequence
        chart.on('created', function() {
        seq = 0;
        });

            // On each drawn element by Chartist we use the Chartist.Svg API to trigger SMIL animations
            chart.on('draw', function(data) {
            seq++;

            if(data.type === 'line') {
                // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
                data.element.animate({
                opacity: {
                    // The delay when we like to start the animation
                    begin: seq * delays + 400,
                    // Duration of the animation
                    dur: durations,
                    // The value where the animation should start
                    from: 0,
                    // The value where it should end
                    to: 1
                }
                });
            } else if(data.type === 'label' && data.axis === 'x') {
                data.element.animate({
                y: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.y + 100,
                    to: data.y,
                    // We can specify an easing function from Chartist.Svg.Easing
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'label' && data.axis === 'y') {
                data.element.animate({
                x: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 100,
                    to: data.x,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'point') {
                data.element.animate({
                x1: {
                    begin: seq * delays,
                    dur: durations - (durations * 0.2),
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                x2: {
                    begin: seq * delays,
                    dur: durations,
                    from: data.x - 10,
                    to: data.x,
                    easing: 'easeOutQuart'
                },
                opacity: {
                    begin: seq * delays,
                    dur: durations,
                    from: 0,
                    to: 1,
                    easing: 'easeOutQuart'
                }
                });
            } else if(data.type === 'grid') {
                // Using data.axis we get x or y which we can use to construct our animation definition objects
                var pos1Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '1'] - 30,
                to: data[data.axis.units.pos + '1'],
                easing: 'easeOutQuart'
                };

                var pos2Animation = {
                begin: seq * delays,
                dur: durations,
                from: data[data.axis.units.pos + '2'] - 100,
                to: data[data.axis.units.pos + '2'],
                easing: 'easeOutQuart'
                };

                var animations = {};
                animations[data.axis.units.pos + '1'] = pos1Animation;
                animations[data.axis.units.pos + '2'] = pos2Animation;
                animations['opacity'] = {
                begin: seq * delays,
                dur: durations,
                from: 0,
                to: 1,
                easing: 'easeOutQuart'
                };

                data.element.animate(animations);
            }
        });
    }

}
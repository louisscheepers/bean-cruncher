class Description{

    constructor(number, module, data){
        this.number = number;
        this.module = module;
        this.data = data;
        if(typeof this.data.labels != 'undefined'){
            this.fromDate = this.data.labels[0];
            this.toDate = this.data.labels[this.data.labels.length-1];
        }

        // console.log(this.data.data);
    }
    
    getDescription(){
        let description;

        //indexes
        let a, b, c, d, e;

        switch(this.module){
        //Cash In VS Cash Out
            case 'cico':

                let cash_in = new Array();
                let cash_out = new Array();
                let net_cash = new Array();

                for(let i = 0; i < this.data.data['cash_in'].length; i++){
                    cash_in[i] = parseInt(this.data.data['cash_in'][i]);
                    cash_out[i] = parseInt(this.data.data['cash_out'][i]);
                    net_cash[i] = cash_in[i] - cash_out[i];
                }

                let sumCashIn = cash_in.reduce((a,b) => a + b, 0);
                let sumCashOut = cash_out.reduce((a,b) => a + b, 0);
                let sumNetCash = net_cash.reduce((a,b) => a + b, 0);

                let maxCashIn = Math.max.apply(null, cash_in);
                let maxCashOut = Math.max.apply(null, cash_out);
                let maxNetCash = Math.max.apply(null, net_cash);
                
                a = cash_in.indexOf(maxCashIn);
                let monthMaxCashIn = this.data.labels[a];
                b = cash_out.indexOf(maxCashOut);
                let monthMaxCashOut = this.data.labels[b];
                c = cash_out.indexOf(maxNetCash);
                let monthMaxNetCash = this.data.labels[c];

                let minCashIn = Math.min.apply(null, cash_in);
                let minCashOut = Math.min.apply(null, cash_out);

                d = cash_in.indexOf(minCashIn);
                let monthMinCashIn = this.data.labels[d];
                e = cash_out.indexOf(minCashOut);
                let monthMinCashOut = this.data.labels[e];

                let avgCashIn = cash_in.reduce((a,b) => a + b, 0) / cash_in.length;
                let avgCashOut = cash_out.reduce((a,b) => a + b, 0) / cash_out.length;
                let avgNetCash = net_cash.reduce((a,b) => a + b, 0) / net_cash.length;

                description = '<p>Cash received for the period ' + this.fromDate + ' to ' + this.toDate + ' was ' + formatCurrency(sumCashIn, 'R ') + '. Average cash received per month was ' + formatCurrency(avgCashIn, 'R ') + '. </p>' + 
                '<p>The most cash was received in ' + monthMaxCashIn + ' (cash received of ' + formatCurrency(maxCashIn, 'R ') + ') while the least cash received was in ' + monthMinCashIn + ' (cash received of ' + formatCurrency(minCashIn, 'R ') + ').</p>' + 
                '<p>Cash paid for the period ' + this.fromDate + ' to ' + this.toDate + ' was ' + formatCurrency(sumCashOut, 'R ') + '. Average cash paid per month was ' + formatCurrency(avgCashOut, 'R ') + '. </p>' + 
                '<p>The most cash paid was in ' + monthMaxCashOut + ' (cash paid of ' + formatCurrency(maxCashOut, 'R ') + ') while the least cash paid was in ' + monthMinCashOut + ' (cash paid of ' + formatCurrency(minCashOut, 'R ') + ').</p>' + 
                '<p>Net cash for the period ' + this.fromDate + ' to ' + this.toDate + ' was ' + formatCurrency(sumNetCash, 'R ') + '. Average net cash per month was ' + formatCurrency(avgNetCash, 'R ') + '.</p>' + 
                '<p>The best performing month was ' + monthMaxNetCash + ' (net cash of ' + formatCurrency(maxNetCash, 'R ') + ').</p>';

            break;

        //Income v Expenses
            case 'ino' :

                let income = new Array();
                let expenses = new Array();
                let profit = new Array();

                for(let i = 0; i < this.data.data['income'].length; i++){
                    income[i] = parseInt(this.data.data['income'][i]);
                    expenses[i] = parseInt(this.data.data['expenses'][i]);
                    profit[i] = income[i] - expenses[i];
                }

                let sumProfit = income.reduce((a,b) => a + b, 0);
                let sumExpenses = expenses.reduce((a,b) => a + b, 0);
                let sumNetProfit = profit.reduce((a,b) => a + b, 0);

                let maxProfit = Math.max.apply(null, income);
                let maxExpenses = Math.max.apply(null, expenses);
                let maxNetProfit = Math.max.apply(null, profit);
                
                a = income.indexOf(maxProfit);
                let monthMaxProfit = this.data.labels[a];
                b = expenses.indexOf(maxExpenses);
                let monthMaxExpenses = this.data.labels[b];
                c = expenses.indexOf(maxNetProfit);
                let monthMaxNetProfit = this.data.labels[c];

                let minProfit = Math.min.apply(null, income);
                let minExpenses = Math.min.apply(null, expenses);

                d = income.indexOf(minProfit);
                let monthMinProfit = this.data.labels[d];
                e = expenses.indexOf(minExpenses);
                let monthMinExpenses = this.data.labels[e];

                let avgProfit = income.reduce((a,b) => a + b, 0) / income.length;
                let avgExpenses = expenses.reduce((a,b) => a + b, 0) / expenses.length;
                let avgNetProfit = profit.reduce((a,b) => a + b, 0) / profit.length;

                description = '<p>Income for the period ' + this.fromDate + ' to ' + this.toDate + ' was ' + formatCurrency(sumProfit, 'R ') + '. Average income per month was ' + formatCurrency(avgProfit, 'R ') + '.</p>' + 
                '<p>The best performing month was ' + monthMaxProfit + ' (income of ' + formatCurrency(maxProfit, 'R ') + ') while the worst performing months were ' + monthMinProfit + ' (income of ' + formatCurrency(minProfit, 'R ') + ').</p>' + 
                '<p>Expenses for the period ' + this.fromDate + ' to ' + this.toDate  + ' were ' + formatCurrency(sumExpenses, 'R ') + '. Average expenses per month were ' + formatCurrency(avgExpenses, 'R ') + '.</p>' + 
                '<p>The most expenses were incurred in ' + monthMaxExpenses + ' (expenses of ' + formatCurrency(maxExpenses, 'R ') + ') while the least expenses were incurred in ' + monthMinExpenses + ' (expenses of ' + formatCurrency(minExpenses, 'R ') + ').</p>' + 
                '<p>Profit for the period ' + this.fromDate + ' to ' + this.toDate + ' was ' + formatCurrency(sumNetProfit, 'R ') + '. Average profit per month was ' + formatCurrency(avgNetProfit, 'R ') + '.</p>' + 
                '<p>The best performing month was ' + monthMaxNetProfit + ' (profit of ' + formatCurrency(maxNetProfit, 'R ') + ').</p>';

            break;
                
        //Asset v Debt
            case 'avd':

                let assets = this.data.data['assets'];
                let debt = this.data.data['debt'];
                let netAssetDebt = assets - debt;

                description = '<p style="font-size: 15px;">Assets for the month ' + this.fromDate + ' totals to an amount of ' + formatCurrency(assets, 'R ') + '.</p>' + 
                '<p style="font-size: 15px;">Liabilities for the month ' + this.fromDate + ' totals to an amount of ' + formatCurrency(debt, 'R ') + '.</p>' + 
                '<p style="font-size: 15px;">The net asset value for the month ' + this.fromDate + ' totals to an amount of ' + formatCurrency(netAssetDebt, 'R ') + '.</p>';
            
            break;
        
        //Profit & Loss
            case 'pl':
                
                let sales_tr = '';
                let total_sales_tr = '';
                let cost_of_sales_tr = '';
                let total_cost_of_sales_tr = '';
                let gross_profit_tr = '';
                let other_income_tr = '';
                let total_other_income_tr = '';
                let expenses_tr = '';
                let total_expenses_tr = '';
                let net_profit_before_tax_tr = '';
                let tax_tr = '';
                let total_tax_tr = '';
                let net_profit_after_tax_tr = '';
              
                //Sales
                for(let i = 0; i < this.data.data['sales'].length; i++){
                    
                    let salesFrom = this.data.data['sales'][i]['amount'][0];
                    let salesTo = this.data.data['sales'][i]['amount'][1];
                    let salesYtd = this.data.data['sales'][i]['ytd'];

                    if(salesFrom != 0 || salesTo != 0 || salesYtd != 0){
                        sales_tr += '<tr>' +
                        '<td>' + this.data.data['sales'][i]['account'] + '</td>' +
                        '<td><span class="float-right">' + formatCurrency(salesFrom, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(salesTo, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + calculateVar(salesFrom, salesTo, true) + '</span></td>' +
                        '<td><span class="float-right">' + calculateVarPercentage(salesFrom, salesTo, true) + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(this.data.data['sales'][i]['ytd'], 'R ') + '</span></td>' +
                        '</tr>';
                    }
                   
                }
                //Total Sales
                total_sales_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Sales</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_sales'][0]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_sales'][1]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(this.data.data['total_sales'][0]['amount'], this.data.data['total_sales'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(this.data.data['total_sales'][0]['amount'], this.data.data['total_sales'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_sales'][0]['ytd'], 'R ') + '</span></td>' +
                    '</tr>';
                //Cost of Sales
                for(let i = 0; i < this.data.data['cost_of_sales'].length; i++){

                    let cosFrom = this.data.data['cost_of_sales'][i]['amount'][0];
                    let cosTo = this.data.data['cost_of_sales'][i]['amount'][1];
                    let cosYtd = this.data.data['cost_of_sales'][i]['ytd'];

                    if(cosFrom != 0 || cosTo != 0 || cosYtd != 0){
                        cost_of_sales_tr += '<tr>' +
                    '<td>' + this.data.data['cost_of_sales'][i]['account'] + '</td>' +
                    '<td><span class="float-right">' + formatCurrency(cosFrom, 'R ') + '</span></td>' +
                    '<td><span class="float-right">' + formatCurrency(cosTo, 'R ') + '</span></td>' +
                    '<td><span class="float-right">' + calculateVar(cosFrom, cosTo, false) + '</span></td>' +
                    '<td><span class="float-right">' + calculateVarPercentage(cosFrom, cosTo, false) + '</span></td>' +
                    '<td><span class="float-right">' + formatCurrency(this.data.data['cost_of_sales'][i]['ytd'], 'R ') + '</span></td>' +
                    '</tr>';
                    }
                    
                }
                //Total Cost of Sales
                total_cost_of_sales_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Cost of Sales</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_cost_of_sales'][0]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_cost_of_sales'][1]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(this.data.data['total_cost_of_sales'][0]['amount'], this.data.data['total_cost_of_sales'][1]['amount'], false) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(this.data.data['total_cost_of_sales'][0]['amount'], this.data.data['total_cost_of_sales'][1]['amount'], false) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_cost_of_sales'][0]['ytd'], 'R ') + '</span></td>' +
                    '</tr>';
                //Gross Profit
                gross_profit_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Gross Profit</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['gross_profit'][0]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['gross_profit'][1]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(this.data.data['gross_profit'][0]['amount'], this.data.data['gross_profit'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(this.data.data['gross_profit'][0]['amount'], this.data.data['gross_profit'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['gross_profit'][0]['ytd'], 'R ') + '</span></td>' +
                    '</tr>';
                //Other Income
                for(let i = 0; i < this.data.data['other_income'].length; i++){

                    let otherFrom = this.data.data['other_income'][i]['amount'][0];
                    let otherTo = this.data.data['other_income'][i]['amount'][1];
                    let otherYtd = this.data.data['other_income'][i]['ytd'];

                    if(otherFrom != 0 || otherTo != 0 || otherYtd != 0){
                        other_income_tr += '<tr>' +
                        '<td>' + this.data.data['other_income'][i]['account'] + '</td>' +
                        '<td><span class="float-right">' + formatCurrency(otherFrom, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(otherTo, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + calculateVar(otherFrom, otherTo, true) + '</span></td>' +
                        '<td><span class="float-right">' + calculateVarPercentage(otherFrom, otherTo, true) + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(this.data.data['other_income'][i]['ytd'], 'R ') + '</span></td>' +
                        '</tr>';
                    }
                    
                }
                //Total Other Income
                total_other_income_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Other Income</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_other_income'][0]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_other_income'][1]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(this.data.data['total_other_income'][0]['amount'], this.data.data['total_other_income'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(this.data.data['total_other_income'][0]['amount'], this.data.data['total_other_income'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_other_income'][0]['ytd'], 'R ') + '</span></td>' +
                    '</tr>';
                //Expenses
                for(let i = 0; i < this.data.data['expenses'].length; i++){

                    let expenseFrom =  this.data.data['expenses'][i]['amount'][0]; 
                    let expenseTo = this.data.data['expenses'][i]['amount'][1] ;
                    let expenseYtd = this.data.data['expenses'][i]['ytd'];

                    if(expenseFrom != 0 || expenseTo != 0 || expenseYtd != 0){
                        // if(!isNaN(expenseYtd)){
                        expenses_tr += '<tr>' +
                        '<td>' + this.data.data['expenses'][i]['account'] + '</td>' +
                        '<td><span class="float-right">' + formatCurrency(expenseFrom, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(expenseTo, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + calculateVar(expenseFrom, expenseTo, false) + '</span></td>' +
                        '<td><span class="float-right">' + calculateVarPercentage(expenseFrom, expenseTo, false) + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(this.data.data['expenses'][i]['ytd'], 'R ') + '</span></td>' +
                        '</tr>';
                        // }
                    }
                    
                }
                //Total Expenses
                total_expenses_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Expenses</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_expenses'][0]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_expenses'][1]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(this.data.data['total_expenses'][0]['amount'], this.data.data['total_expenses'][1]['amount'], false) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(this.data.data['total_expenses'][0]['amount'], this.data.data['total_expenses'][1]['amount'], false) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_expenses'][0]['ytd'], 'R ') + '</span></td>' +
                    '</tr>';
                //Net Profit Before Tax
                net_profit_before_tax_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Net Profit/Loss Before Tax</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['net_profit_before_tax'][0]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['net_profit_before_tax'][1]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(this.data.data['net_profit_before_tax'][0]['amount'], this.data.data['net_profit_before_tax'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(this.data.data['net_profit_before_tax'][0]['amount'], this.data.data['net_profit_before_tax'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['net_profit_before_tax'][0]['ytd'], 'R ') + '</span></td>' +
                    '</tr>';
                //Tax
                for(let i = 0; i < this.data.data['tax'].length; i++){
                    
                    let taxFrom =  this.data.data['tax'][i]['amount'][0];
                    let taxTo = this.data.data['tax'][i]['amount'][1];
                    let taxYtd = this.data.data['tax'][i]['ytd'];

                    if(taxFrom != 0 || taxTo != 0 || taxYtd != 0){
                        tax_tr += '<tr>' +
                        '<td>' + this.data.data['tax'][i]['account'] + '</td>' +
                        '<td><span class="float-right">' + formatCurrency(taxFrom, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(taxTo, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + calculateVar(taxFrom, taxTo, false) + '</span></td>' +
                        '<td><span class="float-right">' + calculateVarPercentage(taxFrom, taxTo, false) + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(this.data.data['tax'][i]['ytd'], 'R ') + '</span></td>' +
                        '</tr>';
                    }

                    
                }
                //Total Tax
                total_tax_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Tax</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_tax'][0]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['total_tax'][1]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(this.data.data['total_tax'][0]['amount'], this.data.data['total_tax'][1]['amount'], false) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(this.data.data['total_tax'][0]['amount'], this.data.data['total_tax'][1]['amount'], false) + '</span></td>' +
                    '<td><span class="float-right">' + formatCurrency(this.data.data['total_tax'][0]['ytd'], 'R ') + '</td></span>' +
                    '</tr>';
                //Net Profit After Tax
                net_profit_after_tax_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Net Profit/Loss After Tax</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['net_profit_after_tax'][0]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['net_profit_after_tax'][1]['amount'], 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(this.data.data['net_profit_after_tax'][0]['amount'], this.data.data['net_profit_after_tax'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(this.data.data['net_profit_after_tax'][0]['amount'], this.data.data['net_profit_after_tax'][1]['amount'], true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(this.data.data['net_profit_after_tax'][0]['ytd'], 'R ') + '</span></td>' +
                    '</tr>';
                
                description = '<table class="table small table-hover table-condensed txt-color">' +
                '<tr>' +
                    '<th></th>' +
                    '<th><span class="float-right">' + this.fromDate.substr(0, 8) + '</span></th>' +
                    '<th><span class="float-right">' + this.toDate.substr(0, 8) + '</span></th>' +
                    '<th><span class="float-right">Variance</span></th>' +
                    '<th><span class="float-right">Variance (%)</span></th>' +
                    '<th><span class="float-right">YTD</span></th>' +
                '</tr>';
                
                
                if(sales_tr.length > 0){
                    description += '<tr>' +
                    '<th><p class="fs-14 m-0">Sales</p></th></th><th></th><th></th><th></th><th></th><th></th>' +
                    '</tr>' + 
                    '<tr id="sales_tr">' + sales_tr + '</tr>' +
                    '<tr id="total_sales_tr">' + total_sales_tr + '</tr>';
                }
                
                if(cost_of_sales_tr.length > 0){
                    description += '<tr>' +
                    '<th><p class="fs-14 m-0">Cost of Sales</p></th></th><th></th><th></th><th></th><th></th><th></th>' +
                    '</tr>' +
                    '<tr id="cost_of_sales_tr">' + cost_of_sales_tr + '</tr>' +
                    '<tr id="total_cost_of_sales_tr">' + total_cost_of_sales_tr + '</tr>' +
                    '<tr id="gross_profit_tr">' + gross_profit_tr + '</tr>';
                }
                

                if(other_income_tr.length > 0){
                    description += '<tr>' +
                    '<th><p class="fs-14 m-0">Other Income</p></th></th><th></th><th></th><th></th><th></th><th></th>' +
                    '</tr>' +
                    '<tr id="other_income_tr">' + other_income_tr + '</tr>' +
                    '<tr id="total_other_income_tr">' + total_other_income_tr + '</tr>' ;
                }
                
                if(expenses_tr.length > 0){
                    description += '<tr>' +
                    '<th><p class="fs-14 m-0">Expenses</p></th></th><th></th><th></th><th></th><th></th><th></th>' +
                    '</tr>' +
                    '<tr id="expenses_tr">' + expenses_tr + '</tr>' +
                    '<tr id="total_expenses_tr">' + total_expenses_tr + '</tr>';
                }
                // if(net_profit_before_tax_tr.length > 0){
                    description += '<tr id="net_profit_before_tax_tr">' + net_profit_before_tax_tr + '</tr>';
                // }
                
                if(tax_tr.length > 0){
                    description += '<tr>' +
                    '<th><p class="fs-14 m-0">Tax</p></th></th><th></th><th></th><th></th><th></th><th></th>' +
                    '</tr>' +
                    '<tr id="tax_tr">' + tax_tr + '</tr>' +
                    '<tr id="total_tax_tr">' + total_tax_tr + '</tr>' +
                    '<tr id="net_profit_after_tax_tr">' + net_profit_after_tax_tr + '</tr>';
                }
                
            description += '</table>';

            break;

        //Balance Sheet
            case 'bs':

                let current_assets_tr = '';
                let total_current_assets_tr = '';
                let non_current_assets_tr = '';
                let total_non_current_assets_tr = '';
                let total_assets_tr = '';
                let current_liabilities_tr = '';
                let total_current_liabilities_tr = '';
                let non_current_liabilities_tr = '';
                let total_non_current_liabilities_tr = '';
                let total_liabilities_tr = '';
                let equity_tr = '';
                let total_equity_tr = '';
                let total_equity_and_liabilities_tr = '';
                let fromTotal;
                let toTotal;
                let fromAssets;
                let fromLiabilities;
                let toAssets;
                let toLiabilities;

                let fromIndex = this.getDateIndexFormat(this.fromDate);
                let toIndex = this.getDateIndexFormat(this.toDate);
            
                //Current Assets
                
                for(let i = 0; i < this.data.data['current_assets'].length; i++){
                    let currentFrom = this.data.data['current_assets'][i]['data'][0]['amount'];
                    let currentTo = this.data.data['current_assets'][i]['data'][1]['amount'];

                    if(currentFrom != 0 || currentTo !=0){
                        current_assets_tr += '<tr>' +
                        '<td>' + this.data.data['current_assets'][i]['account'] + '</td>' +
                        '<td><span class="float-right">' + formatCurrency(currentFrom, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(currentTo, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + calculateVar(currentFrom, currentTo, true) + '</span></td>' +
                        '<td><span class="float-right">' + calculateVarPercentage(currentFrom, currentTo, true) + '</span></td>' +
                        '</tr>';
                    }
                }

                
                //Total Current Assets
                fromTotal = (Number.isInteger(this.data.data['total_current_assets'])) ? this.data.data['total_current_assets'] : this.data.data['total_current_assets'][fromIndex];
                toTotal = (Number.isInteger(this.data.data['total_current_assets'])) ? this.data.data['total_current_assets'] : this.data.data['total_current_assets'][toIndex];

                total_current_assets_tr = '<tr>' +
                    '<td class="font-weight-bold-table">Total Current Assets</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(fromTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(toTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(fromTotal, toTotal, true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(fromTotal, toTotal, true) + '</span></td>' +
                    '</tr>';
                //Non-Current Assets
                for(let i = 0; i < this.data.data['non_current_assets'].length; i++){

                    let noncfrom = this.data.data['non_current_assets'][i]['data'][0]['amount']; 
                    let noncTo = this.data.data['non_current_assets'][i]['data'][1]['amount'] ;

                    if(noncfrom != 0 || noncTo != 0){
                        non_current_assets_tr += '<tr>' +
                        '<td>' + this.data.data['non_current_assets'][i]['account'] + '</td>' +
                        '<td><span class="float-right">' + formatCurrency(noncfrom, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(noncTo, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + calculateVar(noncfrom, noncTo, true) + '</span></td>' +
                        '<td><span class="float-right">' + calculateVarPercentage(noncfrom, noncTo, true) + '</span></td>' +
                        '</tr>';
                    }
                    
                }

                
                //Total Non-Current Assets
                fromTotal = (Number.isInteger(this.data.data['total_non_current_assets'])) ? this.data.data['total_non_current_assets'] : this.data.data['total_non_current_assets'][fromIndex];
                toTotal = (Number.isInteger(this.data.data['total_non_current_assets'])) ? this.data.data['total_non_current_assets'] : this.data.data['total_non_current_assets'][toIndex];
                
                total_non_current_assets_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Non-Current Assets</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(fromTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(toTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(fromTotal, toTotal, true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(fromTotal, toTotal, true) + '</span></td>' +
                    '</tr>';
                //Total Assets
                fromTotal = (Number.isInteger(this.data.data['total_assets'])) ? this.data.data['total_assets'] : this.data.data['total_assets'][fromIndex];
                toTotal = (Number.isInteger(this.data.data['total_assets'])) ? this.data.data['total_assets'] : this.data.data['total_assets'][toIndex];
                
                total_assets_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Assets</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(fromTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(toTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(fromTotal, toTotal, true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(fromTotal, toTotal, true) + '</span></td>' +
                    '</tr>';
                //Current Liabilities
                for(let i = 0; i < this.data.data['current_liabilities'].length; i++){

                    let curlibFrom = this.data.data['current_liabilities'][i]['data'][0]['amount'] ; 
                    let curlibTo = this.data.data['current_liabilities'][i]['data'][1]['amount'] ;

                    if(curlibFrom != 0 || curlibTo != 0){
                        current_liabilities_tr += '<tr>' +
                        '<td>' + this.data.data['current_liabilities'][i]['account'] + '</td>' +
                        '<td><span class="float-right">' + formatCurrency(curlibFrom, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(curlibTo, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + calculateVar(curlibFrom, curlibTo, false) + '</span></td>' +
                        '<td><span class="float-right">' + calculateVarPercentage(curlibFrom, curlibTo, false) + '</span></td>' +
                        '</tr>';
                    }
                    
                }
                //Total Current Liabilities
                fromTotal = (Number.isInteger(this.data.data['total_current_liabilities'])) ? this.data.data['total_current_liabilities'] : this.data.data['total_current_liabilities'][fromIndex];
                toTotal = (Number.isInteger(this.data.data['total_current_liabilities'])) ? this.data.data['total_current_liabilities'] : this.data.data['total_current_liabilities'][toIndex];
                
                total_current_liabilities_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Current Liabilities</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(fromTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(toTotal,'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(fromTotal, toTotal,false) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(fromTotal, toTotal,false) + '</span></td>' +
                    '</tr>';
                //Non-Current Liabilities
                for(let i = 0; i < this.data.data['non_current_liabilities'].length; i++){

                    let noncurFrom = this.data.data['non_current_liabilities'][i]['data'][0]['amount'];
                    let noncurTo = this.data.data['non_current_liabilities'][i]['data'][1]['amount'];

                    if(noncurFrom != 0 || noncurTo != 0){
                        non_current_liabilities_tr += '<tr>' +
                        '<td>' + this.data.data['non_current_liabilities'][i]['account'] + '</td>' +
                        '<td><span class="float-right">' + formatCurrency(noncurFrom, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(noncurTo, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + calculateVar(noncurFrom, noncurTo, false) + '</span></td>' +
                        '<td><span class="float-right">' + calculateVarPercentage(noncurFrom, noncurTo, false) + '</span></td>' +
                        '</tr>';
                    }
                    
                }
                //Total Non-Current Liabilities
                fromTotal = (Number.isInteger(this.data.data['total_non_current_liabilities'])) ? this.data.data['total_non_current_liabilities'] : this.data.data['total_non_current_liabilities'][fromIndex];
                toTotal = (Number.isInteger(this.data.data['total_non_current_liabilities'])) ? this.data.data['total_non_current_liabilities'] : this.data.data['total_non_current_liabilities'][toIndex];
                
                total_non_current_liabilities_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Non-Current Liabilities</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(fromTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(toTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(fromTotal, toTotal, false) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(fromTotal, toTotal, false) + '</span></td>' +
                    '</tr>';

                //Total Liabilities
                fromTotal = (Number.isInteger(this.data.data['total_liabilities'])) ? this.data.data['total_liabilities'] : this.data.data['total_liabilities'][fromIndex];
                toTotal = (Number.isInteger(this.data.data['total_liabilities'])) ? this.data.data['total_liabilities'] : this.data.data['total_liabilities'][toIndex];

                total_liabilities_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Liabilities</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(fromTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(toTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(fromTotal, toTotal, false) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(fromTotal, toTotal, false) + '</span></td>' +
                    '</tr>';
                //Net Assets
                fromAssets = (Number.isInteger(this.data.data['total_assets'])) ? this.data.data['total_assets'] : this.data.data['total_assets'][fromIndex];
                toAssets = (Number.isInteger(this.data.data['total_assets'])) ? this.data.data['total_assets'] : this.data.data['total_assets'][toIndex];
                fromLiabilities = (Number.isInteger(this.data.data['total_liabilities'])) ? this.data.data['total_liabilities'] : this.data.data['total_liabilities'][fromIndex];
                toLiabilities = (Number.isInteger(this.data.data['total_liabilities'])) ? this.data.data['total_liabilities'] : this.data.data['total_liabilities'][toIndex];

                total_liabilities_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Net Assets</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(fromAssets - fromLiabilities, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(toAssets - toLiabilities, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(fromAssets - fromLiabilities, toAssets - toLiabilities, true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(fromAssets - fromLiabilities, toAssets - toLiabilities, true) + '</span></td>' +
                    '</tr>';
                //Equity
                for(let i = 0; i < this.data.data['equity'].length; i++){

                    let equityFrom =  this.data.data['equity'][i]['data'][0]['amount'];
                    let equityTo = this.data.data['equity'][i]['data'][1]['amount'];

                    if(equityTo != 0 || equityFrom != 0){
                        equity_tr += '<tr>' +
                        '<td>' + this.data.data['equity'][i]['account'] + '</td>' +
                        '<td><span class="float-right">' + formatCurrency(equityFrom, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + formatCurrency(equityTo, 'R ') + '</span></td>' +
                        '<td><span class="float-right">' + calculateVar(equityFrom, equityTo, true) + '</span></td>' +
                        '<td><span class="float-right">' + calculateVarPercentage(equityFrom, equityTo, true) + '</span></td>' +
                        '</tr>';
                    }
                    
                }
                //Total Equity
                fromTotal = (Number.isInteger(this.data.data['total_equity'])) ? this.data.data['total_equity'] : this.data.data['total_equity'][fromIndex];
                toTotal = (Number.isInteger(this.data.data['total_equity'])) ? this.data.data['total_equity'] : this.data.data['total_equity'][toIndex];
                
                total_equity_tr += '<tr>' +
                    '<td class="font-weight-bold-table">Total Equity</td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(fromTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + formatCurrency(toTotal, 'R ') + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVar(fromTotal, toTotal, true) + '</span></td>' +
                    '<td class="font-weight-bold-table"><span class="float-right">' + calculateVarPercentage(fromTotal, toTotal, true) + '</span></td>' +
                    '</tr>';
                //Total Equity and Liabilities
                // total_equity_and_liabilities_tr += '<tr>' +
                //     '<td class="font-weight-bold-table">Total Equity and Liabilities</td>' +
                //     '<td class="font-weight-bold-table">' + formatCurrency(this.data.data['total_equity_and_liabilities'][0]) + '</td>' +
                //     '<td class="font-weight-bold-table">' + formatCurrency(this.data.data['total_equity_and_liabilities'][1]) + '</td>' +
                //     '<td class="font-weight-bold-table">' + calculateVar(this.data.data['total_equity_and_liabilities'][0], this.data.data['total_equity_and_liabilities'][1], true) + '</td>' +
                //     '<td class="font-weight-bold-table">' + calculateVarPercentage(this.data.data['total_equity_and_liabilities'][0], this.data.data['total_equity_and_liabilities'][1], true) + '</td>' +
                // '</tr>';

                description = '<table class="table small table-hover table-condensed txt-color">' +
                    '<tr>' +
                        '<th></th>' +
                        '<th><span class="float-right">' + this.fromDate.substr(0, 8) + '</th>' +
                        '<th><span class="float-right">' + this.toDate.substr(0, 8) + '</th>' +
                        '<th><span class="float-right">Variance</th>' +
                        '<th><span class="float-right">Variance (%)</th>' +
                    '</tr>';

                    if(current_assets_tr.length > 0 || non_current_assets_tr.length > 0 ){
                        description += '<tr>' +
                        '<th><p class="fs-14 m-0">Assets</p></th><th></th><th></th><th></th><th></th>' +
                        '</tr>';

                        if(current_assets_tr.length > 0 ){
                            description += '<tr>' +
                            '<th><p class="fs-14 m-0">Current Assets</p></th><th></th><th></th><th></th><th></th>' +
                            '</tr>' +
                            '<tr id="current_assets_tr">' + current_assets_tr + '</tr>' +
                            '<tr id="total_current_assets_tr">' + total_current_assets_tr + '</tr>';
                        }

                        if(non_current_assets_tr.length > 0){
                            description +=  '<tr>' +
                            '<th><p class="fs-14 m-0">Non-Current Assets</p></th><th></th><th></th><th></th><th></th>' +
                            '</tr>' +
                            '<tr id="non_current_assets_tr">' + non_current_assets_tr + '</tr>' +
                            '<tr id="total_non_current_assets_tr">' + total_non_current_assets_tr + '</tr>';
                        }

                        description += '<tr id="total_assets_tr">' + total_assets_tr + '</tr>';
                        
                    }


                    if(current_liabilities_tr.length > 0 || non_current_liabilities_tr.length > 0){
                        description += '<tr>' +
                        '<th><p class="fs-15 m-0">Liabilities</p></th><th></th><th></th><th></th><th></th>' +
                        '</tr>';

                        if(current_liabilities_tr.length > 0 ){
                            description += '<tr>' +
                            '<th><p class="fs-14 m-0">Current Liabilities</p></th><th></th><th></th><th></th><th></th>' +
                            '</tr>' +
                            '<tr id="current_liabilities_tr">' + current_liabilities_tr + '</tr>' +
                            '<tr id="total_current_liabilities_tr">' + total_current_liabilities_tr + '</tr>';
                        }

                        if(non_current_liabilities_tr.length > 0 ){
                            description += '<tr>' +
                            '<th><p class="fs-14 m-0">Non-Current Liabilities</p></th><th></th><th></th><th></th><th></th>' +
                            '</tr>' +
                            '<tr id="non_current_liabilities_tr">' + non_current_liabilities_tr + '</tr>' +
                            '<tr id="total_non_current_liabilities_tr">' + total_non_current_liabilities_tr + '</tr>' +
                            '<tr id="total_liabilities_tr">' + total_liabilities_tr + '</tr>';
                        }


                        
                    }

                    if(equity_tr.length > 0){
                        description += '<tr>' +
                        '<th><p class="fs-14 m-0">Owner\'s Equity</p></th><th></th><th></th><th></th><th></th>' +
                        '</tr>' +
                        '<tr id="equity_tr">' + equity_tr + '</tr>' +
                        '<tr id="total_equity_tr">' + total_equity_tr + '</tr>' +
                        '<tr id="total_equity_and_liabilities_tr">' + total_equity_and_liabilities_tr + '</tr>';
                    }

                    if(current_assets_tr.length == 0 && non_current_assets_tr.length == 0 ){
                        description += '<tr>' +
                        '<th></th><th></th><th><h6 class="mobile-fs-14 mb-0">No Data for period</h6></th><th></th><th></th>' +
                        '</tr>';
                    }

                description +=   '</table>';

                
            break;

        //Accounts Receivable
            case 'ar':

                let ar0_30 = 0;
                let ar30_60 = 0;
                let ar60_90 = 0;
                let ar90_120 = 0;
                let ar120 = 0;
                let arTot = 0;
                let arRowTotp = 0;
                let arRows = '';

                // console.log(this.data.data['accounts']);

                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    ar0_30 += this.data.data['accounts'][i]['0-30'];
                    ar30_60 += this.data.data['accounts'][i]['30-60'];
                    ar60_90 += this.data.data['accounts'][i]['60-90'];
                    ar90_120 += this.data.data['accounts'][i]['90-120'];
                    ar120 += this.data.data['accounts'][i]['120+'];
                    arTot += this.data.data['accounts'][i]['total'];
                }
                // console.log(ar0_30);
                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    arRowTotp = ((100 / arTot) * this.data.data['accounts'][i]['total']).toFixed(1);
                    if(this.data.data['accounts'][i]['total'] != 0){
                        arRows += 
                        '<tr>' +
                            '<td>' + this.data.data['accounts'][i]['name'] + '</td>' +
                            '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['120+'], 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['90-120'], 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['60-90'], 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['30-60'], 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['0-30'], 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['total'], 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + arRowTotp + '%</th>' +
                        '</tr>';
                    }
                }

                let ar0_30p = ((100 / arTot) * ar0_30).toFixed(1);
                let ar30_60p = ((100 / arTot) * ar30_60).toFixed(1);
                let ar60_90p = ((100 / arTot) * ar60_90).toFixed(1);
                let ar90_120p = ((100 / arTot) * ar90_120).toFixed(1);
                let ar120p = ((100 / arTot) * ar120).toFixed(1);
            
                description =
                '<div id="annex-row' + this.number + '" class="row" style="overflow-y:auto;">' +
                    '<table class="small table txt-color">' +
                        '<thead>' +
                            '<tr>' +
                                '<th>Debtor</th>' +
                                '<th><span class="float-right">120+ days</span></th>' +
                                '<th><span class="float-right">90-120 days</span></th>' +
                                '<th><span class="float-right">60-90 days</span></th>' +
                                '<th><span class="float-right">30-60 days</span></th>' +
                                '<th><span class="float-right">0-30 days</span></th>' +
                                '<th><span class="float-right">Total</span></th>' +
                                '<th><span class="float-right">% of Total</span></th>' +
                            '</tr>' +
                        '</thead>' +
                            arRows + 
                        '<tr class="font-weight-bold-table">' +
                            '<td>Total</td>' +
                            '<td><span class="float-right">' + formatCurrency(ar120, 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(ar90_120, 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(ar60_90, 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(ar30_60 , 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(ar0_30, 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(arTot, 'R ') + '</span></td>' +
                            '<td><span class="float-right">100%</td>' +
                        '</tr>' +
                        '<tr class="font-weight-bold-table">' +
                            '<td>Total %</td>' +
                            '<td><span class="float-right">' + ar120p + '%</span></td>' +
                            '<td><span class="float-right">' + ar90_120p + '%</span></td>' +
                            '<td><span class="float-right">' + ar60_90p + '%</span></td>' +
                            '<td><span class="float-right">' + ar30_60p + '%</span></td>' +
                            '<td><span class="float-right">' + ar0_30p + '%</span></td>' +
                            '<td><span class="float-right"></span></td>' +
                            '<td><span class="float-right"></span></td>' +
                        '</tr>' +
                    '</table>' + 
                '</div>';

            break;

        //Debtors days outstanding
            case 'ddo':

                let ddo0_30 = 0;
                let ddo30_60 = 0;
                let ddo60_90 = 0;
                let ddo90_120 = 0;
                let ddo120 = 0;
                let ddoTot = 0;

                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    ddo0_30 += this.data.data['accounts'][i]['0-30'];
                    ddo30_60 += this.data.data['accounts'][i]['30-60'];
                    ddo60_90 += this.data.data['accounts'][i]['60-90'];
                    ddo90_120 += this.data.data['accounts'][i]['90-120'];
                    ddo120 += this.data.data['accounts'][i]['120+'];
                    ddoTot += this.data.data['accounts'][i]['total'];
                }

                let ddo0_30p = ((100 / ddoTot) * ddo0_30).toFixed(1);
                let ddo30_60p = ((100 / ddoTot) * ddo30_60).toFixed(1);
                let ddo60_90p = ((100 / ddoTot) * ddo60_90).toFixed(1);
                let ddo90_120p = ((100 / ddoTot) * ddo90_120).toFixed(1);
                let ddo120p = ((100 / ddoTot) * ddo120).toFixed(1);

                description = 'Total debt outstanding as at ' + this.fromDate + ' is ' + formatCurrency(ddoTot, 'R ') + '.</p>' + 
                '<p>' + formatCurrency(ddo0_30, 'R ') + ' (' + ddo0_30p + '%) is less than 30 days outstanding.</p>' + 
                '<p>' + formatCurrency(ddo30_60, 'R ') + ' (' + ddo30_60p + '%) is between 30 and 60 days outstanding.</p>' + 
                '<p>' + formatCurrency(ddo60_90, 'R ') + ' (' + ddo60_90p + '%) is between 60 and 90 days outstanding.</p>' + 
                '<p>' + formatCurrency(ddo90_120, 'R ') + ' (' + ddo90_120p + '%) is between 90 and 120 days outstanding.</p>' + 
                '<p>' + formatCurrency(ddo120, 'R ') + ' (' + ddo120p + '%) is more than 120 days outstanding.</p>';

            break;

        //Top Customers by Sale
            case 'tcs':

                let tcsTot = 0;    

                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    tcsTot += this.data.data['accounts'][i]['total'];
                }

                if(typeof this.data.data['accounts'][0] !== 'undefined'){
                    let debtor1name = this.data.data['accounts'][0]['name'];
                    let debtor1total = this.data.data['accounts'][0]['total'];
                    let debtor1p = ((100 / tcsTot) * debtor1total).toFixed(1);

                    description = '<p style="font-size: 15px;">The top debtor as at ' + this.fromDate + ' is ' + debtor1name + ' with an outstanding balance of ' + formatCurrency(debtor1total, 'R ') + '.</p>' +
                    '<p style="font-size: 15px;">' + debtor1name + ' accounts for ' + debtor1p + '% of the debtors.</p>';
                }
                if(typeof this.data.data['accounts'][1] !== 'undefined'){
                    let debtor2name = this.data.data['accounts'][1]['name'];
                    let debtor2total = this.data.data['accounts'][1]['total'];
                    let debtor2p = ((100 / tcsTot) * debtor2total).toFixed(1);

                    description += '<p style="font-size: 15px;">The debtor with the 2nd highest outstanding balance (' + formatCurrency(debtor2total, 'R ') + ') is ' + debtor2name + '.</p>' +
                    '<p style="font-size: 15px;">' + debtor2name + ' accounts for ' + debtor2p + '% of the debtors.</p>';
                }
                if(typeof this.data.data['accounts'][2] !== 'undefined'){
                    let debtor3name = this.data.data['accounts'][2]['name'];
                    let debtor3total = this.data.data['accounts'][2]['total'];
                    let debtor3p = ((100 / tcsTot) * debtor3total).toFixed(1);

                    description += '<p style="font-size: 15px;">The debtor with the 3rd highest outstanding balance (' + formatCurrency(debtor3total, 'R ') + ') is ' + debtor3name + '.</p>' +
                    '<p style="font-size: 15px;">' + debtor3name + ' accounts for ' + debtor3p + '% of the debtors.</p>';
                }

                
            break;

        //Retention
            case 'ret':

                let maxRet = Math.max.apply(null, this.data.data['retention']);
                let minRet = Math.min.apply(null, this.data.data['retention']);
                
                a = this.data.data['retention'].indexOf(maxRet);
                let monthMaxRet = this.data.labels[a];
                b = this.data.data['retention'].indexOf(minRet);
                let monthMinRet = this.data.labels[b];

                description = '<p>Month-on-month customer retention illustrates the proportion of customers invoiced in a given month that were also invoiced in the previous month.</p>' + 
                '<p>The highest retention rate was recorded in ' + monthMaxRet + ' with ' + (100 * maxRet).toFixed(2) + '%.</p>' + 
                '<p>The lowest retention rate was recorded in ' + monthMinRet + ' with ' + (100 * minRet).toFixed(2) + '%.</p>';

            break;

        //Accounts Payable
            case 'ap':

                let ap0_30 = 0;
                let ap30_60 = 0;
                let ap60_90 = 0;
                let ap90_120 = 0;
                let ap120 = 0;
                let apTot = 0;
                let apRowTotp = 0;
                let apRows = '';

                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    ap0_30 += this.data.data['accounts'][i]['0-30'];
                    ap30_60 += this.data.data['accounts'][i]['30-60'];
                    ap60_90 += this.data.data['accounts'][i]['60-90'];
                    ap90_120 += this.data.data['accounts'][i]['90-120'];
                    ap120 += this.data.data['accounts'][i]['120+'];
                    apTot += this.data.data['accounts'][i]['total'];
                }
                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    apRowTotp = ((100 / apTot) * this.data.data['accounts'][i]['total']).toFixed(1);
                    if(this.data.data['accounts'][i]['total'] != 0){
                        apRows += 
                            '<tr>' +
                                '<td>' + this.data.data['accounts'][i]['name'] + '</td>' +
                                '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['120+'], 'R ') + '</span></td>' +
                                '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['90-120'], 'R ') + '</span></td>' +
                                '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['60-90'], 'R ') + '</span></td>' +
                                '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['30-60'], 'R ') + '</span></td>' +
                                '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['0-30'], 'R ') + '</span></td>' +
                                '<td><span class="float-right">' + formatCurrency(this.data.data['accounts'][i]['total'], 'R ') + '</span></td>' +
                                '<td><span class="float-right">' + apRowTotp + '%</th>' +
                            '</tr>';
                    }
                }

                let ap0_30p = ((100 / apTot) * ap0_30).toFixed(1);
                let ap30_60p = ((100 / apTot) * ap30_60).toFixed(1);
                let ap60_90p = ((100 / apTot) * ap60_90).toFixed(1);
                let ap90_120p = ((100 / apTot) * ap90_120).toFixed(1);
                let ap120p = ((100 / apTot) * ap120).toFixed(1);
            
                description =
                '<div id="annex-row' + this.number + '" class="row" style="overflow-y:auto;">' +
                    '<table class="small table txt-color">' +
                        '<thead>' +
                            '<tr>' +
                                '<th style="font-size: 12px;">Creditor' + ' ' +'</th>' +
                                '<th><span class="float-right">120+ days</span></th>' +
                                '<th><span class="float-right">90-120 days</span></th>' +
                                '<th><span class="float-right">60-90 days</span></th>' +
                                '<th><span class="float-right">30-60 days</span></th>' +
                                '<th><span class="float-right">0-30 days</span></th>' +
                                '<th><span class="float-right">Total</span></th>' +
                                '<th><span class="float-right">% of Total</span></th>' +
                            '</tr>' +
                        '</thead>' +
                            apRows + 
                        '<tr class="font-weight-bold-table">' +
                            '<td>Total</td>' +
                            '<td><span class="float-right">' + formatCurrency(ap120, 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(ap90_120 , 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(ap60_90, 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(ap30_60, 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(ap0_30, 'R ') + '</span></td>' +
                            '<td><span class="float-right">' + formatCurrency(apTot, 'R ') + '</span></td>' +
                            '<td><span class="float-right">100%</span></td>' +
                        '</tr>' +
                        '<tr class="font-weight-bold-table">' +
                            '<td>Total %</td>' +
                            '<td><span class="float-right">' + ap120p + '%</span></td>' +
                            '<td><span class="float-right">' + ap90_120p  + '%</span></td>' +
                            '<td><span class="float-right">' + ap60_90p + '%</span></td>' +
                            '<td><span class="float-right">' + ap30_60p + '%</span></td>' +
                            '<td><span class="float-right">' + ap0_30p + '%</span></td>' +
                            '<td><span class="float-right"></span></td>' +
                            '<td><span class="float-right"></span></td>' +
                        '</tr>' +
                    '</table>' + 
                '</div>';

            break;

        //Creditors Days Outstanding
            case 'cdo':

                let cdo0_30 = 0;
                let cdo30_60 = 0;
                let cdo60_90 = 0;
                let cdo90_120 = 0;
                let cdo120 = 0;
                let cdoTot = 0;

                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    cdo0_30 += this.data.data['accounts'][i]['0-30'];
                    cdo30_60 += this.data.data['accounts'][i]['30-60'];
                    cdo60_90 += this.data.data['accounts'][i]['60-90'];
                    cdo90_120 += this.data.data['accounts'][i]['90-120'];
                    cdo120 += this.data.data['accounts'][i]['120+'];
                    cdoTot += this.data.data['accounts'][i]['total'];
                }

                let cdo0_30p = ((100 / cdoTot) * cdo0_30).toFixed(1);
                let cdo30_60p = ((100 / cdoTot) * cdo30_60).toFixed(1);
                let cdo60_90p = ((100 / cdoTot) * cdo60_90).toFixed(1);
                let cdo90_120p = ((100 / cdoTot) * cdo90_120).toFixed(1);
                let cdo120p = ((100 / cdoTot) * cdo120).toFixed(1);

                description = '<p style="font-size: 15px;">Total creditors payable as at ' + this.fromDate + ' is ' + formatCurrency(cdoTot, 'R ') + '.</p>' + 
                '<p style="font-size: 15px;">' + formatCurrency(cdo0_30, 'R ') + ' (' + cdo0_30p + '%) is less than 30 days outstanding.' + 
                '<p style="font-size: 15px;">' + formatCurrency(cdo30_60, 'R ') + ' (' + cdo30_60p + '%) is between 30 and 60 days outstanding.</p>' + 
                '<p style="font-size: 15px;">' + formatCurrency(cdo60_90, 'R ') + ' (' + cdo60_90p + '%) is between 60 and 90 days outstanding.</p>' + 
                '<p style="font-size: 15px;">' + formatCurrency(cdo90_120, 'R ') + ' (' + cdo90_120p + '%) is between 90 and 120 days outstanding.</p>' + 
                '<p style="font-size: 15px;">' + formatCurrency(cdo120, 'R ') + ' (' + cdo120p + '%) is more than 120 days outstanding.</p>';

            break;

        //Top Suppliers by Purchases
            case 'tsp':

                let tspTot = 0;    

                for(let i = 0; i < this.data.data['accounts'].length; i++){
                    tspTot += this.data.data['accounts'][i]['total'];
                }

                if(typeof this.data.data['accounts'][0] !== 'undefined'){
                    let creditor1name = this.data.data['accounts'][0]['name'];
                    let creditor1total = this.data.data['accounts'][0]['total'];
                    let creditor1p = ((100 / tspTot) * creditor1total).toFixed(1);

                    description = '<p style="font-size: 15px;">The top creditor as at ' + this.fromDate + ' is ' + creditor1name + ' with an outstanding balance of ' + formatCurrency(creditor1total, 'R ') + '.</p>' +
                    '<p style="font-size: 15px;">' + creditor1name + ' accounts for ' + creditor1p + '% of the creditors.</p>';
                }
                if(typeof this.data.data['accounts'][1] !== 'undefined'){
                    let creditor2name = this.data.data['accounts'][1]['name'];
                    let creditor2total = this.data.data['accounts'][1]['total'];
                    let creditor2p = ((100 / tspTot) * creditor2total).toFixed(1);

                    description += '<p style="font-size: 15px;">The creditor with the 2nd highest outstanding balance (' + formatCurrency(creditor2total, 'R ') + ') is ' + creditor2name + '.</p>' +
                    '<p style="font-size: 15px;">' + creditor2name + ' accounts for ' + creditor2p + '% of the creditors.</p>';
                }
                if(typeof this.data.data['accounts'][2] !== 'undefined'){
                    let creditor3name = this.data.data['accounts'][2]['name'];
                    let creditor3total = this.data.data['accounts'][2]['total'];
                    let creditor3p = ((100 / tspTot) * creditor3total).toFixed(1);

                    description += '<p style="font-size: 15px;">The creditor with the 3rd highest outstanding balance (' + formatCurrency(creditor3total, 'R ') + ') is ' + creditor3name + '.</p>' +
                    '<p style="font-size: 15px;">' + creditor3name + ' accounts for ' + creditor3p + '% of the creditors.</p>';
                }

            break;
            
        //Inventory Management
            case 'im':

                let imRows = '';
                
                for(let i = 0; i < this.data.data['item'].length; i++){
                    let imName = this.data.data['item'][i]['name'];
                    let imQoh = this.data.data['item'][i]['quantity_on_hand'];
                    let imSP = this.data.data['item'][i]['sell_price'];
                    let imPP = this.data.data['item'][i]['purchase_price'];
                    
                    if(imName == null){
                        imName = '-';
                    }
                    if(imQoh == null){
                        imQoh = '-';
                    }
                    if(imPP == null){
                        imPP = '-';
                    }
                    if(imSP == null){
                        imSP = '-';
                    }

                    imRows += 
                    '<tr>' +
                        '<td>' + imName + '</td>' +
                        '<td><span class="float-right">' + imQoh + '</span></td>' +
                        '<td><span class="float-right">R ' + imPP + '</span></td>' +
                        '<td><span class="float-right">R ' + imSP + '</span></td>' +
                    '</tr>';
                }

                description = 
                '<div id="annex-row' + this.number + '" class="row" style="overflow-y:auto; overflow-x=hidden;">' +
                    '<table class="small table txt-color">' +
                        '<thead>' +
                            '<tr>' +
                                '<th>Product Code</th>' +
                                '<th><span class="float-right">Quantity on hand</span></th>' +
                                '<th><span class="float-right">Purchase Price</span></th>' +
                                '<th><span class="float-right">Selling Price</span></th>' +
                            '</tr>' +
                        '<thead>' +
                        imRows + 
                    '</table>' +
                '</div>';

            break;

        //Inventory Analysis - Sales
            case 'inas':

                let inasTot = 0;
                
                Object.size = function(obj) {
                    var size = 0, key;
                    for (key in obj) {
                        if (obj.hasOwnProperty(key)) size++;
                    }
                    return size;
                };
                
                var size = (Object.size(this.data.data)) - 1;
                
                for(let i = 0; i < size; i++){
                    inasTot += parseInt(this.data.data[i]['total_sales']);
                }

                if(typeof this.data.data[0] !== 'undefined'){
                    let sItem1name = this.data.data[0]['item_code'];
                    let sItem1sales = this.data.data[0]['total_sales'];
                    let sItem1salesP = ((100 / inasTot) * sItem1sales).toFixed(1);

                    description = '<p style="font-size: 15px;">The top item sold for the month of ' + this.fromDate + ' is ' + sItem1name + '.</p>' +
                    '<p style="font-size: 15px;">' + sItem1name + ' accounts for ' + sItem1salesP + '% of the sales with an amount of ' + formatCurrency(sItem1sales, 'R ') + '.</p>';
                }
                if(typeof this.data.data[1] !== 'undefined'){
                    let sItem2name = this.data.data[1]['item_code'];
                    let sItem2sales = this.data.data[1]['total_sales'];
                    let sItem2salesP = ((100 / inasTot) * sItem2sales).toFixed(1);

                    description += '<p style="font-size: 15px;">The top item sold for the month of ' + this.fromDate + ' is ' + sItem2name + '.</p>' +
                    '<p style="font-size: 15px;">' + sItem2name + ' accounts for ' + sItem2salesP + '% of the sales with an amount of ' + formatCurrency(sItem2sales, 'R ') + '.</p>';
                }
                if(typeof this.data.data[2] !== 'undefined'){
                    let sItem3name = this.data.data[2]['item_code'];
                    let sItem3sales = this.data.data[2]['total_sales'];
                    let sItem3salesP = ((100 / inasTot) * sItem3sales).toFixed(1);

                    description += '<p style="font-size: 15px;">The top item sold for the month of ' + this.fromDate + ' is ' + sItem3name + '.</p>' +
                    '<p style="font-size: 15px;">' + sItem3name + ' accounts for ' + sItem3salesP + '% of the sales with an amount of ' + formatCurrency(sItem3sales, 'R ') + '.</p>';
                }
                description += '<p style="font-size: 15px;">Total recorded sales for the month of ' + this.fromDate + ' is ' + formatCurrency(inasTot, 'R ') + '.</p>';

            break;

        //Inventory Analysis - Quantity
            case 'inaq':

                let inaqTot = 0;
                
                for(let i = 0; i < this.data.data['item'].length; i++){
                    if(this.data.data['item'][i]['quantity_on_hand'] != null){
                        inaqTot += parseInt(this.data.data['item'][i]['quantity_on_hand']);
                    }
                }

                if(typeof this.data.data['item'][0] !== 'undefined'){
                    let qItem1name = this.data.data['item'][0]['name'];
                    let qItem1qoh = this.data.data['item'][0]['quantity_on_hand'];
                    let qItem1qohP = ((100 / inaqTot) * qItem1qoh).toFixed(1);

                    description = '<p style="font-size: 15px;">The highest quantity item for the month of ' + this.fromDate + ' is ' + qItem1name + '.</p>' +
                    '<p style="font-size: 15px;">' + qItem1name + ' accounts for ' + qItem1qohP + '% of all the items.</p>';
                }
                if(typeof this.data.data['item'][1] !== 'undefined'){
                    let qItem2name = this.data.data['item'][1]['name'];
                    let qItem2qoh = this.data.data['item'][1]['quantity_on_hand'];
                    let qItem2qohP = ((100 / inaqTot) * qItem2qoh).toFixed(1);

                    description += '<p style="font-size: 15px;">The 2nd highest quantity item for the month of ' + this.fromDate + ' is ' + qItem2name + '.</p>' +
                    '<p style="font-size: 15px;">' + qItem2name + ' accounts for ' + qItem2qohP + '% of all the items.</p>';
                }
                if(typeof this.data.data['item'][2] !== 'undefined'){
                    let qItem3name = this.data.data['item'][2]['name'];
                    let qItem3qoh = this.data.data['item'][2]['quantity_on_hand'];
                    let qItem3qohP = ((100 / inaqTot) * qItem3qoh).toFixed(1);

                    description += '<p style="font-size: 15px;">The rd highest quantity item for the month of ' + this.fromDate + ' is ' + qItem3name + '.</p>' +
                    '<p style="font-size: 15px;">' + qItem3name + ' accounts for ' + qItem3qohP + '% of all the items.</p>';
                }

            break;

        //Profit and Loss Ratios
            case 'plr':

                let plrRows = '';
                let plrSize = this.data.labels.length;

                plrRows += 
                '<tr>' +
                    '<td class="text-left">Gross Margin Ratio</td>';
                        for(let i = 0; plrSize; i++){
                            plrRows  += '<td>' + this.data.data['gross_margin'][plrSize - 1 - i].toFixed(1) + '%</td>';
                            if(i == (plrSize - 1) || i == 1){
                                break;
                            }
                        }
                plrRows += 
                    '<td class="pr-0">' + calculateVar(this.data.data['gross_margin'][plrSize - 1], this.data.data['gross_margin'][plrSize - 2], true, '%', 1, false) + '</td>' +
                    '<td class="pr-0">' + calculateVarPercentage(this.data.data['gross_margin'][plrSize - 1], this.data.data['gross_margin'][plrSize - 2],  true, 1) + '</td>' +
                    // '<td>-</td>' +
                '</tr>' +
                '<tr>' +
                    '<td class="text-left">Operating Margin Ratio</td>';
                        for(let i = 0; plrSize; i++){
                            plrRows += '<td>' + this.data.data['operating_margin'][plrSize - 1 - i].toFixed(1) + '%</td>';
                            if(i == (plrSize - 1) || i == 1){
                                break;
                            }
                        }
                plrRows += 
                    '<td class="pr-0">' + calculateVar(this.data.data['operating_margin'][plrSize - 1], this.data.data['operating_margin'][plrSize - 2], false, '%', 1, false) + '</td>' +
                    '<td class="pr-0">' + calculateVarPercentage(this.data.data['operating_margin'][plrSize - 1], this.data.data['operating_margin'][plrSize - 2], false, 1) + '</td>' +
                    // '<td>-</td>' +
                '</tr>' +
                '<tr>' +
                    '<td class="text-left">Net Margin Ratio</td>';
                        for(let i = 0; plrSize; i++){
                            plrRows += '<td>' + this.data.data['net_margin'][plrSize - 1 - i].toFixed(1) + '%</td>';
                            if(i == (plrSize - 1) || i == 1){
                                break;
                            }
                        }
                plrRows += 
                    '<td class="pr-0">' + calculateVar(this.data.data['net_margin'][plrSize - 1], this.data.data['net_margin'][plrSize - 2], true, '%', 1, false) + '</td>' +
                    '<td class="pr-0">' + calculateVarPercentage(this.data.data['net_margin'][plrSize - 1], this.data.data['net_margin'][plrSize - 2], true, 1) + '</td>' +
                    // '<td>-</td>' +
                '</tr>';

                description = 
                '<div id="annex-row' + this.number + '" class="row ml-1" style="overflow-y:auto;">' +
                    '<table class="small table txt-color mb-4 text-right">' +
                        '<thead>' +
                            '<tr>' +
                                '<th></th>';
                                for(let i = 0; plrSize; i++){
                                    if(this.data.labels[plrSize - i - 1].length >= 5){
                                        description += '<th>' + this.data.labels[plrSize - i - 1] + '</th>';
                                    }
                                    else{
                                        let year = this.data.labels[plrSize - i - 2].slice(-4);
                                        let yDate = this.data.labels[plrSize - i - 1] + ' ' + year;
                                        description += '<th>' + yDate + '</th>';
                                    }
                                    if(i == (plrSize - 1) || i == 1){
                                        break;
                                    }
                                }
                                description += 
                                '<th>Variance</th>' +
                                '<th>Variance %</th>' +
                                // '<th>YTD</th>' +
                            '</tr>' +
                        '</thead>' +
                            plrRows + 
                    '</table>' + 
                    '<span class="font-weight-bold mb-1" style="font-size: 15px;">Gross Profit Margin = Gross Profit / Sales </span>' + 
                        '<p style="font-size: 15px;">The gross profit margin ratio represents the amount of profit that an entity retains after incurring the direct costs associated with producing and selling its goods or services. The higher the ratio, the more the entity retains to service its other costs and debt obligations.</p>' + 
                    '<span class="font-weight-bold my-1" style="font-size: 15px;">Operating Expense Ratio = (Operating Expenses - Amortisation - Depreciation) / Sales </span>' + 
                        '<p style="font-size: 15px;">The operating expense ratio represents the efficiency of an entity\'s management by comparing operating expenses to sales. The ratio illustrates how efficient an entity\'s management is at keeping costs low while generating sales. More efficient entities have lower ratios.</p>' + 
                    '<span class="font-weight-bold my-1" style="font-size: 15px;">Net Profit Margin = Net Profit / Sales </span>' + 
                        '<p style="font-size: 15px;">The net profit margin ratio represents the amount of profit that an entity retains after incurring all costs. The higher the ratio, the more profit is generated per each unit of sales.</p>' +
                '</div>';

            break;

        //Balance Sheet Ratios
            case 'bsr':

                let bsrRows = '';
                let bsrSize = this.data.labels.length;

                bsrRows += 
                '<tr>' +
                    '<td class="text-left">Current Ratio</td>';
                        for(let i = 0; bsrSize; i++){
                            bsrRows  += '<td>' + this.data.data['current_ratio'][bsrSize - 1 - i].toFixed(2) + '</td>';
                            if(i == (bsrSize - 1) || i == 1){
                                break;
                            }
                        }
                bsrRows += 
                    '<td class="pr-0">' + calculateVar(this.data.data['current_ratio'][bsrSize - 1], this.data.data['current_ratio'][bsrSize - 2], true, '', 2) + '</td>' +
                    '<td class="pr-0">' + calculateVarPercentage(this.data.data['current_ratio'][bsrSize - 1], this.data.data['current_ratio'][bsrSize - 2], true, 1) + '</td>' +
                '</tr>' +
                '<tr>' +
                    '<td class="text-left">Debt to Equity</td>';
                        for(let i = 0; bsrSize; i++){
                            bsrRows += '<td>' + this.data.data['debt_to_equity'][bsrSize - 1 - i].toFixed(2) + '</td>';
                            if(i == (bsrSize - 1) || i == 1){
                                break;
                            }
                        }
                bsrRows += 
                    '<td class="pr-0">' + calculateVar(this.data.data['debt_to_equity'][bsrSize - 1], this.data.data['debt_to_equity'][bsrSize -2], false, '', 2) + '</td>' +
                    '<td class="pr-0">' + calculateVarPercentage(this.data.data['debt_to_equity'][bsrSize - 1], this.data.data['debt_to_equity'][bsrSize - 2], false, 1) + '</td>' +
                '</tr>';
                // '<tr>' +
                //     '<td class="text-left">Quick Ratio</td>';
                //         for(let i = 0; bsrSize; i++){
                //             bsrRows += '<td>' + this.data.data['quick_ratio'][bsrSize - 1 - i].toFixed(2) + '</td>';
                //             if(i == (bsrSize - 1) || i == 1){
                //                 break;
                //             }
                //         }
                // bsrRows += 
                //     '<td class="pr-0">' + calculateVar(this.data.data['quick_ratio'][bsrSize - 1], this.data.data['quick_ratio'][bsrSize - 2], true, '', 2) + '</td>' +
                //     '<td class="pr-0">' + calculateVarPercentage(this.data.data['quick_ratio'][bsrSize - 1], this.data.data['quick_ratio'][bsrSize - 2], true, 1) + '</td>' +
                // '</tr>';
                                
                description = 
                '<div id="annex-row' + this.number + '" class="row ml-1" style="overflow-y:auto;">' +
                    '<table class="small table txt-color mb-4 text-right">' +
                        '<thead>' +
                            '<tr>' +
                                '<th></th>';
                                for(let i = 0; bsrSize; i++){
                                    if(this.data.labels[bsrSize - i - 1].length >= 5){
                                        description += '<th>' + this.data.labels[bsrSize - i - 1] + '</th>';
                                    }
                                    else{
                                        let year = this.data.labels[bsrSize - i - 2].slice(-4);
                                        let yDate = this.data.labels[bsrSize - i - 1] + ' ' + year;
                                        description += '<th>' + yDate + '</th>';
                                    }
                                    if(i == (bsrSize - 1) || i == 1){
                                        break;
                                    }
                                }
                                description += 
                                '<th>Variance</th>' +
                                '<th>Variance %</th>' +
                            '</tr>' +
                        '</thead>' +
                        bsrRows + 
                    '</table>' + 
                    '<span class="font-weight-bold mb-1" style="font-size: 15px;">Current Ratio = Current Assets / Current Liabilities</span>' + 
                        '<p style="font-size: 15px;">The current ratio represents an entity\'s ability to pay short-term obligations (those due within one year). Lenders, investors, and creditors often use this ratio to understand how an entity can maximize the current assets on its balance sheet to satisfy its current debt and other payables.</p>' +              
                    '<span class="font-weight-bold mt-1 mb-1" style="font-size: 15px;">Debt to Equity (D/E) = Total Liabilities / Total Equity</span>' + 
                        '<p style="font-size: 15px;">The debt to equity ratio represents an entity\'s financial leverage. The ratio is a measure of the degree to which an entity is financing its operations through debt versus wholly owned funds. A lower ratio reflects the ability of shareholder equity to cover all outstanding debts.' + 
                        'There are three major financial metrics that drive ROE: operating efficiency, asset use efficiency and financial leverage.</p>' + 
                    // '<p><p><span class="font-weight-bold">Return on Equity = Operating Efficiency x Asset Use Efficiency x Financial Leverage</span>' + 
                    //     'The return on equity ratio is considered a measure of how effectively management is using an entity’s assets, leverage and expenses to create profits for shareholders. ROE can be increased by improving operating efficiency, asset use efficiency or by increasing financing leverage via more debt funding.' + 
                    // '<span class="font-weight-bold">Operating Efficiency = Net Profit / Sales</span>' + 
                    //     'The operating efficiency ratio is identical to the net margin ratio and represents the amount of profit that an entity retains after incurring all costs. The higher the ratio, the more profit is generated per each unit of sales.' + 
                    // '<span class="font-weight-bold">Quick Ratio = (Cash + Receivables) / Current Liabilities</span>' + 
                    //             'The quick ratio represents an entity\'s ability to pay short-term obligations (those due within one year) using its most liquid assets. ' +
                    //             'The most liquid assets are assets that can be converted quickly to cash. The quick ratio is also known as the acid test ratio.' +
                    // '<p><p><span class="font-weight-bold">Asset Use Efficiency = Sales / Average Total Assets</span>' + 
                    //     'The asset use efficiency ratio is used to demonstrate how effective an entity is at utilizing its assets to generate sales. The higher the ratio, the lower the quantum of assets required to generate sales.' + 
                    // '<p><p><span class="font-weight-bold">Financial Leverage = Average Total Assets / Absolute Value of Average Equity</span>' + 
                    //     'The financial leverage ratio demonstrates how much debt the entity has utilized to finance its business. The higher the ratio, the more debt the entity is using in its funding structure.' +
                '</div>';

            break;

            default:
            
                description = 'Could not find a description';

            break;
        }

        return description;
    }

    getDateIndexFormat(date){

        console.log('Received: ' + date);
        let recMonth = date.substr(0, 3);

        let months = new Array();

        months['Jan'] = '01';
        months['Feb'] = '02';
        months['Mar'] = '03';
        months['Apr'] = '04';
        months['May'] = '05';
        months['Jun'] = '06';
        months['Jul'] = '07';
        months['Aug'] = '08';
        months['Sep'] = '09';
        months['Oct'] = '10'; 
        months['Nov'] = '11';
        months['Dec'] = '12';

        let month = months[recMonth];
        let year = date.substr(4, 4);

        return year + '-' + month + '-01';

    }
}
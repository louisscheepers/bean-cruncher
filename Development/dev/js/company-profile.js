$(document).ready(function(){
    fetchCompany();
});


function fetchCompany(){
    document.getElementById('loader').hidden = false;
    var action = "fetch";

    $.when(
        $.ajax({
            url:"json.php",
            method:"POST",
            data:{company:action},
            success:function(data){
                document.getElementById('loader').hidden = true;
                
                let obj = jQuery.parseJSON(data);
                checkImagePermissions(obj['package']);

                document.getElementById('company-name').value = obj['company_name'];
                document.getElementById('company-email').value = obj['company_email'];
                if(obj['company_brand'] != '<img id="comp-img" src="data:image/jpeg;base64," class="rounded-circle icon-size" alt="Company Brand">'){
                    document.getElementById('nav-comp-icon').innerHTML = obj['company_brand'];
                    document.getElementById('comp-icon').innerHTML = obj['company_brand'];
                }
                else{
                    document.getElementById('nav-comp-icon').innerHTML = '<img id="nav-comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
                }
                let compImgs = $('[id^="comp-img"]');
                if(compImgs[1]){
                    compImgs[1].id = 'comp-img-up';
                }
                document.getElementById('company-title').innerText = obj['company_name'];
                document.getElementById('companyDropDown').innerText = obj['company_name'];

            }
        })
    ).then(
        $.getJSON('json.php?gComp=get', function(result){
            document.getElementById('companiesDropDown').innerHTML = '';
            for(let i = 0; i < result.length; i++){
                document.getElementById('companiesDropDown').innerHTML += 
                '<li>' +
                    '<a id="active-company-dropdown" class="dropdown-item" onclick="loadCompany(\'' + result[i]['name'].replace('\'', '\\\'') + '\', ' + result[i]['id'] + ')">' + result[i]['name'] + '</a>' +
                '</li>';
            }
        })
    )
}

function checkImagePermissions(package){
    console.log("Checking Permissions")
    if(package <= 2 ){
        $("#companyImage").addClass("d-none");
    }else{
        $("#companyImage").removeClass("d-none");

    }
}
function updateCompany(){
    
    var action = "update";

    let name = document.getElementById('company-name').value;
    let email = document.getElementById('company-email').value;

    $.ajax({
        type: 'POST',
        url: 'json.php',
        data: {company:action,name:name,email:email},
        success: function(){
            console.log('updated');
            // alert('Company profile updated');
            document.getElementById('company-title').innerHTML = name;
        }
    });
    document.getElementById('companyDropDown').innerHTML = name;

    
    $.getJSON('json.php?gComp=get', function(result){
        console.log(result);
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            filename = input.files[0]['name'];
            $('#comp-img-up')
                .attr('src', e.target.result)
                .width(115)
                .height(115);
            // document.getElementById('comp-file').innerHTML = filename;
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function loadCompany(name, id){
    document.getElementById('company-settings').hidden = true;
    document.getElementById('err').hidden = true;

    $.getJSON('json.php?aComp=' + id, function(){});

    document.getElementById('companyDropDown').innerHTML = name;
    $.getJSON('json.php?aRole=' + id, function(role){
        if(role == 3){
            document.getElementById('err').hidden = false;
        }
        else{
            document.getElementById('company-settings').hidden = false;
        }
        document.getElementById('comp-img-up').src = './img/default-company-icon.jpg';
        fetchCompany();
    });
}

$(document).ready(function(){
    populateRows();
    document.getElementById('success-alert').style.display = 'inline'; 
    $("#success-alert").hide();
});

function populateRows(){

    let salesRows = document.getElementById('sales');
    let cosRows = document.getElementById('costOfSales');
    let otherIncomeRow = document.getElementById('otherIncome');
    let expensesRow = document.getElementById('expenses');
    let taxRow = document.getElementById('tax');
    let cAssetRows = document.getElementById('currentAssets');
    let ncAssetRows = document.getElementById('nonCurrentAssets');
    let ncLiabilityRows = document.getElementById('nonCurrentLiabilities');
    let cLiabilityRows = document.getElementById('currentLiabilities');
    let equityRows = document.getElementById('equity');

    
    salesRows.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';
    cosRows.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';
    otherIncomeRow.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';
    expensesRow.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';
    taxRow.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';
    cAssetRows.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';
    ncAssetRows.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';
    ncLiabilityRows.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';
    cLiabilityRows.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';
    equityRows.innerHTML = '<div class="mx-4 my-auto"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>';


    $.getJSON('json.php?coa=get', function(result){

        salesRows.innerHTML = '';
        cosRows.innerHTML = '';
        otherIncomeRow.innerHTML = '';
        expensesRow.innerHTML = '';
        taxRow.innerHTML = '';
        cAssetRows.innerHTML = '';
        ncAssetRows.innerHTML = '';
        ncLiabilityRows.innerHTML = '';
        cLiabilityRows.innerHTML = '';
        equityRows.innerHTML = '';

        result.sort(function(a, b) {
            var textA = a['name'].toUpperCase();
            var textB = b['name'].toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
       
        for(let i = 0; i < result.length; i++){

            if(result[i]['type'] == 'Sales'){
                salesRows.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Sales" selected>Revenue</option>' +
                            '<option value="Direct Costs">Cost of Sales</option>' +
                            '<option value="Other Income">Other Income</option>' +
                            '<option value="Expense">Expense</option>' +
                            '<option value="Tax">Tax</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Revenue from business activities' +
                    '</td>' +
                '</tr>';
            }
            else if(result[i]['type'] == 'Direct Costs'){
                cosRows.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Sales">Revenue</option>' +
                            '<option value="Direct Costs" selected>Cost of Sales</option>' +
                            '<option value="Other Income">Other Income</option>' +
                            '<option value="Expense">Expense</option>' +
                            '<option value="Tax">Tax</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Cost related to sales' +
                    '</td>' +
                '</tr>';
            }
            else if(result[i]['type'] == 'Other Income'){
                otherIncomeRow.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Sales">Revenue</option>' +
                            '<option value="Direct Costs">Cost of Sales</option>' +
                            '<option value="Other Income" selected>Other Income</option>' +
                            '<option value="Expense">Expense</option>' +
                            '<option value="Tax">Tax</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Income not related to sales' +
                    '</td>' +
                '</tr>';
            }
            else if(result[i]['type'] == 'Expense'){
                expensesRow.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Sales">Revenue</option>' +
                            '<option value="Direct Costs">Cost of Sales</option>' +
                            '<option value="Other Income">Other Income</option>' +
                            '<option value="Expense" selected>Expense</option>' +
                            '<option value="Tax">Tax</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Business expenses' +
                    '</td>' +
                '</tr>';
            }
            else if(result[i]['type'] == 'Tax'){
                taxRow.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Sales" selected>Revenue</option>' +
                            '<option value="Direct Costs">Cost of Sales</option>' +
                            '<option value="Other Income">Other Income</option>' +
                            '<option value="Expense">Expense</option>' +
                            '<option value="Tax" selected>Tax</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Income tax paid to authorities' +
                    '</td>' +
                '</tr>';
            }
            else if(result[i]['type'] == 'Current Asset'){
                cAssetRows.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Current Asset" selected>Current Asset</option>' +
                            '<option value="Non-current Asset">Non-current Asset</option>' +
                            '<option value="Non-current Liability">Non-current Liability</option>' +
                            '<option value="Current Liability">Current Liability</option>' +
                            '<option value="Equity">Equity</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Assets under 12 months' +
                    '</td>' +
                '</tr>';
            }
            else if(result[i]['type'] == 'Non-current Asset'){
                ncAssetRows.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Current Asset">Current Asset</option>' +
                            '<option value="Non-current Asset" selected>Non-current Asset</option>' +
                            '<option value="Non-current Liability">Non-current Liability</option>' +
                            '<option value="Current Liability">Current Liability</option>' +
                            '<option value="Equity">Equity</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Assets above 12 months' +
                    '</td>' +
                '</tr>';
            }
            else if(result[i]['type'] == 'Non-current Liability'){
                ncLiabilityRows.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Current Asset">Current Asset</option>' +
                            '<option value="Non-current Asset">Non-current Asset</option>' +
                            '<option value="Non-current Liability" selected>Non-current Liability</option>' +
                            '<option value="Current Liability">Current Liability</option>' +
                            '<option value="Equity">Equity</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Liabilities above 12 months' +
                    '</td>' +
                '</tr>';
            }
            else if(result[i]['type'] == 'Current Liability'){
                cLiabilityRows.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Current Asset">Current Asset</option>' +
                            '<option value="Non-current Asset">Non-current Asset</option>' +
                            '<option value="Non-current Liability">Non-current Liability</option>' +
                            '<option value="Current Liability" selected>Current Liability</option>' +
                            '<option value="Equity">Equity</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Liabilities under 12 months' +
                    '</td>' +
                '</tr>';
            }
            else if(result[i]['type'] == 'Equity'){
                equityRows.innerHTML +=
                '<tr class="txt-color font-weight-200 font-size-14">' +
                    '<td>' + 
                        result[i]['name'] +
                    '</td>' +
                    '<td>' +
                        '<select class="custom-select m-0 dp-file-label text-overfl" id="select' + result[i]['id'] + '" onchange="selectChange(\'' + result[i]['id'] + '\')">' +
                            '<option value="Current Asset">Current Asset</option>' +
                            '<option value="Non-current Asset">Non-current Asset</option>' +
                            '<option value="Non-current Liability">Non-current Liability</option>' +
                            '<option value="Current Liability">Current Liability</option>' +
                            '<option value="Equity" selected>Equity</option>' +
                        '</select>' + 
                    '</td>' +
                    '<td>' +
                        'Owner\'s equity' +
                    '</td>' +
                '</tr>';
            }
            else{
                console.log('No type ' + result[i]['type'] + ' for account ' + result[i]['name']);
            }
        }
        if(salesRows.innerText == ''){
            salesRows.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(cosRows.innerText == ''){
            cosRows.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(otherIncomeRow.innerText == ''){
            otherIncomeRow.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(expensesRow.innerText == ''){
            expensesRow.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(taxRow.innerText == ''){
            taxRow.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(cAssetRows.innerText == ''){
            cAssetRows.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(ncAssetRows.innerText == ''){
            ncAssetRows.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(cLiabilityRows.innerText == ''){
            cLiabilityRows.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(ncLiabilityRows.innerText == ''){
            ncLiabilityRows.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(otherIncomeRow.innerText == ''){
            otherIncomeRow.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
        if(equityRows.innerText == ''){
            equityRows.innerHTML = '<tr class="txt-color font-weight-200 font-size-14"><td>No accounts<td><tr>';
        }
    });
}

function selectChange(id){
    console.log('Changed account ' + id + '\'s type to ' + document.getElementById('select' + id).value);


    let newType = document.getElementById('select' + id).value;

    $.ajax({
        type: "GET",
        url: 'json.php',
        data: {uCoa:id, type:newType},
        success: function(){
            if(document.getElementById('success-alert')){
                showAlert();
            }
            else{
                document.getElementById('alert-container').innerHTML = 
                '<div class="alert alert-success fixed-top w-15 mx-auto" id="success-alert" style="z-index:1040; display:none" >' +
                    'Account updated ' +
                    '<button type="button" class="close" data-dismiss="alert" onclick="closeAlert()"><i class="fas fa-times"></i></button>' +
                '</div>';
                showAlert();
            }
            populateRows();
        }
    });
}
function selectElement(id, valueToSelect){
    let element = document.getElementById(id);
    element.value = valueToSelect;
}
function closeAlert(){
    $("#success-alert").hide();
}
function showAlert(){
    $("#success-alert").show();
    $("#success-alert").fadeTo(1000, 500).slideUp(300, function() {
        $("#success-alert").slideUp(100);
    });
}
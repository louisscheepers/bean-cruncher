
// Object for each drag and drop
let data = null;
let dragItem = null;
let boxid = '';
let boxModule;

$(document).ready(function(){
    for(let i = 1; i <= 8; i++){
        $('#popoverBox' + i).popover('disable').popover('hide');
    }
});

// Drag and Drop Functions
function allowDrop(ev){
    ev.preventDefault();
}
function drag(ev){
    dragItem = ev.target.id;
    ev.dataTransfer.setData("text", dragItem);

    console.log('Dragging item ' + dragItem);
}

function dragBox(ev){
    
    dragItem = ev.target.id.substring(4);
    console.log('Dragging box ' + dragItem);
    if(dragItem != null){
        ev.dataTransfer.setData("text", dragItem);

        //dragging a box canvas area - this was a problem on Safari
        if(dragItem.substring(0, 6) == 'canvas'){
            dragItem = dragItem.substring(6);
        }
        console.log('Dragging box ' + dragItem);
    }
    else{
        console.log("Empty box dragged!");
    }
}
function drop(ev, el){
    ev.preventDefault();
    data = ev.dataTransfer.getData("text");

    boxid = el.id + dragItem;
    console.log('Element ID = ' + boxid);

    boxidClean = boxid.substring(0,4);
    displayModule(boxidClean, dragItem);
}

function displayModuleMobile(module){
    if(isMobile){
        clearBox(0, true);
        displayModule('box3', module);
    }
}


function displayModule(box, module, fromDate, toDate, removeHeight){
    if(typeof removeHeight == 'undefined'){
        removeHeight = true;
    }
    clearBox(box.substr(3, 1), null, false, removeHeight);

    //default fromDate
    if(typeof fromDate === 'undefined' || fromDate == null){
        let d1 = new Date;
        let yearF = d1.getFullYear();
        let monthF;
        if(module == 'cico' || module == 'ino' || module == 'ret' || module == 'plr' || module == 'bsr'){
            monthF = (d1.getMonth()-2) + '';
        }
        else if(module == 'pl' || module == 'bs' || module == 'avd' || module == 'ar' || module == 'ddo' || module == 'tcs' || module == 'ap' || module == 'cdo' || module == 'tsp' || module == 'im' || module == 'inas' || module == 'inaq'){
            monthF = (d1.getMonth()) + '';
        }
        if(monthF <= 0){
            yearF--;
            monthF = 12 + parseInt(monthF);
        }

        if(monthF.length == 1){
            monthF = '0' + monthF;
        }
        fromDate = yearF + '-' + monthF + '-01';
    }
    //default toDate
    if(typeof toDate === 'undefined' || toDate == null){
        let d2 = new Date;
        let yearT = d2.getFullYear();
        let monthT;
        if(module == 'pl' || module == 'bs'){
            monthT = (d2.getMonth()-1) + '';
        }
        else{
            monthT = (d2.getMonth()) + '';
        }

        if(monthT <= 0){
            yearT--;
            monthT = 12 + parseInt(monthT);
        }

        if(monthT.length == 1){
            monthT = '0' + monthT;
        }
        toDate = yearT + '-' + monthT + '-30';
    }

    if(module != ''){
        if(box === "box1" || box === "box2" || box === "box4" || box === "box5"){
            new SmallBox(module, box, fromDate, toDate).smallBox();
        }
        else if(box === "box7" || box === "box8"){
            new MedBox(module, box, fromDate, toDate).medBox();
        }
        else if(box === "box3" || box === "box6"){
            new LargeBox(module, box, fromDate, toDate).largeBox();
        }
        else{
            console.log("Invalid box id");
        }

        //unhide buttons
        let number = box.substring(3, 4);
        document.getElementById('dropDownBox' + number).hidden = false;
        document.getElementById('clear' + number).hidden = false;
        document.getElementById('dates' + number).hidden = false;
        document.getElementById('refreshTag' + number).hidden = false;
        document.getElementById('refresh' + number).hidden = false;
        document.getElementById('pop' + number).hidden = false;
        document.getElementById('com' + number).hidden = false;
        document.getElementById('annexBtn' + number).hidden = false;

        fromYear = document.getElementById('fromY' + number);
        fromMonth = document.getElementById('fromM' + number);
        fromTxt = document.getElementById('fromText' + number);

        toYear = document.getElementById('toY' + number);
        toMonth = document.getElementById('toM' + number);
        toTxt = document.getElementById('toText' + number);

        trigger = document.getElementById('datesS' + number);

        let selectedFYear = fromDate.substr(0, 4);
        let selectedFMonth;
        if(fromDate.substr(5, 1) == 0)
            selectedFMonth = fromDate.substr(6, 1);
        else
            selectedFMonth = fromDate.substr(5, 2);
        let selectedTYear = toDate.substr(0, 4);
        let selectedTMonth;
        if(toDate.substr(5, 1) == 0)
            selectedTMonth = toDate.substr(6, 1);
        else
            selectedTMonth = toDate.substr(5, 2);

        //date range mod
        if(module == 'cico' || module == 'ino' || module == 'ret' || module == 'plr' || module == 'bsr'){
            fromYear.value = selectedFYear;
            fromYear.hidden = false;
            fromYear.disabled = false;
            fromMonth.value = selectedFMonth;
            fromMonth.hidden = false;
            fromMonth.disabled = false;

            fromTxt.innerText = 'From: ';
            fromTxt.hidden = false;
    
            toYear.value = selectedTYear;
            toYear.hidden = false;
            toMonth.value = selectedTMonth;
            toMonth.hidden = false;

            toTxt.innerText = 'To: ';
            toTxt.hidden = false;

            trigger.hidden = false;
        }
        //comparison mod
        else if(module == 'pl' || module == 'bs'){

            fromYear.value = selectedFYear;
            fromYear.hidden = false;
            fromYear.disabled = false;
            fromMonth.value = selectedFMonth;
            fromMonth.hidden = false;
            fromMonth.disabled = false;

            fromTxt.innerText = 'Date: ';
            fromTxt.hidden = false;
    
            toYear.value = selectedTYear;
            toYear.hidden = false;
            toMonth.value = selectedTMonth;
            toMonth.hidden = false;

            toTxt.innerText = 'Compare with: ';
            toTxt.hidden = false;

            trigger.hidden = false;
        }
        //single date mod
        else if(module == 'avd' || module == 'inas'){
            fromYear.value = selectedFYear;
            fromYear.hidden = false;
            fromYear.disabled = false;
            fromMonth.value = selectedFMonth;
            fromMonth.hidden = false;
            fromMonth.disabled = false;

            fromTxt.innerText = 'Date: ';
            fromTxt.hidden = false;
    
            toYear.value = selectedTYear;
            toYear.hidden = true;
            toMonth.value = selectedTMonth;
            toMonth.hidden = true;

            toTxt.innerText = 'To: ';
            toTxt.hidden = true;

            trigger.hidden = false;
        }
        //cannot change date ('inaq', 'im', 'ar', 'ddo', 'tcs', 'ap', 'cdo', 'tsp')
        else{
            fromYear.value = selectedFYear;
            fromYear.hidden = true;
            fromYear.disabled = true;
            fromMonth.value = selectedFMonth;
            fromMonth.hidden = true;
            fromMonth.disabled = true;

            fromTxt.innerText = 'Showing data for today';
            fromTxt.hidden = false;
    
            toYear.value = selectedTYear;
            toYear.hidden = true;
            toMonth.value = selectedTMonth;
            toMonth.hidden = true;

            toTxt.innerText = 'To: ';
            toTxt.hidden = true;

            trigger.hidden = true;
        }

        //set id
        let htmlBoxId = document.getElementById(box);
        htmlBoxId.id = box + module;
    }
}

function clearBox(btnNo, isMobile, displayClear, removeHeight){

    if(typeof displayClear == 'undefined'){
        displayClear = false;
    }
    if(typeof removeHeight == 'undefined'){
        removeHeight = true;
    }

    let boxes;
    let boxToClear;
    let action = 'delete';
    //clear all
    if(btnNo === '0'){
        console.log('Clearing all boxes');
        for(let i = 1; i <= 8; i++){
            boxes = document.querySelectorAll('[id^="box' + i + '"]');
            boxToClear = boxes[0].id;

            if(boxes[0].id.substr(4).length != 0){
                expand(i, false, true);
            }

            //box
            document.getElementById(boxToClear).innerHTML = '';
            document.getElementById(boxToClear).id = 'box' + i;

            //pdf box
            $("#pdfbox" + i).removeClass('col-6');
            document.getElementById('pdfbox' + i).innerHTML = '';
            
            // document.getElementById('pdfbox' + i).classList.remove("col-6");

            //label
            document.getElementById('labelupdate' + i).innerHTML = '';

            //annexure
            document.getElementById('annex' + i).innerHTML = '';
            document.getElementById('annex' + i).style.height = 'auto';

            //hide buttons
            document.getElementById('dropDownBox' + i).hidden = true;
            document.getElementById('clear' + i).hidden = true;
            document.getElementById('dates' + i).hidden = true;
            document.getElementById('refreshTag' + i).hidden = true;
            document.getElementById('refresh' + i).hidden = true;
            document.getElementById('pop' + i).hidden = true;
            document.getElementById('com' + i).hidden = true;
            document.getElementById('annexBtn' + i).hidden = true;

            //clear data
            document.getElementById('modal-data' + i).data = null;

            //disable popover
            $('#popoverBox' + btnNo).popover('disable');
        }
    }
    //clear box id
    else{
        expand(btnNo, false, true);

        if(isMobile){
            boxes = document.querySelectorAll('[id^="box3"]');
            boxToClear = boxes[0].id;
        }
        else{
            boxes = document.querySelectorAll('[id^="box'+ btnNo +'"]');
            boxToClear = boxes[0].id;
        }

        console.log("Clearing Box: " + boxToClear);
        
        //box
        document.getElementById(boxToClear).innerHTML = '';
        document.getElementById(boxToClear).id = boxToClear.substr(0, 4);

        if(!isMobile){
            //pdf box
            if(document.getElementById('pdfbox' + btnNo)){
                document.getElementById('pdfbox' + btnNo).innerHTML = '';
            }
            $("#pdfbox" + btnNo).removeClass('col-6');

            //label
            document.getElementById('labelupdate' + btnNo).innerHTML = '';

            //annexure
            document.getElementById('annex' + btnNo).innerHTML = '';
            if(removeHeight){
                document.getElementById('annex' + btnNo).style.height = 'auto';
            }

            //hide buttons
            document.getElementById('dropDownBox' + btnNo).hidden = true;
            document.getElementById('clear' + btnNo).hidden = true;
            document.getElementById('dates' + btnNo).hidden = true;
            document.getElementById('refreshTag' + btnNo).hidden = true;
            document.getElementById('refresh' + btnNo).hidden = true;
            document.getElementById('pop' + btnNo).hidden = true;
            document.getElementById('com' + btnNo).hidden = true;
            document.getElementById('annexBtn' + btnNo).hidden = true;

            //clear data
            document.getElementById('modal-data' + btnNo).data = null;
            
            //disable popover
            $('#popoverBox' + btnNo).popover('disable');
        }
    }
    if(displayClear){
        $.ajax({
            type: 'GET',
            url: 'json.php',
            data: {q:action,box:btnNo},
            success: function(){
            }
        });
    }
}

function setDate(btnNo){
    document.getElementById('date-pick-error' + btnNo).innerText = '';
    document.getElementById('date-pick-error' + btnNo).hidden = true;

    //find box id from button
    let boxes = document.querySelectorAll('[id^="box'+ btnNo +'"]');

    //get item from first box's id
    let boxModule = boxes[0].id.substring(4);

    //create date in suitable string
    let fYear = document.getElementById('fromY' + btnNo).value;
    let fMonth = document.getElementById('fromM' + btnNo).value;
    if(fMonth.length == 1)
        fMonth = '0' + fMonth;
    let tYear = document.getElementById('toY' + btnNo).value;
    let tMonth = document.getElementById('toM' + btnNo).value;
    if(tMonth.length == 1)
        tMonth = '0' + tMonth;

    let fromDate = fYear + '-' + fMonth;
    let toDate = tYear + '-' + tMonth;

    //first check if comparison modules
    if(boxModule == 'bs' || boxModule == 'pl'){
        resetDropDowns(btnNo);
        clearBox(btnNo);
        displayModule('box' + btnNo, boxModule, fromDate, toDate);
    }
    else{
        //Single date exception
        if(document.getElementById('toY' + btnNo).hidden){
            toDate = fromDate;
        }
    
        //from before to
        if(Date.parse(fromDate) > Date.parse(toDate)){
            document.getElementById('date-pick-error' + btnNo).innerText = 'Invalid date range.';
            document.getElementById('date-pick-error' + btnNo).hidden = false;
        }
        // else if(Date.parse(toDate) > Date.parse()){
        //     document.getElementById('date-pick-error' + btnNo).innerText = 'Cannot exceed current date';
        //     document.getElementById('date-pick-error' + btnNo).hidden = false;
        // }
        else{
            resetDropDowns(btnNo);
            clearBox(btnNo);
            displayModule('box' + btnNo, boxModule, fromDate, toDate);
        }
    }
    

}

function resetDropDowns(btnNo){
    if( $('#dateDropDown' + btnNo).hasClass('show')){
        $('#dateDropDown' + btnNo).removeClass('show');
    }
    if( $('#dropDownBoxMenu' + btnNo).hasClass('show')){
        $('#dropDownBox' + btnNo).dropdown('hide');
    }
}

function refresh(btnNo){
    //find box id from button
    let boxes = document.querySelectorAll('[id^="box'+ btnNo +'"]');

    //get item from first box's id
    let boxModule = boxes[0].id.substring(4);
    let boxidClean = boxes[0].id.substring(0,4);

    if(boxModule.length != 0){
        clearBox(btnNo);
        document.getElementById('box' + btnNo).innerHTML = '<div class="col-12 h-100 d-flex align-items-end flex-column"><div class="mt-auto mb-auto mx-auto"><img src="img/BC-preloader-gif.gif" style="height: 50px;"></div></div>';
        
        
        let fromYear = document.getElementById('fromY' + btnNo).value;
        let fromMonth = document.getElementById('fromM' + btnNo).value;
        if(fromMonth.length == 1)
            fromMonth = '0' + fromMonth;
        let toYear = document.getElementById('toY' + btnNo).value;
        let toMonth = document.getElementById('toM' + btnNo).value;
        if(toMonth.length == 1)
            toMonth = '0'+toMonth;

        let fromDate = fromYear + '-' + fromMonth;
        let toDate = toYear + '-' + toMonth;

        console.log('Updating ' + boxModule + ' From: ' + fromDate + ' To: ' + toDate);
        
        $.ajax({
            type: 'GET',
            url: 'json.php',
            data: {uq:boxModule,from_date:fromDate,to_date:toDate},
            success: function(){
                displayModule(boxidClean, boxModule, fromDate, toDate);
            }
        });
    }
}


function openModal(btnNo){

    //find box id from button
    let boxes = document.querySelectorAll('[id^="box'+ btnNo +'"]');

    //get item from first box's id
    let boxModule = boxes[0].id.substring(4);

    if(boxModule.length == 0){
        btnNo = '0';
        console.log("No data in box!");
    }
    else{
        modal = new Modal();
        modal.drawModal(btnNo, boxModule);
    }
}

function scrollToAnnexure(btnNo){
    document.getElementById('annex' + btnNo).scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
}

function checkQuantity(boxid){
    let boxNo = boxid.substr(3,1);
    dragItem = 'inaq';
    displayModule(boxid, dragItem, null, null, false);
}
function checkSales(boxid){
    let boxNo = boxid.substr(3,1);
    dragItem = 'inas';
    displayModule(boxid, dragItem, null, null, false);
}
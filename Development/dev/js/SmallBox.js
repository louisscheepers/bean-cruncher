class SmallBox{

    constructor(dragItem, box, fromDate, toDate){
        this.box = box;
        this.dragItem = dragItem;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    pdfBox(box, cat, title, pdfGraphZone, pdfchartId){
        document.getElementById('pdf' + box).classList.add("col-6");
        let html =
        '<div class="row mt-3 mb-3">' +
            '<div class="col-10"><p class="text-left p-2 txt-color"><strong>' + cat +' -</strong> ' + title + '</p></div>' +
            '<div class="col-10 pr-3">' +
                '<div class="w-80" id="pdf' + box + 'graphZone" style="z-index: 101;"></div>' +
            '</div>' + 
            '<div class="col-1"></div>' +
            '</div>'; 


        $('#pdf' + box).html(html).slidedown;

        if(title == 'Profit & Loss'){
            $(pdfGraphZone).html(
                    '<table style="font-size: 11px;" class="col-12 ml-3 my-3 small txt-color">' +
                        '<tr>' +
                            '<td class="float-left">Sales</td>' +
                            '<td class="float-right" style="margin-right: -0.08rem;" id="' + this.box + 'salesPdf"></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="float-left">Cost of Sales</td>' +
                            '<td class="float-right" id="' + this.box + 'cosPdf"></td>' +                    
                        '</tr>' +
                        '<tr>' +
                            '<td><hr class="hr-custom w-100 my-2"></hr></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="float-left"><b>Gross Profit</b></td>' +
                            '<td class="float-right font-weight-bold-table" style="margin-right: -0.20rem;" id="' + this.box + 'gpPdf"></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td><hr class="hr-custom w-100 my-2"></hr></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="float-left">Other Income</td>' +
                            '<td class="float-right" id="' + this.box + 'oiPdf"></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="float-left">Expenses</td>' +
                            '<td class="float-right" style="margin-right: -0.08rem;" id="' + this.box + 'expensesPdf"></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td><hr class="hr-custom w-100 my-2"></hr></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="float-left"><b>Net Profit</b></td>' +
                            '<td class="float-right font-weight-bold-table" style="margin-right: -0.23rem;" id="' + this.box + 'nplPdf"></td>' +
                        '</tr>' +
                    '</table>'
            ).slidedown;

        } else if(title == 'Balance Sheet'){
            console.log('Display Balance Sheet Summary');

            $(pdfGraphZone).html(
                '<table style="font-size: 11px;" class="col-12 ml-3 my-3 small txt-color">' +
                    '<tr>' +
                        '<td class="float-left">Current Assets</td>' +
                            '<td class="float-right" style="margin-right: 0.10rem;" id="' + this.box + 'caPdf"></td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td class="float-left">Fixed Assets</td>' +
                        '<td class="float-right" style="margin-right: -0.08rem;" id="' + this.box + 'faPdf"></td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td><hr class="hr-custom w-100 my-2"></hr></td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td class="float-left"><b>Total Assets</b></td>' +
                        '<td class="float-right font-weight-bold-table" style="margin-right: -0.20rem;" id="' + this.box + 'taPdf"></td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td><hr class="hr-custom w-100 my-2"></hr></td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td class="float-left">Current Liabilities</td>' +
                        '<td class="float-right" style="margin-right: -0.15rem;" id="' + this.box + 'clPdf"></td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td class="float-left">Non-current Liabilities</td>' +
                        '<td class="float-right" id="' + this.box + 'flPdf"></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><hr class="hr-custom w-100 my-2"></hr></td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td class="float-left"><b>Total Liabilities</b></td>' +
                        '<td class="float-right font-weight-bold-table" style="margin-right: -0.28rem;" id="' + this.box + 'tlPdf"></td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td><hr class="hr-custom w-100 my-2"></hr></td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td class="float-left"><b>Total Equity</b></td>' +
                        '<td class="float-right font-weight-bold-table" style="margin-right: -0.28rem;" id="' + this.box + 'tePdf"></td>' +
                    '</tr>' +
                '</table>'
            ).slidedown;
        } else {

        $(pdfGraphZone).html('<canvas id="' + pdfchartId + '"></canvas>').slidedown;
        $(pdfGraphZone).css('position', 'relative');
        $(pdfGraphZone).css('margin', 'auto');
        $(pdfGraphZone).css('width', '90%');

        }
    }

    smallBox(){

        let html = '';
	    let loaderId = this.box + 'loader';
        let boxNumber = this.box.substr(3);
        let chart;
        let chartId = this.box + 'canvas' + this.dragItem;
        let graphZone = '#' + this.box + 'graphZone';
        let pdfGraphZone = '#pdf' + this.box + 'graphZone';
        let pdfchartId = 'pdf' + this.box + 'canvas';
        let pdfchart;
        let canvas;
        let pdfcanvas;
        let ctx;
        let pdfctx;
        let nodataId = this.box + 'nodata';
        let loader;

        //pl
        let salesId;
        let costOfSalesId;
        let grossProfitId;
        let otherIncomeid;
        let expensesId;
        let netProfitId;

        //plPDF
        let salesIdPdf;
        let costOfSalesIdPdf;
        let grossProfitIdPdf;
        let otherIncomeidPdf;
        let expensesIdPdf;
        let netProfitIdPdf;

        //bs
        let totalAssetsId;
        let totalLiabilitiesId;
        let totalEquityId;

        //bsPDF
        let currentAssetsIdPdf;
        let fixedAssetsIdPdf;
        let totalAssetsIdPdf;
        let currentLiabilitiesIdPdf;
        let fixedLiabilitiesIdPdf;
        let totalLiabilitiesIdPdf;
        let totalEquityIdPdf;


        switch (this.dragItem) {

        //Cash In VS Cash Out
            case 'cico':

                html =
                // '<a class="safariDraggable" draggable="true" id="box1">' +
                '<div class="row no-gutters">' +
                    '<div class="col-11">' +
                        '<h6 class="text-left pl-9 font-size-14 txt-color"><strong>Overview -</strong> Cash In v Cash Out</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' +
                    // '<div id="' + this.box + 'loader" class="loader mx-auto mt-5"></div>' +
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' +
                    '<br>' +
                    '<div class="" id="' + this.box + 'graphZone" style="position: absolute; z-index: 101;"></div>' +
                '</div>';
                // '</a>';
               //DashBox
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas' + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Overview', 'Cash in vs Cash out', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');
                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let cashIn = result.data['cash_in'];
                    let cashOut = result.data['cash_out'];
                    let diff = new Array();
                    let sumCashIn = 0;
                    let sumCashOut = 0;
                    for(let i = 0; i < cashIn.length; i++){
                        diff[i] = cashIn[i] - cashOut[i];
                        sumCashIn += parseInt(cashIn[i]);
                        sumCashOut += parseInt(cashOut[i]);
                    }

                    if(parseInt(sumCashIn) == 0 && parseInt(sumCashOut) == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{

                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                        chart = new BeanChart(chartId, ctx, formatDateRange(result.labels), cashIn, cashOut, diff)
                        chart.drawMixedGroupBarLine(["Cash In", "Cash Out", "Movement"], false);
    
                        pdfchart = new BeanChart(pdfchartId, pdfctx, formatDateRange(result.labels), cashIn, cashOut, diff)
                        pdfchart.drawMixedGroupBarLine(["Cash In", "Cash Out", "Movement"], true);

                        let annexure = new Annexure(boxNumber, 'cico', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;

        //Income vs Expenses
            case 'ino' :

                html =
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left pl-9 font-size-14 txt-color"><strong>Overview -</strong> Income v Expenses</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' +
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' +
                    '<br>' +
                    '<div class="" id="' + this.box + 'graphZone" style="position: absolute; z-index: 101;"></div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Overview', 'Income v Expenses', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let sumInc = 0;
                    let sumExp = 0;
                    for(let i = 0; i < result.data['income'].length; i++){
                        sumInc += result.data['income'][i];
                        sumExp += result.data['expenses'][i];
                    }

                    if(parseInt(sumInc) == 0 && parseInt(sumExp) == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                        chart = new BeanChart(chartId, ctx, formatDateRange(result.labels), result.data['income'], result.data['expenses'], result.data['net_profit'])
                        chart.drawMixedGroupBarLine(["Income", "Expenses", "Net Profit"], false);
    
                        pdfchart = new BeanChart(pdfchartId, pdfctx, formatDateRange(result.labels), result.data['income'], result.data['expenses'], result.data['net_profit'])
                        pdfchart.drawMixedGroupBarLine(["Income", "Expenses", "Net Profit"], true);

                        let annexure = new Annexure(boxNumber, 'ino', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');

                });

            break;
                
        //Asset v Debt
            case 'avd' :

                html = 
                '<div class="row no-gutters" style="height:40px">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left pl-9 font-size-14 txt-color"><strong>Overview -</strong> Asset v Debt</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' + 
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' +   
                    '<div class=" mx-auto" id="' + this.box + 'graphZone"></div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Overview', 'Asset v Debt', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){

                    console.log(result);
                    
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    console.log(result);
                    document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];

                    let asset_debt = [result.data['assets'], result.data['debt']];

                    if(result.data['assets'] == 0 && result.data['debt'] == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        chart = new BeanChart(chartId, ctx, ['Assets', 'Debt'], asset_debt);
                        chart.drawDoughnut([false,'right'], 30);
    
                        pdfchart = new BeanChart(pdfchartId, pdfctx, ['Assets', 'Debt'], asset_debt);
                        pdfchart.drawDoughnut([true,'right'], 30);
                        
                        let annexure = new Annexure(boxNumber, 'avd', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');

                });
            
            break;

        //Profit & Loss
            case 'pl':

                salesId = this.box + 'sales';
                costOfSalesId = this.box + 'cos';
                grossProfitId = this.box + 'gp';
                otherIncomeid = this.box + 'oi';
                expensesId = this.box + 'expenses';
                netProfitId = this.box + 'npl';

                salesIdPdf = this.box + 'salesPdf';
                costOfSalesIdPdf = this.box + 'cosPdf';
                grossProfitIdPdf = this.box + 'gpPdf';
                otherIncomeidPdf = this.box + 'oiPdf';
                expensesIdPdf = this.box + 'expensesPdf';
                netProfitIdPdf = this.box + 'nplPdf';
                
                html = 
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left pl-9 font-size-14 txt-color"><strong>Financial Management -</strong> Profit & Loss</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="row h-75">' +
                    '<div class="small col-12 text-center my-auto h-15">' +
                        '<span class="font-size-12 txt-color-orange">As on ' + this.fromDate.substr(0, 7) + '</span>' +
                        '<hr class="hr-custom w-50"></hr>' +
                    '</div>' +
                    '<div class="col-12 mt-2">' +
                        '<table class="small col-10 mx-auto txt-color font-size-14">' +
                            '<tr>' +
                                '<td class="float-left txt-color">Gross Profit</td>' +
                                '<td class="float-right font-weight-bold-table" id="' + this.box + 'gp"></td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td class="float-left txt-color">Net Profit</td>' +
                                '<td class="float-right font-weight-bold-table" id="' + this.box + 'npl"></td>' +
                            '</tr>' +
                            '<div id="' + this.box + 'loader" class="float-right mr-4 ml-n4 mt-3"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' +
                        '</table>' +
                    '</div>' +
                    '<div class="small col-12 text-center my-auto h-15">' +
                            '<div id="' + this.box + 'nodata" class=""></div>' +
                    '</div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;

                //pdfBox
                this.pdfBox(this.box,'Financial Management', 'Profit & Loss', pdfGraphZone);

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    if(result.data['gross_profit'][0]['amount'] == 0 && result.data['net_profit_before_tax'][0]['amount'] == 0){
                        document.getElementById(nodataId).innerHTML = '<div class="font-size-13 font-weight-200 txt-color-lb text-center">No data for this period</div>';
                    }
                    else{
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                        document.getElementById(grossProfitId).innerHTML = formatCurrency(result.data['gross_profit'][0]['amount'], 'R ');
                        document.getElementById(netProfitId).innerHTML = formatCurrency(result.data['net_profit_before_tax'][0]['amount'], 'R ');

                        document.getElementById(salesIdPdf).innerHTML = formatCurrency(result.data['total_sales'][0]['amount'], 'R ');
                        document.getElementById(costOfSalesIdPdf).innerHTML = formatCurrency(result.data['total_cost_of_sales'][0]['amount'], 'R ');
                        document.getElementById(grossProfitIdPdf).innerHTML = formatCurrency(result.data['gross_profit'][0]['amount'], 'R ');
                        document.getElementById(otherIncomeidPdf).innerHTML = formatCurrency(result.data['total_other_income'][0]['amount'], 'R ');
                        document.getElementById(expensesIdPdf).innerHTML = formatCurrency(result.data['total_expenses'][0]['amount'], 'R ');
                        document.getElementById(netProfitIdPdf).innerHTML = formatCurrency(result.data['net_profit_before_tax'][0]['amount'], 'R ');
                
                        let annexure = new Annexure(boxNumber, 'pl', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;

        //Balance Sheet
            case 'bs':

                totalAssetsId = this.box + 'ta';
                totalLiabilitiesId = this.box + 'tl';
                totalEquityId = this.box + 'te';

                currentAssetsIdPdf = this.box + 'caPdf';
                fixedAssetsIdPdf = this.box + 'faPdf';
                totalAssetsIdPdf = this.box + 'taPdf';
                currentLiabilitiesIdPdf = this.box + 'clPdf';
                fixedLiabilitiesIdPdf = this.box + 'flPdf';
                totalLiabilitiesIdPdf = this.box + 'tlPdf';
                totalEquityIdPdf = this.box + 'tePdf';
                
                var fromDate = this.fromDate;

                if(fromDate.length == 7){
                    fromDate += '-01';
                }
                
                html = 
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left pl-9 font-size-14 txt-color"><strong>Financial Management -</strong> Balance Sheet</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="row h-75">' +
                    '<div class="col-12 font-size-12 txt-color-orange text-center h-15">As on ' + this.fromDate.substr(0, 7) + 
                    '<hr class="hr-custom w-50"></hr>' +
                    '</div>' +
                    '<div class="col-12">' +
                        '<table class="small col-10 mx-auto txt-color font-size-14">' +
                            '<tr>' +
                                '<td class="float-left txt-color-grey">Total Assets</td>' +
                                '<td class="float-right font-weight-bold-table" id="' + this.box + 'ta"></td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td class="float-left txt-color-grey">Total Liabilities</td>' +
                                '<td class="float-right font-weight-bold-table" id="' + this.box + 'tl"></td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td class="float-left txt-color-grey">Equity</td>' +
                                '<td class="float-right font-weight-bold-table" id="' + this.box + 'te"></td>' +
                            '</tr>' +
                            '<div id="' + this.box + 'loader" class="float-right mr-4 ml-n4 mt-3"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' +
                        '</table>' +
                    '</div>' +
                    '<div class="small col-12 text-center my-auto h-15">' +
                            '<div id="' + this.box + 'nodata" class=""></div>' +
                    '</div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;

                //pdfBox
                this.pdfBox(this.box,'Financial Management', 'Balance Sheet', pdfGraphZone);
        
                
                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    console.log(result);
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    if(result.data['total_assets'] == 0 && result.data['total_liabilities'] == 0 && result.data['equity'].length < 2){
                        document.getElementById(nodataId).innerHTML = '<div class="font-size-13 font-weight-200 txt-color-lb text-center">No data for this period</div>';
                    }
                    else{
                        
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                        document.getElementById(totalAssetsId).innerHTML = formatCurrency((Number.isInteger(result.data['total_assets'])) ? result.data['total_assets'] : result.data['total_assets'][fromDate], 'R ');
                        document.getElementById(totalLiabilitiesId).innerHTML = formatCurrency((Number.isInteger(result.data['total_liabilities'])) ? result.data['total_liabilities'] : result.data['total_liabilities'][fromDate], 'R ');
                        document.getElementById(totalEquityId).innerHTML = formatCurrency((Number.isInteger(result.data['total_equity'])) ? result.data['total_equity'] : result.data['total_equity'][fromDate], 'R ');
                    
                        let annexure = new Annexure(boxNumber, 'bs', result);
                        annexure.buildAnnexure();

                        document.getElementById(currentAssetsIdPdf).innerHTML = formatCurrency((Number.isInteger(result.data['total_current_assets'])) ? result.data['total_current_assets'] : result.data['total_current_assets'][fromDate], 'R ');
                        document.getElementById(fixedAssetsIdPdf).innerHTML = formatCurrency((Number.isInteger(result.data['total_non_current_assets'])) ? result.data['total_non_current_assets'] : result.data['total_non_current_assets'][fromDate], 'R ');
                        document.getElementById(totalAssetsIdPdf).innerHTML = formatCurrency((Number.isInteger(result.data['total_assets'])) ? result.data['total_assets'] : result.data['total_assets'][fromDate], 'R ');
                        document.getElementById(currentLiabilitiesIdPdf).innerHTML = formatCurrency((Number.isInteger(result.data['total_current_liabilities'])) ? result.data['total_current_liabilities'] : result.data['total_current_liabilities'][fromDate], 'R ');
                        document.getElementById(fixedLiabilitiesIdPdf).innerHTML = formatCurrency((Number.isInteger(result.data['total_non_current_liabilities'])) ? result.data['total_non_current_liabilities'] :result.data['total_non_current_liabilities'][fromDate], 'R ');
                        document.getElementById(totalLiabilitiesIdPdf).innerHTML = formatCurrency((Number.isInteger(result.data['total_liabilities'])) ? result.data['total_liabilities'] : result.data['total_liabilities'][fromDate], 'R ');
                        document.getElementById(totalEquityIdPdf).innerHTML = formatCurrency((Number.isInteger(result.data['total_equity'])) ? result.data['total_equity'] : result.data['total_equity'][fromDate], 'R ');

                    }
                    $('#popoverBox' + boxNumber).popover('enable');

                });

            break;

        //Accounts Receivable
            case 'ar' :

                html =
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left pl-9 font-size-14 txt-color" style="margin-bottom: 20px"><strong>Customers -</strong> Accounts Receivable</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' +
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' +    
                    '<br>' +
                    '<div class="mt-n4 col-12" id="' + this.box + 'graphZone"></div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '112%');
                $(graphZone).css('max-width', '112%');
                $(graphZone).css('margin-left', '-1.5rem');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Customers', 'Accounts Receivable', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let arTotal = 0;
                    if(typeof result.data['accounts'] != 'undefined'){
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            arTotal += result.data['accounts'][i]['total'];
                        }
                    }
                    if(arTotal == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb pt-2 text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                        let names = new Array();
                        let totals = new Array();
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            names[i] = result.data['accounts'][i]['name'];
                            totals[i] = result.data['accounts'][i]['total'];
                        }
    
                        names = names.splice(0,3);
                        totals = totals.splice(0,3);
    
                        chart = new BeanChart(chartId, ctx, names, totals);
                        chart.drawSingleBar(true);
    
                        pdfchart = new BeanChart(pdfchartId, pdfctx, names, totals);
                        pdfchart.drawSingleBar(true);

                        let annexure = new Annexure(boxNumber, 'ar', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            
            break;

        //Debitors days outstanding
            case 'ddo':

                html = 
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left font-size-14 txt-color mb-4 pl-9"><strong>Customers -</strong> Debtors Days Outstanding</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' + 
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' +
                    '<div class="mt-n3 col-12" id="' + this.box + 'graphZone"></div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(chartId);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Customers', 'Debtors Days Outstanding', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let ddoTotal = 0;
                    if(typeof result.data['accounts'] != 'undefined'){
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            ddoTotal += result.data['accounts'][i]['total'];
                        }
                    }

                    if(ddoTotal == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb pt-2 text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];

                        let days = ['0-30', '30-60', '60-90', '90-120', '120+'];
                        let _0_30 = 0;
                        let _30_60 = 0;
                        let _60_90 = 0;
                        let _90_120 = 0;
                        let _120 = 0;
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            if(result.data['accounts'][i]['0-30'] != 0){
                                _0_30 += result.data['accounts'][i]['0-30'];
                            }
                            if(result.data['accounts'][i]['30-60'] != 0){
                                _30_60 += result.data['accounts'][i]['30-60'];
                            }
                            if(result.data['accounts'][i]['60-90'] != 0){
                                _60_90 += result.data['accounts'][i]['60-90'];
                            }
                            if(result.data['accounts'][i]['90-120'] != 0){
                                _90_120 += result.data['accounts'][i]['90-120'];
                            }
                            if(result.data['accounts'][i]['120+'] != 0){
                                _120 += result.data['accounts'][i]['120+'];
                            }
                        }
                        let data = [_0_30, _30_60, _60_90, _90_120, _120];
                        for(let i = 0; i < data.length; i++){
                            if(data[i] < 0){
                                data[i] = 0;
                            }
                        }
                        chart = new BeanChart(chartId, ctx, days, data);
                        chart.drawDoughnut([false,'left'], 30);

                        pdfchart = new BeanChart(pdfchartId, pdfctx, days, data);
                        pdfchart.drawDoughnut([true,'left'], 30);

                        let annexure = new Annexure(boxNumber, 'ddo', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;

        //Top Customers by Balance Outstanding
            case 'tcs':

                html = 
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left font-size-14 txt-color mb-4"><strong>Customers -</strong> Top Customers by Balance Outstanding</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' + 
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' +             
		            '<div class="mt-n3 col-12 p-0" id="' + this.box + 'graphZone"></div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '110%');
                $(graphZone).css('max-width', '110%');
                $(graphZone).css('margin-left', '-15px');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Customers', 'Top Customers by Balance Outstanding', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let tcsTotal = 0;
                    if(typeof result.data['accounts'] != 'undefined'){
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            tcsTotal += result.data['accounts'][i]['total'];
                        }
                    }

                    if(tcsTotal == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb pt-2 text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        let names = new Array();
                        let totals = new Array();
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            names[i] = result.data['accounts'][i]['name'];
                            totals[i] = result.data['accounts'][i]['total'];
                        }
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
    
                        names = names.splice(0,3);
                        totals = totals.splice(0,5);
    
                        chart = new BeanChart(chartId, ctx, names, totals);
                        chart.drawHorizontalBar(false, "Balance Outstanding");
    
                        pdfchart = new BeanChart(pdfchartId, pdfctx, names, totals);
                        pdfchart.drawHorizontalBar(false, "Balance Outstanding");

                        let annexure = new Annexure(boxNumber, 'tcs', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');

                });

            break;

        //Retention
            case 'ret':

                html = 
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left font-size-14 txt-color mb-4"><strong>Customers -</strong> Retention</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' +
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' +  
                    '<br>' +  
                    '<div class="mt-n4 " id="' + this.box + 'graphZone" style="position: absolute; z-index: 101;"></div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Customers', 'Retention', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');
                
                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }
                    
                    let retSum = 0;
                    if(typeof result.data['accounts'] != 'undefined'){
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            retSum += result.data['accounts'][i]['total'];
                        }
                    }
                    if(retSum == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb pt-2 text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                        let ret = new Array();
                        for(let i = 0; i < result.data['retention'].length; i++){
                            ret[i] = result.data['retention'][i];
                        }
                        chart = new BeanChart(chartId, ctx, formatDateRange(result.labels), ret, ret);
                        chart.drawBarLine(["",""], false, "%",false);

                        pdfchart = new BeanChart(pdfchartId, pdfctx, formatDateRange(result.labels), ret, ret);
                        pdfchart.drawBarLine(["Retention", "Movement"], true, "%", false);

                        let annexure = new Annexure(boxNumber, 'ret', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;

        //Accounts Payable
            case 'ap':

                html = 
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left font-size-14 txt-color mb-4 pl-9"><strong>Suppliers -</strong> Accounts Payable</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' +
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' + 
                    '<br>' +
                    '<div class="mt-n4 " id="' + this.box + 'graphZone"></div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Suppliers', 'Accounts Payable', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let apTotal = 0;
                    if(typeof result.data['accounts'] != 'undefined'){
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            apTotal += result.data['accounts'][i]['total'];
                        }
                    }

                    if(apTotal == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb pt-2 text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        let names = new Array();
                        let totals = new Array();
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            names[i] = result.data['accounts'][i]['name'];
                            totals[i] = result.data['accounts'][i]['total'];
                        }
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];

                        names = names.splice(0,3);
                        totals = totals.splice(0,3);

                        chart = new BeanChart(chartId, ctx, names, totals);
                        chart.drawSingleBar(true);

                        pdfchart = new BeanChart(pdfchartId, pdfctx, names, totals);
                        pdfchart.drawSingleBar(true);

                        let annexure = new Annexure(boxNumber, 'ap', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;

        //Creditors Days Outstanding
            case 'cdo':

                html = 
                '<div class="row no-gutters" style="height:41px">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left font-size-14 txt-color mb-4 pl-9"><strong>Suppliers -</strong> Creditors Days Outstanding</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' + 
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' + 
                    '<br>' +
                    '<div class="mt-n3 col-12" id="' + this.box + 'graphZone"></div>' + 
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Suppliers', 'Creditors Days Outstanding', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let cdoTotal = 0;
                    if(typeof result.data['accounts'] != 'undefined'){
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            cdoTotal += result.data['accounts'][i]['total'];
                        }
                    }

                    if(cdoTotal == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        let days = ['0-30', '30-60', '60-90', '90-120', '120+'];
                        let _0_30 = 0;
                        let _30_60 = 0;
                        let _60_90 = 0;
                        let _90_120 = 0;
                        let _120 = 0;
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            if(result.data['accounts'][i]['0-30'] != 0){
                                _0_30 += result.data['accounts'][i]['0-30'];
                            }
                            if(result.data['accounts'][i]['30-60'] != 0){
                                _30_60 += result.data['accounts'][i]['30-60'];
                            }
                            if(result.data['accounts'][i]['60-90'] != 0){
                                _60_90 += result.data['accounts'][i]['60-90'];
                            }
                            if(result.data['accounts'][i]['90-120'] != 0){
                                _90_120 += result.data['accounts'][i]['90-120'];
                            }
                            if(result.data['accounts'][i]['120+'] != 0){
                                _120 += result.data['accounts'][i]['120+'];
                            }
                        }
                        let data = [_0_30, _30_60, _60_90, _90_120, _120];
                        for(let i = 0; i < data.length; i++){
                            if(data[i] < 0){
                                data[i] = 0;
                            }
                        }
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                        chart = new BeanChart(chartId, ctx, days, data);
                        chart.drawDoughnut([false, ""], 30);

                        pdfchart = new BeanChart(pdfchartId, pdfctx, days, data);
                        pdfchart.drawDoughnut([true, "left"], 30);

                        let annexure = new Annexure(boxNumber, 'cdo', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;

        //Top Suppliers by Balance Outstanding
            case 'tsp':

                html = 
                '<div class="row no-gutters" style="height:41px">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left pl-9 font-size-14 txt-color mb-4"><strong>Suppliers -</strong> Top Suppliers by Balance Outstanding</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' + 
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' + 
                    '<br>' +            
                    '<div class="mt-n3 " id="' + this.box + 'graphZone"></div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Suppliers', 'Top Suppliers by Balance Outstanding', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let tspTotal = 0;
                    if(typeof result.data['accounts'] != 'undefined'){
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            tspTotal += result.data['accounts'][i]['total'];
                        }
                    }

                    if(tspTotal == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        let names = new Array();
                        let totals = new Array();
                        for(let i = 0; i < result.data['accounts'].length; i++){
                            names[i] = result.data['accounts'][i]['name'];
                            totals[i] = result.data['accounts'][i]['total'];
                        }
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];

                        names = names.splice(0,3);
                        totals = totals.splice(0,5);

                        chart = new BeanChart(chartId, ctx, names,totals);
                        chart.drawHorizontalBar(false, "Balance Outstanding");

                        pdfchart = new BeanChart(pdfchartId, pdfctx, names,totals);
                        pdfchart.drawHorizontalBar(false, "Balance Outstanding");

                        let annexure = new Annexure(boxNumber, 'tsp', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;
            
        //Inventory Management
            case 'im':

                html = 
                '<div class="row no-gutters" style="height:29px">' + 
                    '<div class="col-11">' +
                        '<h6 class="text-left pl-9 font-size-14 txt-color mb-4"><strong>Inventory -</strong> Inventory on Hand</h6>' +
                    '</div>' +
                    '<div class="col-1 z-index-n1 txt-align-center">' +
                    '</div>' +
                '</div>'+
                '<div class="col-12">' + 
                    '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' + 
                    '<br>' +
                    '<div class="mt-n3  pl-9" id="' + this.box + 'graphZone"></div>' + 
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Inventory', 'Inventory on Hand', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }
                    console.log(result.data['item']);
                    console.log(result.data['item'].length);

                    let qohIM = 0;

                    if(result.data['item'].length == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data</div>').slidedown;
                    }
                    else{
                        for(let i = 0; i < result.data['item'].length; i++){
                            if(result.data['item'][i]['quantity_on_hand'] == null){
                                qohIM += 0;
                            }
                            else{
                                qohIM += result.data['item'][i]['quantity_on_hand'];
                            }
                        }
                        if(qohIM == 0){
                            $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No inventory on hand</div>').slidedown;
                        }
                        else{
                            document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                            let names = new Array();
                            let qoh = new Array();
        
                            result.data['item'].sort(function(a,b){
                                return  b['quantity_on_hand'] - a['quantity_on_hand'];
                            })
        
                            result.data['item'].sort(function(a,b){
                                return  b['quantity_on_hand'] - a['quantity_on_hand'];
                            })
        
                            for(let i = 0; i < 3; i++){
                                if(typeof result.data['item'][i] === 'undefined'){
                                    break;
                                }
                                else{
                                    names[i] = result.data['item'][i]['name'];
                                    qoh[i] = result.data['item'][i]['quantity_on_hand'];
                                }
                            }
        
                            names = names.splice(0,5);
                            qoh = qoh.splice(0,5);
        
                            chart = new BeanChart(chartId, ctx, names,qoh);
                            chart.drawDoughnut([false, "left"], 30, false);
        
                            pdfchart = new BeanChart(pdfchartId, pdfctx, names,qoh);
                            pdfchart.drawDoughnut([true, "left"], 30, false);
        
                            let annexure = new Annexure(boxNumber, 'im', result);
                            annexure.buildAnnexure();
                        }

                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });
                

            break;


        //Inventory Analysis - Sales
            case 'inas':

                html = 
                '<div class="small">' + 
                    '<div class="row">' +
                        '<div class="col-12 text-center my-auto">' +
                        '<div class="row no-gutters">' + 
                            '<div class="col-11">' +
                                '<h6 class="text-left pl-9 font-size-14 txt-color ml-2"><strong>Inventory -</strong> Top Items by Sales</h6>' +
                            '</div>' +
                            '<div class="col-1 z-index-n1 txt-align-center">' +
                            '</div>' +
                        '</div>'+
                            '<div class="row no-gutters mb-1 fs-11">' +
                                '<div class="col-5 txt-color"' +
                                    '<span class="small my-auto">Sales </span>' +
                                    '<label class="switch my-auto align-middle">' +
                                        '<input type="checkbox" checked=checked onclick="checkQuantity(\'' + this.box + '\', \'inaq\')">' +
                                        '<span class="slider"></span>' +
                                    '</label>' +
                                '</div>' +
                                '<div class="col-5 txt-color"' +
                                    '<span class="small ml-1 my-auto">Quantity </span>' +
                                    '<label class="switch my-auto align-middle">' +
                                        '<input type="checkbox" onclick="checkQuantity(\'' + this.box + '\', \'inaq\')">' +
                                        '<span class="slider"></span>' +
                                    '</label>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-12">' +
                            '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' + 
                            '<div id="' + this.box + 'graphZone">' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');
                //pdfBox
                this.pdfBox(this.box,'Inventory', 'Top Items by Sales', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    if(result.data.length == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
    
                        let items = new Array();
                        let sales = new Array();
                        
                        for(let i = 0; i < 3; i++){
                            if(typeof result.data[i] === 'undefined'){
                                break;
                            }
                            else{
                                items[i] = result.data[i]['item_code'];
                                sales[i] = result.data[i]['total_sales'];
                            }
                        }
    
                        items = items.splice(0,5);
                        sales = sales.splice(0,5);
    
                        chart = new BeanChart(chartId, ctx, items, sales);
                        chart.drawSingleBar();
    
    
                        pdfchart = new BeanChart(pdfchartId, pdfctx, items, sales);
                        pdfchart.drawSingleBar(true);
    
                        let annexure = new Annexure(boxNumber, 'inas', result);
                        annexure.buildAnnexure(true);
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;

        //Inventory Analysis - Quantity
            case 'inaq':

                html = 
                '<div class="small">' + 
                    '<div class="row">' +
                        '<div class="col-12 text-center my-auto">' +
                        '<div class="row no-gutters">' + 
                            '<div class="col-11">' +
                                '<h6 class="text-left pl-9 font-size-14 txt-color ml-2"><strong>Inventory -</strong> Top Items by Quantity</h6>' +
                            '</div>' +
                            '<div class="col-1 z-index-n1 txt-align-center">' +
                            '</div>' +
                        '</div>'+
                            '<div class="row no-gutters mb-1 fs-11">' +
                                '<div class="col-5 txt-color"' +
                                    '<span class="small my-auto">Sales </span>' +
                                    '<label class="switch my-auto align-middle">' +
                                        '<input type="checkbox" onclick="checkSales(\'' + this.box + '\', \'inas\')">' +
                                        '<span class="slider"></span>' +
                                    '</label>'+
                                '</div>' +
                                '<div class="col-5 txt-color"' +
                                    '<span class="small ml-1 my-auto">Quantity </span>' +
                                    '<label class="switch my-auto align-middle">' +
                                        '<input type="checkbox" checked=checked onclick="checkSales(\'' + this.box + '\', \'inas\')">' +
                                        '<span class="slider"></span>' +
                                    '</label>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-12">' +
                            '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' + 
                            '<div id="' + this.box + 'graphZone">' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Inventory', 'Top Items by Quantity', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }
                    
                    let qohInaq = 0;

                    if(result.data['item'].length == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data</div>').slidedown;
                    }
                    else{
                        for(let i = 0; i < result.data['item'].length; i++){
                            if(result.data['item'][i]['quantity_on_hand'] == null){
                                qohInaq += 0;
                            }
                            else{
                                qohInaq += result.data['item'][i]['quantity_on_hand'];
                            }
                        }
                        if(qohInaq == 0){
                            $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No inventory on hand</div>').slidedown;
                        }
                        else{
                            document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                            let names = new Array();
                            let qoh = new Array();

                            result.data['item'].sort(function(a,b){
                                return  b['quantity_on_hand'] - a['quantity_on_hand'];
                            })

                            for(let i = 0; i < 3; i++){
                                if(typeof result.data['item'][i] === 'undefined'){
                                    break;
                                }
                                else{
                                    names[i] = result.data['item'][i]['name'];
                                    qoh[i] = result.data['item'][i]['quantity_on_hand'];
                                }
                            }

                            names = names.splice(0,5);
                            qoh = qoh.splice(0,5);

                            chart = new BeanChart(chartId, ctx, names, qoh);
                            chart.drawSingleBar(false);

                            pdfchart = new BeanChart(pdfchartId, pdfctx, names, qoh);
                            pdfchart.drawSingleBar(false);

                            let annexure = new Annexure(boxNumber, 'inaq', result);
                            annexure.buildAnnexure();
                        }
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });
                    

            break;
        
        //Profit Loss Ratios
            case 'plr':

                html = 
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                            '<h6 class="text-left pl-9 font-size-14 txt-color"><strong>Analysis -</strong> Profit & Loss Ratios</h6>' +
                        '</div>' +
                        '<div class="col-1 z-index-n1 txt-align-center">' +
                        '</div>' +
                    '</div>'+
                    '<div class="col-12">' +
                        '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' + 
                        '<br>' +
                        '<div class="mx-auto" id="' + this.box + 'graphZone"></div>' +
                    '</div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Analysis', 'Profit & Loss Ratios', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                
                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let plrTotal = 0;
                    for(let i = 0; i < result.labels.length; i++){
                        plrTotal += result.data['gross_margin'][i];
                        plrTotal += result.data['operating_margin'][i];
                        plrTotal += result.data['net_margin'][i];
                    }
                    
                    if(plrTotal == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];

                        chart = new BeanChart(chartId, ctx,formatDateRange(result.labels), result.data['gross_margin'], result.data['operating_margin'], result.data['net_margin']);
                        chart.drawTripleLine(false,["Gross Margin", "Operating Margin", "Net Margin"], true);

                        pdfchart = new BeanChart(pdfchartId, pdfctx,formatDateRange(result.labels), result.data['gross_margin'], result.data['operating_margin'], result.data['net_margin']);
                        pdfchart.drawTripleLine(true,["Gross Margin", "Operating Margin", "Net Margin"],true);


                        let annexure = new Annexure(boxNumber, 'plr', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;

        //Balance Sheet Ratios
            case 'bsr':

                html = 
                '<div class="row no-gutters">' + 
                    '<div class="col-11">' +
                            '<h6 class="text-left pl-9 font-size-14 txt-color mb-2"><strong>Analysis -</strong> Balance Sheet Ratios</h6>' +
                        '</div>' +
                        '<div class="col-1 z-index-n1 txt-align-center">' +
                        '</div>' +
                    '</div>'+
                    '<div class="col-12">' +
                        '<div id="' + this.box + 'loader" class="text-center m-top-sm-box"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>' + 
                        '<br>' +
                        '<div class="mx-auto" id="' + this.box + 'graphZone"></div>' +
                    '</div>' +
                '</div>';
                $('#' + this.box).html(html).slidedown;
                $(graphZone).html('<canvas id="' + chartId + '"></canvas>').slidedown;

                $(graphZone).css('position', 'relative');
                $(graphZone).css('margin', 'auto');
                $(graphZone).css('width', '100%');
                $(graphZone).css('max-width', '100%');
                $(graphZone).css('z-index', '0');

                canvas = document.getElementById(this.box + 'canvas'  + this.dragItem);
                canvas.classList.add('safariDraggable');
                ctx = canvas.getContext('2d');

                //pdfBox
                this.pdfBox(this.box,'Analysis', 'Balance Sheet Ratios', pdfGraphZone, pdfchartId);
                pdfcanvas = document.getElementById(pdfchartId)
                pdfctx = pdfcanvas.getContext('2d');

                $.getJSON('json.php?q=' + this.dragItem + '&from_date=' + this.fromDate + '&to_date=' + this.toDate + '&box=' + boxNumber, function(result){
                    loader = document.getElementById(loaderId);
                    if(loader){
                        loader.hidden = true;
                    }

                    let bsrTotal = 0;
                    for(let i = 0; i < result.labels.length; i++){
                        bsrTotal += result.data['current_ratio'][i];
                        bsrTotal += result.data['quick_ratio'][i];
                        bsrTotal += result.data['debt_to_equity'][i];
                    }
                    
                    if(bsrTotal == 0){
                        $(graphZone).html('<div class="font-size-13 font-weight-200 txt-color-lb text-center mt-small-box">No data for this period</div>').slidedown;
                    }
                    else{
                        document.getElementById('labelupdate' + boxNumber).innerHTML = 'Last updated: ' + result.data['last_updated'];
                        chart = new BeanChart(chartId, ctx, formatDateRange(result.labels), result.data['current_ratio'], /*result.data['current_ratio'],*/  result.data['debt_to_equity'])
                        chart.drawTripleLine(false,["Current Ratio", /*"Quick Ratio",*/ "Debt to Equity"], false);

                        pdfchart = new BeanChart(pdfchartId, pdfctx, formatDateRange(result.labels), /*result.data['current_ratio'],*/ null, result.data['debt_to_equity'])
                        // pdfchart = new BeanChart(pdfchartId, pdfctx, formatDateRange(result.labels), result.data['current_ratio'], result.data['quick_ratio'], result.data['debt_to_equity'])
                        pdfchart.drawTripleLine(true,["Current Ratio", /*"Quick Ratio",*/ "Debt to Equity"], false);

                        let annexure = new Annexure(boxNumber, 'bsr', result);
                        annexure.buildAnnexure();
                    }
                    $('#popoverBox' + boxNumber).popover('enable');
                });

            break;

            default:

                console.log("Error retreiving data");

            break;
        }
    }
}

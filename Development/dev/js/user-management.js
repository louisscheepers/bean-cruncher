$( document ).ready(function() {
    populateUsers();
    document.getElementById('invite-loader').hidden = true;
    document.getElementById('error-text').hidden = true;
});

function populateUsers(){
    console.log('populating users');
    let usersLoaded = false;
    let pendingUsersLoaded = false;

    document.getElementById('users-loader').hidden = false;

    document.getElementById('ownerlist').innerHTML = '';
    document.getElementById('adminlist').innerHTML = '';
    document.getElementById('stafflist').innerHTML = '';

    let email;
    let action = 'fetch'

    $.when(

        $.ajax({
            url:"json.php",
            method:"POST",
            data:{profile:action},
            success:function(data){
                let obj = jQuery.parseJSON(data);
                email = obj['user_email'];
            }
        })
    ).then(function(){
        $.getJSON('json.php?gUser', function(result){
            console.log(result);
            let message = '';
            for(let i = 0 ; i < result.length; i++){
                if(result[i]['status'] != 3){
                    message = 
                    '<div class="row">' +
                        '<div class="col-2 my-auto">' +
                            result[i]['name'] +
                        '</div>' +
                        '<div class="col-3 my-auto text-center">' +
                            result[i]['email'] +
                        '</div>' +
                        '<div class="col-2 my-auto text-center">' +
                            result[i]['joined'].substr(0, 10) +
                        '</div>';
                        if(result[i]['role'] == 'Owner' || result[i]['email'] == email){
                            message += 
                            '<div class="col-1 my-auto text-center">' +
                                '<label class="switch w-75 h-75 mb-0">' +
                                    '<input id="check' + result[i]['email'] + '" type="checkbox" checked disabled onclick="switchStatus(\'' + result[i]['email'] + '\', 2)">' +
                                    '<span class="slider-md slider-disabled" style="border: 1px solid #707070;"></span>' +
                                '</label>' +
                            '</div>';
                        }
                        else if(result[i]['status'] == 1){
                            message += 
                            '<div class="col-1 my-auto text-center">' +
                                '<label class="switch w-75 h-75 mb-0">' +
                                    '<input id="check' + result[i]['email'] + '" type="checkbox" checked onclick="switchStatus(\'' + result[i]['email'] + '\', 2)">' +
                                    '<span class="slider-md"></span>' +
                                '</label>' +
                            '</div>';
                        }
                        else{
                            message += 
                            '<div class="col-1 mt-2 text-center">' +
                                '<label class="switch w-75 h-75 mb-0">' +
                                    '<input id="check' + result[i]['email'] + '" type="checkbox" onclick="switchStatus(\'' + result[i]['email'] + '\', 1)">' +
                                    '<span class="slider-md"></span>' +
                                '</label>' +
                            '</div>';
                        }
                        if(result[i]['role'] == 'Owner'){
                            message +=
                            '<div class="col-3 text-center my-auto">' +
                                '<select disabled class="custom-select m-0 my-auto" id="select' + result[i]['email'] + '" onchange="roleChange(\'' + result[i]['email'] + '\')">' +
                                    '<option value="Owner" selected>Owner</option>' +
                                '</select>' + 
                            '</div>';
                        }
                        else if(result[i]['role'] == 'Admin'){
                            if(result[i]['email'] == email){
                                message +=
                                '<div class="col-3 text-center my-auto">' +
                                    '<select disabled class="custom-select m-0 my-auto" id="select' + result[i]['email'] + '" onchange="roleChange(\'' + result[i]['email'] + '\')">' +
                                        '<option value="Admin" selected>Admin</option>' +
                                    '</select>' + 
                                '</div>';
                            }
                            else{
                                message +=
                                '<div class="col-3 text-center my-auto">' +
                                    '<select class="custom-select m-0 my-auto" id="select' + result[i]['email'] + '" onchange="roleChange(\'' + result[i]['email'] + '\')">' +
                                        '<option value="Admin" selected>Admin</option>' +
                                        '<option value="Staff">Staff</option>' +
                                    '</select>' + 
                                '</div>';
                            }
                        }
                        else{
                            message +=
                            '<div class="col-3 text-center my-auto">' +
                                '<select class="custom-select m-0 my-auto" id="select' + result[i]['email'] + '" onchange="roleChange(\'' + result[i]['email'] + '\')">' +
                                    '<option value="Admin">Admin</option>' +
                                    '<option value="Staff" selected>Staff</option>' +
                                '</select>' + 
                            '</div>';
                        }
                        if(result[i]['role'] == 'Owner' || result[i]['email'] == email){
                            message +=
                            '<div class="col-1 my-auto text-center">' +
                                '<a href="#" class="text-secondary" onclick="confirmDelete(\'' + result[i]['email'] + '\') disabled">' +
                                    '<i class="far fa-trash-alt remove-icon-size" style="color:#6c757d"></i>' +
                                '</a>' +
                            '</div>';
                        }
                        else{
                            message +=
                            '<div class="col-1 my-auto text-center">' +
                                '<a href="#" onclick="confirmDelete(\'' + result[i]['email'] + '\')">' +
                                    '<i class="far fa-trash-alt remove-icon-size"></i>' +
                                '</a>' +
                            '</div>';
                        }
                        message +=
                    '</div>';
    
                    message += 
                    '<div class="row">' +
                        '<div class="col-12 w-75"><hr class="mx-n4 px-m8"></div>' +
                    '</div>';
                    
                    if(result[i]['role'] == 'Owner'){
                        document.getElementById('ownerlist').innerHTML += message;
                    }
                    else if(result[i]['role'] == 'Admin'){
                        document.getElementById('adminlist').innerHTML += message;
                    }
                    if(result[i]['role'] == 'Staff'){
                        document.getElementById('stafflist').innerHTML += message;
                    }
                }
            }

            usersLoaded = true;
            if(usersLoaded && pendingUsersLoaded){
                document.getElementById('users-loader').hidden = true;
            }
        });
    });
    
    document.getElementById('pending-container').innerHTML = '';
    $.getJSON('json.php?pUser', function(result){
        let message = '';
        console.log(result);
        if(result.length != 0){
            contain =
            '<div class="container my-5 p-0">' +
                '<div class="row mt-lg-1">' +
                    '<div class="col-12 mt-lg-1">' +
                        '<div class="card">' +
                            '<div class="card-body profile-card">' +
                                '<div class="row">' +
                                    '<div class="col-sm-1 align-self-center">' +
                                        '<div class="text-right">' +
                                            '<img src="img/Management roles icon.png" class="img-fluid profile-icon-size">' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="col-sm-7 my-6 align-self-center">' +
                                        '<div class="txt-inline">' +
                                            '<h5 class="font-weight-normal align-middle txt-color mt-m5">' +
                                                'Pending Users' +
                                            '</h5>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row sm-blue-card font-weight-bold txt-color-w mb-3 mx-4">' +
                                    '<div class="col-3 my-auto text-center">' +
                                        'Status' +
                                    '</div>' +
                                    '<div class="col-3 my-auto text-center">' +
                                        'Email' +
                                    '</div>' +
                                    '<div class="col-3 my-auto text-center">' +
                                        'Invited' +
                                    '</div>' +
                                    '<div class="col-3 my-auto text-center">' +
                                        'Delete' +
                                    '</div>' +
                                '</div>' +
                                '<div id="pendinglist"></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>';
        
            document.getElementById('pending-container').innerHTML = contain;

            for(let i = 0 ; i < result.length; i++){
                message += 
                '<div class="row mx-4 px-8 font-size-14 txt-color">' +
                    '<div class="col-3 my-auto text-center">' +
                        'Pending...' +
                    '</div>' +
                    '<div class="col-3 my-auto text-center">' +
                        result[i]['email'] +
                    '</div>' +
                    '<div class="col-3 my-auto text-center">' +
                        result[i]['invited'] +
                    '</div>' +
                    '<div class="col-3 my-auto text-center">' +
                        '<a href="#" onclick="confirmDelete(\'' + result[i]['email'] + '\', true)">' +
                            '<img src="img/Remove icon.png" class="img-fluid remove-icon-size">' +
                        '</a>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-12 w-75 px-4"><hr class="mx-7"></div>' +
                '</div>';
            }
            document.getElementById('pendinglist').innerHTML += message;
            document.getElementById('pendinglist').innerHTML += '<div class="row"> </div>';
        }

        pendingUsersLoaded = true;
        if(usersLoaded && pendingUsersLoaded){
            document.getElementById('users-loader').hidden = true;
        }
    });
}

function addUser(){
    let loader = document.getElementById('invite-loader');
    loader.hidden = false;

    let errorText = document.getElementById('error-text');
    errorText.innerHTML = '';
    errorText.hidden = true;

    let email = document.getElementById('email').value;
    console.log('Adding ' + email);

    $.ajax({
        type: "GET",
        url: "json.php",
        data: {aUser:email},
        success: function(result) {
            console.log(result);
            if(result === 'false'){
                errorText.innerText = 'Could not send email';
                errorText.hidden = false;

                loader.hidden = true;
            }
            else if(result === 'true' || result === '"User added"'){
                $('#addUser').collapse('hide');

                populateUsers();
                document.getElementById('email').value = '';

                loader.hidden = true;
            }
            else{
                // populateUsers();
                // document.getElementById('email').value = '';
                let text = result.substring(1, result.length-1);

                errorText.innerText = text;
                errorText.hidden = false;

                loader.hidden = true;
            }
        }
    });
}

$("#addUser").on("hide.bs.collapse", function(){
    let errorText = document.getElementById('error-text');
    errorText.innerText = '';
    errorText.hidden = true;
});

function confirmDelete(email){
    $("#confirm-delete").modal();
    document.getElementById('confirm-btn').innerHTML = '<button type="button" onclick="deleteUser(\'' + email + '\')" class="mt-3 btn btn-cross-platform btn-w" data-dismiss="modal">Confirm</button>'
}

function deleteUser(email, isPending){
    //loader.hidden = false;

    if(typeof isPending == 'undefined'){
        isPending = false;
    }

    if(isPending){
        console.log('Deleting user ' + email);
        $.ajax({
            type: "GET",
            url: 'json.php',
            data: {sUser:email,status:3},
            success: function(){
                populateUsers();
            }
        });
    }
    else{
        console.log('Deleting user ' + email);
        $.ajax({
            type: "GET",
            url: 'json.php',
            data: {dUser:email},
            success: function(){
                populateUsers();
            }
        });
    }

}

function switchStatus(email, status){

    let check = document.getElementById('check' + email);
    let newStatus;
    if(status == 2){
        newStatus = 1;
    }
    else{
        newStatus = 2;
    }

    check.setAttribute('onclick', 'switchStatus("' + email + '", ' + newStatus + ')');

    console.log('Changing ' + email + ' to status ' + status);

    $.ajax({
        type: "GET",
        url: 'json.php',
        data: {sUser:email,status:status},
        success: function(){
            // alert('Updated');
        }
    });
}

function roleChange(email){
    console.log('Changed user ' + email + '\'s type to ' + document.getElementById('select' + email).value);

    let newRole = document.getElementById('select' + email).value;

    $.ajax({
        type: "GET",
        url: 'json.php',
        data: {uUser:email, role:newRole},
        success: function(){
            // alert('Role updated');
            populateUsers();
        }
    });
}

function loadCompany(name, id){
    $.getJSON('json.php?aComp=' + id, function(){
        populateUsers();
        document.getElementById('companyDropDown').innerHTML = name;
        document.getElementById('companyTitle').innerHTML = name;
    });

    let action = 'fetch'
    $.ajax({
        url:"json.php",
        method:"POST",
        data:{company:action},
        success:function(data){
            console.log(data);
            let obj = jQuery.parseJSON(data);
            console.log(obj);
            if(obj['company_brand'] != '<img id="comp-img" src="data:image/jpeg;base64," class="rounded-circle icon-size" alt="Company Brand">'){
                document.getElementById('comp-icon').innerHTML = obj['company_brand'];
            }
            else{
                document.getElementById('comp-icon').innerHTML = '<img id="comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
            }
            // document.getElementById('loader').hidden = true;
        }
    });
}
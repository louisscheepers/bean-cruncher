class BeanChart{

    constructor(id, context, labels, data, data2, data3, data4) {
        this.blue ='#39ABD8';
        this.lightBlue='#42BBEA';
        this.orange ='#FF9300';
        this.lightOrange ='#FD9C37';
        this.pink ='#CB0A73';
        this.lightPink ='#E50F83';
        this.darkPink ='#761267';
        this.lightDarkPink ='#991586';
        this.purple ='#4B1879';
        this.lightPurple ='#5B238D';

        Chart.defaults.global.defaultFontSize = 11;
        Chart.defaults.global.legend.display = false;
        Chart.defaults.global.elements.line.fill = false;

        this.context = context;
        this.id = id;
        this.labels = labels;
        this.data = data;

        if(data2 !== 'undefined'){
            this.data2 = data2;
        }
        if(data3 !== 'undefined'){
            this.data3 = data3
        }
        if(data4 !== 'undefined'){
            this.data4 = data4;
        }
        let colorPrimary = '';

    }

    backgroundColors(){
        return [this.blue, this.orange, this.pink, this.darkPink, this.purple, this.lightBlue, this.lightOrange, this.lightPink, this.lightDarkPink, this.lightPurple];
    }

    backgroundReverseAccents(){
        return [this.lightBlue, this.lightOrange,this.lightPink, this.lightDarkPink, this.purple, this.blue, this.orange, this.pink, this.darkPink, this.purple];
    }
    barBackgroundColors(){
        let color1 = new Array();
        let color2 = new Array();
        let j = 0;
        for(let i = 0; i < this.data.length; i++){
            color1.push(this.blue);
            color2.push(this.pink);
            // switch (j) {
            //     case 0:
            //         color1.push(this.blue);
            //         color2.push(this.pink);
            //     break;
            //     case 1:
            //         color1.push(this.pink);
            //         color2.push(this.blue);
            //     break;
            //     case 2:
            //         color1.push(this.darkPink);
            //         color2.push(this.purple);
            //     break;
            //     case 3:
            //         color1.push(this.purple);
            //         color2.push(this.darkPink);
            //     break;
            // }
            if((j % 3) == 0 && j != 0){
                j = 0;
            }
            else{
                j++;
            }
        }
        let colors = new Array();
        colors[0] = color1;
        colors[1] = color2;

        return colors;
    }

    prepareLabels(labels){
        let newLabels = [];
        
        for(var i = 0; i < labels.length; i++){
            if (/\s/.test(labels[i])) {
                //console.log('space detected in "' + labels[i])
                newLabels.push(labels[i].split(" "));
            }else{
                newLabels.push(labels[i]);
            }
        }
        return newLabels;
    }

    drawSingleBar(showCurrency){

        let chartLabels = this.labels;
        new Chart(this.context, {
            type: 'bar',
            data: {
                labels: this.prepareLabels(this.labels),
                datasets: [{
                    barPercentage: 0.5,
                    barThickness: 2,
                    maxBarThickness: 4,
                    minBarLength: 4,
                    borderWidth: 0,
                    backgroundColor: this.backgroundColors(),
                    data: this.data
                }]
            },
            options: {
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItems, data) {
                            //console.log(tooltipItems.index);

                            return chartLabels[tooltipItems[0]['index']];
                        },
                        label: function(tooltipItem, data) {
                            let item = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            let value = "";
                            let currency = "";

                            let rounded = Math.round(item);
                            if(showCurrency){
                                currency = "R ";
                            }
                            value = " " + currency + String(rounded + "").replace(/(.)(?=(\d{3})+$)/g,'$1 ');

                            return value;
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMin: 0,
                            callback: function(label, index, labels) {
                                if(label > 1 || label <= 0){
                                    let currency = "";
                                    if(showCurrency){
                                        currency = "R";
                                    }
                                    if(label < 0 ){
                                        let negLabel = Math.abs(label);

                                        return "-" + currency + " " + String(negLabel).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                    }
                                    return  currency + " " + String(label).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                }
                                return label ;
                            }
                        }
                    }]
                },
            },
            responsive: true,
            maintainAspectRatio: false
        });
    }

    drawGroupedBar(){

    }

    drawBarLine(legendLabels, showLegend, type, showCurrency){
        let percTooltip = false;
        if(type === '%'){
            percTooltip = true;
        }
        new Chart(this.context, {
            type: 'bar',
            data: {
                labels: this.prepareLabels(this.labels),
                datasets: [

                    {
                        label: legendLabels[1],
                        backgroundColor: this.lightOrange,
                        borderColor: this.orange,
                        borderWidth: 0,
                        fill: false,
                        data: this.data2,
                        type: 'line',
                    },
                    {
                        label: legendLabels[0],
                        backgroundColor: this.barBackgroundColors()[0],
                        data: this.data,
                        borderWidth: 0,
                    }
                ],
            },
            options: {
                legend:{
                    display: showLegend,
                    position:  'bottom',
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            let item = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            let value = "";

                            if(percTooltip){
                                value = (item * 100).toFixed(2) + "%"
                            }else{
                                value = item.toFixed(2);
                            }
                            return value;
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMin: 0,
                            callback: function(label, index, labels) {
                                if(label > 1 || label <= 0){
                                    let currency = "";
                                    if(showCurrency){
                                        currency = "R";
                                    }
                                    if(label < 0 ){
                                        let negLabel = Math.abs(label);

                                        return "-" + currency + " " + String(negLabel).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                    }
                                    return currency + " " + String(label).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                }
                                return label ;
                            }
                        }
                    }]
                }
            },
            responsive: true,
            maintainAspectRatio: false
        });
    }
    drawMixedGroupBarLine(legendLabels, showLegend){
        new Chart(this.context, {
            type: 'bar',
            data: {
                labels: this.prepareLabels(this.labels),
                datasets: [
                    {
                        label: legendLabels[2],
                        backgroundColor: this.lightOrange,
                        borderColor: this.orange,
                        borderWidth: 0,
                        fill: false,
                        data: this.data3,
                        type: 'line',
                    },
                    {
                        label: legendLabels[0],
                        backgroundColor: this.barBackgroundColors()[0],
                        data: this.data,
                        borderWidth: 0,
                    }, {
                        label: legendLabels[1],
                        backgroundColor: this.barBackgroundColors()[1],
                        data: this.data2,
                        borderWidth: 0,
                    }

                ],
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            let item = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            let value = "";

                            let rounded = Math.round(item);
                            value = "R " + String(rounded + "").replace(/(.)(?=(\d{3})+$)/g,'$1 ');

                            return value;
                        }
                    }
                },
                legend:{
                     display: showLegend,
                     position:  'bottom',
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMin: 0,
                            callback: function(label, index, labels) {
                                if(label > 1 || label <= 0){
                                    if(label < 0 ){
                                        let negLabel = Math.abs(label);

                                        return "-R " + String(negLabel).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                    }
                                    return "R " + String(label).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                }
                                return label ;
                            }
                        }
                    }]
                }
            },
            responsive: true,
            maintainAspectRatio: false
        });
    }

    drawDoughnut(showLegend, cutOut, showCurrency){

        if(typeof(cutOut) !== 'undefined'){
            //console.log("cutout " + cutOut);
        }else{
            cutOut = 50;
            //console.log('cutout default 50');
        }

        if(typeof(showCurrency) === 'undefined'){
            showCurrency = true;
        }

        //console.log("Drawing Doughnut")
        //console.log(this.data);
        new Chart(this.context, {
            type: 'doughnut',
            data: {
                labels: this.labels,
                datasets: [{
                    data: this.data,
                    backgroundColor: this.backgroundReverseAccents(),
                    borderColor: this.backgroundColors(),
                    borderWidth: 0
                }]
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            let item = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            let value = "";
                            let currency = "";

                            let rounded = Math.round(item);

                            // console.log(showCurrency);

                            if(showCurrency){
                                currency = "R ";
                            }
                            value = " " + currency + String(rounded + "").replace(/(.)(?=(\d{3})+$)/g,'$1 ');

                            return value;
                        }
                    }
                },
                legend:{
                    display: showLegend[0],
                    position:  showLegend[1],
                },
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            //console.log("Data labels Percentage");
                            let sum = 0;
                            let dataArr = ctx.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                if(typeof(data) === 'string'){
                                    data = Number(data);
                                }
                                sum += data;
                            });
                            let percentage = null;
                            //console.log("Value: " + value + " Sum: " + sum +" = Percentage: " + (value *100 / sum) + "%");
                            if(value*100 / sum <= 10){
                                //console.log("IF TRUE");
                                if(value*100 / sum < 5){
                                    //console.log("smaller than 5 percent");
                                    percentage = "";
                                }else{
                                    percentage = (value*100 / sum).toFixed(0)+"%";
                                }

                            }else{
                                //console.log("if False");
                                percentage = (value*100 / sum).toFixed(2)+"%";

                            }

                            //console.log("PERCENTAGE: " + percentage);
                            return percentage;


                        },
                        color: '#fff',
                    }
                },
                cutoutPercentage: cutOut
            }
        });
    }

    drawHorizontalBar(showLegend, label, shouldPrepareLabels, maintainAspectRatio){
        let labels = null;

        if(typeof(shouldPrepareLabels) !== "undefined"){
            if(!shouldPrepareLabels){
                labels = this.labels;
            }else{
                labels = this.prepareLabels(this.labels);
            }
        }else{
            labels = this.prepareLabels(this.labels);
        }

        if(typeof maintainAspectRatio == 'undefined'){
            maintainAspectRatio = true;
        }

        let chartLabels = this.labels;
        new Chart(this.context, {
            type: 'horizontalBar',
            data: {
                labels: labels,
                datasets: [
                    {
                        label: label,
                        backgroundColor:this.backgroundColors(),
                        data: this.data
                    }
                ]
            },
            options: {
                maintainAspectRatio: maintainAspectRatio,
                legend:{
                    display: showLegend,
                    position:  'bottom',
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItems, data) {
                            //console.log(tooltipItems.index);
                            
                            return chartLabels[tooltipItems[0]['index']];
                        },
                        label: function(tooltipItem, data) {
                            let item = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            let value = "";

                            let rounded = Math.round(item);
                            value = "R " + String(rounded + "").replace(/(.)(?=(\d{3})+$)/g,'$1 ');

                            var label = data.datasets[tooltipItem.datasetIndex].label || '';

                            if (label) {
                                label += ': ';
                            }
                            return label += value;
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMin: 0,
                            callback: function(label, index, labels) {
                                if(label > 1 || label <= 0){
                                    if(label < 0 ){
                                        let negLabel = Math.abs(label);

                                        return "-R =" + String(negLabel).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                    }
                                    return "R " + String(label).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                }
                                // console.log(label);
                                
                                if(Array.isArray(label)){
                                    let fullstring = '';
                                    for(let i = 0; i < label.length; i++){
                                        fullstring += label[i] + ' ';
                                    }
                                    if(fullstring.length > 10){
                                        label = fullstring.slice(0, 10) + '...';
                                    }
                                }
                                return label;
                            }
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMin: 0,
                            callback: function(label, index, labels) {
                                if(label > 1 || label <= 0){
                                    if(label < 0 ){
                                        let negLabel = Math.abs(label);

                                        return "-R " + String(negLabel).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                    }
                                    return "R " + String(label).replace(/(.)(?=(\d{3})+$)/g,'$1 ');
                                }
                                return label ;
                            }
                        }
                    }]
                }
            },
            responsive: true,
            maintainAspectRatio: false
        })
    }

    
    drawTripleLine(showLegend, legendLabels, showPercentages){

        let dataset = 
        [
            {
                label: legendLabels[0],
                backgroundColor: this.lightPink,
                borderColor: this.pink,
                borderWidth: 0,
                fill: false,
                data: this.data,
            },
            {
                label: legendLabels[1],
                backgroundColor: this.lightDarkPink,
                borderColor: this.darkPink,
                borderWidth: 0,
                fill: false,
                data: this.data2,
            },
            {
                label: legendLabels[2],
                backgroundColor: this.lightPurple,
                borderColor: this.purple,
                borderWidth: 0,
                fill: false,
                data: this.data3,
            }
        ]


        let size = legendLabels.length;
        // console.log(size);
        // console.log(dataset);
        if(size == 2){
            dataset = dataset.splice(0,2);
        }

        let chartLabels = this.labels;
        new Chart(this.context, {
            type: 'line',
            data: {
                labels: this.prepareLabels(this.labels),
                datasets: dataset
            },
            options: {
                legend:{
                    display: showLegend,
                    position:  'bottom',
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItems, data) {
                            //console.log(tooltipItems.index);
                            return chartLabels[tooltipItems[0]['index']];
                        },
                        label: function(tooltipItem, data) {
                            let item = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            let label = data.datasets[tooltipItem.datasetIndex].label;
                            let percent = '';
                            
                            if(showPercentages){
                                percent = '%';
                            }

                            return label +": " + (item).toFixed(2) + percent;
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMin: 0,
                            callback: function(label, index, labels) {
                                if(showPercentages){
                                    return label + "%";
                                }
                                return label ;
                            }

                        }
                    }]
                }
            },
            responsive: true,
            maintainAspectRatio: false
        });
    }

     removeSymbol(symbol, str){
        var newString = "";
        for(var i = 0; i < str.length; i++) {
            var char = str.charAt(i);
            if(char != symbol){
                newString = newString + char;
            }
        }
        return newString;
    }
}
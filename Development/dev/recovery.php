<?php
require_once __DIR__ . '/core/init.php';

$msg = '';

if(Input::exists()){
    if(isset($_POST['send'])){
        if(isset($_POST['email'])){
            $user = new User();
            $find = $user->find($_POST['email']);
            if($find){
                $email = Email::sendPreset($_POST['email'], 'password');
                if($email){
                    Redirect::to('password-recovery.php?email=' . $_POST['email']);
                }
                else{
                    Redirect::to('password-recovery.php?email=' . $_POST['email'] . '&failed=true');
                }
            }
            else{
                $msg = '<div class="text-color-red fs-13 mb-n3">This email is not registered</div>';
            }
            
        }
        else{
            $msg = '<div class="text-color-red">Please input an email</div>';
        }
    }
}
else{
    // echo 'No input exists';
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

        <title>BC | Recovery</title>
    </head>
    <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
        <div class="container" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-6 mt-6">
                <div class="col-11 col-sm-10 col-md-9 col-lg-6 col-xl-6 card custom-card rounded-0-1 my-auto white-boxShadow">
                    <!-- Logo -->
                    <div class="row d-flex justify-content-center recovery-logo">
                        <div class="col-9 col-sm-7 col-md-7 col-lg-7 col-xl-7">
                            <img src="img/logo.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <!-- Heading -->
                    <div class="row d-flex justify-content-center">    
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <span>Password recovery</span>
                        </div>
                    </div>
                    <!-- Lock -->
                    <div class="row d-flex justify-content-center my-4">
                        <div class="col-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 px-0">
                            <img src="img/Verify icon.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <!-- Form -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-11 col-sm-9 col-md-8 col-lg-9 col-xl-9 px-3">
                            <form action="" method="POST" class="needs-validation">
                    <!-- Recovery code -->
                                <div class="row d-flex justify-content-center recovery-text">
                                    <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center">
                                            <p class="verify-txt-color fs-13">Please enter your email address,<br/>and we will send you a <b>recovery link</b>.</p>
                                            <?php
                                                if($msg != ''){
                                                    echo '<br>' . $msg;
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                    <!-- Email -->
                                <div class="input-group shadow-custom-sm org-name g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border pr-2"><img src="img/Email icon.svg" class="img-fluid email_icon_width" alt=""/></span>
                                    </div>
                                    <input id="email" type="text" class="form-control border-left-0 pl-1 rounded-right-border field-text-color fs-13 my-auto" name="email" placeholder="Email address" required>
                                </div>
                    <!-- Send again -->
                                <div class="row d-flex justify-content-center recover-btn">
                                    <div class="col-5 col-sm-5 col-md-5 col-lg-5 col-xl-5">
                                        <input name="send" type="submit" class="btn btn-block nav-free-but the-border main-purple py-2 fs-12" value="Send">
                                    </div>
                                </div>
                    <!-- Don't have an account? -->
                                <div class="row recover-sign-up">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                                        <div class="bluegrey">
                                            <span class="fs-12"><a href="login.php" class="bluegrey"><b>Sign in</b></a> | <a href="register.php" class="bluegrey"><b>Sign up</b></a></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>
        
        <script>
            (function() {
            'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            // form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    </body>
</html>
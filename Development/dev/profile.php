<?php
require_once(__DIR__ . '/core/init.php');

$user = new User();
if($user->isLoggedIn()){
    $role = Session::get('active_role_id');

    $package = $user->getPackage();

    $showPPic = false;

    if($package > 2){
        $showPPic = true;
    }
}
else{
    Redirect::to('index.php');
}

if(Companies::getActiveCompanyId() == null){
    $companies = Companies::getCompanies();
    if($companies == null){
        Redirect::to('integration.php');
    }
    else{
        Companies::setActiveCompany($comapnies[0]['id']);
    }
  }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Profile Settings</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> 
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
        
        <link href="css/custom.css" rel="stylesheet" />
    </head>
    <body class="content-bg">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
               
                <nav class="sb-topnav navbar navbar-expand nav-mobile-p navbar-dark p-0 my-3 mobile-mx">    
                    <div class="container nav-size">
                        <div class="row d-content no-gutters">
                            <!-- First Colum with nested colums -->
                            <div class="col-2 col-sm-2 col-lg-5">
                                <div class="row">
                                    <div class="col-12 col-lg-2 align-self-center">
                                        <a type="button" class=" btn-lg d-content" href="index.php">
                                            <img class="img-fluid arrow-icon-size pl-1" src="img/arrow-left-icon.png">
                                        </a>
                                    </div>

                                    <div class="col-lg-10 d-none d-lg-block"></div>
                                </div>
                            </div>

                            <!-- Second Colum with nested colums -->    
                            <div class="col-8 col-sm-8 col-lg-2 text-center">
                                <h5 class="nav-settings d-content txt-color">
                                    Profile Settings
                                </h5>
                            </div>

                            <!-- Third Colum with nested colums -->
                            <div class="col-2 col-sm-2 col-lg-5">
                                <div class="row">
                                    <div class="col-lg-1 d-none d-lg-block">
                                        <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0 "></div>
                                    </div>
                                    
                                    <div class="col-lg-9 txt-align-right align-self-center d-none d-lg-block">
                                        <h5 class="nav-settings d-content">
                                            <?php echo Companies::getActiveCompanyName() ?>
                                        </h5>
                                    </div>
                                    <div class="col-12 col-lg-2 align-self-center">
                                        <div id="comp-icon" class="float-right">
                                            <?php
                                                if(Companies::getActiveCompanyBrand() == null){
                                                    echo '<img id="comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
                                                }
                                                else{
                                                    echo '<img id="comp-img" src="data:image/jpeg;base64,' . base64_encode(Companies::getActiveCompanyBrand()) . '" class="rounded-circle icon-size" alt="Profile Icon">';
                                                }
                                            ?>
                                        </div>
                                      </div>
                                </div>
                             </div>
                        </div>
                    </div>
                </nav>
                <main>
                    <div class="container my-5 p-0">
                        <div class="row mt-lg-1 m-card-m">
                            <div class="col-12 mt-lg-1">
                               <div class="card mobile-mx">
                                  <div class="card-body profile-card">
                                        <div class="row no-gutters pt-4">
                                            <img src="img/Management roles icon.png" class="img-fluid profile-icon-size d-none d-lg-block">
                                            <div class="col-sm-10 txt-inline mt-n1">
                                                <h5 class="font-weight-normal align-middle txt-color">Profile</h5>
                                                <p class="txt-color font-weight-200 font-size-14">This information will appear on your profile</p>
                                            </div>
                                        </div>
                                        
                                        <div class="dropdown-divider"></div>
                                        <form method="POST" enctype="multipart/form-data" id="profile-settings" class="row p-5 mb-p-0">
                                            <div class="col-12 col-lg-4 mb-mt-1">
                                                <h5 class="txt-color font-weight-bold">User Information</h5>
                                                <?php 
                                                    if(isset($_POST['upload-details'])){
                                                        $name = $_POST['name'];
                                                        $surname = $_POST['surname'];
                                                        $number = $_POST['number'];
                                                        $email = $_POST['email'];


                                                        if(strlen($number) != 10){
                                                            Session::flash("Profile", "Phone number is invalid");
                                                        }else{
                                                            if($_FILES['pfp']['tmp_name']){
                                                                $img_type = exif_imagetype($_FILES['pfp']['tmp_name']);


                                                                if($img_type > 0 && $img_type <= 3){
                                                                    if($img_type == 1){
                                                                        $image = imagecreatefromgif($_FILES['pfp']['tmp_name']);
                                                                        imagejpeg($image, $_FILES['pfp']['tmp_name']);
                                                                        uploadFileContents($name, $surname, $email, $number);
                                                                    }
                                                                    else if($img_type == 2){
                                                                        uploadFileContents($name, $surname, $email, $number);
                                                                    }
                                                                    else if($img_type == 3){
                                                                        $image = imagecreatefrompng($_FILES['pfp']['tmp_name']);
                                                                        $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
                                                                        imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                                                                        imagealphablending($bg, TRUE);
                                                                        imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
                                                                        imagedestroy($image);
                                                                        imagejpeg($bg, $_FILES['pfp']['tmp_name'], 70);
                                                                        uploadFileContents($name, $surname, $email, $number);
                                                                        imagedestroy($bg);
                                                                    }
                                                                }
                                                                else{
                                                                    echo '<span class="font-size-12 font-weight-200 font-lb-color mx-2">Invalid format</span>';
                                                                }
                                                            }
                                                            else{
                                                                $user->update(array('user_name' => $name, 'user_surname' => $surname, 'user_email' => $email, 'user_number' => $number));
                                                            }
                                                        }
                                                        
                                                    }
                                                    function uploadFileContents($name, $surname, $email, $number){
                                                        $user = new User();
                                                        $data = file_get_contents($_FILES['pfp']['tmp_name']);
                                                        $user->update(array('user_img' => $data, 'user_name' => $name, 'user_surname' => $surname, 'user_email' => $email, 'user_number' => $number));
                                                    }
                                                ?>
                                                    <?php
                                                        if(Session::exists("Profile")){
                                                            echo '<p style="color: red;" class="text-color-red font-weight-200 font-size-14">' . Session::flash("Profile") . '</p>';

                                                        }
                                                    ?>
                                                <input name="name" id="name" type="text" class="form-control mt-6 px-4" placeholder="Name" required>
                                                <input name="surname" id="surname" type="text" class="form-control my-3 px-4" placeholder="Surname" required>
                                                <input name="number" id="number" type="text" class="form-control my-3 px-4" placeholder="Tel number" required>
                                                <input name="email" id="email" type="email" class="form-control my-3 px-4" placeholder="Email" required>
                                                <button type="submit" name="upload-details" class="btn btn-cross-platform my-3 w-40" accept="image/*">Update</button>
                                                
                                                
                                            </div>
                                            <div class="col-sm-1"></div>
                                           
                                            <div class="col-12 col-lg-6 mb-mt-1 mb-mb-0">
                                            <?php 

                                            if($showPPic){
                                                ?>
                                            <h5 class="txt-color font-weight-bold">Upload a New Avatar</h5>
                                            <p class="txt-color font-weight-200 font-size-14">You can upload or change your avatar here</p>
                                            <div class="row mt-5">
                                                <div class="col-2 align-self-center">
                                                    <div id="user-icon">
                                                        <img id="user-img" src="img/profile-icon-pink.png" class="rounded-circle icon-size" alt="Profile Icon">
                                                    </div>
                                                </div>
                                                <div class="col-10">
                                                        <div class="input-group">
                                                            <div class="custom-file">
                                                                <input type="file" onchange="readURL(this);" class="custom-file-input" name="pfp">
                                                                <label id="user-file" class="custom-file-label field-img-upload font-weight-200" for="input-profile-picture">Choose file
                                                                <img class="img-fluid dropdown-icon-upload" src="img/dropdown.png">
                                                                </label>
                                                                
                                                            </div>
                                                        </div>
                                                    <p class="font-size-12 font-weight-200 font-lb-color my-2">The maximum allowed file size is 200KB.</p>
                                                </div>
                                            </div>
                                                <?php
                                            }
                                            ?>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                           </div>
                       </div>
                    </div>
                </main>
            </div>
        </div>
        
    <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Profile Settings Script -->
        <script src="js/profile.js"></script>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    </body>
</html>

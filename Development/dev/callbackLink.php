<?php
  ini_set('display_errors', 'On');
  require_once(__DIR__ . '/core/init.php');
  require_once('const/const.php');
  ////$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');

  $user = new User();

if($user->isLoggedIn()){
  ////$logger->debug('callbackLink.php START - User is logged in!');
}
else{
  //////$logger->debug('callbackLink.php START - User NOT logged in!');
}

if($user->isLoggedIn()){
  $ids = Session::get('tenants');
  $comps = Companies::setCompanies($ids);

  ////$logger->debug("Exists: " );
  for($i = 0; $i < count($comps['exists']); $i++){
    ////$logger->debug($comps['exists'][$i]);
  }
  ////$logger->debug("Success: " );
  for($i = 0; $i < count($comps['success']); $i++){
    //$logger->debug($comps['success'][$i]);
  }

  $oauth = new Oauth();
  $token = $_SESSION['oauth2']['token'];
  $tenantId = $_SESSION['oauth2']['tenant_id'];
  $refreshToken = $_SESSION['oauth2']['refresh_token'];
  $idToken = $_SESSION['oauth2']['id_token'];
  $expires = $_SESSION['oauth2']['expires'];

  $tenantIds = $_SESSION['tenants'];
  // //$logger->debug("Token : " . $token);
  // //$logger->debug("tenantID : " . $tenantId);
  // //$logger->debug("refreshToken : " . $refreshToken);
  // //$logger->debug("ID Token : " . $idToken);
  // //$logger->debug("Expires  : " . $expires);

  for($i = 0; $i < count($tenantIds); $i++){
    //$logger->debug('Setting OauthDB for ID: ' + $tenantIds[$i]);
    $oauth->setDbOauth($token, $tenantIds[$i], $refreshToken, $idToken, $expires);
  }
  
  Session::put('active_role_id', 1);
    $exists = '';
    for($i = 0; $i <= count($comps['exists']) -1 ; $i++){
      //$logger->debug("Existing company " . $i . ": " . $comps['exists'][$i]);
      if($i == (count($comps['exists']) - 1)){
        $exists .= $comps['exists'][$i];
      }
      else{
        $exists .= $comps['exists'][$i] . ',';
      }
    }
    $success = '';
    for($i = 0; $i <= count($comps['success']) -1 ; $i++){
      //$logger->debug("Successful company " . $i . ": " . $comps['success'][$i]);
      if($i == (count($comps['success']) - 1)){
        $success .= $comps['success'][$i];
      }
      else{
        $success .= $comps['success'][$i] . ',';
      }
    }
    //$logger->debug("Exists: " . $exists);
    //$logger->debug("Success: " . $success);
    
    $previous = "javascript:history.go(-2)";
    $previous = $_SERVER['HTTP_REFERER'];

    if($exists == ''){
      Redirect::to('index.php');
    }
    else{
      Redirect::to('integration.php?exists='.$exists.'&success='.$success);
    }
  
}else{
  //$logger->error("User not logged in, redirecting to login");
  Redirect::to('login.php?reenter=true');
}
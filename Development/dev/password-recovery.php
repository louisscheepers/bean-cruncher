<?php
require_once __DIR__ . '/core/init.php';

if(Input::exists()){
    if(isset($_POST['send-again'])){
        $email = Email::sendPreset($_GET['email'], 'password');
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

        <title>BC | Verify</title>
    </head>
    <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
        <div class="container" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-6 mt-6">
                <div class="col-11 col-sm-12 col-md-10 col-lg-6 col-xl-6 card rounded-0-1 my-auto custom-card white-boxShadow">
                <!-- Logo -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-7 col-sm-7 col-md-7 col-lg-6 col-xl-6 logo-size-int-2">
                            <img src="img/logo.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                <!-- Heading -->
                    <div class="row d-flex justify-content-center recovery-2">    
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <span>Password recovery</span>
                        </div>
                    </div>
                <!-- Lock -->
                    <div class="row d-flex justify-content-center kwl-icon recovery-2">
                        <div class="col-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 px-0">
                            <img src="img/Verify icon.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                <!-- Brand text -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center we-sent-row">
                                <?php 
                                    if(Input::get('failed')){
                                        echo '<p class="text-color-red fs-13">An error occurred, please try again.<br/></p>';
                                    }
                                    else{
                                        echo '<p class="text-color-green fs-13">We sent you an email.<br/> <b>Please see the authorisation link</b></p>';
                                    }

                                ?>
                                
                            </div>
                        </div>
                    </div>
                <!-- Form -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-11 col-sm-9 col-md-7 col-lg-9 col-xl-9">
                            <form action="" method="POST">
                <!-- Send again -->   
                                <p class="verify-txt-color fs-12 text-center mb-2">
                                    Didn't receive an email? 
                                </p>

                                <div class="row d-flex justify-content-center recovery-4">
                                    <div class="col-8 col-sm-8 col-md-7 col-lg-6 col-xl-6">
                                        <input type="submit" name="send-again" class="btn btn-block nav-free-but the-border main-purple py-2 fs-12 my-auto py-auto" value="Send again">
                                    </div>
                                </div>
                            </form>
                <!-- Don't have an account? -->
                            <div class="row recover-sign-up">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                                    <div class="bluegrey">
                                        <span class="fs-12">You don't have an account? <a href="register.php" class=" bluegrey"><b>Sign up</b></a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>

<!--
        <div class="container b-black-1 mt-100px w-400px">
            <div class="row text-center">
-->
                <!-- Logo -->
<!--
                <div class="col-12 my-2">
                    <a href="signin.html"><img class="" src="img/logo.png" alt="logo"></a>
                </div> 
-->
                <!-- Heading -->
<!--
                <div class="col-3 my-4">
                    <hr>
                </div>
                <div class="col-6 my-4">
                    <p class="font-weight-bold text-black">Password recovery</p>
                </div>
                <div class="col-3 my-4">
                    <hr>
                </div>
            </div>
            
            <form action="resetpassword.html" method="POST">
                <div class="row text-center">
-->
                    <!-- Text -->
<!--
                    <div class="col-12">
                        We sent you an email.<br/>Please follow the authorisation link.
                    </div>
-->
                    <!-- Reset Button-->
<!--
                    <div class="col-12">
                        <input type="submit" class="btn bg-blue text-white w-400px ml-n3 mt-4" value="Reset my password">
                    </div>
                </div>
            </form>
        </div>
-->

        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    </body>
</html>
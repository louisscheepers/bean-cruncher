<?php
    ini_set('display_errors', 'On');
    require_once('const/const.php');
    require_once(__DIR__ . '/core/init.php');

    $coa = new DataChartOfAccounts(null);
    $chartOfAccounts = $coa->getChartOfAccounts();

    $user = new User();
    if($user->isLoggedIn()){
        $role = Companies::getUserCompanyRole(Companies::getActiveCompanyId());
        if($role == 3){
            Redirect::to('index.php');
        }
    }
    else{
        Redirect::to('index.php');
    }
    
    if(Companies::getActiveCompanyId() == null){
      $companies = Companies::getCompanies();
      if($companies == null){
          Redirect::to('integration.php');
      }
      else{
          Companies::setActiveCompany($comapnies[0]['id']);
      }
    }

    $previous = "javascript:history.go(-1)";
    if(isset($_SERVER['HTTP_REFERER'])) {
        $previous = $_SERVER['HTTP_REFERER'];
    }

    $hasAccounts;
    if(count($chartOfAccounts) != 0){
        $hasAccounts = true;
    }
    else{
        $hasAccounts = false;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Chart of Accounts</title>
        
        <!-- Chartist -->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/custom.css" rel="stylesheet" />
        <link href="css/graphs.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
    </head>

    <body class="content-bg">

    <nav class="sb-topnav navbar navbar-expand navbar-dark p-0 my-3 mobile-mx">    
                    <div class="container nav-size">
                        <div class="row d-content no-gutters">
                            <!-- First Colum with nested colums -->
                            <div class="col-2 col-lg-5">
                                <div class="row">
                                    <div class="col-12 col-lg-2 align-self-center">
                                        <a type="button" class=" btn-lg d-content" href="index.php">
                                            <img class="img-fluid arrow-icon-size pl-1" src="img/arrow-left-icon.png">
                                        </a>
                                    </div>

                                    <div class="col-lg-10 d-none d-lg-block"></div>
                                </div>
                            </div>

                            <!-- Second Colum with nested colums -->    
                            <div class="col-8 col-lg-2 text-center">
                                <h5 class="nav-settings d-content txt-color">
                                    Chart of Accounts
                                </h5>
                            </div>

                            <!-- Third Colum with nested colums -->
                            <div class="col-2 col-lg-5">
                                <div class="row">
                                    <div class="col-lg-1 d-none d-lg-block">
                                        <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0 "></div>
                                    </div>
                                    
                                    <div class="col-lg-9 txt-align-right align-self-center d-none d-lg-block">
                                        <h5 class="nav-settings d-content">
                                            <?php echo Companies::getActiveCompanyName() ?>
                                        </h5>
                                    </div>
                                    <div class="col-12 col-lg-2 align-self-center">
                                        <div id="comp-icon" class="float-right">
                                            <?php
                                                if(Companies::getActiveCompanyBrand() == null){
                                                    echo '<img id="comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
                                                }
                                                else{
                                                    echo '<img id="comp-img" src="data:image/jpeg;base64,' . base64_encode(Companies::getActiveCompanyBrand()) . '" class="rounded-circle icon-size" alt="Profile Icon">';
                                                }
                                            ?>                                        
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
    </nav>

    <main>
        <div class="container p-0">
            <div class="card my-5 mobile-mx">
                <div class="card-body profile-card">
                    <div id="alert-container">
                        <div class="alert alert-success text-center fixed-top w-15 mx-auto" id="success-alert" style="z-index:1040; display:none" >
                            Account updated
                        </div>
                    </div>
                    <div class="row no-gutters pt-4">
                        <img src="img/bg-analysis.png" class="img-fluid circle-icon-size d-none d-lg-block">
                        <div class="col-sm-10 txt-inline mt-n1">
                            <h5 class="font-weight-normal align-middle txt-color">Chart Of Accounts Setup</h5>
                        </div>
                    </div>
                    <div class="dropdown-divider-lg"></div>

                    <div class="row no-gutters sm-blue-card mb-m3 mx-3 mobile-mx-none"></div>

                    <div class="px-5 txt-color-w mb-p-0">
                    
                    <table class="table table-borderless">
                        <?php if($hasAccounts){ ?>
                        <thead class="txt-color-w">
                            <tr>
                                <th scope="col">Account</th>
                                <th scope="col" class="d-none d-lg-block">Category Detail</th>
                                <th scope="col" class="d-lg-none">Category</th>
                                <th scope="col">Class</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Sales</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="sales">
                        </tbody>
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Cost of Sales</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="costOfSales">
                        </tbody>
                        <!-- SALES - COS = GROSS PROFIT -->
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Other Income</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="otherIncome">
                        </tbody>
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Expenses</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="expenses">
                        </tbody>
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Tax</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="tax">
                        </tbody>
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Current Assets</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="currentAssets">
                        </tbody>
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Non-current Assets</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="nonCurrentAssets">
                        </tbody>
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Non-current Liabilities</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="nonCurrentLiabilities">
                        </tbody>
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Current Liabilities</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="currentLiabilities">
                        </tbody>
                        <thead>
                            <tr>
                                <th scope="col" class="txt-color py-6">Equity</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody id="equity">
                        </tbody>
                    
                        <?php }
                        else{
                            echo '<p class="text-center mt-0_7">This company\'s chart of accounts has not been updated yet. Please visit the <a href="/company-settings.php">Company Settings</a> page to update the database.</p>';
                        } ?>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </main>

    <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    
    <!-- Chart of Accounts Script -->
    <script src="js/chart-of-accounts.js"></script>

    <!-- Simple Search Script -->
    <script src="js/simpleSearch.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>

    </body>
</html>

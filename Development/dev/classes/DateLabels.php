<?php

class DateLabels{
    private static $_months = array(),
                    $_years = array();

    public static function setDateRange($fromDate, $toDate){
        // self::$_isFlipped = $isFlipped;

        // if($fromDate > $toDate){
        //     $tempFrom = $fromDate;

        //     $fromDate = $toDate;
        //     $toDate = $tempFrom;
        // }

        self::$_months = array();

        $fromDate = strtotime($fromDate);
        $toDate = strtotime($toDate);

        $year1 = date('Y', $fromDate);
        $year2 = date('Y', $toDate);

        $month1 = date('m', $fromDate);
        $month2 = date('m', $toDate);

        $diff = self::getDifference($year1, $year2, $month1, $month2);

        // echo 'Difference: ' . $diff . '<br>';

        for($i = 0; $i < $diff + 1; $i++){
            if($i == 0){
                $month = date('m', $fromDate);
                $year = date('Y', $fromDate);

            }
            else{
                if($month > 11){
                    $year++;
                    $month = '01';
                }
                else{
                    $month++;
                    if(strlen($month) == 1){
                        $month = '0' . $month;
                    }
                }
            }
            array_push(self::$_months, $month);
            array_push(self::$_years, $year);
        }
    }
    

    private static function getDifference($year1, $year2, $month1, $month2){
        $difference = (($year2 - $year1) * 12) + ($month2 - $month1);
        return abs($difference);
    }
    public static function getMonthRange(){
        return self::$_months;
    }
    public static function getYearRange(){
        return self::$_years;
    }
    public static function isFlipped(){
        return self::$_isFlipped;
    }
}
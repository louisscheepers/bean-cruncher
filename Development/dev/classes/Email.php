<?php
class Email{

    private static  $_headers,
                    $_head,
                    $_foot;
                                

    public function __construct($from = 'info@beancruncher.co.za'){
        self::$_headers = self::getHeaders($from);
        self::$_head = self::getHead();
        self::$_foot = self::getFoot();

    }

    private static function getHeaders($from = 'info@beancruncher.co.za'){
        return 'From: "Beancruncher" <' . $from . '>' . "\r\n" . 
            'MIME-Version: 1.0' . "\r\n" . 
            'Content-Type: text/html; charset=utf-8';
    }

    private static function getHead(){
        return '<html>' .
            '<head>' .
                '<title>Beancruncher</title>' .
            '</head>' .

            '<body style="font-family: Arial, Helvetica, sans-serif; color: #100D4C; text-align: center;">' .
                
                '<img src="https://i.imgur.com/3u7CnUo.png" style="display: block; margin-left: auto; margin-right: auto; width: 8%;">' .
                '<br />' .
                '<img src="https://i.imgur.com/6dkZUl1.png" style="display: block; margin-left: auto; margin-right: auto; width: 18%;">' .
            '<br />' . 
            '<br />';
    }   

    private static function getFoot(){
        return '<p>' .
                    'Regards, ' .
                    '<br />' .
                    'Beancruncher Team' .
                '</p>' .
                '<a href="https://app.beancruncheranalytics.com/unsub?email=thisuser" style="font-size: 0.01px; color:white">Unsubscribe</a>' .
                // '<br />' . 
                // '<a href="http://localhost/beancruncher/dev/index.php" style="text-decoration: none; font-size: small;">www.beancruncher.com</a>' .
            '</body>' .
            '</html>';
    }

    public static function sendCustom($recipient, $subject, $content){
        $email = mail($recipient, $subject, (self::$_head . $content . self::$_foot), self::$_headers);
        return $email;
    }

    public static function sendPreset($recipient, $preset, $addition = null){
        $email = mail($recipient, self::subject($preset), self::preset($preset, $recipient, $addition), self::getHeaders());
        return $email;
        // return true;
    }

    private static function preset($preset, $recipient, $addition){

        switch ($preset) {

            //Sign up verification
            case 'verify':
                $user = new User();
                $html =
                '<p>' .
                    '<b>' .
                        'Thank you for signing up to Beancruncher!' .
                    '</b>' .
                    '<br />' .
                    'Please click on the link below to verify your account:' .
                '</p>' .
                '<p>' .
                    '<a href="https://app.beancruncheranalytics.com/verifyAccount.php?email=' . $recipient . '&hash=' . $user->data()->user_salt . '" style="text-decoration: none; color: #FF9300; text-decoration: underline;">Verify your email</a>' .
                '</p>';
            break;

            //Reset password
            case 'password':
                $user = new User($recipient);
                $html =
                '<p>' .
                    '<b>' .
                        'Looks like you requested a password reset for your Beancruncner account, ' . $recipient .
                    '</b>' .
                    '<br />' .
                    'Please click on the link below to update your Beancruncher password:' .
                '</p>' .
                '<p>' .
                    '<a href="https://app.beancruncheranalytics.com/reset-password.php?email=' . $recipient . '&hash=' . $user->data()->user_salt . '" style="text-decoration: none; color: #FF9300; text-decoration: underline;">Reset your password</a>' .
                '</p>' .
                '<p>' .
                    'If this was not you, please ignore this email.' .
                '</p>';
            break;

            //Initial data import
            case 'import':
                $html =
                '<p>' .
                    '<b>' .
                        Companies::getActiveCompanyName() . '\'s data is being imported to the dashboard.' .
                    '</b>' .
                    '<br />' .
                    'This might take a few minutes.' .
                '</p>' .
                '<p>' .
                    'We will notify you when it is completed.' .
                '</p>';
            break;

            //Finished data import
            case 'importfin':
                $html =
                '<p>' .
                    '<b>' .
                        Companies::getActiveCompanyName() . '\'s data was succesfully imported to Beancruncher!' .
                    '</b>' .
                    '<br />' .
                    'Please click on the link below to return to the dashboard:' .
                '</p>' .
                '<p>' .
                    '<a href="https://app.beancruncheranalytics.com/index.php" style="text-decoration: none; color: #FF9300; text-decoration: underline;">www.beancruncheranalytics.com/index.php</a>' .
                '</p>';
            break;

            //Invite an unregistered user to your company
            case 'invite':
                $user = new User();
                $invitor = $user->data()->user_email;
                $html =
                '<p>' .
                    'You have been invited by ' . $invitor . ' to access ' . Companies::getActiveCompanyName() . '\'s Beancruncher profile.' .
                    '<br />' .
                    '<br />' .
                    'Please click on the link below to register to Beancruncher:' .
                '</p>' .
                '<p>' .
                    '<a href="https://app.beancruncheranalytics.com/register.php?email=' . $recipient . '" style="text-decoration: none; color: #FF9300; text-decoration: underline;">Register</a>' .
                '</p>';
            break;

            //Invite an existing user to your company
            case 'invite_exists':
                $user = new User();
                $invitor = $user->data()->user_email;
                $html =
                '<p>' .
                    'You have been invited by ' . $invitor . ' to access ' . Companies::getActiveCompanyName() . '\'s Beancruncher profile.' .
                    '<br />' .
                '</p>' .
                '<p>' .
                    '<a href="https://app.beancruncheranalytics.com/index.php" style="text-decoration: none; color: #FF9300; text-decoration: underline;">Go to Beancruncher</a>' .
                '</p>';
            break;

            //Email dashboard
            case 'report':
                $user = new User();
                $invitor = $user->data()->user_email;

                $html =
                '<p>' .
                    '<b>' .
                        'You have received a report from ' . $invitor . ' associated with ' . Companies::getActiveCompanyName() . '.' .
                    '</b>' .
                '</p>' .
                'Please click on the link below to see the file. Remember to save the file if you wish to view it again in the future.' .
                '<p>' .
                    '<a href="' . $addition . '" style="text-decoration: none; color: #FF9300; text-decoration: underline;">View PDF</a>' .
                '</p>';
            break;
            
        }

        return self::getHead() . $html . self::getFoot();
    }

    private static function subject($preset){

        switch ($preset) {
            case 'verify':
                $subject = 'Account verification';
            break;
            case 'password':
                $subject = 'Forgot your password?';
            break;
            case 'import':
                $subject = 'Importing data for ' . Companies::getActiveCompanyName();
            break;
            case 'importfin':
                $subject = 'Finished importing data for ' . Companies::getActiveCompanyName();
            break;
            case 'invite':
                $subject = 'You have been invited to ' . Companies::getActiveCompanyName();
            break;
            case 'invite_exists':
                $subject = 'You have been added to ' . Companies::getActiveCompanyName();
            break;
            case 'report':
                $subject = 'Report for ' . Companies::getActiveCompanyName();
            break;
        }

        return $subject;
    }

}
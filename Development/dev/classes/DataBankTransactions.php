<?php
class DataBankTransactions{
    private $_cashReceived,
            $_cashSpent,
            $_apiObject,
            $_apiId,
            $_month,
            $_xero,
            $_instance,
            $_db;

    public function __construct($month){
        $this->_month = $month;
        $this->_apiId = Companies::getApi();
        $this->_db = DB::getInstance();
    }

    private function setBankTransactions(){
        switch ($this->_apiId) {
            // XERO 
            case 1:
                // Xero does not use bank transactions
            break;
            
            // SAGE
            case 2:
                $this->_sage = Sage::getInstance();
                $provider = $this->_sage->getProvider();
                $this->_apiObject = $provider->BankTransactions()->get(Session::get('active_tenant_id'));
            break;
        }
    }

    public function getCashReceived(){
        $this->setBankTransactions();

        switch ($this->_apiId) {
            // XERO 
            case 1:
                // xero uses DataBalanceSheet
            break;

            // SAGE
            case 2:

                $transactions = $this->_apiObject->Results();
                $this->_cashReceived = 0;

                for($i = 0; $i < count($transactions); $i++){
                    $month = substr($transactions[$i]->Date, 0, 7);
                    if($month == $this->_month){
                        if($transactions[$i]->Total > 0){
                            $this->_cashReceived += $transactions[$i]->Total;
                        }
                    }
                }
                if($this->_cashReceived == null){
                    return 0;
                }
                return $this->_cashReceived;

            break;
        }
    }
    public function getCashSpent(){
        $this->setBankTransactions();

        switch ($this->_apiId) {
            // XERO 
            case 1:
                // xero uses DataBalanceSheet
            break;

            // SAGE
            case 2:

                $transactions = $this->_apiObject->Results();
                $this->_cashSpent = 0;

                for($i = 0; $i < count($transactions); $i++){
                    $month = substr($transactions[$i]->Date, 0, 7);
                    if($month == $this->_month){
                        if($transactions[$i]->Total < 0){
                            $this->_cashSpent += $transactions[$i]->Total;
                        }
                    }
                }
                if($this->_cashSpent == null){
                    return 0;
                }
                return abs($this->_cashSpent);

            break;
        }
    }
}
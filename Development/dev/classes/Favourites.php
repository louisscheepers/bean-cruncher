<?php 

//$layout = array('cico', 'avd', 'cico', 'cico', 'cico', 'cico', 'cico', 'cico');

class Favourites{

    public static function setFavourite($name, $layout){
        $db = DB::getInstance();

        $insertFavourite = $db->insert('temp_favourites', array(
            'user_id' => Session::get('user'),
            'favourite_name' => $name
        ));
        if($insertFavourite){
            $idQuery = $db->get('temp_favourites', array(
                'user_id' => array('operator' => '=', 'value' => Session::get('user')),
                'favourite_name' => array('operator' => '=', 'value' => $name)
            ));
            if($idQuery->results()){
                $data = $idQuery->results();
                $favId = $data[0]->favourite_id;
                
                for($i = 0; $i < count((array)$layout); $i++){
                    $module = $layout[$i]['module'];
                    switch($module){
                        case 'cico':
                            $moduleNumber = 1;
                        break;
                        case 'ino':
                            $moduleNumber = 2;
                        break;
                        case 'avd':
                            $moduleNumber = 3;
                        break;
                        case 'pl':
                            $moduleNumber = 4;
                        break;
                        case 'bs':
                            $moduleNumber = 5;
                        break;
                        case 'cs':
                            $moduleNumber = 6;
                        break;
                        case 'ar':
                            $moduleNumber = 7;
                        break;
                        case 'ddo':
                            $moduleNumber = 8;
                        break;
                        case 'tcs':
                            $moduleNumber = 9;
                        break;
                        case 'ret':
                            $moduleNumber = 10;
                        break;
                        case 'ap':
                            $moduleNumber = 11;
                        break;
                        case 'cdo':
                            $moduleNumber = 12;
                        break;
                        case 'tsp':
                            $moduleNumber = 13;
                        break;
                        case 'im':                
                            $moduleNumber = 14;
                        break;
                        case 'inas':
                            $moduleNumber = 15;
                        break;
                        case 'inaq':
                            $moduleNumber = 16;
                        break;
                        case 'plr':
                            $moduleNumber = 17;
                        break;
                        case 'bsr':
                            $moduleNumber = 18;
                        break;
                        case '0':
                            $moduleNumber = 0;
                        break;
                        case null:
                            $moduleNumber = 0;
                        break;
                    }

                    if(!isset($layout[$i]['from_date'])){
                        $fromDate = null;
                    }
                    else{
                        $fromDate = $layout[$i]['from_date'];
                    }
                    if(!isset($layout[$i]['to_date'])){
                        $toDate = null;
                    }
                    else{
                        $toDate = $layout[$i]['to_date'];
                    }

                    $insertFavourite = $db->insert('temp_block_layouts', array(
                        'favourite_id' => $favId,
                        'block_number' => $i + 1,
                        'module_id' => $moduleNumber,
                        'from_date' => $fromDate,
                        'to_date' => $toDate
                    ));
                }
            }
        }
    }

    public static function deleteFavourite($name){
        $db = DB::getInstance();

        $idQuery = $db->get('temp_favourites', array(
            'user_id' => array('operator' => '=', 'value' => Session::get('user')),
            'favourite_name' => array('operator' => '=', 'value' => $name)
        ));
        if($idQuery->results()){
            $data = $idQuery->results();
            $favId = $data[0]->favourite_id;

            $deleteLayout = $db->delete('temp_block_layouts', array(
                'favourite_id', '=', $favId
            ));
            $deleteFavourite = $db->delete('temp_favourites', array(
                'favourite_id', '=', $favId
            ));
        }
    }

    public static function getFavourites(){
        $db = DB::getInstance();
        $names = array();

        $idQuery = $db->get('temp_favourites', array(
            'user_id' => array('operator' => '=', 'value' => Session::get('user'))
        ));
        if($idQuery->results()){
            $data = $idQuery->results();

            for($i = 0; $i < count($data); $i++){
                $name = $data[$i]->favourite_name;
                array_push($names, $name);
            }
            return $names;
        }
    }

    public static function getLayout($name){
        $db = DB::getInstance();
        $boxes = array();
        
        $idQuery = $db->get('temp_favourites', array(
            'user_id' => array('operator' => '=', 'value' => Session::get('user')),
            'favourite_name' => array('operator' => '=', 'value' => $name)
        ));
        if($idQuery->results()){
            $data = $idQuery->results();
            $favId = $data[0]->favourite_id;
            
            $layoutQuery = $db->get('temp_block_layouts', array(
                'favourite_id' => array('operator' => '=', 'value' => $favId)
            ));
            if($layoutQuery->results()){
                $data = $layoutQuery->results();
                for($i = 0; $i < count($data); $i++){
                    $module = $data[$i]->module_id;
                    switch($module){
                        case 1:
                            $mod = 'cico';
                        break;
                        case 2:
                            $mod = 'ino';
                        break;
                        case 3:
                            $mod = 'avd';
                        break;
                        case 4:
                            $mod = 'pl';
                        break;
                        case 5:
                            $mod = 'bs';
                        break;
                        case 6:
                            $mod = 'cs';
                        break;
                        case 7:
                            $mod = 'ar';
                        break;
                        case 8:
                            $mod = 'ddo';
                        break;
                        case 9:
                            $mod = 'tcs';
                        break;
                        case 10:
                            $mod = 'ret';
                        break;
                        case 11:
                            $mod = 'ap';
                        break;
                        case 12:
                            $mod = 'cdo';
                        break;
                        case 13:
                            $mod = 'tsp';
                        break;
                        case 14:                
                            $mod = 'im';
                        break;
                        case 15:
                            $mod = 'inas';
                        break;
                        case 16:
                            $mod = 'inaq';
                        break;
                        case 17:
                            $mod = 'plr';
                        break;
                        case 18:
                            $mod = 'bsr';
                        break;
                        case 0:
                            $mod = null;
                        break;
                    }
                    $fromDate = $data[$i]->from_date;
                    $toDate = $data[$i]->to_date;

                    $boxes[$i]['module'] = $mod;
                    $boxes[$i]['from_date'] = $fromDate;
                    $boxes[$i]['to_date'] = $toDate;
                }
                return $boxes;
            }
        }
    }
}
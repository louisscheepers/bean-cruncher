<?php

use QuickBooksOnline\API\DataService\DataService;
require_once(dirname(__DIR__, 1) . '/const/const.php');

class Quickbooks{
    private static $_instance = null;
    private $_oauth2LoginHelper,
            $_dataService,
            $_qbTenantId = array(),
            $_accessToken,
            $_config,
            $_firstLink,
            $_apiInstance;

    public function __construct($firstLink = false){
        $this->_firstLink = $firstLink;
        $this->setDataService();
        // $this->setAccessToken();
    }

    private function setDataService($accessTokenKey = null, $refreshTokenKey = null, $realmId = null){
        if(!$this->_firstLink){
            $oauth = $this->getOauth2();
            $this->_dataService = DataService::Configure(array(
                'auth_mode' => 'oauth2',
                'ClientID' => __QB_CLIENT_ID__,
                'ClientSecret' =>  __QB_SECRET__,
                'accessTokenKey' => $oauth['access_token'],
                'refreshTokenKey' => $oauth['refresh_token'],
                'QBORealmID' => $oauth['realm_id'],
                'baseUrl' => "production"
            ));
            $OAuth2LoginHelper = $this->_dataService->getOAuth2LoginHelper();
            $accessToken = $OAuth2LoginHelper->refreshToken();
            $this->_dataService->updateOAuth2Token($accessToken);
        }
        else{
            $this->_dataService = DataService::Configure(array(
                'auth_mode' => 'oauth2',
                'ClientID' => __QB_CLIENT_ID__,
                'ClientSecret' =>  __QB_SECRET__,
                'RedirectURI' => __QB_REDIRECT_URI__,
                'scope' => 'com.intuit.quickbooks.accounting',
                'baseUrl' => "production"
            ));
        }
    }
    
    private function setAccessToken(){
        $this->_oauth2LoginHelper = $this->_dataService->getOAuth2LoginHelper();
    }

    private function isOauth2set(){
        $companyId = Companies::getActiveCompanyId();

        $db = DB::getInstance();
        
        $get = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $companyId)
        ));
        if($get->count()){
            return true;
        }
        return false;
    }

    public function getOauth2(){
        $db = DB::getInstance();
        $companyId = Companies::getActiveCompanyId();
        
        $get = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $companyId)
        ));
        if($get->count()){
            $companyId = $get->first()->company_id;
            $realmId = $get->first()->company_api_id;

            $getIntegration = $db->get('quickbooks_integration', array(
                'company_id' => array('operator' => '=', 'value' => $companyId)
            ));

            if($getIntegration->count()){
                $accessToken = $getIntegration->first()->integration_access_token;
                $refreshToken = $getIntegration->first()->refreh_token_key;

                return array(
                    'realm_id' => $realmId,
                    'access_token' => $accessToken,
                    'refresh_token' => $refreshToken
                );
            }
        }
        return false;
    }

    
    public function insertOauth($accessToken, $refreshToken, $realmId){
        $db = DB::getInstance();
        
        $company = $db->get('temp_company', array(
            'api_id' => array('operator' => '=', 'value' => 3),
            'company_api_id' => array('operator' => '=', 'value' => $realmId)
        ));
        if($company->count()){
            $companyId = $company->first()->company_id;

            $key = $db->get('quickbooks_integration', array(
                'company_id' => array('operator' => '=', 'value' => $companyId)
            ));
    
            if($key->count()){
                $dbUpdate = $db->update('quickbooks_integration', $companyId, 'company_id', array(
                    'integration_access_token' => $accessToken,
                    'refreh_token_key' => $refreshToken
                ));
                return true;
            }

            else{
                $dbInsert = $db->insert('quickbooks_integration', array(
                    'integration_access_token' => $accessToken,
                    'refreh_token_key' => $refreshToken,
                    'company_id' => $companyId
                ));

                return true;
            }
        }

        return false;
    }

    public function getOAuth2LoginHelper(){
        return $this->_oauth2LoginHelper;
    }

    public function getDataService(){
        return $this->_dataService;
    }

    public function getAccessToken(){
        if(isset($_SESSION['sessionAccessToken'])){
            return $_SESSION['sessionAccessToken'];
        }
        else{
            $this->setAccessToken();
            return $this->_accessToken;
        }
    }

    public function updateOAuth2Token(){
        $this->_accessToken = $this->getAccessToken();
        $this->_dataService->updateOAuth2Token($this->_accessToken);
    }

    public static function getInstance(){
        if(!isset(self::$_instance)){
            self::$_instance = new Quickbooks();
        }
        return self::$_instance;
    }
}
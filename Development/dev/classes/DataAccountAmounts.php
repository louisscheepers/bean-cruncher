<?php

class DataAccountAmounts{
    private $_profitAndLoss = array(
                'sales' => array(),
                'total_sales' => array('ytd' => 0, 'amount' => 0),
                'cost_of_sales' => array(),
                'total_cost_of_sales' => array('ytd' => 0, 'amount' => 0),
                'gross_profit' => array('ytd' => 0, 'amount' => 0),
                'other_income' => array(),
                'total_other_income' => array('ytd' => 0, 'amount' => 0),
                'expenses' => array(),
                'total_expenses' => array('ytd' => 0, 'amount' => 0),
                'net_profit_before_tax' => array('ytd' => 0, 'amount' => 0),
                'tax' => array(),
                'total_tax' => array('ytd' => 0, 'amount' => 0),
                'net_profit_after_tax' => array('ytd' => 0, 'amount' => 0),
                'last_updated' => array()
            ),  
            $_balanceSheet = array(
                'current_assets' => array(),
                'total_current_assets' => array(),
                'non_current_assets' => array(),
                'total_non_current_assets' => array(),
                'total_assets' => array(),
                'current_liabilities' => array(),
                'total_current_liabilities' => array(),
                'non_current_liabilities' => array(),
                'total_non_current_liabilities' => array(),
                'total_liabilities' => array(),
                'equity' => array(),
                'total_equity' => array(),
                'total_equity_and_liabilities' => array(),
                'last_updated' => array()
            ),
            $_accountAmounts,
            $_ytd = array(),
            $_chartOfAccounts = array(),
            $_interval,
            $_apiId,
            $_apiObject,
            $_fromDate,
            $_toDate,
            $_xero,
            $_sage,
            $_instance,
            $_db;

    public function __construct($fromDate, $interval){
        $this->_fromDate = $fromDate;

        $toDateRaw = new DateTime($this->_fromDate);
        $toDate = $toDateRaw->format('Y-m-t');

        $this->_toDate = $toDate;
        $this->_apiId = Companies::getApi();
        $this->_interval = $interval;
        $this->_db = DB::getInstance();
        $this->_balanceSheet = new BalanceSheetModel();
    }

    private function setAccountAmounts(){
        switch ($this->_apiId) {
            // XERO 
            case 1:
                // Xero does not use account amounts
            break;
            
            // SAGE
            case 2:
                $fromDateRaw = new DateTime($this->_fromDate);
                $fromDateMin = $fromDateRaw->modify('-11 months');
                $fromDate = $fromDateMin->format('Y-m-d');

                $this->_sage = Sage::getInstance();
                $provider = $this->_sage->getProvider();
                $this->_apiObject = $provider->AccountBalance()->get(Session::get('active_tenant_id'), array(
                    'FromDate' => $fromDate,
                    'ToDate' => $this->_toDate
                ));
            break;
            
            // QUICKBOOKS
            case 3:

                // $fromDateRaw = new DateTime($this->_fromDate);
                // $fromDateMin = $fromDateRaw->modify('-11 months');
                // $fromDate = $fromDateMin->format('Y-m-d');

                // $this->_quickbooks = Quickbooks::getInstance();
                // $dataService = $this->_quickbooks->getDataService();
                
                // $serviceContext = $dataService->getServiceContext();
                // $reportService = new ReportService($serviceContext);

                // $reportService->setStartDate('2020-11-01');
                // $reportService->setEndDate('2020-11-30');

                // $this->_apiObject = $reportService->executeReport('TrialBalance');
                
            break;
        }
    }

    public function getBalanceSheetFormat(){
        $this->setAccountAmounts();

        $currentAssetsCount = 0;
        $nonCurrentAssetsCount = 0;
        $currentLiabilitiesCount = 0;
        $nonCurrentLiabilitiesCount = 0;
        $equityCount = 0;

        switch ($this->_apiId) {
            // XERO 
            case 1:
                // xero uses DataBalanceSheet
            break;

            // SAGE
            case 2:
                $rows = $this->_apiObject->Results()->Results;
                $processedAccounts = array();

                foreach($rows as $row){
                    $type = $row->CategoryDescription;
                    $title = $row->Description;
                    $date = substr($row->Date, 0, 7);
                    if($row->Debit != 0){
                        $amount = $row->Debit; //- TOTALS
                    }
                    else{
                        $amount = $row->Credit;
                    }

                    if(!in_array($title, array_column($processedAccounts, 'account'))){
                        

                        $fields = array(
                            'account' => $title,
                            'type' => $type,
                            'data' => array(
                                0 => array(
                                    'date' => $date,
                                    'amount' => $amount
                                )
                            )
                        );

                        array_push($processedAccounts, $fields);
                    }else{
                        for($i = 0; $i < count($processedAccounts); $i++){
                            $account = $processedAccounts[$i];
                            if($title == $account['account']){
                                $matched = false;
                                for($j = 0; $j < count($account['data']); $j++){
                                    if($account['data'][$j]['date'] == $date){
                                        $processedAccounts[$i]['data'][$j]['amount'] +=  $amount;
                                        $matched = true;
                                        break;
                                    }
                                }

                                if(!$matched){
                                    array_push($processedAccounts[$i]['data'], array(
                                        'date' => $date,
                                        'amount' => $amount
                                    ));
                                }
                            }
                        }
                    }
                }

                $processedAccounts = $this->addMissingDates($processedAccounts);
                
                return $this->addToBalanceSheet($this->_balanceSheet, $processedAccounts);

            break;
            // QB 
            case 3:
                
                // var_dump($this->_apiObject->Rows->Row);
                // foreach($this->_apiObject->Rows->Row as $colData){
                //     var_dump($colData);
                // }
                // die();

            break;
        }
    }

    private function addToBalanceSheet($bs, $accounts){
        foreach($accounts as $account){
            $fields = array(
                'account' => $account['account'],
                'data' => $account['data']
            );

            if($account['type'] == 'Current Assets'){
                $bs->addCurrentAsset($fields);
            }
            else if($account['type'] == 'Non-Current Assets'){
                $bs->addNonCurrentAsset($fields);
            }
            else if($account['type'] == 'Current Liabilities'){
                $bs->addCurrentLiability($fields);
            }
            else if($account['type'] == 'Non-Current Liabilities'){
                $bs->addNonCurrentLiability($fields);
            }
            else if($account['type'] == 'Owners Equity'){
                $bs->addEquity($fields);
            }
        }

        return $bs;
    }



    private function addMissingDates($accounts){

        for($i = 0; $i < count($accounts); $i++){
            $data = $accounts[$i]['data'];

            for($k = 0; $k < 12; $k++){
                $date = date('Y-m', strtotime($this->_fromDate. ' - ' . $k . ' month'));
                
                if(!in_array($date, array_column($data, 'date'))){
                    array_push($accounts[$i]['data'], array(
                        'date' => $date,
                        'amount' => 0
                    ));
                }
            }

        }

        return $accounts;
    }

    public function getProfitAndLossFormat(){
        $this->setAccountAmounts();

        $salesCount = 0;
        $directCostCount = 0;
        $otherIncomeCount = 0;
        $expensesCount = 0;
        $taxCount = 0;

        switch ($this->_apiId) {
            // XERO 
            case 1:
                // xero uses DataProfitAndLoss
            break;

            // SAGE
            case 2:
                $rows = $this->_apiObject->Results()->Results;
                $processedAccounts = array();
                
                for($i = 0; $i < count($rows); $i++){
                    $type = $rows[$i]->CategoryDescription;
                    $title = $rows[$i]->Description;

                    if(!in_array($title, $processedAccounts)){
                        for($k = 0; $k < 12; $k++){
                            $date = date('Y-m-d', strtotime($this->_fromDate. ' - ' . $k . ' month'));
                            $amount = 0;
                            if(substr($rows[$i]->Date, 0, 10) == $date){
                                if($rows[$i]->Debit != 0){
                                    $amount = $rows[$i]->Debit; //- TOTALS
                                }
                                else{
                                    $amount = $rows[$i]->Credit;
                                }
                            }
                            $values[$k] = array('date' => $date, 'amount' => $amount);
                        }
                        array_push($processedAccounts, $title);

                        if($type == 'Sales'){
                            $this->_profitAndLoss['sales'][$salesCount] = array(
                                'account' => $title,
                                'amount' => $values
                            );
                            $salesCount++;
                        }
                        else if ($type == 'Cost of Sales'){
                            $this->_profitAndLoss['cost_of_sales'][$directCostCount] = array(
                                'account' => $title,
                                'amount' => $values
                            ); 
                            $directCostCount++;
                        }
                        else if ($type == 'Other Income'){
                            $this->_profitAndLoss['other_income'][$otherIncomeCount] = array(
                                'account' => $title,
                                'amount' => $values
                            );
                            $otherIncomeCount++;
                        }
                        else if ($type == 'Expenses'){
                            $this->_profitAndLoss['expenses'][$expensesCount] = array(
                                'account' => $title,
                                'amount' => $values
                            );
                            $expensesCount++;
                        }
                        else if ($type == 'Tax'){
                            $this->_profitAndLoss['tax'][$taxCount] = array(
                                'account' => $title,
                                'amount' => $values
                            );
                            $taxCount++;
                        }
                        else{
                            // echo 'No type ' . $type . '<br>';
                        }
                    }
                    else{
                        if($type == 'Sales'){
                            $this->processValue('pl', 'sales', $rows[$i], $title);
                        }
                        else if ($type == 'Cost of Sales'){
                            $this->processValue('pl', 'cost_of_sales', $rows[$i], $title);
                        }
                        else if ($type == 'Other Income'){
                            $this->processValue('pl', 'other_income', $rows[$i], $title);
                        }
                        else if ($type == 'Expenses'){
                            $this->processValue('pl', 'expenses', $rows[$i], $title);
                        }
                        else if ($type == 'Tax'){
                            $this->processValue('pl', 'tax', $rows[$i], $title);
                        }
                        else{
                            // echo 'No type ' . $type . '<br>';
                        }
                    }
                }

                $this->calculateTotals('pl');
                return $this->_profitAndLoss;

            break;
        }
    }

    private function processBsValue($accountType, $row, $title, $bsmodelArray = array()){
        $array = $bsmodelArray;

        foreach($array as $section){
            if($title == $section['account']){
                for($m = 0; $m < 12; $m++){
                    $date = date('Y-m-d', strtotime($this->_fromDate. ' - ' . $m . ' month'));

                    if(substr($row->Date, 0, 10) == $date){
                        if($row->Debit != 0){
                            $amount = $row->Debit; //- TOTALS
                        }
                        else{
                            $amount = $row->Credit;
                        }

                        $values = array(
                            'date' => $date,
                            'amount' =>$amount
                        );

                        $fields = array(
                            'account' => $title,
                            'data' => $values
                        );

                        /*  Would prefer that this function returns the fields to be added to the 
                            corresponding balance sheet section in the calling function rather than
                            here but due to time constraints ill just
                            add them here like the author originally wrote */

                        if($accountType == 'current_assets'){
                            $this->_balanceSheet->addCurrentAsset($fields);
                        }
                        else if($accountType == 'non_current_assets'){
                            $this->_balanceSheet->addNonCurrentAsset($fields);
                        }
                        else if($accountType == 'current_liabilities'){
                            $this->_balanceSheet->addCurrentLiability($fields);
                        }
                        else if($accountType == 'non_current_liabilities'){
                            $this->_balanceSheet->addNonCurrentLiability($fields);
                        }
                        else if($accountType == 'equity'){
                            $this->_balanceSheet->addEquity($fields);
                        }
                    }
                }

                break;
            }
        }
    }

    private function processValue($report, $accountType, $row, $title, $bsmodelArray = array()){
        $array = array();
        switch ($report) {
            case 'bs':
                $array = $bsmodelArray;
            break;
            case 'pl':
                $array = $this->_profitAndLoss[$accountType];
            break;
        }

        for($j = 0; $j < count($array); $j++){
            if($title == $array[$j]['account']){
                for($k = 0; $k < 12; $k++){
                    $date = date('Y-m-d', strtotime($this->_fromDate. ' - ' . $k . ' month'));
                    if(substr($row->Date, 0, 10) == $date){
                        if($row->Debit != 0){
                            $amount = $row->Debit; //- TOTALS
                        }
                        else{
                            $amount = $row->Credit;
                        }
                        switch ($report) {
                            case 'bs':
                                $values = array(
                                    'date' => $date,
                                    'amount' =>$amount
                                );

                                $fields = array(
                                    'account' => $title,
                                    'data' => $values
                                );

                                if($accountType == 'current_assets'){
                                    $this->_balanceSheet->addCurrentAsset($fields);
                                }
                                else if($accountType == 'non_current_assets'){
                                    $this->_balanceSheet->addNonCurrentAsset($fields);
                                }
                                else if($accountType == 'current_liabilities'){
                                    $this->_balanceSheet->addCurrentLiability($fields);
                                }
                                else if($accountType == 'non_current_liabilities'){
                                    $this->_balanceSheet->addNonCurrentLiability($fields);
                                }
                                else if($accountType == 'equity'){
                                    $this->_balanceSheet->addEquity($fields);
                                }

                            break;
                            case 'pl':
                                $this->_profitAndLoss[$accountType][$j]['amount'][$k]['amount'] = $amount;
                            break;
                        }
                    }
                }
                break;
            }
        }
    }

    private function calculateTotals($report){
        switch ($report) {
            case 'bs':
                // configured on DataBalanceSheet
            break;
            case 'pl':
                $totals = array('sales', 'cost_of_sales', 'other_income', 'expenses');
                $targets = array('total_sales', 'total_cost_of_sales', 'total_other_income', 'total_expenses');
                $targetAcc = array('Total Income', 'Total Cost of Sales', 'Gross Profit', 'Total Other Income', 'Total Operating Expenses');
                //gross_profit, net_profit_before_tax

                for($i = 0; $i < count($totals); $i++){
                    $this->calculateRowTotal($totals[$i], $targets[$i], $targetAcc[$i]);
                }

                $this->_profitAndLoss['gross_profit'][0]['account'] = 'Gross Profit';
                $this->_profitAndLoss['net_profit_before_tax'][0]['account'] = 'Net Profit';
                for($k = 0; $k < 12; $k++){
                    $date = date('Y-m-d', strtotime($this->_fromDate. ' - ' . $k . ' month'));

                    //gross profit
                    $this->_profitAndLoss['gross_profit'][0]['amount'][$k]['amount'] = $this->_profitAndLoss['total_sales'][0]['amount'][$k]['amount'] - $this->_profitAndLoss['total_cost_of_sales'][0]['amount'][$k]['amount'];
                    $this->_profitAndLoss['gross_profit'][0]['amount'][$k]['date'] = $date;
                    //net profit before tax
                    $this->_profitAndLoss['net_profit_before_tax'][0]['amount'][$k]['amount'] = $this->_profitAndLoss['gross_profit'][0]['amount'][$k]['amount'] - $this->_profitAndLoss['total_expenses'][0]['amount'][$k]['amount'] + $this->_profitAndLoss['total_other_income'][0]['amount'][$k]['amount'];
                    $this->_profitAndLoss['net_profit_before_tax'][0]['amount'][$k]['date'] = $date;
                }
            break;
        }
    }
    private function calculateRowTotal($row, $target, $accountName){

        $this->_profitAndLoss[$target][0]['account'] = $accountName;
        for($k = 0; $k < 12; $k++){
            $total = 0;
            $date = date('Y-m-d', strtotime($this->_fromDate. ' - ' . $k . ' month'));

            for($i = 0; $i < count($this->_profitAndLoss[$row]); $i++){
                $total += $this->_profitAndLoss[$row][$i]['amount'][$k]['amount'];
            }
            $this->_profitAndLoss[$target][0]['amount'][$k]['amount'] = $total;
            $this->_profitAndLoss[$target][0]['amount'][$k]['date'] = $date;
        }
    }
}
<?php 

class Oauth{

    //set after setting company in DB
    function setDbOauth($token, $tenantId, $refreshToken, $idToken, $expires){
        $db = DB::getInstance();
        
		// for($i = 0; $i < count((array)$tenantId); $i++){
			$companies = $db->get('temp_company', array(
				'company_api_id' => array('operator' => '=', 'value' => $tenantId)
			));
			if($companies->results()){
				$company_id = $companies->first()->company_id;
				
                $integration = $db->get('xero_integration', array(
                    'company_id' => array('operator' => '=', 'value' => $company_id)
                ));
                if($integration->results()){
                    $dbUpdate = $db->update('xero_integration', 'company_id', $company_id, array(
                        'integration_token' => $token,
                        'integration_refresh_token' => $refreshToken,
                        'integration_id_token' => $idToken,
                        'integration_expiry' => $expires
                    ));
                }
                else{
                    $fields = array(
                    	'integration_token' => $token,
                    	'integration_refresh_token' => $refreshToken,
                        'integration_id_token' => $idToken,
                        'integration_expiry' => $expires,
                    	'company_id' => $company_id
                    );

                    if(!$db->insert('xero_integration', $fields)){
                        throw new Exception('There was a problem integrating the company');
                    }
                }
			}
			else{
                // return false;
			}
		// }
    }

    function setSessionOauthFromDb(){
        $token = null;
        $expires = null;
        $tenantId = null;
        $refreshToken = null;
        $idToken = null;

        $db = DB::getInstance();
        $comp = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
        ));
        if($comp->results()){
            $tenantId = $comp->first()->company_api_id;
        }
        
        $integrate = $db->get('xero_integration', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
        ));
        if($integrate->results()){
            $token = $integrate->first()->integration_token;
            $expires = $integrate->first()->integration_expiry;
            $refreshToken = $integrate->first()->integration_refresh_token;
            $idToken = $integrate->first()->integration_id_token;
        }

        $_SESSION['oauth2'] = [
            'token' => $token,
            'expires' => $expires,
            'tenant_id' => $tenantId,
            'refresh_token' => $refreshToken,
            'id_token' => $idToken
        ];
    }
    
    function getOauth(){
        if(!isset($_SESSION['oauth2'])){
            $this->setSessionOauthFromDb();
        }
        return $_SESSION['oauth2'];
    }
}
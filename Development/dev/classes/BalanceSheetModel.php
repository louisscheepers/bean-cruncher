<?php 

class BalanceSheetModel{
    private $_currentAssets = array(),
            $_totalCurrentAssets  = array(),
            $_nonCurrentAssets  = array(),
            $_totalNonCurrentAssets  = array(),
            $_totalAssets  = array(), 
            $_currentLiabilities  = array(),
            $_totalCurrentLiabilities = array(),
            $_nonCurrentLiabilities  = array(),
            $_totalNonCurrentLiabilities  = array(),
            $_totalLiabilities  = array(),
            $_equity  = array(),
            $_totalEquity  = array(),
            $_totalEquityAndLiabilities  = array(),
            $_lastUpdated  = array();

    public function __construct()
    {
        //Silence is golden
    }

    public function addCurrentAsset($fields = array()){
        if(is_array($fields) && !empty($fields)){
            if($this->validateFields($fields, 'Current Asset')){
                array_push($this->_currentAssets, $fields);
            }
        }
    }

    public function addNonCurrentAsset($fields = array()){
        if(is_array($fields) && !empty($fields)){
            if($this->validateFields($fields, 'Non Current Asset')){
                array_push($this->_nonCurrentAssets, $fields);
            }
        }
    }

    public function addCurrentLiability($fields = array()){
        if(is_array($fields) && !empty($fields)){
            if($this->validateFields($fields, 'Current Liabilities')){
                array_push($this->_currentLiabilities, $fields);
            }
        }
    }

    public function addNonCurrentLiability($fields = array()){
        if(is_array($fields) && !empty($fields)){
            if($this->validateFields($fields, 'Non Current Liabilities')){
                array_push($this->_nonCurrentLiabilities, $fields);
            }
        }
    }

    public function addEquity($fields = array()){
        if(is_array($fields) && !empty($fields)){
            if($this->validateFields($fields, 'Equity')){
                array_push($this->_equity, $fields);
            }
        }
    }

    public function getCurrentAssets(){
        return $this->_currentAssets;
    }

    public function getNonCurrentAssets(){
        return $this->_nonCurrentAssets;
    }

    public function getCurrentLiabilities(){
        return $this->_currentLiabilities;
    }

    public function getNonCurrentLiabilities(){
        return $this->_nonCurrentLiabilities;
    }

    public function getEquity(){
        return $this->_equity;
    }

    
    public function getTotalCurrentAssets($byDate = false){
        if($byDate){
            return $this->calculateTotalsByDate($this->getCurrentAssets());
        }
        return $this->calculateTotals($this->getCurrentAssets());
    }

    public function getTotalNonCurrentAssets($byDate = false){
        if($byDate){
            return $this->calculateTotalsByDate($this->getNonCurrentAssets());
        }
        return $this->calculateTotals($this->getNonCurrentAssets());
    }

    public function getTotalAssets($byDate = false){
        if($byDate){
            return $this->mergeTotalsByDate($this->getTotalCurrentAssets($byDate), $this->getTotalNonCurrentAssets($byDate) );
        }
        return $this->getTotalCurrentAssets() + $this->getTotalNonCurrentAssets();
    }
    
    public function getTotalCurrentLiabilities($byDate = false){
        if($byDate){
            return $this->calculateTotalsByDate($this->getCurrentLiabilities());
        }
        return $this->calculateTotals($this->getCurrentLiabilities());
    }

    public function getTotalNonCurrentLiabilities($byDate = false){
        if($byDate){
            return $this->calculateTotalsByDate($this->getNonCurrentLiabilities());
        }
        return $this->calculateTotals($this->getNonCurrentLiabilities());
    }

    public function getTotalLiabilities($byDate = false){
        if($byDate){
            return $this->mergeTotalsByDate($this->getTotalCurrentLiabilities($byDate), $this->getTotalNonCurrentLiabilities($byDate));
        }
        return $this->getTotalCurrentLiabilities() + $this->getTotalNonCurrentLiabilities();
    }

    public function getTotalEquity($byDate = false){
        if($byDate){
            return $this->calculateTotalsByDate($this->getEquity());
        }
        return $this->calculateTotals($this->getEquity());
    }

    public function getTotalEquityAndLiabilities($byDate = false){
        if($byDate){
            return $this->mergeTotalsByDate($this->getTotalEquity($byDate), $this->getTotalLiabilities($byDate));
        }
        return $this->getTotalEquity() + $this->getTotalLiabilities();
    }

    public function addLastUpdated($lastUpdated){
        array_push($this->_lastUpdated, $lastUpdated);
    }

    public function getAllLastUpdated(){
        return $this->_lastUpdated;
    }

    public function getLastUpdated(){
        if(isset($this->getAllLastUpdated()[0])){
            $lastupdate =  min($this->getAllLastUpdated());
            if($lastupdate != '' || !empty($lastupdate)){
    
                if(is_array($lastupdate)){
                    return array_unique($lastupdate);
                }
                
                return $lastupdate;
            }
        }

        return 'Never';
    }

    private function calculateTotals($items = array()){
        $total = 0;
        
        foreach($items as $item){
            $data = $item['data'];
            if(array_key_exists(0,$data)){
                foreach($data as $dataitem){
                    $amount = $dataitem['amount'];
                    $total += $amount;
                }
            }else{
                $total += $data['amount'];
            }
        }

        return $total;
    }

    private function calculateTotalsByDate($items){
        $total = array();
        
        foreach($items as $item){
            $data = $item['data'];
            if(array_key_exists(0,$data)){
                //setValues
                foreach($data as $dataitem){
                    if(!array_key_exists($dataitem['date'], $total)){
                        $total[$dataitem['date']] = 0;
                    }

                    $total[$dataitem['date']] += $dataitem['amount'];
                }
            }else{
                $total[$data['date']] += $data['amount'];
            }
        }

        return $total;
        
    }

    private function mergeTotalsByDate($firstTotals = array(), $secondTotals = array()){

        //can be re-written

        $mergedTotals = array();
        if(empty($firstTotals)){
            foreach($secondTotals as $xDate => $xAmount){
                $mergedTotals[$xDate] = $xAmount;
                foreach($firstTotals as $yDate => $yAmount){
                    if($xDate == $yDate){
                        $mergedTotals[$xDate] = $mergedTotals[$xDate] + $yAmount;
                    }
                }
            }
        }
        else{
            foreach($firstTotals as $xDate => $xAmount){
                $mergedTotals[$xDate] = $xAmount;
                foreach($secondTotals as $yDate => $yAmount){
                    if($xDate == $yDate){
                        $mergedTotals[$xDate] = $mergedTotals[$xDate] + $yAmount;
                    }
                }
            }
        }
        return $mergedTotals;
    }

    private function throwKeyException($key, $item){
        throw new Exception('BlanceSheetModel: The $fields array has no ' . $key . ' key while trying to add a ' . $item);
    }

    private function validateFields($fields, $item){
        $valid = true;
        if(!array_key_exists('account', $fields)){
            $valid = false;
            $this->throwKeyException('account', $item);
        }

        if(!array_key_exists('data', $fields)){
            $valid = false;
            $this->throwKeyException('data', $item);
        }else{
            $data = $fields['data'];

            if(array_key_exists(0,$data)){
                $data = $data[0];
            }

            if(!array_key_exists('date', (is_array($data) ? $data : $fields))){
                $valid = false;
                $this->throwKeyException('date', $item);
            }
    
            if(!array_key_exists('amount', $data)){
                $valid = false;
                $this->throwKeyException('amount', $item);
            }
        }  
        
        return $valid;
    }

    public function toJson(){
        

        return json_encode($this->toArray());
    }

    public function toArray($byDate = false){
        $bsArray = array(
            'current_assets' => array(),
            'total_current_assets' => array(),
            'non_current_assets' => array(),
            'total_non_current_assets' => array(),
            'total_assets' => array(),
            'current_liabilities' => array(),
            'total_current_liabilities' => array(),
            'non_current_liabilities' => array(),
            'total_non_current_liabilities' => array(),
            'total_liabilities' => array(),
            'equity' => array(),
            'total_equity' => array(),
            'total_equity_and_liabilities' => array(),
            'last_updated' => array()
        );

        // var_dump($byDate);

        $bsArray['current_assets'] = $this->sortAlphabetically($this->getCurrentAssets());
        $bsArray['total_current_assets'] = (empty($this->getTotalCurrentAssets($byDate))) ? 0 : $this->getTotalCurrentAssets($byDate);
        $bsArray['non_current_assets'] =  $this->sortAlphabetically($this->getNonCurrentAssets());
        $bsArray['total_non_current_assets'] =  (empty($this->getTotalNonCurrentAssets($byDate)) ? 0 : $this->getTotalNonCurrentAssets($byDate));
        $bsArray['total_assets'] = (empty($this->getTotalAssets($byDate))) ? 0 :  $this->getTotalAssets($byDate);
        $bsArray['current_liabilities'] =  $this->sortAlphabetically($this->getCurrentLiabilities());
        $bsArray['total_current_liabilities'] =  (empty($this->getTotalCurrentLiabilities($byDate))) ? 0 :$this->getTotalCurrentLiabilities($byDate);
        $bsArray['non_current_liabilities'] = $this->sortAlphabetically($this->getNonCurrentLiabilities());
        $bsArray['total_non_current_liabilities'] = (empty($this->getTotalNonCurrentLiabilities($byDate))) ? 0 : $this->getTotalNonCurrentLiabilities($byDate);
        $bsArray['total_liabilities'] =  (empty($this->getTotalLiabilities($byDate))) ? 0 : $this->getTotalLiabilities($byDate);
        // var_dump($this->getTotalLiabilities($byDate));
        // var_dump($bsArray['total_liabilities']);
        $bsArray['equity'] = $this->sortAlphabetically($this->getEquity());
        $bsArray['total_equity'] = (empty($this->getTotalEquity($byDate))) ? 0 : $this->getTotalEquity($byDate);
        $bsArray['total_equity_and_liabilities'] = (empty($this->getTotalEquityAndLiabilities($byDate))) ? 0 : $this->getTotalEquityAndLiabilities($byDate);
        $bsArray['last_updated'] = $this->getLastUpdated();

        return $bsArray;
    }

    private function sortAlphabetically($array){
        usort($array, function ($elem1, $elem2) {
            return strcmp($elem1['account'], $elem2['account']);
       });
        return $array;
    }

}
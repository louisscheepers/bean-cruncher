<?php

class DataLineItems{
    private $_invoices,
            $_lineItems,
            $_apiObject,
            $_xero,
            $_instance;

    public function __construct(){
        $this->_db = DB::getInstance();
    }

    private function setApiObject($invoiceId){
        $this->_xero = Xero::getInstance();
        $this->_instance = $this->_xero->getApiInstance();
        $this->_apiObject = $this->_instance->getInvoices(Companies::getActiveTenantId(), '', '', '', $invoiceId)->getInvoices()[0]->getLineItems();
    }

    public function setLineItems(){
        $inv = new DataInvoices(null);
        $this->_invoices = $inv->getInvoices();
        $invoiceCount = count($this->_invoices) - 1;

        for($i = 0; $i < $invoiceCount; $i++){
            $lineItemsCount = 0;

            $invoiceId = $this->_invoices[$i]['api_invoice_id'];
            $this->setApiObject($invoiceId);
            $lineItems[$i] = $this->_apiObject;

            for($j = 0; $j < count($lineItems[$i]); $j++){
                $itemCode = $lineItems[$i][$j]['item_code'];
                $quantity = $lineItems[$i][$j]['quantity'];
                $lineAmount = $lineItems[$i][$j]['line_amount'];
                // $description = $lineItems[$i][$j]['description'];
                
                $this->_lineItems[$i]['invoice_id'] = $invoiceId;
                if($itemCode == null){
                    $this->_lineItems[$i]['line_items'][$lineItemsCount]['item_code'] = 0;
                }
                else{
                    $this->_lineItems[$i]['line_items'][$lineItemsCount]['item_code'] = $itemCode;
                }
                $this->_lineItems[$i]['line_items'][$lineItemsCount]['quantity'] = $quantity;
                $this->_lineItems[$i]['line_items'][$lineItemsCount]['line_amount'] = $lineAmount;
                $lineItemsCount++;
            }
        }
    }

    public function updateDb(){
        $this->setLineItems();

        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        for($i = 0; $i < count($this->_lineItems); $i++){
            // var_dump($this->_lineItems[$i]);

            $dbQueryInvoiceId = $this->_db->get('temp_invoices', array(
                'api_invoice_id' => array('operator' => '=', 'value' => $this->_lineItems[$i]['invoice_id'])
            ));
            if($dbQueryInvoiceId->results()){
                $data = $dbQueryInvoiceId->results();
                $invId = $data[0]->invoice_id;

                $dbQueryLineItemsId = $this->_db->get('temp_line_items', array(
                    'invoice_id' => array('operator' => '=', 'value' => $invId)
                ));
                if($dbQueryInvoiceId->results()){
                    //update
                    $data = $dbQueryInvoiceId->results();
                    $lineItemsId = $data[0]->line_items_id;

                    $dbUpdateLineItemId = $this->_db->update('temp_invoices', $invId, 'invoice_id', array(
                        'line_items_id' => $lineItemsId
                    ));

                    $dbQueryLineItemsId2 = $this->_db->get('temp_line_item_desc', array(
                        'line_items_id' => array('operator' => '=', 'value' => $lineItemsId)
                    ));
                    if($dbQueryLineItemsId2->results()){
                        $data = $dbQueryLineItemsId2->results();

                        if($dbQueryLineItemsId2->count() == count((array)$this->_lineItems[$i]['line_items'])){
                           
                            // echo 'Found all items, just update <br>';
    
                            for($j = 0; $j < count((array)$this->_lineItems[$i]['line_items']); $j++){
                                $lineItemDescId = $data[$j]->line_item_desc_id;
                                // echo 'line_item_desc_id: ' . $lineItemDescId . '<br>';
                                $dbUpdateLineItemDesc = $this->_db->update('temp_line_item_desc', $lineItemDescId, 'line_item_desc_id', array(
                                    'line_items_id' => $lineItemsId,
                                    'item_code' => $this->_lineItems[$i]['line_items'][$j]['item_code'],
                                    'quantity' => $this->_lineItems[$i]['line_items'][$j]['quantity'],
                                    'line_amount' => $this->_lineItems[$i]['line_items'][$j]['line_amount'],
                                    'last_updated' => $now,
                                ));
                            }
                        }
                        else{
                            // echo 'Item amounts did not match, deleteing and inserting all again <br>';
                            $deleteComment = $this->_db->delete('temp_line_item_desc', array(
                                'line_items_id', '=', $lineItemsId
                            ));
                            
                            for($j = 0; $j < count((array)$this->_lineItems[$i]['line_items']); $j++){
                                $dbInsert = $this->_db->insert('temp_line_item_desc', array(
                                    'line_items_id' => $lineItemsId,
                                    'item_code' => $this->_lineItems[$i]['line_items'][$j]['item_code'],
                                    'quantity' => $this->_lineItems[$i]['line_items'][$j]['quantity'],
                                    'line_amount' => $this->_lineItems[$i]['line_items'][$j]['line_amount'],
                                    'last_updated' => $now,
                                ));
                            }
                        }
                    }
                    else{
                        // echo 'Line items completely missing, inserting ...<br>';
                        for($j = 0; $j < count((array)$this->_lineItems[$i]['line_items']); $j++){
                            $dbInsert = $this->_db->insert('temp_line_item_desc', array(
                                'line_items_id' => $lineItemsId,
                                'item_code' => $this->_lineItems[$i]['line_items'][$j]['item_code'],
                                'quantity' => $this->_lineItems[$i]['line_items'][$j]['quantity'],
                                'line_amount' => $this->_lineItems[$i]['line_items'][$j]['line_amount'],
                                'last_updated' => $now,
                            ));
                        }
                    }
                    

                }
                else{
                    $dbInsert = $this->_db->insert('temp_line_items', array(
                        'invoice_id' => $invId
                    ));
                    if($dbInsert){
                        $dbQueryLineItemsId2 = $this->_db->get('temp_line_items', array(
                            'invoice_id' => array('operator' => '=', 'value' => $invId)
                        ));
                        if($dbQueryLineItemsId2->results()){
                            $data = $dbQueryLineItemsId2->results();
                            $lineItemsId = $data[0]->line_items_id;
                            
                            $dbUpdateLineItemId = $this->_db->update('temp_invoices', $invId, 'invoice_id', array(
                                'line_items_id' => $lineItemsId
                            ));

                            for($j = 0; $j < count((array)$this->_lineItems[$i]['line_items']); $j++){
                                //     echo 'Invoice ' . $i . '<br>';
                                //     echo 'Line items id' . $lineItemsId . '<br>';

                                $insertLineDesc = $this->_db->insert('temp_line_item_desc', array(
                                    'line_items_id' => $lineItemsId,
                                    'item_code' => $this->_lineItems[$i]['line_items'][$j]['item_code'],
                                    'quantity' => $this->_lineItems[$i]['line_items'][$j]['quantity'],
                                    'line_amount' => $this->_lineItems[$i]['line_items'][$j]['line_amount'],
                                    'last_updated' => $now,
                                ));
                                if($insertLineDesc){
                                    // echo 'Inserted<br>';
                                }
                            }
                            // echo '<br>';
                        }

                    }
                }
            }
        }
    }

    public function getLineItems(){

        $updates = array();
        $items = array();
        $sales = array();
        $totals = array(
            // 'item_code' => array(),
            // 'total_sales' => array()
        );

        $dbQueryInvoice = $this->_db->get('temp_invoices', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
        ));
        if($dbQueryInvoice->results()){
            $data = $dbQueryInvoice->results();
            for($i = 0; $i < count((array)$data); $i++){
            // for($i = 0; $i < 25; $i++){
                $lineItemId = $data[$i]->line_items_id;

                $dbQueryLineItemDesc = $this->_db->get('temp_line_item_desc', array(
                    'line_items_id' => array('operator' => '=', 'value' => $lineItemId)
                ));
                if($dbQueryLineItemDesc->results()){
                    $itemData = $dbQueryLineItemDesc->results();
                
                    for($j = 0; $j < count((array)$itemData); $j++){
                        if($itemData[$j]->item_code != '0'){
                            array_push($items, $itemData[$j]->item_code);
                            array_push($sales, $itemData[$j]->line_amount);
                            array_push($updates, $itemData[$j]->last_updated);
                        }
                    }
                }
            }
        }

        $itemCount = 0;

        for($i = 0; $i < count($items); $i++){
            // echo 'Item Code: ' . $items[$i] . '<br>';
            // echo 'Amount: ' . $sales[$i] . '<br>';
            if(count($totals) == 0){
                $totals[$itemCount]['item_code'] = $items[$i];
                $totals[$itemCount]['total_sales'] = $sales[$i];
                $itemCount++;
            }
            else{
                for($j = 0; $j <= count($totals); $j++){
                    if($j != count($totals)){
                        if($totals[$j]['item_code'] == $items[$i]){
                            // echo 'Found in array<br>';
                            $totals[$j]['total_sales'] += $sales[$i];
                            break;
                        }
                    }
                    else{
                        // echo 'Not found in array<br>';
                        $totals[$itemCount]['item_code'] = $items[$i];
                        $totals[$itemCount]['total_sales'] = $sales[$i];
                        $itemCount++;
                        break;
                    }
                }
            }
            // echo '<br>';
        }
        $tot = array_column($totals, 'total_sales');
        array_multisort($tot, SORT_DESC, $totals);

        if($updates != null){
            $oldestUpdate = min($updates);
            $totals['last_updated'] = $oldestUpdate;
        }

        return $totals;
    }
}
<?php 

class Companies{

    private $_db;

    private function __construct(){
        $this->_db = DB::getInstance();
    }

    public static function setCompany($api, $apiId, $name){
        // echo 'Setting company ' . $name . ' ...<br>';
        $name = trim($name);
        $dbQuery = DB::getInstance()->get('temp_company', array(
            'api_id' => array('operator' => '=', 'value' => $api),
            'company_api_id' => array('operator' => '=', 'value' => $apiId)
        ));
        if(!$dbQuery->results()){
            // echo 'Inserting...<br>';
            $id = self::insert($api, $apiId, $name);
            self::setOwnerActive($id);
            // self::setActiveCompany($id);
            Session::put('active_tenant_id', $apiId);
            return true;
        }
        return false;
    }

    public static function insert($api, $apiId, $name){
        $dbInsert = DB::getInstance()->insert('temp_company', array(
            'api_id' => $api,
            'company_api_id' => $apiId,
            'company_name' => $name
        ));
        if($dbInsert){
            $dbQuery = DB::getInstance()->get('temp_company', array(
                'api_id' => array('operator' => '=', 'value' => $api),
                'company_api_id' => array('operator' => '=', 'value' => $apiId)
            ));
            if($dbQuery->results()){
                $id = $dbQuery->first()->company_id;
                Session::put('active_company_id', $id);
                return $id;
            }
        }
        return false;
    }

    public static function setOwnerActive($id){
        $db = DB::getInstance();

        $exist = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $id)
        ));

        if($exist){
            $dbUserCompany = $db->get('temp_user_company', array(
                'company_id' => array('operator' => '=', 'value' => $id)
            ));
            if(!$dbUserCompany->results()){
                $insertUserCompany= $db->insert('temp_user_company', array(
                    'company_id' => $id
                ));

                $dbUserCompany2 = $db->get('temp_user_company', array(
                    'company_id' => array('operator' => '=', 'value' => $id)
                ));
                if($dbUserCompany2->results()){
                    $data = $dbUserCompany2->results();
                    $userCompId = $data[0]->user_company_id;

                    $user = new User();
                    $insertUserCompDesc = $db->insert('temp_user_company_desc', array(
                        'user_company_id' => $userCompId,
                        'user_id' => $user->data()->user_id,
                        'role_id' => 1,
                        'status' => 1
                    ));
                    return true;
                }
            }
            return false;
        }
    }

    public static function getApi($id = null){
        if($id == null){
            $id = Session::get('active_company_id');
        }

        $comp = DB::getInstance()->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $id)
        ));
        if($comp->results()){
            return $comp->first()->api_id;
        }
    }


    public static function setCompanies($ids){
        $db = DB::getInstance();
        $existingCompanies = array();
        $successCompanies = array();

        // var_dump($ids);

        $user = new User();
        if($user->isLoggedIn()){
            $xero = Xero::getInstance();
            $instance = $xero->getApiInstance();
    
            for($i = 0; $i < count((array)$ids); $i++){
                $dbQuery = $db->get('temp_company', array(
                    'company_api_id' => array('operator' => '=', 'value' => $ids[$i])
                ));
                if(!$dbQuery->results()){
                    $object = $instance->getOrganisations($ids[$i])->getOrganisations()[0];
    
                    // echo 'Did not find ' . $ids[$i] . '<br>';
    
                    $dbInsert = $db->insert('temp_company', array(
                        'api_id' => 1,
                        'company_api_id' => $ids[$i],
                        'company_name' => $object->getName(),
                        'company_email' => ''
                    ));
                    array_push($successCompanies, $object->getName());
    
                    $newCompany = $db->get('temp_company', array(
                        'company_api_id' => array('operator' => '=', 'value' => $ids[$i])
                    ));
                    if($newCompany->results()){
                        $data = $newCompany->results();
                        $id = $data[0]->company_id;
    
                        $dbUserCompany = $db->get('temp_user_company', array(
                            'company_id' => array('operator' => '=', 'value' => $id)
                        ));
                        if(!$dbUserCompany->results()){
                            $insertUserCompany= $db->insert('temp_user_company', array(
                                'company_id' => $id
                            ));
    
                            $dbUserCompany2 = $db->get('temp_user_company', array(
                                'company_id' => array('operator' => '=', 'value' => $id)
                            ));
                            if($dbUserCompany2->results()){
                                $data = $dbUserCompany2->results();
                                $userCompId = $data[0]->user_company_id;
    
                                $insertUserCompDesc = $db->insert('temp_user_company_desc', array(
                                    'user_company_id' => $userCompId,
                                    'user_id' => $user->data()->user_id,
                                    'role_id' => 1,
                                    'status' => 1
                                ));
                            }
                        }
    
                        Session::put('active_tenant_id', $ids[$i]);
                        Session::put('active_company_id', $id);
                    }
                }
                else{
                    $name = $dbQuery->first()->company_name;
                    array_push($existingCompanies, $name);
                }
            }
            $result = array(
                'exists' => $existingCompanies,
                'success' => $successCompanies
            );
            return $result;
        }
        return false;
    }

    public static function companyIdExists($id){
        $db = DB::getInstance();
        $dbQuery = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $id)
        ));
        if($dbQuery->results){
            return true;
        }
        return false;
    }

    public static function deleteCompany($id){
        $db = DB::getInstance();
        $dbQuery = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $id)
        ));
        if($dbQuery->results()){
            $data = $dbQuery->results();
            $companyId = $data[0]->company_id;
            $apiId = $data[0]->company_api_id;

            //Delete all company data in other tables
            $accounts = $db->get('temp_accounts', array(
                'company_id', '=', $companyId
            ));
            if($accounts->results()){
                $accs = $accounts->results();
                for($i = 0; $i < $accounts->count(); $i++){
                    $accId = $accs[$i]->account_id;
                    $deleteAmount = $db->delete('temp_account_amounts', array(
                        'account_id', '=', $accId
                    ));
                    $deleteAccount = $db->delete('temp_accounts', array(
                        'account_id', '=', $accId
                    ));
                }
            }
            $deleteCashReceived = $db->delete('temp_cash_received', array(
                'company_id', '=', $companyId
            ));
            $deleteCashSpent = $db->delete('temp_cash_spent', array(
                'company_id', '=', $companyId
            ));
            $deleteComments = $db->delete('temp_comments', array(
                'company_id', '=', $companyId
            ));
            $deleteItems = $db->delete('temp_items', array(
                'company_id', '=', $companyId
            ));

            $invoices = $db->get('temp_invoices', array(
                'company_id', '=', $companyId
            ));
            if($invoices->results()){
                $inv = $invoices->results();
                for($i = 0; $i < $invoices->count(); $i++){
                    $invId = $inv[$i]->invoice_id;
                    $lineItems = $db->get('temp_line_items', array(
                        'invoice_id', '=', $invId
                    ));
                    if($lineItems->results()){
                        $li = $lineItems->results();
                        $liId = $li[$i]->line_items_id;

                        $deleteLineItemsDesc = $db->delete('temp_line_item_desc', array(
                            'line_items_id', '=', $liId
                        ));
                        $deleteLineItems = $db->delete('temp_line_items', array(
                            'invoice_id', '=', $invId
                        ));
                        $deleteInvoices = $db->delete('temp_invoices', array(
                            'company_id', '=', $companyId
                        ));
                    }
                }
            }
            $getUserCompany = $db->get('temp_user_company', array(
                'company_id', '=', $companyId
            ));
            if($getUserCompany->results()){
                $data = $getUserCompany->results();
                $userCompId = $data[0]->user_company_id;

                $deleteUserCompanyDesc = $db->delete('temp_user_company_desc', array(
                    'user_company_id', '=', $userCompId
                ));
            }
            $deleteUserCompany = $db->delete('temp_user_company', array(
                'company_id', '=', $companyId
            ));
            $getPending = $db->get('temp_pending_user_company', array(
                'company_id', '=', $companyId
            ));
            if($getPending->results()){
                $data = $getPending->results();
                for($i = 0; $i < count($data); $i++){
                    $pendingUserCompId = $data[$i]->pending_user_company_id;

                    $deletePendingDesc = $db->delete('temp_pending_user_company_desc', array(
                        'pending_user_company_id', '=', $pendingUserCompId
                    ));
                }
            }
            $deletePending = $db->delete('temp_pending_user_company', array(
                'company_id', '=', $companyId
            ));
            $deleteXeroIntegration = $db->delete('xero_integration', array(
                'company_id', '=', $companyId
            ));
            $deleteQbIntegration = $db->delete('quickbooks_integration', array(
                'company_id', '=', $companyId
            ));
            $deleteCompany = $db->delete('temp_company', array(
                'company_id', '=', $companyId
            ));

            //delete session tenant
            // $oauth2 = Session::get('oauth2');
            // $tenantIds = $oauth2['tenant_id'];
            
            // $key = array_search($apiId, (array)$tenantIds);
            // if ($key !== false) {
            //     unset($tenantIds[$key]);
            // }
            // $tenantIds = array_values($tenantIds);
            // Session::put('tenants', $tenantIds);

            //set new company active
            $companies = self::getCompanies();
            if($companies != null){
                self::setActiveCompany($companies[0]['id']);
            }
        }
    }

    public static function setActiveCompany($id){
        $db = DB::getInstance();

        if(Companies::getApi() == 2){
            $sage = new Sage();
            $key = $sage->getCompanyKey($id)->sage_key;
            Session::put('sage_auth_key', $key);
        }

        $dbQuery = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $id)
        ));
        if($dbQuery->results()){
            $data = $dbQuery->results();
            $apiId = $data[0]->company_api_id;
            $companyId = $data[0]->company_id;

            Session::put('active_tenant_id', $apiId);
            Session::put('active_company_id', $companyId);

            $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                'user_id' => array('operator' => '=', 'value' => Session::get('user'))
            ));
            if($dbUserCompDesc->results()){
                $data = $dbUserCompDesc->results();

                for($i = 0; $i < count((array)$data); $i++){   
                    $userCompId = $data[$i]->user_company_id;

                    $dbUserComp = $db->get('temp_user_company', array(
                        'user_company_id' => array('operator' => '=', 'value' => $userCompId),
                        'company_id' => array('operator' => '=', 'value' => $companyId)
                    ));
                    if($dbUserComp->results()){
                        $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                            'user_company_id' => array('operator' => '=', 'value' => $userCompId)
                        ));
                        if($dbUserCompDesc->results()){
                            $roleId = $dbUserCompDesc->first()->role_id;
                            Session::put('active_role_id', $roleId);
                        }
                    }

                    
                }
            }
        }
        else{
            //name = id?
            $dbQuery = $db->get('temp_company', array(
                'company_id' => array('operator' => '=', 'value' => $id)
            ));
            if($dbQuery->results()){
                $data = $dbQuery->results();
                $apiId = $data[0]->company_api_id;
                $companyId = $data[0]->company_id;
    
                Session::put('active_tenant_id', $apiId);
                Session::put('active_company_id', $companyId);
    
                $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                    'user_id' => array('operator' => '=', 'value' => Session::get('user'))
                ));
                if($dbUserCompDesc->results()){
                    $data = $dbUserCompDesc->results();
    
                    for($i = 0; $i < count((array)$data); $i++){   
                        $userCompId = $data[$i]->user_company_id;
    
                        $dbUserComp = $db->get('temp_user_company', array(
                            'user_company_id' => array('operator' => '=', 'value' => $userCompId),
                            'company_id' => array('operator' => '=', 'value' => $companyId)
                        ));
                        if($dbUserComp->results()){
                            $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                                'user_company_id' => array('operator' => '=', 'value' => $userCompId)
                            ));
                            if($dbUserCompDesc->results()){
                                $roleId = $dbUserCompDesc->first()->role_id;
                                Session::put('active_role_id', $roleId);
                            }
                        }
                    }
                }
            }
            else{
                return false;
            }
        }
    }

    public static function getUserCompanyRole($id = null){

        if($id == null){
            $id = self::getActiveCompanyId();
        }

        $db = DB::getInstance();
        $dbQuery = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $id)
        ));
        if($dbQuery->results()){
            $data = $dbQuery->results();
            $companyId = $data[0]->company_id;

            $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                'user_id' => array('operator' => '=', 'value' => Session::get('user'))
            ));
            if($dbUserCompDesc->results()){
                $data = $dbUserCompDesc->results();
            
                for($i = 0; $i < count((array)$data); $i++){   
                    $userCompId = $data[$i]->user_company_id;

                    $dbUserComp = $db->get('temp_user_company', array(
                        'user_company_id' => array('operator' => '=', 'value' => $userCompId),
                        'company_id' => array('operator' => '=', 'value' => $companyId)
                    ));
                    if($dbUserComp->results()){
                        $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                            'user_company_id' => array('operator' => '=', 'value' => $userCompId)
                        ));
                        if($dbUserCompDesc->results()){
                            $roleId = $dbUserCompDesc->first()->role_id;
                            return $roleId;
                        }
                    }
                }
            }
        }
    }

    public static function getCompanyOwnerID($id = null){

        if($id){
            $companyId = $id;
        }else{
            $companyId = self::getActiveCompanyId();

        }

        $db = DB::getInstance();

        $activeCompany = $db->get('temp_user_company', array('company_id' , "=", $companyId));

        if($activeCompany->count()){
            $activeCompany = $activeCompany->first();

            $userCompID = $activeCompany->user_company_id;
            // var_dump("UserCompId: " . $userCompID);

            $compDescription = $db->get('temp_user_company_desc', array('user_company_id', '=', $userCompID));

            if($compDescription->count()){
                $ownerID = $compDescription->first()->user_id;

                return $ownerID;
            }
        }

        return null;
    }

    public static function getCompanyOwnerPackage($companyID = null){

        $ownerID = null;

        
        if($companyID){
            $ownerID = self::getCompanyOwnerId($companyID);
        }else{
            $ownerID = self::getCompanyOwnerId();
        }

        if($ownerID){
            $package = new Package();
            return $package->getPackage($ownerID);
        }

        return null;
        
    }

    public static function updateType($id, $type){
        $db = DB::getInstance();

        $dbCompanyType = $db->get('temp_company_types', array(
            'type' => array('operator' => '=', 'value' => $type)
        ));
        if($dbCompanyType->results()){
            $typeId = $dbCompanyType->results()[0]->company_type_id;

            $dbCompany = $db->get('temp_company', array(
                'company_id' => array('operator' => '=', 'value' => $id)
            ));
            if($dbCompany->results()){
                $companyId = $dbCompany->first()->company_id;

                $dbUpdate = $db->update('temp_company', $companyId, 'company_id', array(
                    'company_type_id' => $typeId
                ));
            }
        }
    }

    public static function updateCompany($fields = array(), $id = null){
        $db = DB::getInstance();

        if(!$id){
            $id = self::getActiveCompanyId();
        }

        if(!$db->update('temp_company', 'company_id', $id, $fields)){
            throw new Exception('There was a problem updating');  
        }
    }

    public static function addNewUser($email, $companyId, $sendEmail = true){
        $db = DB::getInstance();

        require_once (dirname(__DIR__, 1) . '/vendor/autoload.php');
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');

        $check = $db->get('user', array('user_email','=',$email));
        if($check->results()){
            $userId = $check->first()->user_id;
            
            if($sendEmail){
                $didEmailSend = Email::sendPreset($email, 'invite_exists');
                $didEmailSend = true;
            }
            if($didEmailSend || !$sendEmail){
                //user already exists
                $assigned = false;
                if(!$sendEmail){
                    //$logger->debug("Send email = false");
                    $didEmailSend = true;
                }
                else{
                    //$logger->debug("Sent email to existing user");
                }

                if($companyId == null){
                    $companyId = self::getActiveCompanyId();
                }
                
                $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                    'user_id' => array('operator' => '=', 'value' => $userId)
                ));
                if($dbUserCompDesc->results()){
                    $userCompIds = $dbUserCompDesc->results();
                    for($i = 0; $i < count((array)$userCompIds); $i++){
                        $userCompanyId = $userCompIds[$i]->user_company_id;
                        
                        $dbUserComp = $db->get('temp_user_company', array(
                            'user_company_id' => array('operator' => '=', 'value' => $userCompanyId)
                        ));
                        if($dbUserComp->results()){
                            $dbCompany = $dbUserComp->first()->company_id;
                            if($dbCompany == $companyId){
                                $assigned = true;

                                $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                                    'user_company_id' => array('operator' => '=', 'value' => $userCompanyId)
                                ));
                                if($dbUserCompDesc->first()->status == 3){
                                    $dbUpdate = $db->update('temp_user_company_desc', $userCompanyId, 'user_company_id', array(
                                        'status' => 1
                                    ));

                                    return $didEmailSend;
                                }
                                
                                return 'User already assigned to this company';
                                break;
                            }
                        }
                    }
                }
                if(!$assigned){
                    $insertUserComp = $db->insert('temp_user_company', array(
                        'company_id' => $companyId
                    ));
                    if($insertUserComp){
                        $dbUserCompany = $db->get('temp_user_company', array(
                            'company_id' => array('operator' => '=', 'value' => $companyId)
                        ));
                        if($dbUserCompany->results()){
                            $data = $dbUserCompany->results();
                            $last = end($data);
                            $userCompId = $last->user_company_id;

                            $insertUserCompDesc = $db->insert('temp_user_company_desc', array(
                                'user_company_id' => $userCompId,
                                'user_id' => $userId,
                                'role_id' => 3,
                                'status' => 1
                            ));
                        }
                    }
                    //user has been added
                    return $didEmailSend;
                }
            }
            //$logger->debug("Failed to send email to existing user");
            return $didEmailSend;
        }
        
        else if(!$check->results()){
            $didEmailSend = Email::sendPreset($email, 'invite');
            
            if($didEmailSend){
                //$logger->debug("Sent email to unregistered user");
                $companyId = self::getActiveCompanyId();
    
                $check = $db->get('temp_pending_user', array('user_email','=',$email));
                if($check->results()){
                    //pending user found
                    $pendingUserId = $check->first()->pending_user_id;
                    $assigned = false;
    
                    $dbPendingUserDesc = $db->get('temp_pending_user_company_desc', array(
                        'pending_user_id' => array('operator' => '=', 'value' => $pendingUserId)
                    ));
                    if($dbPendingUserDesc->results()){
                        $data = $dbPendingUserDesc->results();
    
                        for($i = 0; $i < count($data); $i++){
                            $pendingUserCompId = $data[$i]->pending_user_company_id;
    
                            $dbPendingUser = $db->get('temp_pending_user_company', array(
                                'pending_user_company_id' => array('operator' => '=', 'value' => $pendingUserCompId),
                                'company_id' => array('operator' => '=', 'value' => $companyId)
                            ));
                            if($dbPendingUser->results()){
                                return 'User already invited to this company, another invite has been sent';
                            }
                            else{
                                $insertUserComp = $db->insert('temp_pending_user_company', array(
                                    'company_id' => $companyId
                                ));
                                if($insertUserComp){
                                    $dbUserComp = $db->get('temp_pending_user_company', array(
                                        'company_id' => array('operator' => '=', 'value' => $companyId)
                                    ));
                                    if($dbUserComp->results()){
    
                                        $data = $dbUserComp->results();
                                        $last = end($data);
                                        $pendingUserCompId = $last->pending_user_company_id;
        
                                        $insertUserCompDesc = $db->insert('temp_pending_user_company_desc', array(
                                            'pending_user_company_id' => $pendingUserCompId,
                                            'pending_user_id' => $pendingUserId
                                        ));

                                        return $didEmailSend;
                                    }
                                }
                            }
    
                        }
                    }
                }
                else {
                    //insert new user pending_user
                    $insertUserComp = $db->insert('temp_pending_user', array(
                        'user_email' => $email,
                        'date_invited' => date('Y-m-d')
                    ));
                    if($insertUserComp){
                        $dbPendingUser = $db->get('temp_pending_user', array(
                            'user_email' => array('operator' => '=', 'value' => $email)
                        ));
                        if($dbPendingUser->results()){
                            $pendingUserId = $dbPendingUser->first()->pending_user_id;
                            
                            $insertUserComp = $db->insert('temp_pending_user_company', array(
                                'company_id' => $companyId
                            ));
                            if($insertUserComp){
                                $dbUserComp = $db->get('temp_pending_user_company', array(
                                    'company_id' => array('operator' => '=', 'value' => $companyId)
                                ));
                                if($dbUserComp->results()){
                                    $data = $dbUserComp->results();
                                    $last = end($data);
                                    $pendingUserCompId = $last->pending_user_company_id;
    
                                    $insertUserCompDesc = $db->insert('temp_pending_user_company_desc', array(
                                        'pending_user_company_id' => $pendingUserCompId,
                                        'pending_user_id' => $pendingUserId
                                    ));
                                }
                            }
                        }
                    }
                }
                return $didEmailSend;
            }
            else{
                //$logger->debug("Failed to send email to unregistered user");
                return $didEmailSend;
            }
        }
    }

    public static function updateUserRole($email, $newRole){
        $db = DB::getInstance();

        switch ($newRole) {
            case 'Owner':
                $roleId = 1;
            break;
            case 'Admin':
                $roleId = 2;
            break;
            case 'Staff':
                $roleId = 3;
            break;
        }

        $dbUser = $db->get('user', array(
            'user_email' => array('operator' => '=', 'value' => $email)
        ));
        if($dbUser->results()){
            $userId = $dbUser->results()[0]->user_id;

            $companyId = self::getActiveCompanyId();

            $dbuserCompanyDesc = $db->get('temp_user_company_desc', array(
                'user_id' => array('operator' => '=', 'value' => $userId)
            ));
            if($dbuserCompanyDesc->results()){
                $data = $dbuserCompanyDesc->results();

                for($i = 0; $i < count($data); $i++){
                    $userCompanyId = $data[$i]->user_company_id;
                    $id = $data[$i]->user_company_desc_id;

                    $dbuserCompany = $db->get('temp_user_company', array(
                        'user_company_id' => array('operator' => '=', 'value' => $userCompanyId)
                    ));
                    if($dbuserCompany->results()){
                        $dbCompanyId = $dbuserCompany->results()[0]->company_id;
                        if($dbCompanyId == $companyId){
                            $dbUpdate = $db->update('temp_user_company_desc', $id, 'user_company_desc_id', array(
                                'role_id' => $roleId
                            ));
                        }
                    }
                }
            }
        }
    }

    public static function setUpdatedModules($status){
        $db = DB::getInstance();
        $companyId = self::getActiveCompanyId();

        $dbComp = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $companyId)
        ));
        if($dbComp->results()){
            $dbUpdate = $db->update('temp_company', $companyId, 'company_id', array(
                'updated_modules' => $status
            ));
            return true;
        }
        return false;
    }

    public static function getUpdatedModules($name = null){
        $db = DB::getInstance();

        if($name){
            $dbComp = $db->get('temp_company', array(
                'company_name' => array('operator' => '=', 'value' => $name)
            ));
        }

        else{
            $companyId = self::getActiveCompanyId();
            $dbComp = $db->get('temp_company', array(
                'company_id' => array('operator' => '=', 'value' => $companyId)
            ));
        }
        
        if($dbComp->results()){
            return $dbComp->first()->updated_modules;
        }
    }


    public static function setUserStatus($email, $status, $companyId = null){

        $db = DB::getInstance();
        if(!$companyId){
            $companyId = self::getActiveCompanyId();
        }
        
        $dbUser = $db->get('user', array(
            'user_email' => array('operator' => '=', 'value' => $email)
        ));
        if($dbUser->results()){
            $userId = $dbUser->results()[0]->user_id;

            $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                'user_id' => array('operator' => '=', 'value' => $userId)
            ));
            if($dbUserCompDesc->results()){
                $data = $dbUserCompDesc->results();

                for($i = 0; $i < count($data); $i++){
                    $userCompDescId = $data[$i]->user_company_desc_id;
                    $userCompId = $data[$i]->user_company_id;
                    
                    $dbUserComp = $db->get('temp_user_company', array(
                        'user_company_id' => array('operator' => '=', 'value' => $userCompId)
                    ));
                    if($dbUserComp->results()){
                        $dbCompanyId = $dbUserComp->results()[0]->company_id;
                        
                        if($dbCompanyId == $companyId){
                            $dbUpdate = $db->update('temp_user_company_desc', $userCompDescId, 'user_company_desc_id', array(
                                'status' => $status
                            ));
                        }
                    }
                }
            }
        }
    }

    public static function deleteUser($email, $companyId = null){
        $db = DB::getInstance();
        if(!$companyId){
            $companyId = self::getActiveCompanyId();
        }


        $dbUser = $db->get('user', array(
            'user_email' => array('operator' => '=', 'value' => $email)
        ));
        if($dbUser->results()){
            $userId = $dbUser->results()[0]->user_id;
            
            $dbUserCompDesc = $db->get('temp_user_company_desc', array(
                'user_id' => array('operator' => '=', 'value' => $userId)
            ));
            if($dbUserCompDesc->results()){
                $dbUserCompDescs = $dbUserCompDesc->results();

                foreach($dbUserCompDescs as $userCompDesc){
                    $userCompId = $userCompDesc->user_company_id;

                    $dbUserComp = $db->get('temp_user_company', array(
                        'user_company_id' => array('operator' => '=', 'value' => $userCompId)
                    ));
                    if($dbUserComp->results() && $dbUserComp->first()->company_id == $companyId){
                        $deleteUserComp = $db->delete('temp_user_company', array(
                            'user_company_id', '=', $userCompId
                        ));
                        $deleteUserCompDesc = $db->delete('temp_user_company_desc', array(
                            'user_company_id', '=', $userCompId
                        ));
                        break;
                    }
                }
            }
        }
        //refined until before this comment!
        else{
            //pending user
            $dbPendingUser = $db->get('temp_pending_user', array(
                'user_email' => array('operator' => '=', 'value' => $email)
            ));
            if($dbUser->results()){
                $pendingUserId = $dbPendingUser->results()[0]->pending_user_id;

                // var_dump($pendingUserId);

                $dbPendingUserCompDesc = $db->get('temp_pending_user_company_desc', array(
                    'pending_user_id' => array('operator' => '=', 'value' => $pendingUserId)
                ));
                if($dbPendingUserCompDesc->results()){
                    $data = $dbPendingUserCompDesc->results();

                    for($i = 0; $i < count((array)$data); $i++){
                        $pendingUserCompId = $data[$i]->pending_user_company_id;

                        $dbPendingUserComp = $db->get('temp_pending_user_company', array(
                            'pending_user_company_id' => array('operator' => '=', 'value' => $pendingUserCompId)
                        ));
                        if($dbPendingUserComp->results()){
                            if($dbPendingUserComp->first()->company_id == $companyId){
                                $deleteUserComp = $db->delete('temp_pending_user_company', array(
                                    'pending_user_company_id', '=', $pendingUserCompId
                                ));
                                $deleteUserCompDesc = $db->delete('temp_pending_user_company_desc', array(
                                    'pending_user_company_id', '=', $pendingUserCompId
                                ));
                                
                            break;
                            }
                        }
                    }
                }
            }
        }
    }

    public static function getCompanies(){
        $db = DB::getInstance();
        $companies = array();
        
        $dbUserCompDesc = $db->get('temp_user_company_desc', array(
            'user_id' => array('operator' => '=', 'value' => Session::get('user'))
        ));


        if($dbUserCompDesc->results()){
            $data = $dbUserCompDesc->results();

            for($i = 0; $i < count($data); $i++){
                $role = $data[$i]->role_id;
                $status = $data[$i]->status;

                if($status != 0){
                    $userCompId = $data[$i]->user_company_id;
    
                    $dbUserComp = $db->get('temp_user_company', array(
                        'user_company_id' => array('operator' => '=', 'value' => $userCompId)
                    ));
                    if($dbUserComp->results()){
                        $compId = $dbUserComp->first()->company_id;
    
                        $dbComp = $db->get('temp_company', array(
                            'company_id' => array('operator' => '=', 'value' => $compId)
                        ));
                        if($dbComp->results()){
                            $compData = $dbComp->first();
                            $type = $compData->company_type_id;
    
                            $dbType= $db->get('temp_company_types', array(
                                'company_type_id' => array('operator' => '=', 'value' => $type)
                            ));
                            if($dbType->results()){
                                $typeDesc = $dbType->first()->type;
                            }
                            else{
                                $typeDesc = null;
                            }
                            
                            $userid = Session::get('user');

                            $user = new User($userid);

                            $companyOwner = self::getCompanyOwnerId($compData->company_id);
                            $userisOwner = false;

                            if($user->data()->user_id == $companyOwner){
                                $userisOwner = true;
                            }

                            $companies[$i]['name'] = $compData->company_name;
                            $companies[$i]['id'] = $compData->company_id;
                            $companies[$i]['type'] = $typeDesc;
                            $companies[$i]['email'] = $compData->company_email;
                            $companies[$i]['api'] = $compData->api_id;
                            $companies[$i]['api_id'] = $compData->company_api_id;
                            $companies[$i]['role'] = $role;
                            $companies[$i]['status'] = $status;
                            $companies[$i]['brand'] = $compData->company_brand;
                            $companies[$i]['package'] = self::getCompanyOwnerPackage($compData->company_id);
                            $companies[$i]['userPackage'] = $user->getPackage();
                            $companies[$i]['userisowner'] = $userisOwner;
                            
                        }
                    }
                }
            }
        }
        return array_values($companies);
    }

    public static function getActiveCompanyInfo($companyId = null){
        $db = DB::getInstance();

        if(!$companyId){
            $companyId = self::getActiveCompanyId();
        }

        $dbComp = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => $companyId)
        ));

        if($dbComp->count()){
            $data = $dbComp->first();
           
            
            $array = array (
                "company_id" => $data->company_id,
                "api_id"=> $data->api_id,
                "company_api_id"=> $data->company_api_id,
                "company_name"=> $data->company_name,
                "company_type_id"=> $data->company_type_id,
                "company_brand"=> $data->company_brand,
                "company_email"=> $data->company_email,
                "updated_modules"=> $data->updated_modules,
                "userRole" => self::getUserCompanyRole(),
                "package"=> self::getCompanyOwnerPackage($data->company_id),

            );

            
            return $array;
        }

        return false;

    }

    public static function getActiveCompanyId(){
        if(Session::exists('active_company_id')){
            return Session::get('active_company_id');
        }
        else{
            return null;
        }
    }

    public static function getActiveTenantId(){
        if(Session::exists('active_tenant_id')){
            return Session::get('active_tenant_id');
        }
        $db = DB::getInstance();

        $dbComp = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => self::getActiveCompanyId())
        ));
        if($dbComp->results()){
            $tenantId = $dbComp->first()->company_api_id;
            return $tenantId;
        }
    }

    public static function getActiveCompanyName(){
        $db = DB::getInstance();
        $dbQuery = $db->get('temp_company', array(
            'company_id' => array('operator' => '=', 'value' => Session::get('active_company_id'))
        ));
        if($dbQuery->results()){
            $data = $dbQuery->results();
            $name = $data[0]->company_name;

            return $name;
        }
        else{
            return null;
        }
    }

    public static function getActiveCompanyBrand(){
        $db = DB::getInstance();
        $dbQuery = $db->get('temp_company', array('company_id', '=', Session::get('active_company_id')));
        if($dbQuery->count()){
            $data = $dbQuery->first();
            $brand = $data->company_brand;

            return $brand;
        }
        else{
            return null;
        }
    }

    public static function getTypes(){
        $db = DB::getInstance();
        $types = array();

        $dbQuery = $db->get('temp_company_types', array(
            'company_type_id' => array('operator' => '>', 'value' => 0)
        ));
        if($dbQuery->results()){
            $data = $dbQuery->results();
            for($i = 0 ; $i < count($data); $i++){
                array_push($types, $data[$i]->type);
            }
            return $types;
        }
        else{
            return null;
        }
    }

    public static function getCompanyUsers(){
        $db = DB::getInstance();

        $users = array();
        $companyId = self::getActiveCompanyId();
        
        $dbQuery = $db->get('temp_user_company', array(
            'company_id' => array('operator' => '=', 'value' => $companyId)
        ));
        if($dbQuery->results()){
            $data = $dbQuery->results();

            for($u = 0; $u < count($data); $u++){
                $userCompId = $data[$u]->user_company_id;
                
                $usersQuery = $db->get('temp_user_company_desc', array(
                    'user_company_id' => array('operator' => '=', 'value' => $userCompId)
                ));
                if($usersQuery->results()){
                    $dataDesc = $usersQuery->results();

                    $userId = $dataDesc[0]->user_id;
                    $status = $dataDesc[0]->status;
                    $roleId = $dataDesc[0]->role_id;
                    
                    $userQuery = $db->get('user', array(
                        'user_id' => array('operator' => '=', 'value' => $userId)
                    ));
                    if($userQuery->results()){
                        $user = $userQuery->results()[0];
                        
                        $name = $user->user_name;
                        $joined = $user->user_joined;
                        $email = $user->user_email;

                        switch ($roleId) {
                            case 1:
                                $role = 'Owner';
                            break;
                            case 2:
                                $role = 'Admin';
                            break;
                            case 3:
                                $role = 'Staff';
                            break;
                        }

                        $users[$u]['name'] = $name;
                        $users[$u]['joined'] = $joined;
                        $users[$u]['status'] = $status;
                        $users[$u]['role'] = $role;
                        $users[$u]['email'] = $email;
                    }
                }
            }
        }

        return $users;
    }

    public static function getPendingCompanyUsers(){
        $db = DB::getInstance();

        $users = array();
        $companyId = self::getActiveCompanyId();
        
        $dbQuery = $db->get('temp_pending_user_company', array(
            'company_id' => array('operator' => '=', 'value' => $companyId)
        ));
        if($dbQuery->results()){
            $data = $dbQuery->results();

            for($u = 0; $u < count($data); $u++){
                $pendingUserCompId = $data[$u]->pending_user_company_id;
                
                $usersQuery = $db->get('temp_pending_user_company_desc', array(
                    'pending_user_company_id' => array('operator' => '=', 'value' => $pendingUserCompId)
                ));
                if($usersQuery->results()){
                    $dataDesc = $usersQuery->results();

                    $pendingUserId = $dataDesc[0]->pending_user_id;
                    
                    $userQuery = $db->get('temp_pending_user', array(
                        'pending_user_id' => array('operator' => '=', 'value' => $pendingUserId)
                    ));
                    if($userQuery->results()){
                        $user = $userQuery->results()[0];
                        
                        $email = $user->user_email;
                        $invited = $user->date_invited;

                        $users[$u]['email'] = $email;
                        $users[$u]['invited'] = $invited;
                    }
                }
            }
        }

        return array_values($users);

    }
}
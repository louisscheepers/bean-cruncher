<?php

class FormulaQuickRatio{
    private $_quickRatio,
            $_fields;
    public function __construct($fields){
        $this->_fields = $fields;
    }

    private function setQuickRatio(){
        $cashAndReceivables = $this->_fields['cash_recievables'];
        // for($i = 0; $i < count($this->_balanceSheet->getCurrentAssets()); $i++){
        //     if(strpos($this->_balanceSheet->getCurrentAssets()[$i]['account'], 'Bank') !== false){

        //         var_dump($this->_balanceSheet
        //         $cashAndReceivables += $this->_balanceSheet->getCurrentAssets()[$i]['data']['amount'];
        //     }
        // }

        $currentLiabilities = $this->_fields['total_current_liabilities'];

        if($currentLiabilities != 0){
            $this->_quickRatio['quick_ratio'] = $cashAndReceivables / $currentLiabilities;
        }
        else{
            $this->_quickRatio['quick_ratio'] = 0;
        }
        
        $oldestUpdate = $this->_fields['last_updated'];
        $this->_quickRatio['last_updated'] = $oldestUpdate;
    }

    public function getQuickRatio(){
        $this->setQuickRatio();
        return $this->_quickRatio;
    }
}
<?php

class FormulaCurrentRatio{
    private $_currentRatio,
            $_fields;
    public function __construct($fields){
        $this->_fields = $fields;
    }

    private function setCurrentRatio(){
        $currentAssets = $this->_fields['total_current_assets'];
        $currentLiabilities = $this->_fields['total_current_liabilities'];

        if($currentLiabilities != 0){
            $this->_currentRatio['current_ratio'] = $currentAssets / $currentLiabilities;
        }
        else{
            $this->_currentRatio['current_ratio'] = 0;
        }
        
        
        $this->_currentRatio['last_updated'] = $this->_fields['last_updated'];
    }

    public function getCurrentRatio(){
        $this->setCurrentRatio();
        return $this->_currentRatio;
    }
}
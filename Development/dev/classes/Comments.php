<?php 

class Comments{

    public static function setComment($module, $newComment){

        $db = DB::getInstance();

        $moduleNumber = self::getModuleNumber($module);

        $dbGet = $db->get('temp_comments', array(
            'module_id', '=', $moduleNumber
        ));

        $timezone = date_default_timezone_get();
        $date = date('Y-m-d h:i');

        $insert = $db->insert('temp_comments', array(
            'user_id' => Session::get('user'),
            'company_id' => Companies::getActiveCompanyId(),
            'module_id' => $moduleNumber,
            'date_time' => $date,
            'comment' => $newComment
        ));
    }

    public static function deleteComment($id){
        $db = DB::getInstance();
        $deleteComment = $db->delete('temp_comments', array(
            'comment_id', '=', $id
        ));
    }

    public static function getComments($module){
        $db = DB::getInstance();

        $moduleNumber = self::getModuleNumber($module);

        $dbQuery = $db->get('temp_comments', array(
            'module_id' => array('operator' => '=', 'value' => $moduleNumber),
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
        ));
        
        if($dbQuery->count()){
            $data = $dbQuery->results();
            // $response = array();

            for($i = 0; $i < count($data); $i++){
                $user = new User($data[$i]->user_id);
                if($user->data() != null){
                    $commentor = $user->data()->user_name . ' ' . $user->data()->user_surname;
                }
                else{
                    $commentor = '';
                }
                $response[$i] = array(
                    'id' => $data[$i]->comment_id,
                    'user_id' => $data[$i]->user_id,
                    'user' => $commentor,
                    'date_time' => $data[$i]->date_time,
                    'comment' => $data[$i]->comment
                );
            }
            return array_reverse($response);
        }
        else{
            return '';
        }

    }

    public static function editComment($id, $comment){
        $db = DB::getInstance();

        $dbUpdate = $db->update('temp_comments', $id, 'comment_id', array(
            'comment' => $comment
        ));
    }

    private static function getModuleNumber($module){
        $modules = array(
            'cico' => 1,
            'ino' => 2,
            'avd' => 3,
            'pl' => 4,
            'bs' => 5,
            'cs' => 6,
            'ar' => 7,
            'ddo' => 8,
            'tcs' => 9,
            'ret' => 10,
            'ap' => 11,
            'cdo' => 12,
            'tsp' => 13,
            'im' => 14,
            'inas' => 15,
            'inaq' => 16,
            'plr' => 17,
            'bsr' => 18
        );
        return $modules[$module];
    }
}
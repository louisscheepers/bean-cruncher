<?php 

abstract class AccountTypes{
    //Sales 
    const REVENUE = 'REVENUE';
    const SALES = 'SALES';

    //Cost of Sales
    const DIRECTCOSTS = 'DIRECTCOSTS';
    const DIRECT_COSTS = 'DIRECT COSTS';
    const COS = 'COST OF SALES';

    // Other Income 
    const OTHERINCOME = 'OTHERINCOME';
    const OTHER_INCOME = 'OTHER INCOME';

    // Expenses
    const DEPRECIATN = 'DEPRECIATN';
    const EXPENSE = 'EXPENSE';
    const EXPENSES = 'EXPENSES';
    const OVERHEADS = 'OVERHEADS';
    const PREPAYMENT = 'PREPAYMENT';
    const SUPERANNUATIONEXPENSE = 'SUPERANNUATIONEXPENSE';
    const WAGESEXPENSE = 'WAGESEXPENSE';

    // Tax
    const TAX = 'TAX';
    const INCOME_TAX = 'INCOME TAX';

    // Current Assets
    const ASSET = 'ASSET';
    const INVENTORY = 'INVENTORY';
    const BANK = 'BANK';
    const CURRENT = 'CURRENT';
    const CURRENT_ASSETS = 'CURRENT ASSETS';
    const CURRENT_ASSET = 'CURRENT ASSET';

    // Non-current Assets
    const FIXED = 'FIXED';
    const NONCURRENT = 'NONCURRENT';
    const NON_CURRENT_ASSETS = 'NON-CURRENT ASSETS';
    const NON_CURRENT_ASSET = 'NON-CURRENT ASSET';

    // Current Liabilities
    const CURRLIAB = 'CURRLIAB';
    const LIABILITY = 'LIABILITY';
    const PAYGLIABILITY = 'PAYGLIABILITY';
    const SUPERANNUATIONLIABILITY = 'SUPERANNUATIONLIABILITY';
    const CURRENT_LIABILITIES = 'CURRENT LIABILITIES';
    const CURRENT_LIABILITY = 'CURRENT LIABILITY';

    // Non-current Liabilities
    const TERMLIAB = 'TERMLIAB';
    const NON_CURRENT_LIABILITIES = 'NON-CURRENT LIABILITIES';
    const NON_CURRENT_LIABILITY = 'NON-CURRENT LIABILITY';

    // Equity
    const EQUITY = 'EQUITY';
    const OWNERS_EQUITY = 'OWNERS EQUITY';

    // IDs 
    const SALES_ID = 1;
    const COS_ID = 2;
    const OTHER_INCOME_ID = 3;
    const EXPENSES_ID = 4;
    const TAX_ID = 5;
    const C_ASSETS_ID = 6;
    const NC_ASSETS_ID = 7;
    const NC_LIABLITIES_ID = 8;
    const C_LIABLITIES_ID = 9;
    const EQUITY_ID = 10;

    public static function getTypeId($string){
        $string = strtoupper($string);

        $sales =            [self::REVENUE, self::SALES];
        $cos =              [self::DIRECTCOSTS, self::DIRECT_COSTS, self::COS];
        $otherInc =         [self::OTHERINCOME, self::OTHER_INCOME];
        $expenses =         [self::DEPRECIATN, self::EXPENSE, self::EXPENSES, self::OVERHEADS, self::PREPAYMENT,self::SUPERANNUATIONEXPENSE , self::WAGESEXPENSE];
        $taxes =            [self::TAX, self::INCOME_TAX];
        $cAssets =          [self::INVENTORY, self::BANK, self::CURRENT, self::ASSET, self::CURRENT_ASSETS, self::CURRENT_ASSET];
        $ncAssets =         [self::FIXED, self::NONCURRENT, self::NON_CURRENT_ASSETS, self::NON_CURRENT_ASSET];
        $cLiabilities =     [self::CURRLIAB, self::LIABILITY, self::PAYGLIABILITY, self::SUPERANNUATIONLIABILITY, self::CURRENT_LIABILITIES, self::CURRENT_LIABILITY];
        $ncLiabilities =    [self::TERMLIAB, self::NON_CURRENT_LIABILITIES, self::NON_CURRENT_LIABILITY];
        $equity =           [self::EQUITY, self::OWNERS_EQUITY];

        if(self::isType($string, $sales)){
            return self::SALES_ID;
        }

        if(self::isType($string, $cos)){
            return self::COS_ID;
        }
        
        if(self::isType($string, $otherInc)){
            return self::OTHER_INCOME_ID;
        }
        
        if(self::isType($string, $expenses)){
            return self::EXPENSES_ID;
        }
        
        if(self::isType($string, $taxes)){
            return self::TAX_ID;
        }
        
        if(self::isType($string, $cAssets)){
            return self::C_ASSETS_ID;
        }
        
        if(self::isType($string, $ncAssets)){
            return self::NC_ASSETS_ID;
        }
        
        if(self::isType($string, $cLiabilities)){
            return self::C_LIABLITIES_ID;
        }
        
        if(self::isType($string, $ncLiabilities)){
            return self::NC_LIABLITIES_ID;
        }
        
        if(self::isType($string, $equity)){
            return self::EQUITY_ID;
        }

        return false;
    }

    private static function isType($string, $array){
        foreach($array as $type){
            if($type == $string){
                return true;
            }
        }
        return false;
    }

    public static function getTypeString($id){
        switch ($id){
            case 1:
                return 'Sales';
            break;
            case 2:
                return 'Direct Costs';
            break;
            case 3:
                return 'Other Income';
            break;
            case 4: 
                return 'Expense';
            break;
            case 5:
                return 'Tax';
            break;
            case 6:
                return 'Current Asset';
            break;
            case 7:
                return 'Non-current Asset';
            break;
            case 8:
                return 'Non-current Liability';
            break;
            case 9:
                return 'Current Liability';
            break;
            case 10:
                return 'Equity';
            break;
            case 11:
                return 'Total Income';
            break;
            case 12:
                return 'Total Cost of Sales';
            break;
            case 13:
                return 'Gross Profit';
            break;
            case 14:
                return 'Total Other Income';
            break;
            case 15:
                return 'Total Operating Expenses';
            break;
            case 16:
                return 'Net Profit';
            break;
            case 17:
                return 'Total Tax';
            break;

            return '';
        }
    }

}
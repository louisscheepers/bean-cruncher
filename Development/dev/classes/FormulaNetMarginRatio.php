<?php

class FormulaNetMarginRatio{
    private $_netMarginRatio;

    public function __construct($profitAndLoss){
        $this->_profitAndLoss = $profitAndLoss;
    }
    
    private function setNetMarginRatio(){
        $netProfit = $this->_profitAndLoss['net_profit_before_tax']['amount'];
        $totalSales = $this->_profitAndLoss['total_sales']['amount'];

        if($totalSales != 0){
            $this->_netMarginRatio['net_margin_ratio'] = ($netProfit / $totalSales) * 100;
        }
        else{
            $this->_netMarginRatio['net_margin_ratio'] = 0;
        }
        
        $oldestUpdate = $this->_profitAndLoss['last_updated'];
        $this->_netMarginRatio['last_updated'] = $oldestUpdate;
    }

    public function getNetMarginRatio(){
        $this->setNetMarginRatio();
        return $this->_netMarginRatio;
    }
}
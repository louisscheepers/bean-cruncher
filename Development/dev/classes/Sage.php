<?php

use SageAccounting\Provider;
require_once dirname(__DIR__, 1) . '/vendor/autoload.php';

class Sage{
    private static $_instance = null;
    private $_provider,
            $_apiInstance;

    public function __construct(){
        $this->setProvider();
    }

    public function setProvider(){
        $this->_provider = new Provider(array(
            'apiKey'   => Constant::getSageApiKey(),
            'baseUrl'  => Constant::getSageBaseUrl(),
            'authKey'  => Session::get('sage_auth_key')
        ));
    }
    public function getProvider(){
        return $this->_provider;
    }

    public function storeKeySession($authKey){
        Session::put('sage_auth_key', $authKey);
        $this->setProvider();
    }

    public function getCompanyKey($companyId = null){
        if(null === $companyId){
            $companyId = Companies::getActiveCompanyId();
        }
        
        $get = DB::getInstance()->get('sage_keys', array(
            'company_id' => array('operator' => '=', 'value' => $companyId)
        ));

        if($get){
            return $get->first();
        }
        return false;
    }

    public function storeKeyDb($authKey, $companyId){
        $db = DB::getInstance();
        
        $key = $db->get('sage_keys', array(
            'company_id' => array('operator' => '=', 'value' => $companyId)
        ));

        if($key->results()){
            $dbUpdate = $db->update('sage_keys', $companyId, 'company_id', array(
                'sage_key' => $authKey,
                'user_id' => Session::get('user')
            ));
            return true;
        }
        else{
            $dbInsert = $db->insert('sage_keys', array(
                'sage_key' => $authKey,
                'company_id' => $companyId,
                'user_id' => Session::get('user')
            ));
            return true;
        }

        return false;
    }

    public function validateLogin($name, $password){
        $provider = $this->getProvider();
        $response = $provider->Login()->get(array(
            "Username" => $name,
            "Password" => $password
        )); 
        return $response->body();
    }

    public function userHasKey($id = null){
        $db = DB::getInstance();
        if($id == null){
            $id = Session::get('user');
        }
        $key = $db->get('sage_keys', array(
            'user_id' => array('operator' => '=', 'value' => $id)
        ));
        if(!$key->results()){
            return false;
        }
        else{
            return $key->results()[count($key->results()) - 1];
        }
    }

    public static function getInstance(){
        if(!isset(self::$_instance)){
            self::$_instance = new Sage();
        }
        return self::$_instance;
    }
}
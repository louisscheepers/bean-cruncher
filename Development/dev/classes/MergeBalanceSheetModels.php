<?php 

class MergeBalanceSheetModels{

    private $_firstModel,
            $_secondModel,
            $_mergedModel;

    public function __construct($first, $second)
    {
        $this->_firstModel = $first;
        $this->_secondModel = $second;

        $this->_mergedModel = new BalanceSheetModel();
    }

    public function merge(){
        $this->mergeCurrentAssets();
        $this->mergeNonCurrentAssets();
        $this->mergeCurrentLiabilities();
        $this->mergeNonCurrentLiabilities();
        $this->mergeEquity();
        $this->mergeLastUpdated();
    }

    public function get(){
        return $this->_mergedModel;
    }

    private function mergeCurrentAssets(){
        foreach($this->_firstModel->getCurrentAssets() as $firstAsset){
            $firstData = $firstAsset['data'];
            $secondData = $this->_secondModel->getCurrentAssets()[array_search($firstAsset['account'], array_column($this->_secondModel->getCurrentAssets(), 'account'))]['data'];
            
            if(array_key_exists(0, $firstData)){

                array_push($firstData, ($this->isInModel($firstAsset['account'], $this->_secondModel->getCurrentAssets())) ? $secondData : array(
                    'date' => $firstData[0]['date'],
                    'amount' => 0
                ));
                $this->_mergedModel->addCurrentAsset(array(
                    'account' => $firstAsset['account'],
                    'data' => $firstData,
                ));
            }else{
                $this->_mergedModel->addCurrentAsset(array(
                    'account' => $firstAsset['account'],
                    'data' => array(
                        $firstData,
                        ($this->isInModel($firstAsset['account'], $this->_secondModel->getCurrentAssets())) ? $secondData : array(
                            'date' => $firstData[0]['date'],
                            'amount' => 0
                        ),
                    )
                ));
            }

            
        }
    }

    private function mergeNonCurrentAssets(){
        foreach($this->_firstModel->getNonCurrentAssets() as $firstAsset){
            $firstData = $firstAsset['data'];
            $secondData = $this->_secondModel->getNonCurrentAssets()[array_search($firstAsset['account'], array_column($this->_secondModel->getNonCurrentAssets(), 'account'))]['data'];
           
            if(array_key_exists(0, $firstData)){

                array_push($firstData, ($this->isInModel($firstAsset['account'], $this->_secondModel->getNonCurrentAssets())) ? $secondData : array(
                    'date' => $firstData[0]['date'],
                    'amount' => 0
                ));
                $this->_mergedModel->addNonCurrentAsset(array(
                    'account' => $firstAsset['account'],
                    'data' => $firstData,
                ));
            }else{
                $this->_mergedModel->addNonCurrentAsset(array(
                    'account' => $firstAsset['account'],
                    'data' => array(
                        $firstData,
                        ($this->isInModel($firstAsset['account'], $this->_secondModel->getNonCurrentAssets())) ? $secondData : array(
                            'date' => $firstData[0]['date'],
                            'amount' => 0
                        ),
                    )
                ));
            }
        }
    }

    private function mergeCurrentLiabilities(){
        foreach($this->_firstModel->getCurrentLiabilities() as $firstAsset){
            $firstData = $firstAsset['data'];
            $secondData = $this->_secondModel->getCurrentLiabilities()[array_search($firstAsset['account'], array_column($this->_secondModel->getCurrentLiabilities(), 'account'))]['data'];
           
            if(array_key_exists(0, $firstData)){

                array_push($firstData, ($this->isInModel($firstAsset['account'], $this->_secondModel->getCurrentLiabilities())) ? $secondData : array(
                    'date' => $firstData[0]['date'],
                    'amount' => 0
                ));
                $this->_mergedModel->addCurrentLiability(array(
                    'account' => $firstAsset['account'],
                    'data' => $firstData,
                ));
            }else{
                $this->_mergedModel->addCurrentLiability(array(
                    'account' => $firstAsset['account'],
                    'data' => array(
                        $firstData,
                        ($this->isInModel($firstAsset['account'], $this->_secondModel->getCurrentLiabilities())) ? $secondData : array(
                            'date' => $firstData[0]['date'],
                            'amount' => 0
                        ),
                    )
                ));
            }
        }
    }

    private function mergeNonCurrentLiabilities(){
        foreach($this->_firstModel->getNonCurrentLiabilities() as $firstAsset){
            $firstData = $firstAsset['data'];
            $secondData = $this->_secondModel->getNonCurrentLiabilities()[array_search($firstAsset['account'], array_column($this->_secondModel->getNonCurrentLiabilities(), 'account'))]['data'];
            
            if(array_key_exists(0, $firstData)){

                array_push($firstData, ($this->isInModel($firstAsset['account'], $this->_secondModel->getNonCurrentLiabilities())) ? $secondData : array(
                    'date' => $firstData[0]['date'],
                    'amount' => 0
                ));
                $this->_mergedModel->addNonCurrentLiability(array(
                    'account' => $firstAsset['account'],
                    'data' => $firstData,
                ));
            }else{
                $this->_mergedModel->addNonCurrentLiability(array(
                    'account' => $firstAsset['account'],
                    'data' => array(
                        $firstData,
                        ($this->isInModel($firstAsset['account'], $this->_secondModel->getNonCurrentLiabilities())) ? $secondData : array(
                            'date' => $firstData[0]['date'],
                            'amount' => 0
                        ),
                    )
                ));
            }
        }
    }

    private function mergeEquity(){
        foreach($this->_firstModel->getEquity() as $firstAsset){
            $firstData = $firstAsset['data'];
            $secondData = $this->_secondModel->getEquity()[array_search($firstAsset['account'], array_column($this->_secondModel->getEquity(), 'account'))]['data'];
            
            if(array_key_exists(0, $firstData)){

                array_push($firstData, ($this->isInModel($firstAsset['account'], $this->_secondModel->getEquity())) ? $secondData : array(
                    'date' => $firstData[0]['date'],
                    'amount' => 0
                ));
                $this->_mergedModel->addEquity(array(
                    'account' => $firstAsset['account'],
                    'data' => $firstData,
                ));
            }else{
                $this->_mergedModel->addEquity(array(
                    'account' => $firstAsset['account'],
                    'data' => array(
                        $firstData,
                        ($this->isInModel($firstAsset['account'], $this->_secondModel->getEquity())) ? $secondData : array(
                            'date' => $firstData[0]['date'],
                            'amount' => 0
                        ),
                    )
                ));
            }
        }
    }

    private function mergeLastUpdated(){
       
        $this->_mergedModel->addLastUpdated($this->_firstModel->getAllLastUpdated());
        $this->_mergedModel->addLastUpdated($this->_secondModel->getAllLastUpdated());
    }

    private function isInModel($account, $haystack){
        if(in_array($account, array_column($haystack, 'account'))){
            return true;
        }

        return false;
    }
}
<?php
class ApiStorage
{
	// private $_db;
	// 		$_token,
	// 		$_expires,
	// 		$_tenant_id,
	// 		$_refresh_token,
	// 		$_id_token,
	// 		$_state;

	function __construct() {
		if(!isset($_SESSION) ){
        	$this->init_session();
		}

		$_db = DB::getInstance();
		// var_dump($_SESSION);
		// var_dump(Companies::getActiveCompanyName());
   	}

   	public function init_session(){
    	session_start();
	}

    public function getSession() {
		// if(!isset($_SESSION['oauth2'])){
		// 	$this->setToken();
		// }
		return $_SESSION['oauth2'];
    }

 	public function startSession($token, $secret, $expires = null)
	{
       	session_start();
	}

	public function setToken($token, $expires = null, $tenantId = array(), $refreshToken, $idToken)
	{   
	    $_SESSION['oauth2'] = [
	        'token' => $token,
	        'expires' => $expires,
	        'tenant_id' => $tenantId,
	        'refresh_token' => $refreshToken,
	        'id_token' => $idToken
	    ];
	}

	public function getToken()
	{
	    //If it doesn't exist or is expired, return null
	    if (!empty($this->getSession())
	        || ($_SESSION['oauth2']['expires'] !== null
	        && $_SESSION['oauth2']['expires'] <= time())
	    ) {
	        return null;
	    }
	    return $this->getSession();
	}

	public function getAccessToken()
	{
	    return $_SESSION['oauth2']['token'];
	}

	public function getRefreshToken()
	{
	    return $_SESSION['oauth2']['refresh_token'];
	}

	public function getExpires()
	{
	    return $_SESSION['oauth2']['expires'];
	}

	public function getXeroTenantId()
	{
		if(isset($_SESSION['oauth2']['tenant_id'])){
			return $_SESSION['oauth2']['tenant_id'];
		}
		else{
			$db = DB::getInstance();
			$companyIntegration = $db->get('temp_company', array(
				'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
			));
			if($companyIntegration->results()){
				$tenantId = $companyIntegration->first()->company_api_id;
				return $tenantId;
			}
		}
	}

	public function getIdToken()
	{
	    return $_SESSION['oauth2']['id_token'];
	}

	public function getHasExpired()
	{
		if (!empty($this->getSession())) 
		{
			if(time() > $this->getExpires())
			{
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
}
?>
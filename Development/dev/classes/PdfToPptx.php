<?php


class PdfToPptx
{
    public static function convert($rndpdf, $realname){
        $outDir = __DIR__ . '/tmp/pptx/';
        $source = __DIR__ . '/tmp/pdf/' . $rndpdf . '.pdf';
        $shortSource = 'tmp/pdf/' . $rndpdf . '.pdf';



        if(file_exists($source)){
            echo shell_exec('soffice --infilter="impress_pdf_import" --convert-to pptx --outdir ' . $outDir. ' ' . $source);

            $rndpptx = $outDir . $rndpdf . '.pptx';
            $newpptx = $outDir . $realname . '.pptx';

            if(file_exists($rndpptx)){
                if(file_exists($newpptx)){
                    echo "unlinking file : already exists \n";
                    unlink($newpptx);
                }
                if(rename($rndpptx, $newpptx)){
                    echo "renaming pptx file \n";
                    return true;
                }
            }else{
                echo '\n pptx file does not exist';
            }

        } else {
            echo "\n Missing source file " . $source;
        }

        return false;

    }
}
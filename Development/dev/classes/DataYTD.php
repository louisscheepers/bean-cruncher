<?php

use QuickBooksOnline\API\ReportService\ReportService;

class DataYTD{

    private $_fromDate,
            $_apiId,
            $_chartOfAccounts = array(),
            $_interval,
            $_apiObject,
            $_quickbooks,
            $_trialBalance,
            $_xero,
            $_instance,
            $_db;

    public function __construct($fromDate, $interval){
        $this->_fromDate = $fromDate;
        $this->_apiId = Companies::getApi();
        $this->_interval = $interval;
        $this->_db = DB::getInstance();
        
    }

    private function setApiObject($fromDate, $toDate){
        switch ($this->_apiId) {
            //xero
            case 1:
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();
                $this->_apiObject = $this->_instance->getReportTrialBalance(Companies::getActiveTenantId(), $this->_fromDate);
            break;

            //sage
            case 2:
                // sage uses DataAccountAmounts
            break;

            //quickbooks
            case 3:

                // //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
                // //$logger->info('Getting Trial Balance report');

                // $fr = new DateTime($this->_fromDate);
                // $to = $fr->format('Y-m-t');
                
                // $min = new DateTime($to);
                // $min->modify('first day of -11 month');

                // $from = $min->format('Y-m-d');

                // $this->_quickbooks = Quickbooks::getInstance();
                // $dataService = $this->_quickbooks->getDataService();
                
                // $serviceContext = $dataService->getServiceContext();
                // $reportService = new ReportService($serviceContext);

                // // $reportService->setDateMacro('This Fiscal Year-to-date');
                // $reportService->setStartDate($from);
                // $reportService->setEndDate($to);

                // $this->_apiObject = $reportService->executeReport('TrialBalance');

                // $fr = new DateTime($this->_fromDate);
                // $to = $fr->format('Y-m-t');
                
                // $min = new DateTime($to);
                // $min->modify('first day of -11 month');

                // $from = $min->format('Y-m-d');

                // $this->_quickbooks = Quickbooks::getInstance();
                // $dataService = $this->_quickbooks->getDataService();
                
                // $serviceContext = $dataService->getServiceContext();
                // $reportService = new ReportService($serviceContext);

                // $reportService->setStartDate($from);
                // $reportService->setEndDate($to);

                // $this->_apiObject = $reportService->executeReport('ProfitAndLoss');

            break;
        }
    }

    public function setYTD(){

        $this->setApiObject($this->_fromDate, $this->_interval);


        switch ($this->_apiId) {
            //xero
            case 1:

                $rows = $this->_apiObject->getReports()[0]->getRows();
        
                for($i = 0; $i < count($rows) - 1; $i++){
                    $innerRows = $rows[$i]->getRows();
                    foreach((array)$innerRows as $innerRow){
                        $title = $innerRow->getCells()[0]->getValue();
        
                        $last = strrpos($title, "(");
                        $title = substr($title, 0, $last);
        
                        $ytd = 0;
                        if($innerRow->getCells()[4]->getValue() != 0){
                            $ytd = $innerRow->getCells()[4]->getValue();
                        }
                        else if($innerRow->getCells()[3]->getValue() != 0){
                            $ytd = $innerRow->getCells()[3]->getValue();
                        }
                        if($ytd == null){
                            $ytd = 0;
                        }
        
                        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
                        //$logger->info('YTD - Date: ' . $this->_fromDate . ' Account: ' . $title . ' Amount: ' . $ytd);
                        
        
                        $this->updateYTD($title, $ytd);
                    }
                }

            break;

            //sage
            case 2:
                // sage uses DataAccountAmounts
            break;

            //quickbooks
            case 3:

                // var_dump($this->_apiObject->Columns);
                // var_dump($this->_apiObject->Rows->Row[0]->Header);
                // var_dump($this->_apiObject->Rows->Row[0]->Rows->Row[0]);


                // die();
                // foreach($this->_apiObject->Rows->Row as $row){


                //     var_dump($row);



                    // $name = $row->ColData[0]->value;

                    // if($this->_apiId == 3){
                    //     $nameArr = explode(':', $name);
                    //     $name = $nameArr[0];
                    // }

                    // if(!$row->Summary){
                    //     $amount = $row->ColData[1]->value;
                    //     if($amount == ''){
                    //         $amount = $row->ColData[2]->value;
                    //     }
    
                    //     // var_dump($row);
                    //     if($amount != null){
                    //         $this->updateYTD($name, $amount);
                    //     }
                    //     // echo 'Name: ' . $name . '<br>';
                    //     // echo 'Amount: ' . $amount . '<br>';
                    // }
                // }
                
            break;
        }
    }

    public function updateYTD($name, $amount){

        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        $id = $this->findAccountId($name);

        $find = $this->_db->get('temp_account_ytd', array(
            'account_id' => array('operator' => '=', 'value' => $id),
            'date' => array('operator' => '=', 'value' => substr($this->_fromDate, 0, 7))
        ));
        if($find->count()){
            $accountYtdId = $find->first()->account_ytd_id;

            $update = $this->_db->update('temp_account_ytd', 'account_ytd_id', $accountYtdId, array(
                'ytd' => $amount,
                'last_updated' => $now
            ));
        }
        else{
            $insert = $this->_db->insert('temp_account_ytd', array(
                'account_id' => $id,
                'date' => substr($this->_fromDate, 0, 7),
                'ytd' => $amount,
                'last_updated' => $now
            ));
        }
    }

    public function findAccountId($name){

        $find = $this->_db->get('temp_accounts', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
            'account_name' => array('operator' => '=', 'value' => $name)
        ));
        if($find->count()){
            return $find->first()->account_id;
        }

        return false;
    }



}
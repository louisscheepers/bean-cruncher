<?php

class DataInvoices{
    private $_invoices = array(),
            $_apiInvoices = array(),
            $_apiPages = array(),
            $_modifiedAfter,
            $_apiObject,
            $_xero,
            $_quickbooks,
            $_instance,
            $_api,
            $_db,
            $_invoiceCount;

    public function __construct($modifiedAfter){
        $this->_db = DB::getInstance();
        $this->_apiId = Companies::getApi();
        $this->_modifiedAfter = $modifiedAfter;
    }

    private function getInvoiceCount(){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        //$logger->info('Getting all pages');

        $this->_xero = Xero::getInstance();
        $this->_instance = $this->_xero->getApiInstance();
        $this->_invoiceCount = count($this->_instance->getInvoices(Companies::getActiveTenantId()));
        return $this->_invoiceCount;
    }
    
    private function setApiObject($page){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');

        
        switch ($this->_apiId) {
            //xero
            case 1:
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();

                if($this->_modifiedAfter == null){
                    //$logger->info('Getting page ' . $page);
                    $this->_apiObject = $this->_instance->getInvoices(Companies::getActiveTenantId(), '', '', '', '', '', '', 'AUTHORISED,PAID', $page);
                    //$logger->info('Found ' . count($this->_apiObject->getInvoices()) . ' invoices');
                    
                    $pageInvoices = $this->_apiObject->getInvoices();
                    array_push($this->_apiPages, $pageInvoices);
                }
                else{
                    $dateFormat = str_replace('-', ',', $this->_modifiedAfter);
                    $this->_apiObject = $this->_instance->getInvoices(Companies::getActiveTenantId(), '', 'updateddateutc >= DateTime('.$dateFormat.')', '', '', '', '', 'AUTHORISED,PAID', $page);
                }
            break;

            //sage
            case 2:
                //not used
            break;

        }
    }

    private function setInvoices(){
        require_once (dirname(__DIR__, 1) . '/vendor/autoload.php');
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');

        switch ($this->_apiId) {
            //xero
            case 1:

                $this->getInvoiceCount();
                //$logger->info('Invoice count: ' . $this->_invoiceCount);
        
                if($this->_invoiceCount < 100){
                    $this->setApiObject(1);
                    $this->_apiInvoices = $this->_apiObject->getInvoices();
                    //$logger->info('One page only');
                }
                else{
                    $this->_pageCount = ceil(($this->_invoiceCount / 100));
                    //$logger->info('Pages: ' . $this->_pageCount);
        
                    for($i = 1; $i <= $this->_pageCount; $i++){
                        $this->setApiObject($i);
                    }
        
                    $invCount = 0;
                    for($i = 0; $i < count($this->_apiPages); $i++){
                        for($j = 0; $j < count($this->_apiPages[$i]); $j++){
                            $this->_apiInvoices[$invCount] = $this->_apiPages[$i][$j];
                            $invCount++;
                        }
                    }
                }
        
                array_values($this->_apiInvoices);
                // $this->getInvoicePage(0);
        
                for($i = 0; $i < count($this->_apiInvoices); $i++){
                    $type = $this->_apiInvoices[$i]->getType();
                    // //$logger->info("Inv Type " . $type);
                    if($type == 'ACCREC'){
                        $this->_invoices[$i]['type'] = 1;
                    }
                    else{
                        $this->_invoices[$i]['type'] = 2;
                    }

        
                    $created = date('Y-m-d', (substr($this->_apiInvoices[$i]->getDate(), 6, 13) / 1000));
                    $due = date('Y-m-d', (substr($this->_apiInvoices[$i]->getDueDate(), 6, 13) / 1000));
        
                    $this->_invoices[$i]['api_id'] = $this->_apiInvoices[$i]->getInvoiceid();
                    // $this->_invoices[$i]['amount_paid'] = $this->_apiObject->getInvoices()[$i]->getType();
                    $this->_invoices[$i]['amount_due'] = $this->_apiInvoices[$i]->getAmountDue();
                    $this->_invoices[$i]['date_created'] = $created;
                    $this->_invoices[$i]['date_due'] = $due;
                    $this->_invoices[$i]['contact'] = $this->_apiInvoices[$i]->getContact()->getName();
                    $this->_invoices[$i]['line_items'] = $this->_apiInvoices[$i]->getLineItems();
                }
        
                $this->_invoices = array_values($this->_invoices);

                $op = new DataOverpayments($this->_modifiedAfter);
                $overpayments = $op->getOverpayments();

                for($i = 0; $i < count($overpayments); $i++){
                    array_push($this->_invoices, $overpayments[$i]);
                }

                $cn = new DataCreditNotes($this->_modifiedAfter);
                $creditNotes = $cn->getCreditNotes();

                for($i = 0; $i < count($creditNotes); $i++){
                    array_push($this->_invoices, $creditNotes[$i]);
                }

            break;

            //sage
            case 2:
                $this->_sage = Sage::getInstance();
                $provider = $this->_sage->getProvider();
                $salesOrders = $provider->SalesOrders()->get(Session::get('active_tenant_id'))->results();
                $supplierInvoices = $provider->Supplierinvoices()->get(Session::get('active_tenant_id'))->results();

                for($i = 0; $i < count($salesOrders); $i++){
                    $date = substr($salesOrders[$i]->Date, 0, 10);
                    $due = substr($salesOrders[$i]->DeliveryDate, 0, 10);

                    $this->_invoices[$i]['type'] = 1;
                    $this->_invoices[$i]['api_id'] = $salesOrders[$i]->ID;
                    $this->_invoices[$i]['amount_due'] = $salesOrders[$i]->AmountDue;
                    $this->_invoices[$i]['date_created'] = $date;
                    $this->_invoices[$i]['date_due'] = $due;
                    $this->_invoices[$i]['contact'] = $salesOrders[$i]->CustomerName;
                    $this->_invoices[$i]['contact_api_id'] = $salesOrders[$i]->CustomerId;
                    for($j = 0; $j < count($salesOrders[$i]->Lines); $j++){
                        $this->_invoices[$i]['line_items'][$j]['item_code'] = $salesOrders[$i]->Lines[$j]->SelectionId;
                        $this->_invoices[$i]['line_items'][$j]['quantity'] = $salesOrders[$i]->Lines[$j]->Quantity;
                        $this->_invoices[$i]['line_items'][$j]['line_amount'] = $salesOrders[$i]->Lines[$j]->Total;
                    }
                }
                for($i = 0; $i < count($supplierInvoices); $i++){
                    $date = substr($supplierInvoices[$i]->Date, 0, 10);
                    $due = substr($supplierInvoices[$i]->DueDate, 0, 10);
                    
                    $this->_invoices[count($salesOrders)+$i]['type'] = 2;
                    $this->_invoices[count($salesOrders)+$i]['api_id'] = $supplierInvoices[$i]->ID;
                    $this->_invoices[count($salesOrders)+$i]['amount_due'] = $supplierInvoices[$i]->AmountDue;
                    $this->_invoices[count($salesOrders)+$i]['date_created'] = $date;
                    $this->_invoices[count($salesOrders)+$i]['date_due'] = $due;
                    $this->_invoices[count($salesOrders)+$i]['contact'] = $supplierInvoices[$i]->SupplierName;
                    $this->_invoices[count($salesOrders)+$i]['contact_api_id'] = $supplierInvoices[$i]->SupplierId;
                    for($j = 0; $j < count($supplierInvoices[$i]->Lines); $j++){
                        $this->_invoices[count($salesOrders)+$i]['line_items'][$j]['item_code'] = $supplierInvoices[$i]->Lines[$j]->SelectionId;
                        $this->_invoices[count($salesOrders)+$i]['line_items'][$j]['quantity'] = $supplierInvoices[$i]->Lines[$j]->Quantity;
                        $this->_invoices[count($salesOrders)+$i]['line_items'][$j]['line_amount'] = $supplierInvoices[$i]->Lines[$j]->Total;
                    }
                }
                $this->getOpeningBalances();

            break;

            //quickbooks
            case 3:

                //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
                //$logger->info('Getting QB instance');

                $this->_quickbooks = Quickbooks::getInstance();
                $dataService = $this->_quickbooks->getDataService();
                
                // get bills
                //$logger->info('Getting Bills...');

                $billCount = 0;
                $bills = $dataService->FindAll('Bill', 1, 500);
                if($bills){
                    $billCount = count($bills);
                }

                // get invoices
                //$logger->info('Getting Invoices...');
                $invoices = $dataService->FindAll('Invoice', 1, 500);

                // get payments
                $paymentCount = 0;
                //$logger->info('Getting Payments...');
                $payments = $dataService->FindAll('Payment', 1, 500);
                if($payments){
                    $paymentCount = count($payments);
                }

                // get items
                //$logger->info('Getting Items...');
                $items = $dataService->FindAll('Item', 1, 500);

                // get vendors
                //$logger->info('Getting Vendors...');
                $vendors = $dataService->FindAll('Vendor', 1, 500);

                // get customers
                //$logger->info('Getting Customers...');
                $customers = $dataService->FindAll('Customer', 1, 500);

                //$logger->info('Payment count: ' . $paymentCount);
                for($i = 0; $i < $paymentCount; $i++){
                    if($payments[$i]->UnappliedAmt != 0){
                        //$logger->info('Processing payment ' . $i);
    
                        $contact = '';
                        foreach($customers as $customer){
                            if($customer->Id == $invoices[$i]->CustomerRef){
                                $contact = $customer->DisplayName;
                                break;
                            }
                        }
    
                        $this->_invoices[$i]['type'] = 1;
                        $this->_invoices[$i]['api_id'] = $payments[$i]->Id;
                        $this->_invoices[$i]['amount_due'] = 0 - $payments[$i]->TotalAmt;
                        $this->_invoices[$i]['date_created'] = $payments[$i]->TxnDate;
                        $this->_invoices[$i]['date_due'] = $payments[$i]->TxnDate;
                        $this->_invoices[$i]['contact'] = $contact;
                        
                        $lineItems = $payments[$i]->Line;
                        if(is_array($payments[$i]->Line)){
                            // for($j = 0; $j < count($lineItems); $j++){
                                if($payments[$i]->Line->Description != '' || $payments[$i]->Line->Description != null){
                                    $this->_invoices[$i]['line_items'][$j]['item_code'] = $payments[$i]->Line->Description;
                                    $this->_invoices[$i]['line_items'][$j]['quantity'] = $payments[$i]->Line->LineNum;
                                    $this->_invoices[$i]['line_items'][$j]['line_amount'] = $payments[$i]->Line->Amount;
                                }
                            // }
                        }
                    }
                }

                //$logger->info('Bill count: ' . $billCount);
                for($i = 0; $i < $billCount; $i++){
                    //$logger->info('Processing bill ' . $i);

                    $contact = '';
                    foreach($vendors as $vendor){
                        if($vendor->Id == $bills[$i]->VendorRef){
                            $contact = $vendor->DisplayName;
                            break;
                        }
                    }

                    $this->_invoices[$i + $paymentCount]['type'] = 2;
                    $this->_invoices[$i + $paymentCount]['api_id'] = $bills[$i]->Id;
                    $this->_invoices[$i + $paymentCount]['amount_due'] = $bills[$i]->TotalAmt;
                    $this->_invoices[$i + $paymentCount]['date_created'] = $bills[$i]->TxnDate;
                    $this->_invoices[$i + $paymentCount]['date_due'] = $bills[$i]->DueDate;
                    $this->_invoices[$i + $paymentCount]['contact'] = $contact;
                    
                    $lineItems = $bills[$i]->Line;
                    if(is_array($bills[$i]->Line)){
                        for($j = 0; $j < count($lineItems); $j++){
                            if($bills[$i]->Line[$j]->Description != ''){
                                $this->_invoices[$i + $paymentCount]['line_items'][$j]['item_code'] = $bills[$i]->Line[$j]->Description;
                                $this->_invoices[$i + $paymentCount]['line_items'][$j]['quantity'] = $bills[$i]->Line[$j]->SalesItemLineDetail->Qty;
                                $this->_invoices[$i + $paymentCount]['line_items'][$j]['line_amount'] = $bills[$i]->Line[$j]->Amount;
                            }
                        }
                    }
                }

                //$logger->info('Invoice count: ' . count($invoices));
                for($i = 0; $i < count($invoices); $i++){
                    //$logger->info('Processing invoice ' . $i);
                    
                    $contact = '';
                    foreach($customers as $customer){
                        if($customer->Id == $invoices[$i]->CustomerRef){
                            $contact = $customer->DisplayName;
                            break;
                        }
                    }

                    $this->_invoices[$i + $billCount + $paymentCount]['type'] = 1;
                    $this->_invoices[$i + $billCount + $paymentCount]['api_id'] = $invoices[$i]->DocNumber;
                    $this->_invoices[$i + $billCount + $paymentCount]['amount_due'] = $invoices[$i]->Balance;
                    $this->_invoices[$i + $billCount + $paymentCount]['date_created'] = $invoices[$i]->TxnDate;
                    $this->_invoices[$i + $billCount + $paymentCount]['date_due'] = $invoices[$i]->DueDate;
                    $this->_invoices[$i + $billCount + $paymentCount]['contact'] = $contact;
                    
                    $lineItems = $invoices[$i]->Line;
                    if(is_array($invoices[$i]->Line)){
                        for($j = 0; $j < count($lineItems); $j++){
                            if($invoices[$i]->Line[$j]->Description != ''){
                                $itemRef = $invoices[$i]->Line[$j]->SalesItemLineDetail->ItemRef;
                                $itemCode = '';
                                foreach($items as $item){
                                    if($itemRef == $item->Id){
                                        $itemCode = $item->Name;
                                    }
                                }
                                $this->_invoices[$i + $billCount + $paymentCount]['line_items'][$j]['item_code'] = $itemCode;
                                $this->_invoices[$i + $billCount + $paymentCount]['line_items'][$j]['quantity'] = $invoices[$i]->Line[$j]->SalesItemLineDetail->Qty;
                                $this->_invoices[$i + $billCount + $paymentCount]['line_items'][$j]['line_amount'] = $invoices[$i]->Line[$j]->Amount;
                            }
                        }
                    }
                }

                $this->_invoices = array_values($this->_invoices);

            break;
        }

    }

    public function getOpeningBalances(){
        //customers
        $provider = $this->_sage->getProvider();
        $openingBalancesCustomer = $provider->CustomerOpeningBalance()->get(Session::get('active_tenant_id'))->results();
        $customers = $provider->Customers()->get(Session::get('active_tenant_id'))->results();
        
        $invoiceCount = count($this->_invoices);

        for($i = 0; $i < count($openingBalancesCustomer); $i++){
            $balance = $openingBalancesCustomer[$i]->Balance;
            if($balance != 0){
                $date = substr($openingBalancesCustomer[$i]->Date, 0, 10);
    
                $contactName = null;
                $contactId = $openingBalancesCustomer[$i]->CustomerId;
                for($j = 0; $j < count($customers); $j++){
                    if($contactId == $customers[$j]->ID){
                        $contactName = $customers[$j]->Name;
                        break;
                    }
                }
    
                $type = 1;
                $this->_invoices[$invoiceCount + $i]['api_id'] = $openingBalancesCustomer[$i]->ID;
                $this->_invoices[$invoiceCount + $i]['amount_due'] = $balance;
                $this->_invoices[$invoiceCount + $i]['date_created'] = $date;
                $this->_invoices[$invoiceCount + $i]['date_due'] = $date;
                $this->_invoices[$invoiceCount + $i]['contact'] = $contactName;
                $this->_invoices[$invoiceCount + $i]['contact_api_id'] = $contactId;
                $this->_invoices[$invoiceCount + $i]['line_items'] = null;
                $this->_invoices[$invoiceCount + $i]['type'] = $type;
            }
        }
        $this->_invoices = array_values($this->_invoices);

        //suppliers
        $openingBalancesSupplier = $provider->SupplierOpeningBalance()->get(Session::get('active_tenant_id'))->results();
        $suppliers = $provider->Suppliers()->get(Session::get('active_tenant_id'))->results();

        $invoiceCount = count($this->_invoices);

        for($i = 0; $i < count($openingBalancesSupplier); $i++){
            $balance = $openingBalancesSupplier[$i]->Balance;
            if($balance != 0){
                $date = substr($openingBalancesSupplier[$i]->Date, 0, 10);
    
                $contactName = null;
                $contactId = $openingBalancesSupplier[$i]->SupplierId;
                for($j = 0; $j < count($suppliers); $j++){
                    if($contactId == $suppliers[$j]->ID){
                        $contactName = $suppliers[$j]->Name;
                        break;
                    }
                }
    
                $type = 2;
                $this->_invoices[$invoiceCount + $i]['api_id'] = $openingBalancesSupplier[$i]->ID;
                $this->_invoices[$invoiceCount + $i]['amount_due'] = $balance;
                $this->_invoices[$invoiceCount + $i]['date_created'] = $date;
                $this->_invoices[$invoiceCount + $i]['date_due'] = $date;
                $this->_invoices[$invoiceCount + $i]['contact'] = $contactName;
                $this->_invoices[$invoiceCount + $i]['contact_api_id'] = $contactId;
                $this->_invoices[$invoiceCount + $i]['line_items'] = null;
                $this->_invoices[$invoiceCount + $i]['type'] = $type;
            }
        }
        $this->_invoices = array_values($this->_invoices);
    }

    public function updateDb(){
        $this->setInvoices();
        
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        //$logger->info('Adding invoices to DB');

        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        $allInvoices = $this->_db->get('temp_invoices', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
        ));

        if($allInvoices->count()){
            $allDbInvoices = $allInvoices->results();
            $ids = $this->extractInvoiceApiIds($allDbInvoices);
        }
        
        for($i = 0; $i < count($this->_invoices); $i++){
            $dbInvoices = $this->_db->get('temp_invoices', array(
                'api_invoice_id' => array('operator' => '=', 'value' => $this->_invoices[$i]['api_id']), 
                'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
            ));
            if($dbInvoices->results()){
                $data = $dbInvoices->results();
                $invoiceId = $data[0]->invoice_id;

                $ids = $this->removeInvoiceApiId($this->_invoices[$i]['api_id'], $ids);

                $dbUpdate = $this->_db->update('temp_invoices', 'invoice_id', $invoiceId, array(
                    'api_invoice_id' => $this->_invoices[$i]['api_id'],
                    'type_id' => $this->_invoices[$i]['type'],
                    'date_created' => $this->_invoices[$i]['date_created'],
                    'date_due' => $this->_invoices[$i]['date_due'],
                    'amount_due' => $this->_invoices[$i]['amount_due'],
                    'contact_name' => $this->_invoices[$i]['contact'],
                    'last_updated' => $now
                ));
                if(isset($this->_invoices[$i]['line_items'])){
                    $dbLineItems = $this->_db->get('temp_line_items', array(
                        'invoice_id' => array('operator' => '=', 'value' => $invoiceId), 
                    ));
                    if($dbLineItems->results()){
                        $lineItemsId = $dbLineItems->first()->line_items_id;
    
                        $updateInvoiceLine = $this->_db->update('temp_invoices', 'invoice_id', $invoiceId, array(
                            'line_items_id' => $lineItemsId
                        ));
    
                        $dbLineItemsDesc = $this->_db->get('temp_line_item_desc', array(
                            'line_items_id' => array('operator' => '=', 'value' => $lineItemsId), 
                        ));
                        if($dbLineItemsDesc->results()){
                            $data = $dbLineItemsDesc->results();
    
                            for($j = 0; $j < count($data); $j++){
                                $lineItemsDescId = $data[$j]->line_item_desc_id;
    
                                $dbUpdate = $this->_db->update('temp_line_item_desc', 'line_item_desc_id', $lineItemsDescId, array(
                                    'line_items_id' => $lineItemsId,
                                    'item_code' => $this->_invoices[$i]['line_items'][$j]['item_code'],
                                    'quantity' => $this->_invoices[$i]['line_items'][$j]['quantity'],
                                    'line_amount' => $this->_invoices[$i]['line_items'][$j]['line_amount'],
                                    'last_updated' => $now
                                ));
                            }
                        }
                        else{
                            for($j = 0; $j < count($data); $j++){
                                $insertLineItemDesc = $this->_db->insert('temp_line_item_desc', array(
                                    'line_items_id' => $lineItemsId,
                                    'item_code' => $this->_invoices[$i]['line_items'][$j]['item_code'],
                                    'quantity' => $this->_invoices[$i]['line_items'][$j]['quantity'],
                                    'line_amount' => $this->_invoices[$i]['line_items'][$j]['line_amount'],
                                    'last_updated' => $now
                                ));
                            }
                        }
                    }
                    else{
                        $insertLineItems = $this->_db->insert('temp_line_items', array(
                            'invoice_id' => $invoiceId
                        ));
                        
                        $updateInvoiceLine = $this->_db->update('temp_invoices', 'invoice_id', $invoiceId, array(
                            'line_items_id' => $lineItemsId
                        ));
                    }
                }
            }

            //no invoice
            else{
                $ids = $this->removeInvoiceApiId($this->_invoices[$i]['api_id'], $ids);

                $insertInvoice = $this->_db->insert('temp_invoices', array(
                    'api_invoice_id' => $this->_invoices[$i]['api_id'],
                    'company_id' => Companies::getActiveCompanyId(),
                    'type_id' => $this->_invoices[$i]['type'],
                    'date_created' => $this->_invoices[$i]['date_created'],
                    'date_due' => $this->_invoices[$i]['date_due'],
                    'amount_due' => $this->_invoices[$i]['amount_due'],
                    'contact_name' => $this->_invoices[$i]['contact'],
                    'last_updated' => $now
                ));
                if($this->_invoices[$i]['line_items'] != null){
                    $dbInvoices = $this->_db->get('temp_invoices', array(
                        'api_invoice_id' => array('operator' => '=', 'value' => $this->_invoices[$i]['api_id']), 
                        'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
                    ));
                    if($dbInvoices->results()){
                        $invoiceId = $dbInvoices->first()->invoice_id;

                        $insertLineItem = $this->_db->insert('temp_line_items', array(
                            'invoice_id' => $invoiceId
                        ));
                        $dbLineItem = $this->_db->get('temp_line_items', array(
                            'invoice_id' => array('operator' => '=', 'value' => $invoiceId)
                        ));
                        if($dbLineItem->results()){
                            $lineItemsId = $dbLineItem->results()[0]->line_items_id;
                            
                            $updateInvoiceLine = $this->_db->update('temp_invoices', 'invoice_id', $invoiceId, array(
                                'line_items_id' => $lineItemsId
                            ));

                            for($j = 0; $j < count($this->_invoices[$i]['line_items']); $j++){
                                $insertLineItemDesc = $this->_db->insert('temp_line_item_desc', array(
                                    'line_items_id' => $lineItemsId,
                                    'item_code' => $this->_invoices[$i]['line_items'][$j]['item_code'],
                                    'quantity' => $this->_invoices[$i]['line_items'][$j]['quantity'],
                                    'line_amount' => $this->_invoices[$i]['line_items'][$j]['line_amount'],
                                    'last_updated' => $now
                                ));
                            }
                        }
                    }
                }
            }
        }

        $this->purgeInvoices($ids);
    }

    public function extractInvoiceApiIds($array){
        $idArray = array();
        foreach($array as $obj){
            $apiId = $obj->api_invoice_id;
            $invId = $obj->invoice_id;
            $ids = array(
                'api_id' => $apiId,
                'invoice_id' => $invId
            );
            array_push($idArray, $ids);
        }
        return $idArray;
    }

    public function removeInvoiceApiId($id, $idArray){
        $index = array_search($id, $idArray);
        unset($idArray[$index]);
        $idArray = array_values($idArray);

        return $idArray;
    }
    public function purgeInvoices($array){
        foreach($array as $id){
            $lineItemId = $this->_db->get('temp_line_items', array(
                'invoice_id' => array('operator' => '=', 'value' => $array->invoice_id)
            ));
            if($lineItemId->count()){
                $lineId = $lineItemId->line_items_id;

                $deleteLineItemDesc = $this->_db->delete('temp_line_item_desc', array(
                    'line_item_id', '=', $lineId
                ));
                $deleteLineItem = $this->_db->delete('temp_line_item', array(
                    'line_item_id', '=', $lineId
                ));
            }
            $delete = $this->_db->delete('temp_invoices', array(
                'invoice_id', '=', $array->invoice_id
            ));
        }
    }

    public function getInvoices(){

        $updates = array();
        $dbQuery = $this->_db->get('temp_invoices', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
        ));
        if($dbQuery->results()){
            for($i = 0; $i < count($dbQuery->results()); $i++){
                $this->_invoices[$i]['type'] = $dbQuery->results()[$i]->type_id;
                $this->_invoices[$i]['date_created'] = $dbQuery->results()[$i]->date_created;
                $this->_invoices[$i]['date_due'] = $dbQuery->results()[$i]->date_due;
                $this->_invoices[$i]['amount_due'] = $dbQuery->results()[$i]->amount_due;
                $this->_invoices[$i]['contact'] = $dbQuery->results()[$i]->contact_name;
                $this->_invoices[$i]['api_invoice_id'] = $dbQuery->results()[$i]->api_invoice_id;
                // $this->_invoices[$i]['last_updated'] = $dbQuery->results()[$i]->last_updated;
                array_push($updates, $dbQuery->results()[$i]->last_updated);
            }
            if($updates != null){
                $oldestUpdate = min($updates);
            }
            if(count((array)$oldestUpdate) != 1){
                $oldestUpdate = $oldestUpdate[0];
            }
            $this->_invoices['last_updated'] = $oldestUpdate;
        }

        return $this->_invoices;
    }

    // $type = 1 / 2
    public function getAccounts($month, $type){
        $this->getInvoices();

        $array = array();
        $names = array();
        $monthEnd = strtotime(date("Y-m-t", strtotime($month)));

        for($i = 0; $i < count($this->_invoices) - 1; $i++){
            if($this->_invoices[$i]['amount_due'] != 0 && $this->_invoices[$i]['type'] == $type){
                // $dateCreated = strtotime($this->_invoices[$i]['date_created']);
                // $overDue = ceil(($monthEnd - $dateCreated) / (60 * 60 * 24));
                array_push($names, $this->_invoices[$i]['contact']);
            }
        }
        $names = array_unique($names);
        $names = array_values($names);

        for($i = 0; $i < count($names); $i++){
            $array['accounts'][$i]['name'] = $names[$i];
            $array['accounts'][$i]['120+'] = 0;
            $array['accounts'][$i]['90-120'] = 0;
            $array['accounts'][$i]['60-90'] = 0;
            $array['accounts'][$i]['30-60'] = 0;
            $array['accounts'][$i]['0-30'] = 0;
            $array['accounts'][$i]['total'] = 0;
        }

        for($i = 0; $i < count($this->_invoices) - 1; $i++){
            if($this->_invoices[$i]['amount_due'] != 0 && $this->_invoices[$i]['type'] == $type){
                $dateCreated = strtotime($this->_invoices[$i]['date_created']);
                $overDue = ceil(($dateCreated - $monthEnd) / (60 * 60 * 24));

                // this part must be re-written into a method when I have time

                if(Companies::getApi() == 1){
                    $overDue = abs($overDue);
                    for($j = 0; $j < count($array['accounts']); $j++){
                        if($array['accounts'][$j]['name'] == $this->_invoices[$i]['contact']){
                            if($overDue < 30 && $overDue >= 0){
                                $array['accounts'][$j]['0-30'] += $this->_invoices[$i]['amount_due'];
                                $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                            }
                            else if($overDue < 60 && $overDue >= 30){
                                $array['accounts'][$j]['30-60'] += $this->_invoices[$i]['amount_due'];
                                $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                            }
                            else if($overDue < 90 && $overDue >= 60){
                                $array['accounts'][$j]['60-90'] += $this->_invoices[$i]['amount_due'];
                                $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                            }
                            else if($overDue < 120 && $overDue >= 90){
                                $array['accounts'][$j]['90-120'] += $this->_invoices[$i]['amount_due'];
                                $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                            }
                            else if($overDue >= 120){
                                $array['accounts'][$j]['120+'] += $this->_invoices[$i]['amount_due'];
                                $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                            }
                        }
                    }
                }
                else{
                    if($overDue < 0){
                        $overDue = abs($overDue);
                        for($j = 0; $j < count($array['accounts']); $j++){
                            if($array['accounts'][$j]['name'] == $this->_invoices[$i]['contact']){
                                if($overDue < 30 && $overDue >= 0){
                                    $array['accounts'][$j]['0-30'] += $this->_invoices[$i]['amount_due'];
                                    $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                                }
                                else if($overDue < 60 && $overDue >= 30){
                                    $array['accounts'][$j]['30-60'] += $this->_invoices[$i]['amount_due'];
                                    $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                                }
                                else if($overDue < 90 && $overDue >= 60){
                                    $array['accounts'][$j]['60-90'] += $this->_invoices[$i]['amount_due'];
                                    $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                                }
                                else if($overDue < 120 && $overDue >= 90){
                                    $array['accounts'][$j]['90-120'] += $this->_invoices[$i]['amount_due'];
                                    $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                                }
                                else if($overDue >= 120){
                                    $array['accounts'][$j]['120+'] += $this->_invoices[$i]['amount_due'];
                                    $array['accounts'][$j]['total'] += $this->_invoices[$i]['amount_due'];
                                }
                            }
                        }
                    }
                }
            }
        }
        if(isset($array['accounts'])){
            $total = array_column($array['accounts'], 'total');
            array_multisort($total, SORT_DESC, $array['accounts']);
        }
        if(isset($this->_invoices['last_updated'])){
            $array['last_updated'] = $this->_invoices['last_updated'];
        }

        return $array;
    }
    
    public function getLineItems($month){
        $updates = array();
        $items = array();
        $sales = array();
        $totals = array();

        $dbQueryInvoice = $this->_db->get('temp_invoices', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
            'date_created' => array('operator' => 'LIKE', 'value' => substr($month, 0, 7).'%')
        ));
        if($dbQueryInvoice->results()){
            $data = $dbQueryInvoice->results();

            for($i = 0; $i < count((array)$data); $i++){

                $lineItemId = $data[$i]->line_items_id;

                $dbQueryLineItemDesc = $this->_db->get('temp_line_item_desc', array(
                    'line_items_id' => array('operator' => '=', 'value' => $lineItemId)
                ));
                if($dbQueryLineItemDesc->results()){
                    $itemData = $dbQueryLineItemDesc->results();
                
                    for($j = 0; $j < count((array)$itemData); $j++){
                        if($itemData[$j]->item_code != null){
                            array_push($items, $itemData[$j]->item_code);
                            array_push($sales, $itemData[$j]->line_amount);
                            array_push($updates, $itemData[$j]->last_updated);
                        }
                    }
                }
            }
        }

        $itemCount = 0;

        for($i = 0; $i < count($items); $i++){
            // echo 'Item Code: ' . $items[$i] . '<br>';
            // echo 'Amount: ' . $sales[$i] . '<br>';
            if(count($totals) == 0){
                $totals[$itemCount]['item_code'] = $items[$i];
                $totals[$itemCount]['total_sales'] = $sales[$i];
                $itemCount++;
            }
            else{
                for($j = 0; $j <= count($totals); $j++){
                    if($j != count($totals)){
                        if($totals[$j]['item_code'] == $items[$i]){
                            // echo 'Found in array<br>';
                            $totals[$j]['total_sales'] += $sales[$i];
                            break;
                        }
                    }
                    else{
                        // echo 'Not found in array<br>';
                        $totals[$itemCount]['item_code'] = $items[$i];
                        $totals[$itemCount]['total_sales'] = $sales[$i];
                        $itemCount++;
                        break;
                    }
                }
            }
            // echo '<br>';
        }
        $tot = array_column($totals, 'total_sales');
        array_multisort($tot, SORT_DESC, $totals);

        if($updates != null){
            $oldestUpdate = min($updates);
            $totals['last_updated'] = $oldestUpdate;
        }

        return $totals;
    }

    public function getXeroInvoices(){
        $this->setInvoices();
        return $this->_invoices;
    }
}
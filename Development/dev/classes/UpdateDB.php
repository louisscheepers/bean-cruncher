<?php

class UpdateDB{
    private static $_data = array();

    public static function setData($action, $fromDate, $toDate){


        DateLabels::setDateRange($fromDate, $toDate);
        $months = DateLabels::getMonthRange();
        $years = DateLabels::getYearRange(); 
        
        require_once (dirname(__DIR__, 1) . '/vendor/autoload.php');
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');

        if($action){

            switch ($action) {

                //Cash In v Cash Out
                case 'cico':
                    
                    $cashReceivedArr = array();
                    $cashSpentArr = array();

                    for($i = 0; $i < count($months); $i++){
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));

                        $cashReceived = new DataCashReceived($fromDate);
                        $cashSpent = new DataCashSpent($fromDate);

                        $cashReceived->updateDb();
                        $cashSpent->updateDb();
                    }

                    return true;
    
                break;
                case 'ino':
                    
                    for($i = 0; $i < count($months); $i++){
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));
                        $toDate = date("Y-m-t", strtotime($fromDate));

                        $pl = new DataProfitAndLoss($fromDate, $toDate, 11);
                        $pl->updateDb();
                    }

                    return true;

                break;
                case 'avd':

                        //$logger->debug('Updating AVD module');

                        $bs = new DataBalanceSheet($fromDate . '-01', 11);
                        $bs->updateDb();
                        //$logger->debug('Update AVD module finished');
                        
                    return true;
                    
                break;
                case 'pl':

                    $fromDate1 = date('Y-m-d', mktime(0, 0, 0, $months[0] , 01, $years[0]));
                    $toDate1 = date("Y-m-t", strtotime($fromDate1));
                    $fromDate2 = date('Y-m-d', mktime(0, 0, 0, $months[1] , 01, $years[1]));
                    $toDate2 = date("Y-m-t", strtotime($fromDate2));

                    $pl1 = new DataProfitAndLoss($fromDate1, $toDate1, 11);
                    $pl2 = new DataProfitAndLoss($fromDate2, $toDate2, 11);

                    $pl1->updateDb();
                    $pl2->updateDb();

                    return true;

                break;
                case 'bs':

                    $bs1 = new DataBalanceSheet($fromDate, 11);
                    $bs2 = new DataBalanceSheet($toDate, 11);

                    $bs1->updateDb();
                    $bs2->updateDb();

                    return true;

                break;
                case 'ar':

                    $inv = new DataInvoices(null);
                    $inv->updateDb();

                    return true;
                    
                break;
                case 'ddo':

                    $inv = new DataInvoices(null);
                    $inv->updateDb();

                    return true;
                    
                break;
                case 'tcs':

                    $inv = new DataInvoices(null);
                    $inv->updateDb();

                    return true;
                    
                break;
                case 'ret':

                    $inv = new DataInvoices(null);
                    $inv->updateDb();

                    return true;
                    
                break;
                case 'ap':

                    $inv = new DataInvoices(null);
                    $inv->updateDb();

                    return true;
                    
                break;
                case 'cdo':

                    $inv = new DataInvoices(null);
                    $inv->updateDb();

                    return true;
                    
                break;
                case 'tsp':

                    $inv = new DataInvoices(null);
                    $inv->updateDb();

                    return true;
                    
                break;
                case 'im':

                    $inv = new DataInventory(null);
                    $inv->updateDb();

                    return true;
                    
                break;
                case 'inas':

                    $li = new DataInvoices(null);
                    $li->updateDb();

                    return true;
                    
                break;
                case 'inaq':

                    $inv = new DataInventory(null);
                    $inv->updateDb();

                    return true;
                    
                break;
                case 'plr':
                    
                    for($i = 0; $i < count($months); $i++){
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));
                        $toDate = date("Y-m-t", strtotime($fromDate));

                        $pl = new DataProfitAndLoss($fromDate, $toDate, 11);
                        $pl->updateDb();
                    }

                    return true;
                    
                break;
                case 'bsr':
                    
                    for($i = 0; $i < count($months); $i++){
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));
                        
                        $bs = new DataBalanceSheet($fromDate, 11);
                        $bs->updateDb();
                    }

                    return true;
                    
                break;
            }
        }
    }
}
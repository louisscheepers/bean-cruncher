<?php

use QuickBooksOnline\API\ReportService\ReportService;

class DataBalanceSheet{
    private $_balanceSheet,
            $_balanceSheetOld,
            $_chartOfAccounts = array(),
            $_apiId,
            $_interval,
            $_apiObject,
            $_fromDate,
            $_xero,
            $_quickbooks,
            $_sage,
            $_instance,
            $_db,
            $_now;

    //private //$logger;

    public function __construct($fromDate, $interval){
        $this->_fromDate = new DateTime('first day of ' . $fromDate);
        $this->_fromDate = $this->_fromDate->format('Y-m-d');
        $this->_apiId = Companies::getApi();
        $this->_interval = $interval;
        $this->_db = DB::getInstance();
        require_once (dirname(__DIR__, 1) . '/vendor/autoload.php');
        //$this->logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        //$this->logger->debug('BalanceSheet Class Initialized');
        $this->_balanceSheet = new BalanceSheetModel();
        $today = new DateTime();
        $this->_now = $today->format('Y/m/d H:i');
    }

    private function setApiObject($fromDate){

        switch ($this->_apiId) {
            //xero
            case 1:
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();
                $this->_apiObject = $this->_instance->getReportBalanceSheet(Companies::getActiveTenantId(), $this->_fromDate, 11, 'MONTH');
            break;

            //sage
            case 2:
                // sage uses DataAccountAmounts
            break;
            
            //quickbooks
            case 3:

                $this->_quickbooks = Quickbooks::getInstance();
                $dataService = $this->_quickbooks->getDataService();
                
                $serviceContext = $dataService->getServiceContext();
                $reportService = new ReportService($serviceContext);
        
                $time = new DateTime($this->_fromDate);
                $monthEnd = $time->format('Y-m-t');

                $reportService->setStartDate($monthEnd);
                $reportService->setEndDate($monthEnd);
        
                $this->_apiObject = $reportService->executeReport('BalanceSheet');

            break;
        }
    }
    
    public function setBalanceSheet(){
        //$this->logger->debug('BalanceSheet Class:: Setting Balance Sheet');

        $this->setApiObject($this->_fromDate);
        
        $coa = new DataChartOfAccounts(null);
        $this->_chartOfAccounts = $coa->getChartOfAccounts();
        
        $currentAssetsCount = 0;
        $nonCurrentAssetsCount = 0;
        $currentLiabilitiesCount = 0;
        $nonCurrentLiabilitiesCount = 0;
        $equityCount = 0;

        $titleArray = array();
        $nameArray = array();

        switch ($this->_apiId) {
            //xero
            case 1:
                //$this->logger->debug('BalanceSheet Class::updating XERO Company');

                $rows = $this->_apiObject->getReports()[0]->getRows();
                for($i = 0; $i < count($rows); $i++){

                    //$this->logger->debug('BalanceSheetClass::Processing Row ' . $i);

                    $innerRows = $rows[$i]->getRows();
                    foreach((array)$innerRows as $innerRow){

                        $title = $innerRow->getCells()[0]->getValue();

                        if(!in_array($title, $titleArray)){
                            array_push($titleArray, $title);
                        }

                        //$this->logger->debug('BalanceSheetClass::Processing innerRow "' . $title . '"');


                        for($k = 0; $k < 12; $k++){
                            $values[$k] = array('date' => date('Y-m-d', strtotime($this->_fromDate. ' - ' . $k . ' month')),
                             'amount' => $innerRow->getCells()[$k + 1]->getValue());
                             //$this->logger->debug('BalanceSheetClass::Getting Values | date -> ' . $values[$k]['date'] . ' amount ' . $values[$k]['amount']);
                        }

                        for($j = 0; $j < count($this->_chartOfAccounts); $j++){

                            if(isset($this->_chartOfAccounts[$j]['name']) && $this->_chartOfAccounts[$j]['name'] == $title){
                                //$this->logger->debug('BalanceSheetClass::Processing through Chart of Accounts ');
                                $type = $this->_chartOfAccounts[$j]['type'];
                                $itemData = array(
                                    'account' => $title,
                                    'data' => $values
                                );
                                if($type == 'Current Asset'){
                                    $this->_balanceSheet->addCurrentAsset($itemData); 
                                    $currentAssetsCount++;
                                }
                                else if($type == 'Non-current Asset'){
                                    $this->_balanceSheet->addNonCurrentAsset($itemData);
                                    $nonCurrentAssetsCount++;
                                }
                                else if($type == 'Current Liability'){
                                    $this->_balanceSheet->addCurrentLiability($itemData);
                                    $currentLiabilitiesCount++;
                                }
                                else if($type == 'Non-current Liability'){
                                    $this->_balanceSheet->addNonCurrentLiability($itemData);
                                    $nonCurrentLiabilitiesCount++;
                                }
                                else if($type == 'Equity'){
                                    $this->_balanceSheet->addEquity($itemData);
                                    $equityCount++;
                                }
                                else{
                                    // echo 'No type ' + $type . '<br>';
                                }
                            }
                        }
                    }
                }

                for($j = 0; $j < count($this->_chartOfAccounts); $j++){
                    if(!in_array($this->_chartOfAccounts[$j]['name'], $nameArray)){
                        array_push($nameArray, $this->_chartOfAccounts[$j]['name']);
                    }
                }

                foreach($nameArray as $name){
                    if(!in_array($name, $titleArray)){
                       // echo $name . ' was skipped in the update <br>';
                    }
                }

                //$this->logger->debug('BalanceSheetClass::SetBalanceSheet DONE ');

            break;
            //sage
            case 2:

                $accountAmounts = new DataAccountAmounts($this->_fromDate, 11);
                $this->_balanceSheet = $accountAmounts->getBalanceSheetFormat();
                
            break;
            //quickbooks
            case 3:

                //[0] => Assets [1] => Liabilities + Equity
                $totals = $this->_apiObject->Rows->Row;

                foreach($totals as $total){

                    //[0] => Curr Assets [1] => Fixed Assets
                    $sections = $total->Rows->Row;

                    foreach($sections as $section){

                        //[0] => Bank Accounts [1] => Accounts Receivable ...
                        $subsections = $section->Rows->Row;

                        foreach($subsections as $subsection){

                            if($subsection->type == 'Section'){

                                if(isset($subsection->group)){
                                    $rows = $subsection->Rows->Row;
        
                                    foreach($rows as $account){

                                        if($account->type == 'Data'){
                                            
                                            $name = $account->ColData[0]->value;
                                            $amount = $account->ColData[1]->value;

                                            $this->updateAccountAmount($name, substr($this->_fromDate, 0 , 7), $amount);
                                        }
                                        else if($account->type == 'Section'){
                                            $innerRows = $account->Rows->Row;

                                            foreach($innerRows as $innerRow){

                                                $name = $innerRow->ColData[0]->value;
                                                $amount = $innerRow->ColData[1]->value;
            
                                                $this->updateAccountAmount($name, substr($this->_fromDate, 0 , 7), $amount);
                                            }
                                        }
        
                                    }
                                }
                                else{
                                    $name = substr($subsection->Summary->ColData[0]->value, 6);
                                    $amount = $subsection->Summary->ColData[1]->value;
        
                                    $this->updateAccountAmount($name, substr($this->_fromDate, 0 , 7), $amount);

                                }
                            }
                            else if($subsection->type == 'Data'){
                                $name = $subsection->ColData[0]->value;
                                $amount = $subsection->ColData[1]->value;

                                if($name != 'Net Income'){
                                    $this->updateAccountAmount($name, substr($this->_fromDate, 0 , 7), $amount);
                                }
                            }
                        }
                    }
                }

            break;
        }
    }
    

    private function updateAccountAmount($name, $month, $amount){
        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        $explodePeriod = explode('.', $name, 3);
        if($explodePeriod[2] != null || $explodePeriod[2] != ''){
            if(is_numeric($explodePeriod[0]) && is_numeric($explodePeriod[1])){
                $explodeSpace = explode(' ', $explodePeriod[2], 2);
                $name = $explodeSpace[1];
            }
        }

        $accountId = $this->getAccountId($name);
        if($accountId){
            if($amount == null || $amount == ''){
                $amount = 0;
            }
    
            $getAmount = $this->_db->get('temp_account_amounts', array(
                'account_id' => array('operator' => '=', 'value' => $accountId),
                'date' => array('operator' => '=', 'value' => $month)
            ));
    
            if($getAmount->count()){
                $amountId = $getAmount->first()->account_amount_id;
    
                $updateAmount = $this->_db->update('temp_account_amounts', 'account_amount_id', $amountId, array(
                    'amount' => $amount,
                    'last_updated' => $now
                ));
                
                if($updateAmount){
                    return true;
                }
            }
            else{
                $insertAmount = $this->_db->insert('temp_account_amounts', array(
                    'account_id' => $accountId,
                    'date' => $month,
                    'amount' => $amount,
                    'last_updated' => $now
                ));
                if($insertAmount){
                    return true;
                }
            }
        }
        else{
            //$this->logger->error('Could not find ID for account: ' . $name);
        }

        return false;

    }

    private function getAccountId($name){
        $getAccount = $this->_db->get('temp_accounts', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
            'account_name' => array('operator' => '=', 'value' => $name)
        ));
        if($getAccount->count()){
            return $getAccount->first()->account_id;
        }
        return false;
    }



    public function updateDb($merge = true){
        $this->setBalanceSheet();

        if($this->_apiId != 3){

            $bs = new BalanceSheetModel();
            $balanceSheet = $this->_balanceSheet;
            if($merge){
                $this->_balanceSheet = new BalanceSheetModel();
                $dbBalancesheet = $this->getBalanceSheet();
                $merger = new MergeBalanceSheetModels($dbBalancesheet, $balanceSheet);
                $merger->merge();
                $mergedSheet = $merger->get();
                $balanceSheet = $mergedSheet;
            }

            $bs = $balanceSheet;
            

            //$this->logger->debug('BalanceSheetClass::updateDB  ');
            //$this->logger->debug('BalanceSheetClass::updateDB  updating CA');
            $this->processBatch($bs->getCurrentAssets());
            //$this->logger->debug('CA - DONE');
            //$this->logger->debug('BalanceSheetClass::updateDB  updating NCA');

            $this->processBatch($bs->getNonCurrentAssets());
            //$this->logger->debug('NCA - DONE');
            //$this->logger->debug('BalanceSheetClass::updateDB  updating CL');

            $this->processBatch($bs->getCurrentLiabilities());
            //$this->logger->debug('CL - DONE');
            //$this->logger->debug('BalanceSheetClass::updateDB  updating NCL');

            $this->processBatch($bs->getNonCurrentLiabilities());
            //$this->logger->debug('NCL - DONE');
            //$this->logger->debug('BalanceSheetClass::updateDB  updating EQ');

            $this->processBatch($bs->getEquity());
            //$this->logger->debug('EQ - DONE');
            //$this->logger->debug('Update - DONE');

            return true;
        }
    }

    private function processBatch($items = array()){
        foreach($items as $item){
            $account = $item['account'];
            //$this->logger->debug('Processing Account: ' . $account);
            $section = $this->mergeUnique($item['data']);

            if(!$this->processSection($account, $section)){
                //$this->logger->debug($account . 'does not exist in the database! Cannot get the account amounts!');
            };
        }
    }

    private function mergeUnique($array){

        $data = array();
        foreach($array as $item){
            if(array_key_exists('date', $item)){
                if(!$this->isDateInArray($item, $data) && $item['date'] != null){
                    $item['origin'] = 'db';
                    array_push($data, $item);
                }
            }else{
                foreach($item as $array){
                    if(!$this->isDateInArray($array, $data) && $array['date'] != null){
                        $array['origin'] = 'api';
                        array_push($data, $array);
                    }else if($array['date'] != null){
                        $array['origin'] = 'api';
                        foreach($data as $key=>$item){
                            if($item['date'] == $array['date'] && $item['amount'] != $array['amount']){
                                $data[$key] = $array;
                            }
                        }
                    }
                   
                }
            }
        }

        return $data;
    }


    private function isDateInArray($entity, $array){
        foreach($array as $item){
            if($item['date'] == $entity['date']){
                return true;
            }
        }

        return false;
    }




    private function updateAccountAmounts($id, $amount, $time){
        return $this->_db->update('temp_account_amounts', 'account_amount_id', $id, array(
            'amount' => $amount,
            'last_updated' => $time
        ));
    }

    private function insertAccountAmounts($id, $amount, $date ,$time){
        return $this->_db->insert('temp_account_amounts', array(
            'account_id' => $id,
            'date' => $date,
            'amount' => $amount,
            'last_updated' => $time
        ));
    }

    private function processSection($account, $sections){

        if(!$accountData = $this->getAccount($account)){
            return false;
        }

        //$this->logger->error('Processing Sections for '. $account);


        $id = $accountData->account_id;

        
        if(!$amountData = $this->getAccountAmounts($id)){
            foreach($sections as $section){
                if(!$this->insertAccountAmounts($id, $section['amount'], substr($section['date'],0 ,7) ,$this->_now )){
                    //$this->logger->error('Insert failed for account id: ' . $id . ' No amounts for given account');
                }
            }
        }else{
            foreach($sections as $section){

                $matched = false;
                foreach($amountData as $result){
                    if(substr($section['date'], 0, 7) == $result->date){
                        $matched = true;
                        $accountAmountId = $result->account_amount_id;
                        if(!$this->updateAccountAmounts($accountAmountId,$section['amount'],$this->_now)){
                            //$this->logger->error('Insert failed for account_amount id: ' . $accountAmountId);
                        }
    
                        break;
                    }
                }
    
                //no matches where made 
                if(!$matched){
                    if(!$this->insertAccountAmounts($id,  $section['amount'], substr($section['date'], 0, 7), $this->_now)){
                        //$this->logger->error('Insert failed for account id: ' . $id . ' No matches found for dates');
                    }
                }
            }
        }
       return true; 
    }

    private function getAccount($account){
        $data = $this->_db->get('temp_accounts', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
            'account_name' => array('operator' => '=', 'value' => $account)
        ));

        if($data->count()){
            return $data->first();
        }

        return false;
    }

    private function getAccountAmounts($id){
        $data = $this->_db->get('temp_account_amounts', array(
            'account_id', '=', $id
        ));

        if($data->count()){
            return $data->results();
        }

        return false;
    }

    public function getBalanceSheet(){
        //$this->logger->debug('BalanceSheetClass::getBalanceSheet Start ');

        $currentAssetsCount = 0;
        $nonCurrentAssetsCount = 0;
        $currentLiabilitiesCount = 0;
        $nonCurrentLiabilitiesCount = 0;
        $equityCount = 0;
        $updateDateCount = 0;
        $updates = array();

        $accounts = $this->_db->get('temp_accounts', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
            'type_id' => array('operator' => '>', 'value' => 5)
        ));
        if($accounts->results()){
            $accountsArr = $accounts->results();
            for($i = 0; $i < count($accountsArr); $i++){
                $type = $accountsArr[$i]->type_id;

                if($type > 5 && $type < 11){
                    $id = $accountsArr[$i]->account_id;
                    $name = $accountsArr[$i]->account_name;
                    
                    $amounts = $this->_db->get('temp_account_amounts', array(
                        'account_id' => array('operator' => '=', 'value' => $id),
                        'date' => array('operator' => '=', 'value' => substr($this->_fromDate, 0, 7))
                    ));

                    
                    if($amounts->results()){
    
                        $values = array(
                            'date' => $this->_fromDate,
                            'amount' => $amounts->first()->amount,
                        );

                        
                        $lastUpdate = $amounts->first()->last_updated;
    
                        $this->_balanceSheet->addLastUpdated($lastUpdate);
                        $updateDateCount++;
                        $fields = array(
                            'account' => $name,
                            'data' => $values
                        );

                        switch ($type) {
                            case 6:
                                $this->_balanceSheet->addCurrentAsset($fields); 
                                $currentAssetsCount++;
                            break;
                            case 7:
                                $this->_balanceSheet->addNonCurrentAsset($fields); 
                                $nonCurrentAssetsCount++;
                            break;
                            case 8:
                                $this->_balanceSheet->addNonCurrentLiability($fields); 
                                $nonCurrentLiabilitiesCount++;
                            break;
                            case 9:
                                $this->_balanceSheet->addCurrentLiability($fields); 
                                $currentLiabilitiesCount++;
                            break;
                            case 10:
                                $this->_balanceSheet->addEquity($fields);
                                $equityCount++;
                            break;
                        }
                    }
                }
            }
        }
        
        $totalAssets = $this->_balanceSheet->getTotalAssets();
        
        $totalLiabilities = $this->_balanceSheet->getTotalLiabilities();
        
        $totalEquity = $this->_balanceSheet->getTotalEquity();

        $profitAndLossYtd = $totalAssets - $totalLiabilities - $totalEquity;
        $this->_balanceSheet->addEquity(array(
            'account' => 'Profit & Loss (This year)',
            'data' => array(
                'date' => $this->_fromDate, 
                'amount' => $profitAndLossYtd
            )
        ));
        //$this->logger->debug('BalanceSheetClass::getBalanceSheet Done ');

        return $this->_balanceSheet;
    }
}
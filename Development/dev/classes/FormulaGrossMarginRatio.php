<?php

class FormulaGrossMarginRatio{
    private $_grossMarginRatio;

    public function __construct($profitAndLoss){
        $this->_profitAndLoss = $profitAndLoss;
    }

    private function setGrossMarginRatio(){
        $grossProfit = $this->_profitAndLoss['gross_profit']['amount'];
        $totalSales = $this->_profitAndLoss['total_sales']['amount'];

        if($totalSales != 0){
            $this->_grossMarginRatio['gross_margin_ratio'] = ($grossProfit / $totalSales) * 100;
        }
        else{
            $this->_grossMarginRatio['gross_margin_ratio'] = 0;
        }
        
        $oldestUpdate = $this->_profitAndLoss['last_updated'];
        $this->_grossMarginRatio['last_updated'] = $oldestUpdate;
    }

    public function getGrossMarginRatio(){
        $this->setGrossMarginRatio();
        return $this->_grossMarginRatio;
    }
}
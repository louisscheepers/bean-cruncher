<?php

class Xero{
    private static $_instance = null;
    private $_storage,
            $_provider,
            $_xeroTenantId = array(),
            $_newAccessToken,
            $_config,
            $_apiInstance;

    public function __construct(){
        $this->_storage = new ApiStorage();
        $this->_xeroTenantId = $this->_storage->getSession()['tenant_id'];
        
        $this->setProvider();
        $this->setConfig();
        $this->setApiInstance();
    }

    private function setProvider(){
        if ($this->_storage->getHasExpired()) {
            $this->_provider = new \League\OAuth2\Client\Provider\GenericProvider([
                'clientId'                =>  Constant::getXeroClientId(),   
                'clientSecret'            =>  Constant::getXeroSecret(),
                'redirectUri'             =>  Constant::getRedirectUri(), 
                'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
                'urlAccessToken'          => 'https://identity.xero.com/connect/token',
                'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
            ]);
            
            $this->_newAccessToken = $this->_provider->getAccessToken('refresh_token', [
              'refresh_token' => $this->_storage->getRefreshToken()
            ]);
            
            // Save my token, expiration and refresh token

            $oauth = new Oauth();
            $oauth->setDbOauth($this->_newAccessToken->getToken(), $this->_xeroTenantId, $this->_newAccessToken->getRefreshToken(), $this->_newAccessToken->getValues()["id_token"], $this->_newAccessToken->getExpires());

            $this->_storage->setToken(
                $this->_newAccessToken->getToken(),
                $this->_newAccessToken->getExpires(), 
                $this->_xeroTenantId,
                $this->_newAccessToken->getRefreshToken(),
                $this->_newAccessToken->getValues()["id_token"] );
          }
    }

    private function setConfig(){
        $this->_config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$this->_storage->getSession()['token'] );
        $this->_config->setHost("https://api.xero.com/api.xro/2.0");   
    }

    private function setApiInstance(){
        $this->_apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $this->_config
        );
    }

    public function getApiInstance(){
        return $this->_apiInstance;
    }
    public function getXeroTenantId(){
        return $this->_xeroTenantId;
    }

    public function getStorage(){
        return $this->_getStorage;
    }

    public static function getInstance(){
        if(!isset(self::$_instance)){
            self::$_instance = new Xero();
        }
        return self::$_instance;
    }
}
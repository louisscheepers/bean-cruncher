<?php

use QuickBooksOnline\API\ReportService\ReportService;

class DataProfitAndLoss{
    private $_profitAndLoss = array(
                'sales' => array(),
                'total_sales' => array('ytd' => 0, 'amount' => 0),
                'cost_of_sales' => array(),
                'total_cost_of_sales' => array('ytd' => 0, 'amount' => 0),
                'gross_profit' => array('ytd' => 0, 'amount' => 0),
                'other_income' => array(),
                'total_other_income' => array('ytd' => 0, 'amount' => 0),
                'expenses' => array(),
                'total_expenses' => array('ytd' => 0, 'amount' => 0),
                'net_profit_before_tax' => array('ytd' => 0, 'amount' => 0),
                'tax' => array(),
                'total_tax' => array('ytd' => 0, 'amount' => 0),
                'net_profit_after_tax' => array('ytd' => 0, 'amount' => 0),
                'last_updated' => array()
            ),
            $_ytd = array(),
            $_chartOfAccounts = array(),
            $_apiId,
            $_interval,
            $_apiObject,
            $_apiObjectYtd,
            $_fromDate,
            $_toDate,
            $_xero,
            $_quickbooks,
            $_instance,
            $_db;

    public function __construct($fromDate, $toDate, $interval){
        $this->_fromDate = $fromDate;
        $this->_toDate = $toDate;
        $this->_apiId = Companies::getApi();
        $this->_interval = $interval;
        $this->_db = DB::getInstance();
    }

    private function setApiObject($fromDate, $toDate){
        switch ($this->_apiId) {
            //xero
            case 1:
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();
                $this->_apiObject = $this->_instance->getReportProfitAndLoss(Companies::getActiveTenantId(), $fromDate, $toDate, $this->_interval, 'MONTH');
            break;

            //sage
            case 2:
                // sage uses DataAccountAmounts
            break;

            //quickbooks
            case 3:

                $this->_quickbooks = Quickbooks::getInstance();
                $dataService = $this->_quickbooks->getDataService();
                
                $serviceContext = $dataService->getServiceContext();

                $reportService = new ReportService($serviceContext);
                $reportService->setStartDate($this->_fromDate);
                $reportService->setEndDate($this->_toDate);
                
                $date = new DateTime($this->_fromDate);
                $date->modify('first day of -12 month');
                $ytdStart = $date->format('Y-m-d');

                $reportServiceYTD = new ReportService($serviceContext);
                $reportServiceYTD->setStartDate($ytdStart);
                $reportServiceYTD->setEndDate($this->_toDate);

                $this->_apiObject = $reportService->executeReport('ProfitAndLoss');
                $this->_apiObjectYtd = $reportServiceYTD->executeReport('ProfitAndLoss');

            break;
        }
        
    }
    
    private function setProfitAndLoss(){
        $this->setApiObject($this->_fromDate, $this->_toDate);
        
        $coa = new DataChartOfAccounts(null);
        $this->_chartOfAccounts = $coa->getChartOfAccounts();

        $salesCount = 0;
        $directCostCount = 0;
        $otherIncomeCount = 0;
        $expensesCount = 0;
        $taxCount = 0;

        switch ($this->_apiId) {
            //xero
            case 1:
                $rows = $this->_apiObject->getReports()[0]->getRows();
                for($i = 0; $i < count($rows); $i++){
                    $innerRows = $rows[$i]->getRows();
                    foreach((array)$innerRows as $innerRow){
                        $title = $innerRow->getCells()[0]->getValue();
        
                        for($k = 0; $k < 12; $k++){
                                $value = $innerRow->getCells()[$k + 1]->getValue();
                                $values[$k] = array('date' => date('Y-m-d', strtotime($this->_fromDate. ' - ' . $k . ' month')),
                                    'amount' => $value);
                        }
                        
                        if($title == 'Total Income'){
                            $this->_profitAndLoss['total_sales'][0] = array(
                                'account' => $title,
                                'amount' => $values
                            );
                        }
                        else if($title == 'Total Cost of Sales'){
                            $this->_profitAndLoss['total_cost_of_sales'][0] = array(
                                'account' => $title,
                                'amount' => $values
                            );
                        }
                        else if($title == 'Gross Profit'){
                            $this->_profitAndLoss['gross_profit'][0] = array(
                                'account' => $title,
                                'amount' => $values
                            );                    
                        }
                        else if($title == 'Total Other Income'){
                            $this->_profitAndLoss['total_other_income'][0] = array(
                                'account' => $title,
                                'amount' => $values
                            );                    
                        }
                        else if($title == 'Total Operating Expenses'){
                            $this->_profitAndLoss['total_expenses'][0] = array(
                                'account' => $title,
                                'amount' => $values
                            );                    
                        }
                        else if($title == 'Net Profit'){
                            $this->_profitAndLoss['net_profit_before_tax'][0] = array(
                                'account' => $title,
                                'amount' => $values
                            );                    
                        }
                        else{
                            for($j = 0; $j < count($this->_chartOfAccounts); $j++){
                                if(isset($this->_chartOfAccounts[$j]['name']) && $this->_chartOfAccounts[$j]['name'] == $title){
                                    $type = $this->_chartOfAccounts[$j]['type'];
                                    
                                    if($type == 'Sales'){
                                        $this->_profitAndLoss['sales'][$salesCount] = array(
                                            'account' => $title,
                                            'amount' => $values
                                        );
                                        $salesCount++;
                                    }
                                    else if ($type == 'Direct Costs'){
                                        $this->_profitAndLoss['cost_of_sales'][$directCostCount] = array(
                                            'account' => $title,
                                            'amount' => $values
                                        ); 
                                        $directCostCount++;
                                    }
                                    else if ($type == 'Other Income'){
                                        $this->_profitAndLoss['other_income'][$otherIncomeCount] = array(
                                            'account' => $title,
                                            'amount' => $values
                                        );
                                        $otherIncomeCount++;
                                    }
                                    else if ($type == 'Expense'){
                                        $this->_profitAndLoss['expenses'][$expensesCount] = array(
                                            'account' => $title,
                                            'amount' => $values
                                        );
                                        $expensesCount++;
                                    }
                                    else if ($type == 'Tax'){
                                        $this->_profitAndLoss['tax'][$taxCount] = array(
                                            'account' => $title,
                                            'amount' => $values
                                        );
                                        $taxCount++;
                                    }
                                }
                            }
                        }
                    }
                }

            break;
            //sage
            case 2:

                $accountAmounts = new DataAccountAmounts($this->_fromDate, 11);
                $this->_profitAndLoss = $accountAmounts->getProfitAndLossFormat();

            break;
            //qb
            case 3:
                
                // this must be re-written when i have time

                $types = $this->_apiObject->Rows->Row;
                foreach($types as $type){
                    $isSummary = false;

                    $sumName = $type->Summary->ColData[0]->value;
                    switch ($sumName) {
                        case 'Total Income':
                            $sumName = 'Total Income';
                        break;
                        case 'Total Cost of Goods Sold':
                            $sumName = 'Total Cost of Sales';
                        break;
                        case 'Gross Profit':
                            $sumName = 'Gross Profit';
                            $isSummary = true;
                        break;
                        case 'Total Expenses':
                            $sumName = 'Total Operating Expenses';
                        break;
                        case 'Net Operating Income':
                            $sumName = 'Net Profit';
                            $isSummary = true;
                        break;
                        case 'Total Other Expenses':
                            $sumName = 'Total Operating Expenses';
                        break;
                        case 'Net Other Income':
                            $sumName = 'Total Operating Expenses'; // add to expenses
                            $isSummary = true;
                        break;
                        case 'Net Income':
                            $sumName = 'Net Profit'; // add to net profit
                            $isSummary = true;
                        break;
                    }

                    if(!$isSummary){
                        $rows = $type->Rows;

                        foreach($rows as $accounts){
                            
                            foreach($accounts as $account){

                                if($account->type == 'Data'){
                                    $name = $account->ColData[0]->value;

                                    $amount = $account->ColData[1]->value;
                                    // echo 'Account: ' . $name . ' <br>Amount: ' . $amount . '<br>';

                                }
                                if($account->type == 'Section'){
                                    $name = substr($account->Summary->ColData[0]->value, 6); // remove "Total " to match DB account (QB is weird)
                                    $amount = $account->Summary->ColData[1]->value;
                                    // echo 'Account: ' . $name . ' <br>Amount: ' . $amount . '<br>';
                                }
                                $this->updateAccountAmount($name, substr($this->_fromDate, 0 , 7), $amount);
                            }
                        }
                    }
                }

                // YTD
                $types = $this->_apiObjectYtd->Rows->Row;
                foreach($types as $type){
                    $isSummary = false;

                    $typeDesc = $type->group;
                    // echo '<br><b>Type: ' . $typeDesc . '</b><br>';

                    $sumName = $type->Summary->ColData[0]->value;
                    switch ($sumName) {
                        case 'Total Income':
                            $sumName = 'Total Income';
                        break;
                        case 'Total Cost of Goods Sold':
                            $sumName = 'Total Cost of Sales';
                        break;
                        case 'Gross Profit':
                            $sumName = 'Gross Profit';
                            $isSummary = true;
                        break;
                        case 'Total Expenses':
                            $sumName = 'Total Operating Expenses';
                        break;
                        case 'Net Operating Income':
                            $sumName = 'Net Profit';
                            $isSummary = true;
                        break;
                        case 'Total Other Expenses':
                            $sumName = 'Total Operating Expenses';
                        break;
                        case 'Net Other Income':
                            $sumName = 'Total Operating Expenses'; // add to expenses
                            $isSummary = true;
                        break;
                        case 'Net Income':
                            $sumName = 'Net Profit'; // add to net profit
                            $isSummary = true;
                        break;
                    }

                    if(!$isSummary){
                        $rows = $type->Rows;

                        foreach($rows as $accounts){
                            
                            foreach($accounts as $account){

                                if($account->type == 'Data'){
                                    $name = $account->ColData[0]->value;

                                    $amount = $account->ColData[1]->value;
                                    // echo 'Account: ' . $name . ' <br>Amount: ' . $amount . '<br>';

                                }
                                if($account->type == 'Section'){
                                    $name = substr($account->Summary->ColData[0]->value, 6); // remove "Total " to match DB account (QB is weird)
                                    $amount = $account->Summary->ColData[1]->value;
                                    // echo 'Account: ' . $name . ' <br>Amount: ' . $amount . '<br>';
                                }
                                $this->updateAccountAmountYTD($name, substr($this->_fromDate, 0 , 7), $amount);
                            }
                        }
                    }
                }
                
            break;
        }
    }

    private function updateAccountAmount($name, $month, $amount){

        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        // echo $month . ' - Name: ' . $name . ' Amount: ' . $amount. '<br>';

        $explodePeriod = explode('.', $name, 3);
        if($explodePeriod[2] != null || $explodePeriod[2] != ''){
            if(is_numeric($explodePeriod[0]) && is_numeric($explodePeriod[1])){
                $explodeSpace = explode(' ', $explodePeriod[2], 2);
                $name = $explodeSpace[1];
            }
        }
        
        $accountId = $this->getAccountId($name);

        $getAmount = $this->_db->get('temp_account_amounts', array(
            'account_id' => array('operator' => '=', 'value' => $accountId),
            'date' => array('operator' => '=', 'value' => $month)
        ));

        if($getAmount->count()){
            $amountId = $getAmount->first()->account_amount_id;
            $updateAmount = $this->_db->update('temp_account_amounts', 'account_amount_id', $amountId, array(
                'amount' => $amount,
                'last_updated' => $now
            ));
            
            if($updateAmount){
                return true;
            }
        }
        else{
            $insertAmount = $this->_db->insert('temp_account_amounts', array(
                'account_id' => $accountId,
                'date' => $month,
                'amount' => $amount,
                'last_updated' => $now
            ));
            if($insertAmount){
                return true;
            }
        }

        return false;

    }
    
    private function updateAccountAmountYTD($name, $month, $amount){

        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        // echo $month . ' - Name: ' . $name . ' Amount: ' . $amount. '<br>';

        $explodePeriod = explode('.', $name, 3);
        if($explodePeriod[2] != null || $explodePeriod[2] != ''){
            if(is_numeric($explodePeriod[0]) && is_numeric($explodePeriod[1])){
                $explodeSpace = explode(' ', $explodePeriod[2], 2);
                $name = $explodeSpace[1];
            }
        }
        
        $accountId = $this->getAccountId($name);

        $getAmount = $this->_db->get('temp_account_ytd', array(
            'account_id' => array('operator' => '=', 'value' => $accountId),
            'date' => array('operator' => '=', 'value' => $month)
        ));

        if($getAmount->count()){
            $ytdId = $getAmount->first()->account_ytd_id;
            $updateAmount = $this->_db->update('temp_account_ytd', 'account_ytd_id', $ytdId, array(
                'ytd' => $amount,
                'last_updated' => $now
            ));
            
            if($updateAmount){
                return true;
            }
        }
        else{
            $insertAmount = $this->_db->insert('temp_account_ytd', array(
                'account_id' => $accountId,
                'date' => $month,
                'ytd' => $amount,
                'last_updated' => $now
            ));
            if($insertAmount){
                return true;
            }
        }

        return false;

    }

    private function getAccountId($name){
        $getAccount = $this->_db->get('temp_accounts', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
            'account_name' => array('operator' => '=', 'value' => $name)
        ));
        if($getAccount->count()){
            return $getAccount->first()->account_id;
        }
        return false;
    }
    private function getAccountName($id){
        $getAccount = $this->_db->get('temp_accounts', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
            'account_id' => array('operator' => '=', 'value' => $id)
        ));
        if($getAccount->count()){
            return $getAccount->first()->account_name;
        }
        return false;
    }
    private function getAccountAmounts($month, $name){
        $accountId = $this->getAccountId($name);

        $getAmount = $this->_db->get('temp_account_amounts', array(
            'account_id' => array('operator' => '=', 'value' => $accountId),
            'date' => array('operator' => '=', 'value' => $month)
        ));

        if($getAmount->count()){
            return $getAmount->first()->account_amount_id;
        }

        return false;
    }


    


    public function updateDb(){
        require_once (dirname(__DIR__, 1) . '/vendor/autoload.php');
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');

        $this->setProfitAndLoss();

        if($this->_apiId != 3){
    
            $today = new DateTime();
            $now = $today->format('Y/m/d H:i');
    
            for($j = 0; $j <= 12; $j++){
                switch ($j) {
                    case 0:
                        $row = 'sales';
                    break;
                    case 1:
                        $row = 'total_sales';
                    break;
                    case 2:
                        $row = 'cost_of_sales';
                    break;
                    case 3:
                        $row = 'total_cost_of_sales';
                    break;
                    case 4:
                        $row = 'gross_profit';
                    break;
                    case 5:
                        $row = 'other_income';
                    break;
                    case 6:
                        $row = 'total_other_income';
                    break;
                    case 7:
                        $row = 'expenses';
                    break;
                    case 8:
                        $row = 'total_expenses';
                    break;
                    case 9:
                        $row = 'net_profit_before_tax';
                    break;
                    case 10:
                        $row = 'tax';
                    break;
                    case 11:
                        $row = 'total_tax';
                    break;
                    case 12:
                        $row = 'net_profit_after_tax';
                    break;
                }
    
                // echo '<br><b> Row: ' . $row . ' - Count: ' . count($this->_profitAndLoss[$row]) . '</b><br>';
    
                for($i = 0; $i < count($this->_profitAndLoss[$row]); $i++){
                    if(isset($this->_profitAndLoss[$row][$i]['account'])){
                        $accountName = $this->_profitAndLoss[$row][$i]['account'];
        
                        // var_dump($accountName);
                        $dbQuery = $this->_db->get('temp_accounts', array(
                            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
                            'account_name' => array('operator' => '=', 'value' => $accountName)
                        ));
                        if($dbQuery->count()){
                            $data = $dbQuery->results();
                            $accountId = $data[0]->account_id;
        
                            // echo $accountName . ' - ' . $accountId . '<br>';
        
                            $dbQueryId = $this->_db->get('temp_account_amounts', array(
                                'account_id', '=', $accountId
                            ));
                            if($dbQueryId->results()){
                                for($k = 0; $k < 12; $k++){
                                    // echo '<br>';
                                    for($r = 0; $r <= count($dbQueryId->results()); $r++){
                                        if($r != count($dbQueryId->results())){
                                            $date = $dbQueryId->results()[$r]->date;
                                            // echo 'Checking ' . $accountName . ' date ' . $date . '<br>';
                                            if($date == substr($this->_profitAndLoss[$row][$i]['amount'][$k]['date'], 0, 7)){
                                                // echo $accountName . ' matched with ' . $date . '<br>';
                                                $id = $dbQueryId->results()[$r]->account_amount_id;
                                                $dbUpdate = $this->_db->update('temp_account_amounts', 'account_amount_id', $id, array(
                                                    'amount' => $this->_profitAndLoss[$row][$i]['amount'][$k]['amount'],
                                                    'last_updated' => $now
                                                ));
                                                break 1;
                                            }
                                        }
                                        else{
                                            // echo $accountName . ' had no matching dates!<br>';
                                            $dbInsert = $this->_db->insert('temp_account_amounts', array(
                                                'account_id' => $accountId,
                                                'date' => substr($this->_fromDate, 0, 7),
                                                'amount' => $this->_profitAndLoss[$row][$i]['amount'][11]['amount'],
                                                'last_updated' => $now
                                            ));
                                        }
                                    }
                                }
                            }
                            else{
                                // echo 'Inserting ' . $accountId . '<br>';
                                for($k = 0; $k < 12; $k++){
                                    $dbInsert = $this->_db->insert('temp_account_amounts', array(
                                        'account_id' => $accountId,
                                        'date' => substr($this->_profitAndLoss[$row][$i]['amount'][$k]['date'], 0, 7),
                                        'amount' => $this->_profitAndLoss[$row][$i]['amount'][$k]['amount'],
                                        'last_updated' => $now
                                    ));
                                }
                            }
                        }
                        else{
                            // echo $accountName . ' not found in DB <br>';
                            //update chart of accounts DB
                            // $coa->updateDb();
                            //run again
                        }
                    }
                }
            }
        }
    }

    public function getProfitAndLoss(){

        $salesCount = 0;
        $directCostCount = 0;
        $otherIncomeCount = 0;
        $expensesCount = 0;
        $taxCount = 0;
        $updateDateCount = 0;
        $updates = array();
        $emptyYtd = array();

        // re-write this part ASAP!

        if(Companies::getApi() == 3){
            $accounts = $this->_db->get('temp_accounts', array(
                'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
            ));
            if($accounts->results()){
                $accountsArr = $accounts->results();
                for($i = 0; $i < count($accountsArr); $i++){
                    $type = $accountsArr[$i]->type_id;
    
                    if($type < 6 || $type > 10){
                        $id = $accountsArr[$i]->account_id;
                        if(Companies::getApi() == 3){
                            $ytdAmounts = $this->_db->get('temp_account_ytd', array(
                                'account_id' => array('operator' => '=', 'value' => $id),
                                'date' => array('operator' => '=', 'value' => substr($this->_fromDate, 0, 7))
                            ));
                            if($ytdAmounts->count()){
                                $yearToDate = $ytdAmounts->first()->ytd;
                                array_push($emptyYtd, [$this->getAccountName($id), $yearToDate]);
                            }
                        }
                    }
                }
            }
        }

                  
        
        if(Companies::getApi() == 3){
            $accounts = $this->_db->get('temp_accounts', array(
                'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
            ));
            if($accounts->results()){
                $accountsArr = $accounts->results();
                for($i = 0; $i < count($accountsArr); $i++){
                    $type = $accountsArr[$i]->type_id;
    
                    if($type < 6 || $type > 10){
                        $id = $accountsArr[$i]->account_id;
    
                        $amounts = $this->_db->get('temp_account_amounts', array(
                            'account_id' => array('operator' => '=', 'value' => $id),
                            'date' => array('operator' => '=', 'value' => substr($this->_fromDate, 0, 7))
                        ));
    
                        if($amounts->results()){
                            $name = $accountsArr[$i]->account_name;
                            for($j = 0; $j < count($emptyYtd); $j++){
                                if($emptyYtd[$j][0] == $name){
                                    unset($emptyYtd[$j]);
                                }
                            }
                        }
                    }
                }
            }
            $emptyYtd = array_values($emptyYtd);
        }

        //YTD for QB
        if(Companies::getApi() == 3){

            foreach($emptyYtd as $yearToDate){
                $name = $yearToDate[0];
                $ytd = $yearToDate[1];
                $accountId = $this->getAccountId($name);

                $accounts = $this->_db->get('temp_accounts', array(
                    'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
                    'account_id' =>array('operator' => '=', 'value' => $accountId)
                ));
                if($accounts->count()){
                    $type = $accounts->first()->type_id;

                    if($type < 6 || $type > 10){
                        switch ($type) {
                            case 1:
                                $this->_profitAndLoss['sales'][$salesCount] = array(
                                    'account' => $name,
                                    'amount' => 0,
                                    'ytd' => $ytd
                                );
                                $salesCount++;
                            break;
                            case 2:
                                $this->_profitAndLoss['cost_of_sales'][$directCostCount] = array(
                                    'account' => $name,
                                    'amount' => 0,
                                    'ytd' => $ytd
                                ); 
                                $directCostCount++;
                            break;
                            case 3:
                                $this->_profitAndLoss['other_income'][$otherIncomeCount] = array(
                                    'account' => $name,
                                    'amount' => 0,
                                    'ytd' => $ytd
                                );
                                $otherIncomeCount++;
                            break;
                            case 4:
                                $this->_profitAndLoss['expenses'][$expensesCount] = array(
                                    'account' => $name,
                                    'amount' => 0,
                                    'ytd' => $ytd
                                );
                                $expensesCount++;
                            break;
                            case 5:
                                $this->_profitAndLoss['tax'][$taxCount] = array(
                                    'account' => $name,
                                    'amount' => 0,
                                    'ytd' => $ytd
                                );
                                $taxCount++;
                            break;
                            case 11:
                                $this->_profitAndLoss['total_sales']['ytd'] = $ytd;
                            break;
                            case 12:
                                $this->_profitAndLoss['total_cost_of_sales']['ytd'] = $ytd;
                            break;
                            case 13:
                                $this->_profitAndLoss['gross_profit']['ytd'] = $ytd;
                            break;
                            case 14:
                                $this->_profitAndLoss['total_other_income']['ytd'] = $ytd;
                            break;
                            case 15:
                                $this->_profitAndLoss['total_expenses']['ytd'] = $ytd;
                            break;
                            case 16:
                                $this->_profitAndLoss['net_profit_before_tax']['ytd'] = $ytd;
                            break;
                            case 17:
                                $this->_profitAndLoss['total_tax']['ytd'] = $ytd;
                            break;
                            case 18:
                                $this->_profitAndLoss['net_profit_after_tax']['ytd'] = $ytd;
                            break;
                        }
                    }
                }
            }

            $accounts = $this->_db->get('temp_accounts', array(
                'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
            ));
            if($accounts->results()){
                $accountsArr = $accounts->results();
                for($i = 0; $i < count($accountsArr); $i++){
                    $type = $accountsArr[$i]->type_id;
    
                    if($type < 6 || $type > 10){
                        $id = $accountsArr[$i]->account_id;
    
                        $amounts = $this->_db->get('temp_account_amounts', array(
                            'account_id' => array('operator' => '=', 'value' => $id),
                            'date' => array('operator' => '=', 'value' => substr($this->_fromDate, 0, 7))
                        ));
    
                        $name = $accountsArr[$i]->account_name;
    
                        if($amounts->results()){
        
                            $value = $amounts->first()->amount;
                            $lastUpdate = $amounts->first()->last_updated;
                            $ytd = 0;
    
                            $dbYTD = $this->_db->get('temp_account_ytd', array(
                                'account_id' => array('operator' => '=', 'value' => $id),
                                'date' => array('operator' => '=', 'value' => substr($this->_toDate, 0, 7))
                            ));
                            if($dbYTD->results()){
                                $data = $dbYTD->results();
                                $ytd = $data[0]->ytd;
                            }
        
                            $updates[$updateDateCount] = $lastUpdate;
                            $updateDateCount++;
    
                            switch ($type) {
                                case 1:
                                    $this->_profitAndLoss['sales'][$salesCount] = array(
                                        'account' => $name,
                                        'amount' => $value,
                                        'ytd' => $ytd
                                    );
                                    $salesCount++;
                                break;
                                case 2:
                                    $this->_profitAndLoss['cost_of_sales'][$directCostCount] = array(
                                        'account' => $name,
                                        'amount' => $value,
                                        'ytd' => $ytd
                                    ); 
                                    $directCostCount++;
                                break;
                                case 3:
                                    $this->_profitAndLoss['other_income'][$otherIncomeCount] = array(
                                        'account' => $name,
                                        'amount' => $value,
                                        'ytd' => $ytd
                                    );
                                    $otherIncomeCount++;
                                break;
                                case 4:
                                    $this->_profitAndLoss['expenses'][$expensesCount] = array(
                                        'account' => $name,
                                        'amount' => $value,
                                        'ytd' => $ytd
                                    );
                                    $expensesCount++;
                                break;
                                case 5:
                                    $this->_profitAndLoss['tax'][$taxCount] = array(
                                        'account' => $name,
                                        'amount' => $value,
                                        'ytd' => $ytd
                                    );
                                    $taxCount++;
                                break;
                                case 11:
                                    $this->_profitAndLoss['total_sales']['ytd'] = $ytd;
                                break;
                                case 12:
                                    $this->_profitAndLoss['total_cost_of_sales']['ytd'] = $ytd;
                                break;
                                case 13:
                                    $this->_profitAndLoss['gross_profit']['ytd'] = $ytd;
                                break;
                                case 14:
                                    $this->_profitAndLoss['total_other_income']['ytd'] = $ytd;
                                break;
                                case 15:
                                    $this->_profitAndLoss['total_expenses']['ytd'] = $ytd;
                                break;
                                case 16:
                                    $this->_profitAndLoss['net_profit_before_tax']['ytd'] = $ytd;
                                break;
                                case 17:
                                    $this->_profitAndLoss['total_tax']['ytd'] = $ytd;
                                break;
                                case 18:
                                    $this->_profitAndLoss['net_profit_after_tax']['ytd'] = $ytd;
                                break;
                            }
                        }
                    }
                }
                $totalSales = 0;
                $totalSalesYtd = 0;
                for($i = 0; $i < count($this->_profitAndLoss['sales']); $i++){
                    $totalSales += $this->_profitAndLoss['sales'][$i]['amount'];
                    $totalSalesYtd += $this->_profitAndLoss['sales'][$i]['ytd'];
                }
    
                $totalCos = 0;
                $totalCosYtd = 0;
                for($i = 0; $i < count($this->_profitAndLoss['cost_of_sales']); $i++){
                    $totalCos += $this->_profitAndLoss['cost_of_sales'][$i]['amount'];
                    $totalCosYtd += $this->_profitAndLoss['cost_of_sales'][$i]['ytd'];
                }
    
                $grossProfit = $totalSales - $totalCos;
                $grossProfitYtd = $totalSalesYtd - $totalCosYtd;
    
                $totalOtherIncome = 0;
                $totalOtherIncomeYtd = 0;
                for($i = 0; $i < count($this->_profitAndLoss['other_income']); $i++){
                    $totalOtherIncome += $this->_profitAndLoss['other_income'][$i]['amount'];
                    $totalOtherIncomeYtd += $this->_profitAndLoss['other_income'][$i]['ytd'];
                }
    
                $totalExpenses = 0;
                $totalExpensesYtd = 0;
                for($i = 0; $i < count($this->_profitAndLoss['expenses']); $i++){
                    $totalExpenses += $this->_profitAndLoss['expenses'][$i]['amount'];
                    $totalExpensesYtd += $this->_profitAndLoss['expenses'][$i]['ytd'];
                }
    
                $netProfitBeforeTax = $grossProfit + $totalOtherIncome - $totalExpenses;
    
                $totalTax = 0;
                $totalTaxYtd = 0;
                for($i = 0; $i < count($this->_profitAndLoss['tax']); $i++){
                    $totalTax += $this->_profitAndLoss['tax'][$i]['amount'];
                    $totalTaxYtd += $this->_profitAndLoss['tax'][$i]['ytd'];
                }
    
                $netProfitAfterTax = $netProfitBeforeTax - $totalTax;
                $netProfitAfterTaxYtd = $grossProfitYtd - $totalExpensesYtd;
    
                $this->_profitAndLoss['total_sales']['amount'] = $totalSales;
                $this->_profitAndLoss['total_sales']['ytd'] = $totalSalesYtd;
                $this->_profitAndLoss['total_cost_of_sales']['amount'] = $totalCos;
                $this->_profitAndLoss['total_cost_of_sales']['ytd'] = $totalCosYtd;
                $this->_profitAndLoss['gross_profit']['amount'] = $grossProfit;
                $this->_profitAndLoss['gross_profit']['ytd'] = $grossProfitYtd;
                $this->_profitAndLoss['total_other_income']['amount'] = $totalOtherIncome;
                $this->_profitAndLoss['total_other_income']['ytd'] = $totalOtherIncomeYtd;
                $this->_profitAndLoss['total_expenses']['amount'] = $totalExpenses;
                $this->_profitAndLoss['total_expenses']['ytd'] = $totalExpensesYtd;
                $this->_profitAndLoss['net_profit_before_tax']['amount'] = $netProfitBeforeTax;
                $this->_profitAndLoss['net_profit_before_tax']['ytd'] = $netProfitAfterTaxYtd;
                $this->_profitAndLoss['total_tax']['amount'] = $totalTax;
                $this->_profitAndLoss['total_tax']['ytd'] = $totalTaxYtd;
                $this->_profitAndLoss['net_profit_after_tax']['amount'] = $netProfitAfterTax;
                $this->_profitAndLoss['net_profit_after_tax']['ytd'] = $netProfitAfterTaxYtd;
                if($updates != null){
                    $this->_profitAndLoss['last_updated'] = max($updates);
                }
    
                foreach($this->_profitAndLoss as $key=>$array){
                    if(is_array($array)){
                        if(array_key_exists(0, $array)){
                            $this->_profitAndLoss[$key] = $this->sortAlphabetically($array);
                        }
                    }
                }
    
                return $this->_profitAndLoss;
    
            }
        }

        $accounts = $this->_db->get('temp_accounts', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
        ));
        if($accounts->results()){
            $accountsArr = $accounts->results();
            for($i = 0; $i < count($accountsArr); $i++){
                $type = $accountsArr[$i]->type_id;

                if($type < 6 || $type > 10){
                    $id = $accountsArr[$i]->account_id;

                    $amounts = $this->_db->get('temp_account_amounts', array(
                        'account_id' => array('operator' => '=', 'value' => $id),
                        'date' => array('operator' => '=', 'value' => substr($this->_fromDate, 0, 7))
                    ));

                    $name = $accountsArr[$i]->account_name;

                    if($amounts->results()){
    
                        $value = $amounts->first()->amount;
                        $lastUpdate = $amounts->first()->last_updated;
                        $ytd = 0;

                        $dbYTD = $this->_db->get('temp_account_ytd', array(
                            'account_id' => array('operator' => '=', 'value' => $id),
                            'date' => array('operator' => '=', 'value' => substr($this->_toDate, 0, 7))
                        ));
                        if($dbYTD->results()){
                            $data = $dbYTD->results();
                            $ytd = $data[0]->ytd;
                        }
    
                        $updates[$updateDateCount] = $lastUpdate;
                        $updateDateCount++;

                        switch ($type) {
                            case 1:
                                $this->_profitAndLoss['sales'][$salesCount] = array(
                                    'account' => $name,
                                    'amount' => $value,
                                    'ytd' => $ytd
                                );
                                $salesCount++;
                            break;
                            case 2:
                                $this->_profitAndLoss['cost_of_sales'][$directCostCount] = array(
                                    'account' => $name,
                                    'amount' => $value,
                                    'ytd' => $ytd
                                ); 
                                $directCostCount++;
                            break;
                            case 3:
                                $this->_profitAndLoss['other_income'][$otherIncomeCount] = array(
                                    'account' => $name,
                                    'amount' => $value,
                                    'ytd' => $ytd
                                );
                                $otherIncomeCount++;
                            break;
                            case 4:
                                $this->_profitAndLoss['expenses'][$expensesCount] = array(
                                    'account' => $name,
                                    'amount' => $value,
                                    'ytd' => $ytd
                                );
                                $expensesCount++;
                            break;
                            case 5:
                                $this->_profitAndLoss['tax'][$taxCount] = array(
                                    'account' => $name,
                                    'amount' => $value,
                                    'ytd' => $ytd
                                );
                                $taxCount++;
                            break;
                            case 11:
                                $this->_profitAndLoss['total_sales']['ytd'] = $ytd;
                            break;
                            case 12:
                                $this->_profitAndLoss['total_cost_of_sales']['ytd'] = $ytd;
                            break;
                            case 13:
                                $this->_profitAndLoss['gross_profit']['ytd'] = $ytd;
                            break;
                            case 14:
                                $this->_profitAndLoss['total_other_income']['ytd'] = $ytd;
                            break;
                            case 15:
                                $this->_profitAndLoss['total_expenses']['ytd'] = $ytd;
                            break;
                            case 16:
                                $this->_profitAndLoss['net_profit_before_tax']['ytd'] = $ytd;
                            break;
                            case 17:
                                $this->_profitAndLoss['total_tax']['ytd'] = $ytd;
                            break;
                            case 18:
                                $this->_profitAndLoss['net_profit_after_tax']['ytd'] = $ytd;
                            break;
                        }
                    }
                }
            }
            $totalSales = 0;
            $totalSalesYtd = 0;
            for($i = 0; $i < count($this->_profitAndLoss['sales']); $i++){
                $totalSales += $this->_profitAndLoss['sales'][$i]['amount'];
                $totalSalesYtd += $this->_profitAndLoss['sales'][$i]['ytd'];
            }

            $totalCos = 0;
            $totalCosYtd = 0;
            for($i = 0; $i < count($this->_profitAndLoss['cost_of_sales']); $i++){
                $totalCos += $this->_profitAndLoss['cost_of_sales'][$i]['amount'];
                $totalCosYtd += $this->_profitAndLoss['cost_of_sales'][$i]['ytd'];
            }

            $grossProfit = $totalSales - $totalCos;
            $grossProfitYtd = $totalSalesYtd - $totalCosYtd;

            $totalOtherIncome = 0;
            $totalOtherIncomeYtd = 0;
            for($i = 0; $i < count($this->_profitAndLoss['other_income']); $i++){
                $totalOtherIncome += $this->_profitAndLoss['other_income'][$i]['amount'];
                $totalOtherIncomeYtd += $this->_profitAndLoss['other_income'][$i]['ytd'];
            }

            $totalExpenses = 0;
            $totalExpensesYtd = 0;
            for($i = 0; $i < count($this->_profitAndLoss['expenses']); $i++){
                $totalExpenses += $this->_profitAndLoss['expenses'][$i]['amount'];
                $totalExpensesYtd += $this->_profitAndLoss['expenses'][$i]['ytd'];
            }

            $netProfitBeforeTax = $grossProfit + $totalOtherIncome - $totalExpenses;

            $totalTax = 0;
            $totalTaxYtd = 0;
            for($i = 0; $i < count($this->_profitAndLoss['tax']); $i++){
                $totalTax += $this->_profitAndLoss['tax'][$i]['amount'];
                $totalTaxYtd += $this->_profitAndLoss['tax'][$i]['ytd'];
            }

            $netProfitAfterTax = $netProfitBeforeTax - $totalTax;
            $netProfitAfterTaxYtd = $grossProfitYtd - $totalExpensesYtd;

            $this->_profitAndLoss['total_sales']['amount'] = $totalSales;
            $this->_profitAndLoss['total_sales']['ytd'] = $totalSalesYtd;
            $this->_profitAndLoss['total_cost_of_sales']['amount'] = $totalCos;
            $this->_profitAndLoss['total_cost_of_sales']['ytd'] = $totalCosYtd;
            $this->_profitAndLoss['gross_profit']['amount'] = $grossProfit;
            $this->_profitAndLoss['gross_profit']['ytd'] = $grossProfitYtd;
            $this->_profitAndLoss['total_other_income']['amount'] = $totalOtherIncome;
            $this->_profitAndLoss['total_other_income']['ytd'] = $totalOtherIncomeYtd;
            $this->_profitAndLoss['total_expenses']['amount'] = $totalExpenses;
            $this->_profitAndLoss['total_expenses']['ytd'] = $totalExpensesYtd;
            $this->_profitAndLoss['net_profit_before_tax']['amount'] = $netProfitBeforeTax;
            $this->_profitAndLoss['net_profit_before_tax']['ytd'] = $netProfitAfterTaxYtd;
            $this->_profitAndLoss['total_tax']['amount'] = $totalTax;
            $this->_profitAndLoss['total_tax']['ytd'] = $totalTaxYtd;
            $this->_profitAndLoss['net_profit_after_tax']['amount'] = $netProfitAfterTax;
            $this->_profitAndLoss['net_profit_after_tax']['ytd'] = $netProfitAfterTaxYtd;
            if($updates != null){
                $this->_profitAndLoss['last_updated'] = max($updates);
            }

            foreach($this->_profitAndLoss as $key=>$array){
                if(is_array($array)){
                    if(array_key_exists(0, $array)){
                        $this->_profitAndLoss[$key] = $this->sortAlphabetically($array);
                    }
                }
            }

            return $this->_profitAndLoss;

        }
    }

    private function sortAlphabetically($array){
        usort($array, function ($elem1, $elem2) {
            return strcmp($elem1['account'], $elem2['account']);
       });
        return $array;
    }
}
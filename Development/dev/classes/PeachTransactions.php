<?php


class PeachTransactions
{
    private $_db,
            $_data;

    public function __construct(){
        $this->_db = DB::getInstance();
        $this->get();
    }

    public function create($fields){
        if(!$this->_db->insert('transactions', $fields)){
            throw new Exception('There was a problem creating a PeachPayments Customer.');
        }
    }

    private function get(){
        $data = $this->_db->get('transactions', array('id', '>', 0));

        if($data->count()){
            $this->_data = $data->results();
        }
    }

    public function data(){
        return $this->_data;
    }

    public static function generateID($userID, $type){
        return "BC" . date("ymd") . str_pad($userID, 6, '0', STR_PAD_LEFT) . $type . rand(1000, 9999);
    }
}
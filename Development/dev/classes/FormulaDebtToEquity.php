<?php

class FormulaDebtToEquity{
    private $_debtToEquity,
            $_fields;
    public function __construct($fields){
        $this->_fields = $fields;
    }

    private function setDebtToEquity(){
        $totalLiabilities = $this->_fields['total_liabilities'];
        $totalEquity = $this->_fields['total_equity'];

        // echo 'Total liabilities: ' . $totalLiabilities . '<br>';
        // echo 'Total equity: ' . $totalEquity . '<br>';

        if($totalEquity != 0){
            $this->_debtToEquity['debt_to_equity'] = $totalLiabilities / $totalEquity;
        }
        else{
            $this->_debtToEquity['debt_to_equity'] = 0;
        }

        // echo 'Debt to equity: ' . $this->_debtToEquity['debt_to_equity']  . '<br><br>';


        $oldestUpdate = $this->_fields['last_updated'];
        $this->_debtToEquity['last_updated'] = $oldestUpdate;
    }

    public function getDebtToEquity(){
        $this->setDebtToEquity();
        return $this->_debtToEquity;
    }
}
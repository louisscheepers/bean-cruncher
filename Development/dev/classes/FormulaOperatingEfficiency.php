<?php

// operating efficiency = net profit / sales

class FormulaOperatingEfficiency{
    private $_operatingEfficiency,
            $_netProfit,
            $_income;

    public function __construct($fromDate, $toDate){
        $this->_fromDate = $fromDate;
        $this->_toDate = $toDate;
        $this->setOperatingEfficiency();
    }

    private function setOperatingEfficiency(){
        $np = new DataNetProfit($this->_fromDate, $this->_toDate);
        $this->_netProfit = $np->getNetProfit();

        // TEMPORARY
        $in = new DataIncome($this->_fromDate, $this->_toDate);
        $this->_income = $in->getIncome();

        if($this->_income == 0){
            $this->_operatingEfficiency = 0;
        }
        else{
            $this->_operatingEfficiency = $this->_netProfit / $this->_income;   
        }
    }
    
    public function getOperatingEfficiency(){
        return $this->_operatingEfficiency;
    }
}
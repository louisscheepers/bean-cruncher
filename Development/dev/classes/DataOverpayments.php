<?php

class DataOverpayments{
    private $_overpayments = array(),
            $_apiOverpayments = array(),
            $_apiPages = array(),
            $_modifiedAfter,
            $_apiObject,
            $_xero,
            $_instance,
            $_api,
            $_db,
            $_overpaymentsCount;

    public function __construct($modifiedAfter){
        $this->_apiId = Companies::getApi();
        $this->_modifiedAfter = $modifiedAfter;
    }

    private function getOverpaymentCount(){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        //$logger->info('Getting all overpayments');

        $this->_xero = Xero::getInstance();
        $this->_instance = $this->_xero->getApiInstance();
        $this->_overpaymentsCount = count($this->_instance->getoverpayments(Companies::getActiveTenantId()));
        return $this->_overpaymentsCount;
    }

    private function setApiObject($page){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');

        switch ($this->_apiId) {
            // XERO 
            case 1:
                
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();
                
                if($this->_modifiedAfter == null){
                    //$logger->info('Getting page ' . $page);
                    $this->_apiObject = $this->_instance->getOverpayments(Companies::getActiveTenantId(), '', '', '', '', '', '', 'AUTHORISED', $page);
                    //$logger->info('Found ' . count($this->_apiObject->getOverpayments()) . ' overpayments');
                    
                    $pageOverpayments = $this->_apiObject->getOverpayments();
                    array_push($this->_apiPages, $pageOverpayments);
                }
                else{
                    $dateFormat = str_replace('-', ',', $this->_modifiedAfter);
                    $this->_apiObject = $this->_instance->getOverpayments(Companies::getActiveTenantId(), '', 'updateddateutc >= DateTime('.$dateFormat.')', '', '', '', '', 'AUTHORISED', $page);
                }
                
                
            break;
            
            // SAGE
            case 2:
                // Sage does not use overpayments
            break;
        }
    }

    public function setOverpayments(){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        
        switch ($this->_apiId) {
            //xero
            case 1:

                $this->getOverpaymentCount();
                //$logger->info('Overpayment count: ' . $this->_overpaymentsCount);
        
                if($this->_overpaymentsCount < 100){
                    $this->setApiObject(1);
                    $this->_apiOverpayments = $this->_apiObject->getOverpayments();
                    //$logger->info('One page only');
                }
                else{
                    $this->_pageCount = ceil(($this->_overpaymentsCount / 100));
                    //$logger->info('Pages: ' . $this->_pageCount);
        
                    for($i = 1; $i <= $this->_pageCount; $i++){
                        $this->setApiObject($i);
                    }
        
                    $opCount = 0;
                    for($i = 0; $i < count($this->_apiPages); $i++){
                        for($j = 0; $j < count($this->_apiPages[$i]); $j++){
                            $this->_apiOverpayments[$opCount] = $this->_apiPages[$i][$j];
                            $opCount++;
                        }
                    }
                }
        
                array_values($this->_apiOverpayments);
        
                for($i = 0; $i < count($this->_apiOverpayments); $i++){
                    $amount = $this->_apiOverpayments[$i]->getRemainingCredit();
                    $status = $this->_apiOverpayments[$i]->getStatus();
                    if($amount != 0 && $status != 'VOIDED'){
                        $type = $this->_apiOverpayments[$i]->getType();

                        // //$logger->info("Inv Type " . $type);
                        if($type == 'RECEIVE-OVERPAYMENT'){
                            $this->_overpayments[$i]['type'] = 1;
                        }
                        else{
                            $this->_overpayments[$i]['type'] = 2;
                        }

                        var_dump($this->_apiOverpayments[$i]);

                        $date = date('Y-m-d', (substr($this->_apiOverpayments[$i]->getUpdatedDateUtc(), 6, 13) / 1000));
            
                        $this->_overpayments[$i]['api_id'] = $this->_apiOverpayments[$i]->getOverpaymentId();
                        $this->_overpayments[$i]['amount_due'] = 0 - $this->_apiOverpayments[$i]->getRemainingCredit();
                        $this->_overpayments[$i]['date_created'] = $date;
                        $this->_overpayments[$i]['date_due'] = $date;
                        $this->_overpayments[$i]['contact'] = $this->_apiOverpayments[$i]->getContact()->getName();
                        $this->_overpayments[$i]['line_items'] = $this->_apiOverpayments[$i]->getLineItems();
                    }
                }
                
                $this->_overpayments = array_values($this->_overpayments);

            break;
            
            //sage
            case 2:
                //sage does not use overpayments
            break;
        }
    }

    public function getOverpayments(){
        $this->setOverpayments();
        return $this->_overpayments;
    }
}
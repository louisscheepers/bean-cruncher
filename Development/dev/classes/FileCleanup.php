<?php

class FileCleanup{
    public static function clean($files, $path, $expires){
        require_once (dirname(__DIR__, 1) . '/vendor/autoload.php');    
        //$logger = new Katzgrau\KLogger\Logger(dirname(__DIR__, 1) . '/logs');
        
        $removed = 0;
        $skipped = 0; 
        $successRemoval = 0;
        $failedRemoval = 0;
        
        $total = 0;
        //$logger->info("Starting file cleanup script in " . $path);
        $now = time();
        foreach ($files as $file) {

            $total++;
            //$logger->info("Checking file " . $file->getFilename());
            
            var_dump($file);

            // //$logger->info("" . $file->getFilename() . ": " . $file->getCTime());
            // //$logger->info("" . $file->getFilename() . ": " . filectime($file->getFilename()));
            // //$logger->info("" . $file->getFilename() . ": " . filemtime($file->getFilename()));
            if ($now - $file->getCTime() >= 60 * 60 * 24 * $expires){
                //$logger->info("" . $file->getFilename() . " has expired. Removing File");
                unlink('tmp/pdf/' . $file->getFilename());

                //$logger->info("Checking if " . $file->getFilename() . " has been removed");
                $filename = dirname(__DIR__, 1) . $path  . "/" . $file->getFilename();

              
                clearstatcache();
                if (file_exists($filename)){
                    //$logger->info("" . $file->getFilename() . " removal failed");
                    $failedRemoval++;
                }else{
                    //$logger->info("" . $file->getFilename() . " has been removed succesfully");
                    $successRemoval++;
                }

                $removed++;

            }else{
                //$logger->info("" . $file->getFilename() . "has not expired. Skipping file");
                $skipped++;
            }
                
        }

        //$logger->info("Cleanup Script finished | " . $total .  " Total Files " . $skipped . " Skipped " . $removed . " to Remove " . $successRemoval . " Passed " . $failedRemoval .  " Failed");
    }
}
<?php


class PeachCustomer
{
    private $_db,
            $_data,
            $_sessionName;

        public function __construct($user = null){
            $this->_db = DB::getInstance();
            $this->_sessionName = Config::get('session/session_name');

            if(!$user){
                if(Session::exists($this->_sessionName)){
                    $user = Session::get($this->_sessionName);
                    $this->find($user);
                }
            }else{
                $this->find($user);
            }
        }

        public function find($user){
            if($user){
                $data = $this->_db->get('customers', array('user_id', '=', $user));

                if($data->count()){
                    $this->_data = $data->first();
                    return true;
                }
            }

            return false;
        }

        public function create($fields){
            if(!$this->_db->insert('customers', $fields)){
                throw new Exception('There was a problem creating a PeachPayments Customer.');
            }
        }


        public function update($id, $fields){
            if(!$this->_db->update('customers', 'id', $id, $fields)){
                throw new Exception('There was a problem updating the Peach Customer');
            }
        }

        public function data(){
            return $this->_data;
        }

    public function exists(){
        return (!empty($this->_data)) ? true : false;
    }
}
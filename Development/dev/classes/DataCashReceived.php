<?php

use QuickBooksOnline\API\ReportService\ReportService;

class DataCashReceived{
    private $_cashReceived,
            $_apiObject,
            $_apiId,
            $_month,
            $_xero,
            $_quickbooks,
            $_instance,
            $_db,
            $_transactionList;

    public function __construct($month){
        $this->_month = $month;
        $this->_apiId = Companies::getApi();
        $this->_db = DB::getInstance();
    }

    private function setApiObject($month){

        switch ($this->_apiId) {
            //xero
            case 1:
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();
                $this->_apiObject = $this->_instance->getReportExecutiveSummary(Companies::getActiveTenantId(), $month);
            break;

            //sage
            case 2:
                // sage uses DataBankTansactions
            break;

            //quickbooks
            case 3:

                //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
                //$logger->info('Getting transaction list for cash received - ' . $month);

                $dateObj = new DateTime($month);
                $toDate = $dateObj->format('Y-m-t');

                //$logger->info('Getting QB instance');
                $this->_quickbooks = Quickbooks::getInstance();
                $dataService = $this->_quickbooks->getDataService();
                
                //$logger->info('Getting service object');
                $serviceContext = $dataService->getServiceContext();
                $reportService = new ReportService($serviceContext);
        
                //$logger->info('Setting dates');
                $reportService->setStartDate($month);
                $reportService->setEndDate($toDate);
        
                //$logger->info('Executing transaction list report');
                $this->_transactionList = $reportService->executeReport('TransactionList');

            break;
        }
    }

    private function setCashReceived(){
        $this->setApiObject($this->_month);

        switch ($this->_apiId) {
            //xero
            case 1:
                $rows = $this->_apiObject->getReports()[0]->getRows();
                for($i = 0; $i < count($rows); $i++){
                    $innerRows = $rows[$i]->getRows();
                    foreach((array)$innerRows as $innerRow){
                        $title = $innerRow->getCells()[0]->getValue();
                        $value = $innerRow->getCells()[1]->getValue();
                        if($title == 'Cash received'){
                            if($value == null){
                                $this->_cashReceived = 0;
                            }
                            else{
                                $this->_cashReceived = $value;
                            }
                        }
                    }
                }
            break;

            //sage 
            case 2:
                $transactions = new DataBankTransactions(substr($this->_month, 0, 7));
                $this->_cashReceived = $transactions->getCashReceived();
            break;

            //quickbooks
            case 3:

                //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
                //$logger->info('Setting cash received rows');
                
                $in = 0;

                // $columns = $this->_transactionList->Columns->Column;
                // foreach($columns as $column){
                //     var_dump($column);
                // }
                // echo 'Rows<br>';
                // $rows = $this->_transactionList->Rows->Row;
                // foreach($rows as $row){
                //     var_dump($row);
                // }
                // die();
                
                foreach($this->_transactionList->Rows->Row as $line){
                    $amount = $line->ColData[8]->value;
                    $type = $line->ColData[1]->value;
                    
                    if($amount > 0 && $type == 'Deposit'){
                        $in += $amount;
                    }
                }

                $this->_cashReceived = $in;

            break;
        }
    }
    
    public function updateDb(){
        $this->setCashReceived();

        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        //$logger->info('Updating DB');

        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        $dbQuery = $this->_db->get('temp_cash_received', array(
            'date' => array('operator' => '=', 'value' => substr($this->_month, 0, 7)),
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
        ));
        if($dbQuery->count()){
            // echo 'Found in DB, updating ...<br>';
            $data = $dbQuery->results();
            $id = $data[0]->cash_received_id;

            $dbUpdate = $this->_db->update('temp_cash_received', 'cash_received_id', $id, array(
                'amount' => $this->_cashReceived,
                'last_updated' => $now
            ));
            if($dbUpdate){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            // echo 'Not found, inserting...<br>';
            $dbInsert = $this->_db->insert('temp_cash_received', array(
                'company_id' => Companies::getActiveCompanyId(),
                'integration_id' => 2,
                'date' => substr($this->_month, 0, 7),
                'amount' => $this->_cashReceived,
                'last_updated' => $now
            ));
            if($dbInsert){
                return true;
            }
            else{
                return false;
            }
        }
    }
    
    public function getCashReceived(){
        // $this->setCashReceived();
        // return $this->_cashReceived;


        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        //$logger->info('Getting cash received for ' . $this->_month);

        $dbQuery = $this->_db->get('temp_cash_received', array(
            'date' => array('operator' => '=', 'value' => substr($this->_month, 0, 7)),
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
        ));
        if($dbQuery->count()){
            //$logger->info('Found');
            $data = $dbQuery->results();
            $this->_response[0] = $data[0]->amount;
            $this->_response[1] = $data[0]->last_updated;
            return $this->_response;
        }
        else{
            //$logger->info('Not found, updating...');
            $this->updateDb();
            $today = new DateTime();
            $now = $today->format('Y/m/d H:i');
            //$logger->info('Response after update: ' . $this->_cashReceived);
            $this->_response[0] = $this->_cashReceived;
            $this->_response[1] = $now;
            return $this->_response;
        }
    }
}
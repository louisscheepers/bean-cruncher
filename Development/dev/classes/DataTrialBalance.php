<?php

class DataTrialBalance{
    private $_apiId,
            $_fromDate,
            $_toDate,
            $_db;

    public function __construct($fromDate, $toDate){
        $this->_fromDate = $fromDate;
        $this->_toDate = $toDate;
        $this->_apiId = Companies::getApi();
        $this->_db = DB::getInstance();
    }

    private function getApiData(){

        $this->_quickbooks = Quickbooks::getInstance();
        $dataService = $this->_quickbooks->getDataService();
        
        $serviceContext = $dataService->getServiceContext();
        $reportService = new ReportService($serviceContext);

        $reportService->setStartDate($this->_fromDate);
        $reportService->setEndDate($this->_toDate);

        $trialBalance = $reportService->executeReport('TrialBalance');

        $columnData = array();

        foreach($trialBalance->Rows->Row as $colData){
            array_push($columnData, $colData->ColData);
        }

        return $columnData;
    }   
    private function getDbData(){
        //select all from company id
    }
    public function updateDb(){
        $apiData = $this->getApiData();
        $dbData = $this->getDbData();
        $month = substr($this->_fromDate, 0, 7);

        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        foreach($apiData as $apiAccount){

            $name = $apiAccount[0]->value;
            if($this->_apiId == 3){
                $nameArr = explode(':', $name);
                $name = $nameArr[0];
            }

            $amount = $apiAccount[1]->value;
            if($amount == ''){
                $amount = $apiAccount[2]->value;
            }
            // echo 'Name: ' . $name . '<br>';
            // echo 'Amount: ' . $amount . '<br>';

            //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
            //$logger->info('Account: ' . $name);
            //$logger->info('Amount: ' . '.'.$amount.'.');
            
            $amountId = $this->getAccountAmounts($month, $name);

            if($amount != '' || $amount != null || !is_int($amount)){
                if($name != ''){
                    if(!$amountId){
                        // echo 'Not found, inserting...<br>';
                        $accountId = $this->getAccountId($name);
                        if($accountId){
                            $insertAmount = $this->_db->insert('temp_account_amounts', array(
                                'account_id' => $accountId,
                                'date' => $month,
                                'amount' => $amount,
                                'last_updated' => $now
                            ));
                        }
                    }
                    else{
                        // echo 'Found, updating...<br>';
                        $updateAmount = $this->_db->update('temp_account_amounts', 'account_amount_id', $amountId, array(
                            'amount' => $amount,
                            'last_updated' => $now
                        ));
                    }
                }
            }
        }

        $deletePending = $this->_db->delete('temp_account_amounts', array(
            'amount', '=', ''
        ));

    }

    private function getAccountId($name){
        $getAccount = $this->_db->get('temp_accounts', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
            'account_name' => array('operator' => '=', 'value' => $name)
        ));
        if($getAccount->count()){
            return $getAccount->first()->account_id;
        }
        return false;
    }

    private function getAccountAmounts($month, $name){
        $accountId = $this->getAccountId($name);

        $getAmount = $this->_db->get('temp_account_amounts', array(
            'account_id' => array('operator' => '=', 'value' => $accountId),
            'date' => array('operator' => '=', 'value' => $month)
        ));

        if($getAmount->count()){
            return $getAmount->first()->account_amount_id;
        }

        return false;
    }
}
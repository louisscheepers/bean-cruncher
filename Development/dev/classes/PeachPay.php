<?php


class PeachPay
{
    private $_authToken,
        $_entityId,
        $_entityId3d,
        $_currency,
        $_checkoutUrl,
        $_paymentStatusUrl,
        $_returnUrl,
        $_paymentType,
        $_registrationId,
        $_checkoutId,
        $_checkoutResponse,
        $_payStatusResponse,
        $_repeatPaymentResponse,
        $_db,
        $_subscriptionData,
        $_transactHistory;

    public function __construct($currency, $type, $user = null)
    {
        $this->_db = DB::getInstance();
        $this->setAuthToken("OGFjN2E0Yzc3M2UyNjI3ODAxNzNlM2FlMjU4MjAzZmR8Z1p0ZDNrRU1Qcw=="); //move this to an enviromental variable file that is obscured from the public
        $this->setEntityId("8ac7a4ca73e26b7d0173e3ae2a9c0c23"); //move this to an enviromental variable file that is obscured from the public
        $this->setEntityId3d("8ac7a4c773e262780173e3ae416f0400");
        $this->setCurrency($currency);
        $this->setReturnUrl("payment.php");
        $this->setPaymentType($type);
        $this->setCheckoutUrl('https://test.oppwa.com/v1/checkouts');
    }

    public function checkout($amount,$id, $user)
    {
        //Prepare Checkout
        $this->setCheckoutResponse(json_decode($this->prepareCheckout($amount, $id, $user)));

//        //process result
//
//
//        //register the user in the database for payments
//        $this->regUser();
    }

    private function prepareCheckout($amount, $tranId, $user)
    {
        //do the peachpayments prepare checkout call

        $data = "entityId=" . $this->getEntityId3d() .
            "&amount=" . $amount .
            "&currency=" . $this->getCurrency() .
            "&paymentType=" . $this->getPaymentType() .
            "&recurringType=INITIAL" .
            "&createRegistration=true" .
            "&merchantTransactionId=" . $tranId .
            "&customer.givenName=" . $user->name .
            "&customer.surname=" . $user->surname;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getCheckoutUrl());
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer ' . $this->getAuthToken()));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return $responseData;
    }

    public function getPayStatus($id)
    {
        //set the checkout id
        $this->setCheckoutId($id);

        $this->setPaymentStatusUrl('https://test.oppwa.com/v1/checkouts/' . $this->getCheckoutId() . '/payment');
        $this->setPayStatusResponse(json_decode($this->checkPayStatus()));

        return $this->getPayStatusResponse();
    }

    private function checkPayStatus()
    {
        $url = "" . $this->getPaymentStatusUrl();
        $url .= "?entityId=" . $this->getEntityId3d();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer ' . $this->getAuthToken()));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return $responseData;
    }

    public function repeatPayment($id, $amount, $tranId){
        $this->setRegistrationId($id);
        $this->setRepeatPaymentResponse(json_decode($this->subscriptionPayment($id, $amount, $tranId)));
        return $this->getRepeatPaymentResponse();
    }

    private function subscriptionPayment($id, $amount,$tranId){
        $url = "https://test.oppwa.com/v1/registrations/" . $id . "/payments";
        $data = "entityId=" . $this->getEntityId() .
            "&amount=" . $amount .
            "&currency=" . $this->getCurrency() .
            "&paymentType=" . $this->getPaymentType() .
            "&recurringType=REPEATED" .
            "&merchantTransactionId=" . $tranId;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer ' . $this->getAuthToken()));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return $responseData;
    }

    public function isSuccesful($response){

    }
    /**
     * @return mixed
     */
    public function getEntityId3d()
    {
        return $this->_entityId3d;
    }

    /**
     * @param mixed $entityId3d
     */
    public function setEntityId3d($entityId3d)
    {
        $this->_entityId3d = $entityId3d;
    }

    /**
     * @return mixed
     */
    public function getCheckoutResponse()
    {
        return $this->_checkoutResponse;
    }

    /**
     * @param mixed $checkoutResponse
     */
    public function setCheckoutResponse($checkoutResponse)
    {
        $this->_checkoutResponse = $checkoutResponse;
    }

    /**
     * @return mixed
     */
    public function getCheckoutUrl()
    {
        return $this->_checkoutUrl;
    }

    /**
     * @param mixed $checkoutUrl
     */
    public function setCheckoutUrl($checkoutUrl)
    {
        $this->_checkoutUrl = $checkoutUrl;
    }

    /**
     * @return mixed
     */
    public function getPayStatusResponse()
    {
        return $this->_payStatusResponse;
    }

    /**
     * @param mixed $payStatusResponse
     */
    public function setPayStatusResponse($payStatusResponse)
    {
        $this->_payStatusResponse = $payStatusResponse;
    }

    /**
     * @return mixed
     */
    public function getPaymentStatusUrl()
    {
        return $this->_paymentStatusUrl;
    }

    /**
     * @param mixed $paymentStatusUrl
     */
    public function setPaymentStatusUrl($paymentStatusUrl)
    {
        $this->_paymentStatusUrl = $paymentStatusUrl;
    }

    /**
     * @return mixed
     */
    public function getRepeatPaymentResponse()
    {
        return $this->_repeatPaymentResponse;
    }

    /**
     * @param mixed $repeatPaymentResponse
     */
    public function setRepeatPaymentResponse($repeatPaymentResponse)
    {
        $this->_repeatPaymentResponse = $repeatPaymentResponse;
    }


    private function regUser()
    {

    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return $this->_authToken;
    }

    /**
     * @param string $authToken
     */
    public function setAuthToken($authToken)
    {
        $this->_authToken = $authToken;
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->_entityId;
    }

    /**
     * @param string $entityId
     */
    public function setEntityId($entityId)
    {
        $this->_entityId = $entityId;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->_currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->_currency = $currency;
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->_returnUrl;
    }

    /**
     * @param string $returnUrl
     */
    public function setReturnUrl($returnUrl)
    {
        $this->_returnUrl = $returnUrl;
    }

    /**
     * @return mixed|null
     */
    public function getPaymentType()
    {
        return $this->_paymentType;
    }

    /**
     * @param mixed|null $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->_paymentType = $paymentType;
    }

    /**
     * @return mixed
     */
    public function getRegistrationId()
    {
        return $this->_registrationId;
    }

    /**
     * @param mixed $registrationId
     */
    public function setRegistrationId($registrationId)
    {
        $this->_registrationId = $registrationId;
    }

    /**
     * @return mixed
     */
    public function getCheckoutId()
    {
        return $this->_checkoutId;
    }

    /**
     * @param mixed $checkoutId
     */
    public function setCheckoutId($checkoutId)
    {
        $this->_checkoutId = $checkoutId;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionData()
    {
        return $this->_subscriptionData;
    }

    /**
     * @param mixed $subscriptionData
     */
    public function setSubscriptionData($subscriptionData)
    {
        $this->_subscriptionData = $subscriptionData;
    }

    /**
     * @return mixed
     */
    public function getTransactHistory()
    {
        return $this->_transactHistory;
    }

    /**
     * @param mixed $transactHistory
     */
    public function setTransactHistory($transactHistory)
    {
        $this->_transactHistory = $transactHistory;
    }


}

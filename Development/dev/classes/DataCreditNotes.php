<?php

class DataCreditNotes{
    private $_creditNotes = array(),
            $_apiCreditNotes = array(),
            $_apiPages = array(),
            $_modifiedAfter,
            $_apiObject,
            $_xero,
            $_instance,
            $_api,
            $_db,
            $_creditNotesCount;

    public function __construct($modifiedAfter){
        $this->_apiId = Companies::getApi();
        $this->_modifiedAfter = $modifiedAfter;
    }

    // private function getCreditNoteCount(){
    //     //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
    //     //$logger->info('Getting all credit notes');

    //     $this->_xero = Xero::getInstance();
    //     $this->_instance = $this->_xero->getApiInstance();
    //     $this->_creditNotesCount = count($this->_instance->getCreditNotes(Companies::getActiveTenantId()));
    //     return $this->_creditNotesCount;
    // }

    private function setApiObject($page){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');

        switch ($this->_apiId) {
            // XERO 
            case 1:
                
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();
                
                if($this->_modifiedAfter == null){
                    //$logger->info('Getting page ' . $page);
                    $this->_apiObject = $this->_instance->getCreditNotes(Companies::getActiveTenantId(), '', '', '', '', '', '', 'AUTHORISED', $page);
                    //$logger->info('Found ' . count($this->_apiObject->getCreditNotes()) . ' credit notes');
                    
                    $pageOverpayments = $this->_apiObject->getCreditNotes();
                    array_push($this->_apiPages, $pageOverpayments);
                }
                else{
                    $dateFormat = str_replace('-', ',', $this->_modifiedAfter);
                    $this->_apiObject = $this->_instance->getCreditNotes(Companies::getActiveTenantId(), '', 'updateddateutc >= DateTime('.$dateFormat.')', '', '', '', '', 'AUTHORISED', $page);
                }
                
                
            break;
            
            // SAGE
            case 2:
                // Sage does not use credit notes
            break;
        }
    }

    public function setCreditNotes(){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        
        switch ($this->_apiId) {
            //xero
            case 1:

                // $this->getCreditNoteCount();
                // //$logger->info('Overpayment count: ' . $this->_creditNotesCount);
        
                // if($this->_creditNotesCount < 100){
                    $this->setApiObject(1);
                    $this->_apiCreditNotes = $this->_apiObject->getCreditNotes();
                    //$logger->info('One page only');

                // }
                // else{
                //     $this->_pageCount = ceil(($this->_creditNotesCount / 100));
                //     //$logger->info('Pages: ' . $this->_pageCount);
        
                //     for($i = 1; $i <= $this->_pageCount; $i++){
                //         $this->setApiObject($i);
                //     }
        
                //     $opCount = 0;
                //     for($i = 0; $i < count($this->_apiPages); $i++){
                //         for($j = 0; $j < count($this->_apiPages[$i]); $j++){
                //             $this->_apiCreditNotes[$opCount] = $this->_apiPages[$i][$j];
                //             $opCount++;
                //         }
                //     }
                // }
        
                array_values($this->_apiCreditNotes);
        
                for($i = 0; $i < count($this->_apiCreditNotes); $i++){
                    $amount = $this->_apiCreditNotes[$i]->getRemainingCredit();
                    $status = $this->_apiCreditNotes[$i]->getStatus();
                    if($amount != 0 && $status != 'VOIDED'){
                        $type = $this->_apiCreditNotes[$i]->getType();

                        // //$logger->info("Inv Type " . $type);
                        if($type == 'ACCRECCREDIT'){
                            $this->_creditNotes[$i]['type'] = 1;
                        }
                        else{
                            $this->_creditNotes[$i]['type'] = 2;
                        }

                        $date = date('Y-m-d', (substr($this->_apiCreditNotes[$i]->getUpdatedDateUtc(), 6, 13) / 1000));
                        $dateDue = date('Y-m-d', (substr($this->_apiCreditNotes[$i]->getDueDate(), 6, 13) / 1000));
            
                        $this->_creditNotes[$i]['api_id'] = $this->_apiCreditNotes[$i]->getCreditNoteId();
                        $this->_creditNotes[$i]['amount_due'] = 0 - $this->_apiCreditNotes[$i]->getRemainingCredit();
                        $this->_creditNotes[$i]['date_created'] = $date;
                        $this->_creditNotes[$i]['date_due'] = $dateDue;
                        $this->_creditNotes[$i]['contact'] = $this->_apiCreditNotes[$i]->getContact()->getName();
                        $this->_creditNotes[$i]['line_items'] = $this->_apiCreditNotes[$i]->getLineItems();
                    }
                }
        
                $this->_creditNotes = array_values($this->_creditNotes);

            break;
            
            //sage
            case 2:
                //sage does not use credit notes
            break;
        }
    }

    public function getCreditNotes(){
        $this->setCreditNotes();
        return $this->_creditNotes;
    }
}
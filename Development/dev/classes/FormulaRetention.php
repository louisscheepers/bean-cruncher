<?php

// retention rate = number of clients invoiced in both months / number of clients last month

class FormulaRetention{
    private $_invoices = array(),
            $_retention;

    public function __construct($fromDate){
        $this->_fromDate = $fromDate;
        $this->setRetention();
    }

    private function setRetention(){
        $lastMonthClients = array();
        $thisMonthClients = array();

        $thisMonth = new DateTime($this->_fromDate);
        $dt = new DateTime($this->_fromDate);
        $lastMonth = $dt->modify('first day of last month');

        $in = new DataInvoices(null);
        $this->_invoices = $in->getInvoices();

        // var_dump($this->_invoices);

        for($i = 0; $i < count($this->_invoices) - 1; $i++){
            $date = substr($this->_invoices[$i]['date_created'], 0, 7);
            // echo $date . '<br>';
            if($this->_invoices[$i]['type'] == 1){
                if($date == $lastMonth->format('Y-m')){
                    $lastMonthClients[$i] = $this->_invoices[$i]['contact'];
                }
                else if($date == $thisMonth->format('Y-m')){
                    $thisMonthClients[$i] = $this->_invoices[$i]['contact'];
                }
            }
        }

        $lastMonthClients = array_unique($lastMonthClients);
        $lastMonthClients = array_values($lastMonthClients);
        $thisMonthClients = array_unique($thisMonthClients);
        $thisMonthClients = array_values($thisMonthClients);

        $totalClientsBothMonths = count(array_intersect($lastMonthClients, $thisMonthClients));
        $totalClientsLastMonth = count($lastMonthClients);

        if($totalClientsLastMonth != 0){
            $this->_retention = $totalClientsBothMonths / $totalClientsLastMonth;
        }
        else{
            $this->_retention = 0;
        }
    }

    public function getRetention(){
        $ret;
        $last;
        if(isset($this->_retention)){
            $ret = $this->_retention;
        }
        else{
            $ret = '';
        }
        if(isset($this->_invoices['last_updated'])){
            $last = $this->_invoices['last_updated'];
        }
        else{
            $last = '';
        }
        $response = array(
            'retention' => $ret,
            'last_updated' => $last
        );
        return $response;
    }
}
<?php

class PullRange{
    private static $_data = array();


    public static function setData($action, $fromDate, $toDate, $isFlipped = false){

        if($isFlipped){
            DateLabels::setDateRange($toDate, $fromDate);
        }
        else{
            DateLabels::setDateRange($fromDate, $toDate);
        }
        $months = DateLabels::getMonthRange();
        $years = DateLabels::getYearRange();

        if($action){
            switch($action){

            //Cash In v Cash Out
                case 'cico':

                    $cr = array();
                    $cs = array();
                    $cashReceivedArr = array();
                    $cashSpentArr = array();
                    $updates = array();

                    for($i = 0; $i < count($months); $i++){
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));
                        
                        $cr = new DataCashReceived($fromDate);
                        $cs = new DataCashSpent($fromDate);

                        $cashReceived = $cr->getCashReceived();
                        $cashSpent = $cs->getCashSpent();

                        array_push($cashReceivedArr, $cashReceived[0]);
                        array_push($cashSpentArr, $cashSpent[0]);
                        if($cashReceived[1] != ''){
                            array_push($updates, $cashReceived[1]);
                        }
                        if($cashSpent[1] != ''){
                            array_push($updates, $cashSpent[1]);
                        }
                    }

                    
                    if($updates != null){
                        $oldestUpdate = min($updates);
                        if(count((array)$oldestUpdate) != 1 && count((array)$oldestUpdate) != 0){
                            $oldestUpdate = $oldestUpdate[0];
                        }
                    }
                    else{
                        $oldestUpdate = 'Never';
                    }

                    self::$_data = array(
                        'cash_in' => $cashReceivedArr,
                        'cash_out' => $cashSpentArr,
                        'last_updated' => $oldestUpdate
                    );

                break;
            
            //Income v Expenses
                case 'ino':

                    $plArr = array();
                    $incomeArr = array();
                    $expensesArr = array();
                    $netProfitArr = array();
                    $updates = array();

                    for($i = 0; $i < count($months); $i++){
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));
                        $toDate = date("Y-m-t", strtotime($fromDate));

                        $pl = new DataProfitAndLoss($fromDate, $toDate, null);
                        $plArr = $pl->getProfitAndLoss();

                        if(!empty($plArr)){
                            array_push($incomeArr, $plArr['total_sales']['amount'] + $plArr['total_other_income']['amount']);
                            array_push($expensesArr, ($plArr['total_expenses']['amount'] + $plArr['total_cost_of_sales']['amount']));
                            array_push($netProfitArr, $plArr['net_profit_before_tax']['amount']);

                            if($plArr['last_updated'] != ''){
                                array_push($updates, $plArr['last_updated']);
                            }
                        }
                    }

                    
                    if($updates != null){
                        $oldestUpdate = min($updates);
                        if(count((array)$oldestUpdate) != 1 && count((array)$oldestUpdate) != 0){
                            $oldestUpdate = $oldestUpdate[0];
                        }
                    }
                    else{
                        $oldestUpdate = 'Never';
                    }
                    
                    self::$_data = array(
                        'income' => $incomeArr,
                        'expenses' => $expensesArr,
                        'net_profit' => $netProfitArr,
                        'last_updated' => $oldestUpdate
                    );

                break;
                    
            //Asset v Debt
                case 'avd':
                    require_once (dirname(__DIR__, 1) . '/vendor/autoload.php');
                    //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');

                    $bsArr = array();

                    $bs = new DataBalanceSheet($fromDate, null);
                    $bsArr = $bs->getBalanceSheet();
                    
                    $oldestUpdate = $bsArr->getLastUpdated();
                    
                    self::$_data = array(
                        'assets' => $bsArr->getTotalAssets(),
                        'debt' => $bsArr->getTotalLiabilities(),
                        'last_updated' => $oldestUpdate
                    );

                break;
        
            //Profit & Loss
                case 'pl':

                    $plArr1 = array();
                    $plArr2 = array();
                    $plArrFull = array(
                        'sales' => array(),
                        'total_sales' => array(),
                        'cost_of_sales' => array(),
                        'total_cost_of_sales' => array(),
                        'gross_profit' => array(),
                        'other_income' => array(),
                        'total_other_income' => array(),
                        'expenses' => array(),
                        'total_expenses' => array(),
                        'net_profit_before_tax' => array(),
                        'tax' => array(),
                        'total_tax' => array(),
                        'net_profit_after_tax' => array()
                    );
                    $updates = array();

                    $from1 = strtotime($fromDate);
                    $fromDate1 = date('Y-m-d', $from1);

                    $to1 = strtotime($fromDate);
                    $toDate1 = date('Y-m-t', $to1);

                    $from2 = strtotime($toDate);
                    $fromDate2 = date('Y-m-d', $from2);

                    $to2 = strtotime($toDate);
                    $toDate2 = date('Y-m-t', $to2);

                    require_once (dirname(__DIR__, 1) . '/vendor/autoload.php');
                    //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
                    //$logger->info('toDate1 ' . $toDate1);
                    //$logger->info('toDate2 ' . $toDate2);
                    //$logger->info('Getting YTD value of ' . $toDate2);

                    if(Companies::getApi() == 1){
                    // if(Companies::getApi() == 1 || Companies::getApi() == 3){
                        $ytd = new DataYTD($toDate1, 11);
                        $ytd->setYTD();
                    }

                    $pl1 = new DataProfitAndLoss($fromDate1, $toDate1, null);
                    $pl2 = new DataProfitAndLoss($fromDate2, $toDate2, null);

                    $plArr1 = $pl1->getProfitAndLoss();
                    $plArr2 = $pl2->getProfitAndLoss();

                    $salesCount = 0;
                    $cosCount = 0;
                    $oIncomeCount = 0;
                    $expensesCount = 0;
                    $taxCount = 0;

                    // Sales
                    for($i = 0; $i < count((array)$plArr1['sales']); $i++){
                        if(in_array($plArr1['sales'][$i]['account'], array_column($plArr2['sales'], 'account'))){
                            $plArrFull['sales'][$salesCount]['account'] = $plArr1['sales'][$i]['account'];
                            $plArrFull['sales'][$salesCount]['amount'][0] = $plArr1['sales'][$i]['amount'];
                            $plArrFull['sales'][$salesCount]['amount'][1] = $plArr2['sales'][array_search($plArrFull['sales'][$salesCount]['account'], array_column($plArr2['sales'], 'account'))]['amount'];
                            $plArrFull['sales'][$salesCount]['ytd'] = $plArr1['sales'][$i]['ytd'];
                            $salesCount++;
                            // echo 'Sales Value ' . $plArr1['sales'][$i]['account'] . ' found in both arrays <br>';
                        }
                        else{
                            $plArrFull['sales'][$salesCount]['account'] = $plArr1['sales'][$i]['account'];
                            $plArrFull['sales'][$salesCount]['amount'][0] = $plArr1['sales'][$i]['amount'];
                            $plArrFull['sales'][$salesCount]['amount'][1] = 0;
                            $plArrFull['sales'][$salesCount]['ytd'] = $plArr1['sales'][$i]['ytd'];
                            $salesCount++;
                            // echo 'CA Value ' . $plArr1['sales'][$i]['account'] . ' in Arr1 is not in Arr2 <br>';
                        }
                    }
                    for($j = 0; $j < count((array)$plArr2['sales']); $j++){
                        if(!in_array($plArr2['sales'][$j]['account'], array_column($plArr1['sales'], 'account'))){
                            $plArrFull['sales'][$salesCount]['account'] = $plArr2['sales'][$j]['account'];
                            $plArrFull['sales'][$salesCount]['amount'][0] = 0;
                            $plArrFull['sales'][$salesCount]['amount'][1] = $plArr2['sales'][$j]['amount'];
                            $salesCount++;
                            // echo 'CA Value ' . $plArr2['sales'][$j]['account'] . ' in Arr2 is not in Arr1 <br>';
                        }
                    }


                    // Total Sales
                    $plArrFull['total_sales'][0] = $plArr1['total_sales'];
                    $plArrFull['total_sales'][1] = $plArr2['total_sales'];

                    // Cost Of Sales
                    for($i = 0; $i < count((array)$plArr1['cost_of_sales']); $i++){
                        if(in_array($plArr1['cost_of_sales'][$i]['account'], array_column($plArr2['cost_of_sales'], 'account'))){
                            $plArrFull['cost_of_sales'][$cosCount]['account'] = $plArr1['cost_of_sales'][$i]['account'];
                            $plArrFull['cost_of_sales'][$cosCount]['amount'][0] = $plArr1['cost_of_sales'][$i]['amount'];
                            $plArrFull['cost_of_sales'][$cosCount]['amount'][1] = $plArr2['cost_of_sales'][array_search($plArrFull['cost_of_sales'][$cosCount]['account'], array_column($plArr2['cost_of_sales'], 'account'))]['amount'];
                            // $plArrFull['cost_of_sales'][$cosCount]['ytd'] = $plArr2['cost_of_sales'][$i]['ytd'];
                            $plArrFull['cost_of_sales'][$cosCount]['ytd'] = $plArr1['cost_of_sales'][$i]['ytd'];
                            $cosCount++;
                            // echo 'cost_of_sales Value ' . $plArr1['cost_of_sales'][$i]['account'] . ' found in both arrays <br>';
                        }
                        else{
                            $plArrFull['cost_of_sales'][$cosCount]['account'] = $plArr1['cost_of_sales'][$i]['account'];
                            $plArrFull['cost_of_sales'][$cosCount]['amount'][0] = $plArr1['cost_of_sales'][$i]['amount'];
                            $plArrFull['cost_of_sales'][$cosCount]['amount'][1] = 0;
                            // $plArrFull['cost_of_sales'][$cosCount]['ytd'] = $plArr2['cost_of_sales'][$i]['ytd'];
                            $plArrFull['cost_of_sales'][$cosCount]['ytd'] = $plArr1['cost_of_sales'][$i]['ytd'];
                            $cosCount++;
                            // echo 'cost_of_sales Value ' . $plArr1['cost_of_sales'][$i]['account'] . ' in Arr1 is not in Arr2 <br>';
                        }
                    }
                    for($j = 0; $j < count((array)$plArr2['cost_of_sales']); $j++){
                        if(!in_array($plArr2['cost_of_sales'][$j]['account'], array_column($plArr1['cost_of_sales'], 'account'))){
                            $plArrFull['cost_of_sales'][$cosCount]['account'] = $plArr2['cost_of_sales'][$j]['account'];
                            $plArrFull['cost_of_sales'][$cosCount]['amount'][0] = 0;
                            $plArrFull['cost_of_sales'][$cosCount]['amount'][1] = $plArr2['cost_of_sales'][$j]['amount'];
                            // $plArrFull['cost_of_sales'][$cosCount]['ytd'] = $plArr2['cost_of_sales'][$i]['ytd'];
                            $cosCount++;
                            // echo 'cost_of_sales Value ' . $plArr2['cost_of_sales'][$j]['account'] . ' in Arr2 is not in Arr1 <br>';
                        }
                    }

                    // Total Cost of Sales
                    $plArrFull['total_cost_of_sales'][0] = $plArr1['total_cost_of_sales'];
                    $plArrFull['total_cost_of_sales'][1] = $plArr2['total_cost_of_sales'];

                    // Gross Profit
                    $plArrFull['gross_profit'][0] = $plArr1['gross_profit'];
                    $plArrFull['gross_profit'][1] = $plArr2['gross_profit'];

                    // Other Income
                    for($i = 0; $i < count((array)$plArr1['other_income']); $i++){
                        if(in_array($plArr1['other_income'][$i]['account'], array_column($plArr2['other_income'], 'account'))){
                            $plArrFull['other_income'][$oIncomeCount]['account'] = $plArr1['other_income'][$i]['account'];
                            $plArrFull['other_income'][$oIncomeCount]['amount'][0] = $plArr1['other_income'][$i]['amount'];
                            $plArrFull['other_income'][$oIncomeCount]['amount'][1] = $plArr2['other_income'][array_search($plArrFull['other_income'][$oIncomeCount]['account'], array_column($plArr2['other_income'], 'account'))]['amount'];
                            // $plArrFull['other_income'][$oIncomeCount]['ytd'] = $plArr2['other_income'][$i]['ytd'];
                            $plArrFull['other_income'][$oIncomeCount]['ytd'] = $plArr1['other_income'][$i]['ytd'];
                            $oIncomeCount++;
                            // echo 'other_income Value ' . $plArr1['other_income'][$i]['account'] . ' found in both arrays <br>';
                        }
                        else{
                            $plArrFull['other_income'][$oIncomeCount]['account'] = $plArr1['other_income'][$i]['account'];
                            $plArrFull['other_income'][$oIncomeCount]['amount'][0] = $plArr1['other_income'][$i]['amount'];
                            $plArrFull['other_income'][$oIncomeCount]['amount'][1] = 0;
                            // $plArrFull['other_income'][$oIncomeCount]['ytd'] = $plArr2['other_income'][$i]['ytd'];
                            $plArrFull['other_income'][$oIncomeCount]['ytd'] = $plArr1['other_income'][$i]['ytd'];
                            $oIncomeCount++;
                            // echo 'other_income Value ' . $plArr1['other_income'][$i]['account'] . ' in Arr1 is not in Arr2 <br>';
                        }
                    }
                    for($j = 0; $j < count((array)$plArr2['other_income']); $j++){
                        if(!in_array($plArr2['other_income'][$j]['account'], array_column($plArr1['other_income'], 'account'))){
                            $plArrFull['other_income'][$oIncomeCount]['account'] = $plArr2['other_income'][$j]['account'];
                            $plArrFull['other_income'][$oIncomeCount]['amount'][0] = 0;
                            $plArrFull['other_income'][$oIncomeCount]['amount'][1] = $plArr2['other_income'][$j]['amount'];
                            // $plArrFull['other_income'][$oIncomeCount]['ytd'] = $plArr2['other_income'][$i]['ytd'];
                            $oIncomeCount++;
                            // echo 'other_income Value ' . $plArr2['other_income'][$j]['account'] . ' in Arr2 is not in Arr1 <br>';
                        }
                    }

                    // Total Other Income
                    $plArrFull['total_other_income'][0] = $plArr1['total_other_income'];
                    $plArrFull['total_other_income'][1] = $plArr2['total_other_income'];

                    // Expenses
                    for($i = 0; $i < count((array)$plArr1['expenses']); $i++){
                        if(in_array($plArr1['expenses'][$i]['account'], array_column($plArr2['expenses'], 'account'))){
                            $plArrFull['expenses'][$expensesCount]['account'] = $plArr1['expenses'][$i]['account'];
                            $plArrFull['expenses'][$expensesCount]['amount'][0] = $plArr1['expenses'][$i]['amount'];
                            $plArrFull['expenses'][$expensesCount]['amount'][1] = $plArr2['expenses'][array_search($plArrFull['expenses'][$expensesCount]['account'], array_column($plArr2['expenses'], 'account'))]['amount'];
                            $plArrFull['expenses'][$expensesCount]['ytd'] = $plArr1['expenses'][$i]['ytd'];
                            // $plArrFull['expenses'][$expensesCount]['ytd'] = $plArr2['expenses'][$i]['ytd'];
                            $expensesCount++;
                            // echo 'expenses Value ' . $plArr1['expenses'][$i]['account'] . ' found in both arrays <br>';
                        }
                        else{
                            $plArrFull['expenses'][$expensesCount]['account'] = $plArr1['expenses'][$i]['account'];
                            $plArrFull['expenses'][$expensesCount]['amount'][0] = $plArr1['expenses'][$i]['amount'];
                            $plArrFull['expenses'][$expensesCount]['amount'][1] = 0;
                            $plArrFull['expenses'][$expensesCount]['ytd'] = $plArr1['expenses'][$i]['ytd'];
                            // $plArrFull['expenses'][$expensesCount]['ytd'] = $plArr2['expenses'][$i]['ytd'];
                            $expensesCount++;
                            // echo 'expenses Value ' . $plArr1['expenses'][$i]['account'] . ' in Arr1 is not in Arr2 <br>';
                        }
                    }
                    for($j = 0; $j < count((array)$plArr2['expenses']); $j++){
                        if(!in_array($plArr2['expenses'][$j]['account'], array_column($plArr1['expenses'], 'account'))){
                            $plArrFull['expenses'][$expensesCount]['account'] = $plArr2['expenses'][$j]['account'];
                            $plArrFull['expenses'][$expensesCount]['amount'][0] = 0;
                            $plArrFull['expenses'][$expensesCount]['amount'][1] = $plArr2['expenses'][$j]['amount'];
                            // $plArrFull['expenses'][$expensesCount]['ytd'] = $plArr2['expenses'][$i]['ytd'];
                            $expensesCount++;
                            // echo 'expenses Value ' . $plArr2['expenses'][$j]['account'] . ' in Arr2 is not in Arr1 <br>';
                        }
                    }

                    // Total Expenses
                    $plArrFull['total_expenses'][0] = $plArr1['total_expenses'];
                    $plArrFull['total_expenses'][1] = $plArr2['total_expenses'];

                    // Net Profit Before Tax
                    $plArrFull['net_profit_before_tax'][0] = $plArr1['net_profit_before_tax'];
                    $plArrFull['net_profit_before_tax'][1] = $plArr2['net_profit_before_tax'];

                    // Tax
                    for($i = 0; $i < count((array)$plArr1['tax']); $i++){
                        if(in_array($plArr1['tax'][$i]['account'], array_column($plArr2['tax'], 'account'))){
                            $plArrFull['tax'][$taxCount]['account'] = $plArr1['tax'][$i]['account'];
                            $plArrFull['tax'][$taxCount]['amount'][0] = $plArr1['tax'][$i]['amount'];
                            $plArrFull['tax'][$taxCount]['amount'][1] = $plArr2['tax'][array_search($plArrFull['tax'][$taxCount]['account'], array_column($plArr2['tax'], 'account'))]['amount'];
                            $plArrFull['tax'][$taxCount]['ytd'] = $plArr1['tax'][$i]['ytd'];
                            // $plArrFull['tax'][$taxCount]['ytd'] = $plArr2['tax'][$i]['ytd'];
                            $taxCount++;
                            // echo 'tax Value ' . $plArr1['tax'][$i]['account'] . ' found in both arrays <br>';
                        }
                        else{
                            $plArrFull['tax'][$taxCount]['account'] = $plArr1['tax'][$i]['account'];
                            $plArrFull['tax'][$taxCount]['amount'][0] = $plArr1['tax'][$i]['amount'];
                            $plArrFull['tax'][$taxCount]['amount'][1] = 0;
                            $plArrFull['tax'][$taxCount]['ytd'] = $plArr1['tax'][$i]['ytd'];
                            // $plArrFull['tax'][$taxCount]['ytd'] = $plArr2['tax'][$i]['ytd'];
                            $taxCount++;
                            // echo 'tax Value ' . $plArr1['tax'][$i]['account'] . ' in Arr1 is not in Arr2 <br>';
                        }
                    }
                    for($j = 0; $j < count((array)$plArr2['tax']); $j++){
                        if(!in_array($plArr2['tax'][$j]['account'], array_column($plArr1['tax'], 'account'))){
                            $plArrFull['tax'][$taxCount]['account'] = $plArr2['tax'][$j]['account'];
                            $plArrFull['tax'][$taxCount]['amount'][0] = 0;
                            $plArrFull['tax'][$taxCount]['amount'][1] = $plArr2['tax'][$j]['amount'];
                            // $plArrFull['tax'][$taxCount]['ytd'] = $plArr2['tax'][$i]['ytd'];
                            $taxCount++;
                            // echo 'tax Value ' . $plArr2['tax'][$j]['account'] . ' in Arr2 is not in Arr1 <br>';
                        }
                    }

                    // Total Tax
                    $plArrFull['total_tax'][0] = $plArr1['total_tax'];
                    $plArrFull['total_tax'][1] = $plArr2['total_tax'];

                    // Net Profit After Tax
                    $plArrFull['net_profit_after_tax'][0] = $plArr1['net_profit_after_tax'];
                    $plArrFull['net_profit_after_tax'][1] = $plArr2['net_profit_after_tax'];

                    array_push($updates, [$plArr1['last_updated'], $plArr2['last_updated']]);
                    $oldestUpdate = min($updates);

                    if($oldestUpdate[0]){
                        $plArrFull['last_updated'] = $oldestUpdate[0];
                    }
                    else{
                        $plArrFull['last_updated'] = $oldestUpdate;
                    }

                    self::$_data = $plArrFull;

                break;
        
            //Balance Sheet
                case 'bs':

                    $bs1 = new DataBalanceSheet($fromDate, null);
                    $bs2 = new DataBalanceSheet($toDate, null);
                    
                    $bsmerger = new MergeBalanceSheetModels($bs1->getBalanceSheet(), $bs2->getBalanceSheet());
                    $bsmerger->merge();
                    $mbs = $bsmerger->get();
                    
                    self::$_data = $mbs->toArray(true);

                break;
        
            //Accounts Receivable
                case 'ar':

                    $inv = new DataInvoices(null);
                    self::$_data = $inv->getAccounts($fromDate, 1);
                    
                break;
        
            //Debitors days outstanding
                case 'ddo':

                    $inv = new DataInvoices(null);
                    self::$_data = $inv->getAccounts($fromDate, 1);

                break;
        
            //Top Customers by Sale
                case 'tcs':

                    $inv = new DataInvoices(null);
                    self::$_data = $inv->getAccounts($fromDate, 1);
        
                break;
        
            //Retention
                case 'ret':

                    $updates = array();
                    $retArr = array('retention' => array());

                    for($i = 0; $i < count($months); $i++){
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));

                        $ret = new FormulaRetention($fromDate);
                        $retention = $ret->getRetention();
                        array_push($retArr['retention'], $retention['retention']);
                        array_push($updates, $retention['last_updated']);
                    }
                    if($updates != null){
                        $oldestUpdate = min($updates);
                        if(count((array)$oldestUpdate) != 1 && count((array)$oldestUpdate) != 0){
                            $oldestUpdate = $oldestUpdate[0];
                        }
                    }
                    else{
                        $oldestUpdate = 'Never';
                    }
                    $retArr['last_updated'] = $oldestUpdate;

                    self::$_data = $retArr;
        
                break;
        
            //Accounts Payable
                case 'ap':

                    $inv = new DataInvoices(null);
                    self::$_data = $inv->getAccounts($fromDate, 2);
        
                break;
        
            //Creditors Days Outstanding
                case 'cdo':

                    $inv = new DataInvoices(null);
                    self::$_data = $inv->getAccounts($fromDate, 2);
        
                break;
        
            //Top Suppliers by Purchases
                case 'tsp':

                    $inv = new DataInvoices(null);
                    self::$_data = $inv->getAccounts($fromDate, 2);
        
                break;
                
            //Inventory Management
                case 'im':

                    $inv = new DataInventory(null);
                    self::$_data = $inv->getInventory();
        
                break;
        
            //Inventory Analysis - Sales
                case 'inas':

                    $inv = new DataInvoices(null);
                    self::$_data = $inv->getLineItems($fromDate);
        
                break;
        
            //Inventory Analysis - Quantity
                case 'inaq':

                    $inv = new DataInventory(null);
                    self::$_data = $inv->getInventory();
        
                break;
            
            //Profit Loss Ratios
                case 'plr':

                    $gmrArr = array();
                    $omrArr = array();
                    $nmrArr = array();
                    $updates = array();

                    for($i = 0; $i < count($months); $i++){
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));
                        $toDate = date("Y-m-t", strtotime($fromDate));

                        $pl = new DataProfitAndLoss($fromDate, $toDate, 11);
                        $profitAndLoss = $pl->getProfitAndLoss();

                        $gmr = new FormulaGrossMarginRatio($profitAndLoss);
                        $omr = new FormulaOperatingMarginRatio($profitAndLoss);
                        $nmr = new FormulaNetMarginRatio($profitAndLoss);

                        array_push($gmrArr, $gmr->getGrossMarginRatio()['gross_margin_ratio']);
                        array_push($omrArr, $omr->getOperatingMarginRatio()['operating_margin_ratio']);
                        array_push($nmrArr, $nmr->getNetMarginRatio()['net_margin_ratio']);
                        array_push($updates, $gmr->getGrossMarginRatio()['last_updated']);
                        array_push($updates, $omr->getOperatingMarginRatio()['last_updated']);
                        array_push($updates, $nmr->getNetMarginRatio()['last_updated']);
                    }
                    if($updates != null){
                        $oldestUpdate = min($updates);
                        if(count((array)$oldestUpdate) != 1 && count((array)$oldestUpdate) != 0){
                            $oldestUpdate = $oldestUpdate[0];
                        }
                    }
                    else{
                        $oldestUpdate = 'Never';
                    }
                    
                    self::$_data = array(
                        'gross_margin' => $gmrArr,
                        'operating_margin' => $omrArr,
                        'net_margin' => $nmrArr,
                        'last_updated' => $oldestUpdate
                    );
        
                break;
        
            //Balance Sheet Ratios
                case 'bsr':

                    $crArr = array();
                    $qrArr = array();
                    $deArr = array();
                    $updates = array();

                    
                    // echo 'Month count: ' . count($months) . '<br><br>';

                    for($i = 0; $i < count($months); $i++){
                        $fromDate = date('Y-m-d', mktime(0, 0, 0, $months[$i] , 01, $years[$i]));
                        $toDate = date("Y-m-t", strtotime($fromDate));
                        // echo 'Date: ' . $fromDate . '<br>';
                        $bs = new DataBalanceSheet($fromDate, 11);
                        $balanceSheet = $bs->getBalanceSheet();

                        $cashReceivables = self::getCashRecievables($balanceSheet);

                       
                        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
                        //$logger->info('Getting CurrentRatio for ' . $fromDate);
                        $cr = new FormulaCurrentRatio(array(
                            'total_current_assets' => $balanceSheet->getTotalCurrentAssets(),
                            'total_current_liabilities' => $balanceSheet->getTotalCurrentLiabilities(),
                            'last_updated' => $balanceSheet->getLastUpdated()
                        ));
                        //$logger->info('CurrentRatio: ' . $cr->getCurrentRatio()['current_ratio']);

                        //$logger->info('Getting QuickRatio for ' . $fromDate);
                        $qr = new FormulaQuickRatio(array(
                            'total_current_liabilities' => $balanceSheet->getTotalCurrentLiabilities(),
                            'cash_recievables' => $cashReceivables,
                            'last_updated' => $balanceSheet->getLastUpdated()
                        ));
                        //$logger->info('QuickRatio: ' . $qr->getQuickRatio()['quick_ratio']);

                        //$logger->info('Getting DebtToEquity for ' . $fromDate);
                        $de = new FormulaDebtToEquity(array(
                            'total_equity' => $balanceSheet->getTotalEquity(),
                            'total_liabilities' => $balanceSheet->getTotalLiabilities(),
                            'last_updated' => $balanceSheet->getLastUpdated()
                        ));
                        //$logger->info('DebtToEquity: ' . $de->getDebtToEquity()['debt_to_equity']);

                        //$re = new FormulaReturnOnEquity($fromDate, $toDate);

                        array_push($crArr, $cr->getCurrentRatio()['current_ratio']);
                        array_push($qrArr, $qr->getQuickRatio()['quick_ratio']);
                        array_push($deArr, $de->getDebtToEquity()['debt_to_equity']);
                        // array_push($oeArr, $oe->getReturnOnEquity());
                        array_push($updates, $cr->getCurrentRatio()['last_updated']);
                        array_push($updates, $qr->getQuickRatio()['last_updated']);
                        array_push($updates, $de->getDebtToEquity()['last_updated']);
                    }
                    // die();


                    if($updates != null){
                        $oldestUpdate = min($updates);
                        if(count((array)$oldestUpdate) != 1 && count((array)$oldestUpdate) != 0){
                            $oldestUpdate = $oldestUpdate[0];
                        }
                    }
                    else{
                        $oldestUpdate = 'Never';
                    }
                    $retArr['last_updated'] = $oldestUpdate;

                    self::$_data = array(
                        'current_ratio' => $crArr,
                        'quick_ratio' => $qrArr,
                        'debt_to_equity' => $deArr,
                        // 'return_on_equity' => $deArr,
                        'last_updated' => $oldestUpdate
                    );
        
                break;

                default:

                    self::$_data = null;
                    
                break;
            }
        }
    }

    public static function getData($action, $fromDate, $toDate, $isFlipped = false){
        self::setData($action, $fromDate, $toDate, $isFlipped);
        return self::$_data;
    }

    public static function inArrayKey($array, $key, $val) {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val)
                return true;
        return false;
    }

    public static function getCashRecievables($balanceSheet){
        $cashReceivables = 0;
        for($i = 0; $i < count($balanceSheet->getCurrentAssets()); $i++){
                if(strpos($balanceSheet->getCurrentAssets()[$i]['account'], 'Bank') !== false){
                    $cashReceivables += $balanceSheet->getCurrentAssets()[$i]['data']['amount'];
                }
            }
        return $cashReceivables;
    }
}
<?php
class DataChartOfAccounts{
    private $_chartOfAccounts = array(),
            $_modifiedAfter,
            $_apiObject,
            $_xero,
            $_sage,
            $_quickbooks,
            $_instance,
            $_db,
            $_apiId;

    public function __construct($modifiedAfter){
        $this->_db = DB::getInstance();
        $this->_apiId = Companies::getApi();
        $this->_modifiedAfter = $modifiedAfter;
    }

    private function setApiObject(){

        switch ($this->_apiId) {
            //xero
            case 1:
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();
                if($this->_modifiedAfter == null){
                    $this->_apiObject = $this->_instance->getAccounts(Companies::getActiveTenantId());
                }
                else{
                    $dateFormat = str_replace('-', ',', $this->_modifiedAfter);
                    $this->_apiObject = $this->_instance->getAccounts(Companies::getActiveTenantId(), '', 'updateddateutc >= DateTime('.$dateFormat.')');
                }
            break;

            //sage
            case 2:
                $this->_sage = Sage::getInstance();
                $provider = $this->_sage->getProvider();
                $this->_apiObject = $provider->SystemAccounts()->get(Session::get('active_tenant_id'));
            break;

            //quickbooks
            case 3:
                $this->_quickbooks = Quickbooks::getInstance();
                $dataService = $this->_quickbooks->getDataService();
                // $i = 1;
                // while(1){
                    $this->_apiObject = $dataService->FindAll('Account', 1, 500);
                    // if (!$allAccounts || (0 == count($allAccounts))) {
                    //     break;
                    // }
                    // array_push($allAccounts, $account);

                    // if($i = 3){
                    //     break;
                    // }
                    // echo $i . '<br>';

                    // for ($j = 0; $j < count($allAccounts); $j++) {
                    //     $this->_apiObject[$j] = $allAccounts[$j];
                    //     echo "Account[".($i++)."]: {$oneAccount->Name}<br>";
                    //     echo "    * Id: [{$oneAccount->Id}]<br>";
                    //     echo "    * AccountType: [{$oneAccount->AccountType}]<br>";
                    //     echo "    * AccountSubType: [{$oneAccount->AccountSubType}]<br>";
                    //     echo "    * Active: [{$oneAccount->Active}]<br>";
                    //     echo "<br>";
                    // }
                // }

                // $this->_apiObject = $allAccounts;
            break;
            
        }
    }

    private function setChartOfAccounts(){
        $this->setApiObject();

        switch($this->_apiId){
            case 1:
                $accounts = $this->_apiObject->getAccounts();
                for($i = 0; $i < count($accounts); $i++){
                    $this->_chartOfAccounts[$i]['name'] = $accounts[$i]->getName();
                    $this->_chartOfAccounts[$i]['type'] = $accounts[$i]->getType();
                    $this->_chartOfAccounts[$i]['class'] = $accounts[$i]->getClass();
                    $this->_chartOfAccounts[$i]['code'] = $accounts[$i]->getCode();
                    $this->_chartOfAccounts[$i]['status'] = $accounts[$i]->getStatus();
                }
            break;
            case 2:
                $accounts = $this->_apiObject->results();
                for($i = 0; $i < count($accounts); $i++){
                    $this->_chartOfAccounts[$i]['name'] = $accounts[$i]->Name;
                    $this->_chartOfAccounts[$i]['type'] = $accounts[$i]->Category->Description;
                    $this->_chartOfAccounts[$i]['code'] = $accounts[$i]->ID;
                    $this->_chartOfAccounts[$i]['status'] = 'ACTIVE';
                }
            break;
            case 3:
                for($i = 0; $i < count($this->_apiObject); $i++){
                    $qualifiedName = $this->_apiObject[$i]->FullyQualifiedName;
                    $nameArr = explode(':', $qualifiedName);
                    $name = $nameArr[0];

                    $type = $this->_apiObject[$i]->Classification;
                    $accountType = $this->_apiObject[$i]->AccountType;
                    if($type == 'Expense' && $accountType == 'Cost of Goods Sold'){
                        $type = 'COST OF SALES';
                    }
                    if($type == 'Revenue' && $accountType == 'Other Income'){
                        $type = 'OTHER INCOME';
                    }
                    if($type == 'Liability' && $accountType == 'Long Term Liability'){
                        $type = 'TERMLIAB';
                    }
                    if($type == 'Asset' && $accountType == 'Fixed Asset'){
                        $type = 'FIXED';
                    }

                    $this->_chartOfAccounts[$i]['name'] = $name;
                    $this->_chartOfAccounts[$i]['type'] = $type;
                    $this->_chartOfAccounts[$i]['code'] = $this->_apiObject[$i]->Id;
                    $this->_chartOfAccounts[$i]['status'] = 'ACTIVE';
                }
            break;
        }

        $systemAccounts = array('Total Income', 'Total Cost of Sales', 'Gross Profit', 'Total Other Income', 'Total Operating Expenses', 'Net Profit', 'Total Tax');
        for($i = 11; $i <= 17; $i++){
            $index = $i - 11;
            $this->setSystemAccount($systemAccounts[$index],  $i, '000', 'ACTIVE', count($this->_chartOfAccounts) + $index);
        }

        $this->_chartOfAccounts = array_values($this->_chartOfAccounts);
    }

    private function setSystemAccount($name, $typeId, $code, $status, $increment){
        $this->_chartOfAccounts[$increment]['name'] = $name;
        $this->_chartOfAccounts[$increment]['type_id'] = $typeId;
        $this->_chartOfAccounts[$increment]['code'] = $code;
        $this->_chartOfAccounts[$increment]['status'] = $status;
    }

    public function updateDb(){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');

        $this->setChartOfAccounts();
        
        for($i = 0; $i < count($this->_chartOfAccounts); $i++){
            
            if(isset($this->_chartOfAccounts[$i]['type'])){
                $typeId = AccountTypes::getTypeId($this->_chartOfAccounts[$i]['type']);
            }
            else{
                $typeId = $this->_chartOfAccounts[$i]['type_id'];
            }
            
            $status = 0;
            if($this->_chartOfAccounts[$i]['status'] == 'ACTIVE'){
                $status = 1;
            }

            $accQuery = $this->_db->get('temp_accounts', array(
                'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()),
                'account_name' => array('operator' => '=', 'value' => $this->_chartOfAccounts[$i]['name'])
            ));
            if(!$accQuery->count()){
                $accIns = $this->_db->insert('temp_accounts', array(
                    'company_id' => Companies::getActiveCompanyId(),
                    'account_name' => $this->_chartOfAccounts[$i]['name'],
                    'type_id' => $typeId,
                    'account_code' => $this->_chartOfAccounts[$i]['code'],
                    'status' => $status
                ));
            }
        }

        // var_dump($this->_chartOfAccounts);
    }

    public function getChartOfAccounts(){
        $dbQuery = $this->_db->get('temp_accounts', array(
            'company_id', '=', Companies::getActiveCompanyId()
        ));
        if($dbQuery->count()){
            $data = $dbQuery->results();

            for($i = 0; $i < count($data); $i++){

                $type = AccountTypes::getTypeString($data[$i]->type_id);

                $status = 'INACTIVE';
                if($data[$i]->status == 1){
                    $status = 'ACTIVE';
                }

                $this->_chartOfAccounts[$i] = array(
                    'id' => $data[$i]->account_id,
                    'name' => $data[$i]->account_name,
                    'type' => $type,
                    'code' => $data[$i]->account_code,
                    'status' => $status
                );
            }
        }
        return $this->_chartOfAccounts;
    }

    public function getXeroChartOfAccounts(){
        $this->setChartOfAccounts();
        // return $this->_chartOfAccounts;
        return $this->_apiObject->getAccounts();
    }

    public function updateType($id, $newType){

        $dbCompany = $this->_db->get('temp_accounts', array(
            'company_id', '=', Companies::getActiveCompanyId()
        ));
        if($dbCompany->results()){
            for($i = 0; $i < count($dbCompany->results()); $i++){
                if($dbCompany->results()[$i]->account_id == $id){

                    $type = AccountTypes::getTypeId($newType);

                    $account_id = $dbCompany->results()[$i]->account_id;
                    $dbUpdate = $this->_db->update('temp_accounts', $account_id, 'account_id', array(
                        'type_id' => $type
                    ));
                }
            }
        }
    }
}
<?php

class FormulaOperatingMarginRatio{
    private $_operatingMarginRatio;

    public function __construct($profitAndLoss){
        $this->_profitAndLoss = $profitAndLoss;
    }

    private function setOperatingMargin(){
        $totaLExpenses = $this->_profitAndLoss['total_expenses']['amount'];
        $totalSales = $this->_profitAndLoss['total_sales']['amount'];

        if($totalSales != 0){      
            $this->_operatingMarginRatio['operating_margin_ratio'] = ($totaLExpenses / $totalSales) * 100;
        }
        else{
            $this->_operatingMarginRatio['operating_margin_ratio'] = 0;
        }
        
        $oldestUpdate = $this->_profitAndLoss['last_updated'];
        $this->_operatingMarginRatio['last_updated'] = $oldestUpdate;
    }
    
    public function getOperatingMarginRatio(){
        $this->setOperatingMargin();
        return $this->_operatingMarginRatio;
    }
}
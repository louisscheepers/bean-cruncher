<?php

use QuickBooksOnline\API\ReportService\ReportService;

class DataCashSpent{
    private $_cashSpent,
            $_apiObject,
            $_apiId,
            $_month,
            $_xero,
            $_instance,
            $_db,
            $_response = array();

    public function __construct($month){
        $this->_month = $month;
        $this->_apiId = Companies::getApi();
        $this->_db = DB::getInstance();
    }

    private function setApiObject($month){

        switch ($this->_apiId) {
            //xero
            case 1:
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();
                $this->_apiObject = $this->_instance->getReportExecutiveSummary(Companies::getActiveTenantId(), $month);
            break;

            //sage
            case 2:
                // sage uses DataBankTansactions
            break;

            //quickbooks
            case 3:

                //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
                //$logger->info('Getting transaction list for cash spent - ' . $month);

                $dateObj = new DateTime($month);
                $toDate = $dateObj->format('Y-m-t');

                $this->_quickbooks = Quickbooks::getInstance();
                $dataService = $this->_quickbooks->getDataService();
                
                $serviceContext = $dataService->getServiceContext();
                $reportService = new ReportService($serviceContext);
        
                $reportService->setStartDate($month);
                $reportService->setEndDate($toDate);
        
                $this->_transactionList = $reportService->executeReport('TransactionList');

            break;
        }
    }

    private function setCashSpent(){
        $this->setApiObject($this->_month);

        switch ($this->_apiId) {
            //xero
            case 1:
                $rows = $this->_apiObject->getReports()[0]->getRows();
                for($i = 0; $i < count($rows); $i++){
                    $innerRows = $rows[$i]->getRows();
                    foreach((array)$innerRows as $innerRow){
                        $title = $innerRow->getCells()[0]->getValue();
                        $value = $innerRow->getCells()[1]->getValue();
                        if($title == 'Cash spent'){
                            if($value == null){
                                $this->_cashSpent = 0;
                            }
                            else{
                                $this->_cashSpent = $value;
                            }
                        }
                    }
                }
            break;

            //sage 
            case 2:
                $transactions = new DataBankTransactions(substr($this->_month, 0, 7));
                $this->_cashSpent = $transactions->getCashSpent();
            break;

            //quickbooks
            case 3:
                
                $out = 0;
                
                foreach($this->_transactionList->Rows->Row as $line){
                    $amount = $line->ColData[8]->value;
                    $type = $line->ColData[1]->value;

                    if($amount < 0 && $type == 'Deposit'){
                        $out += $amount;
                    }
                }

                $this->_cashSpent = abs($out);

            break;
        }

    }

    public function updateDb(){
        $this->setCashSpent();

        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        //$logger->info('Updating DB');

        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        $dbQuery = $this->_db->get('temp_cash_spent', array(
            'date' => array('operator' => '=', 'value' => substr($this->_month, 0, 7)), 
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()), 
        ));
        if($dbQuery->count()){
            // echo 'Found in DB, updating ...<br>';
            $data = $dbQuery->results();
            $id = $data[0]->cash_spent_id;

            $dbUpdate = $this->_db->update('temp_cash_spent', 'cash_spent_id', $id, array(
                'amount' => $this->_cashSpent,
                'last_updated' => $now
            ));
            if($dbUpdate){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            // echo 'Not found, inserting...<br>';
            $dbInsert = $this->_db->insert('temp_cash_spent', array(
                'company_id' => Companies::getActiveCompanyId(),
                'integration_id' => 2,
                'date' => substr($this->_month, 0, 7),
                'amount' => $this->_cashSpent,
                'last_updated' => $now
            ));
            if($dbInsert){
                // return true;
            }
            else{
                // return false;
            }
        }
    }

    public function getCashSpent(){
        // $this->setCashSpent();
        // return $this->_cashSpent;


        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        //$logger->info('Getting cash received for ' . $this->_month);
        
        $dbQuery = $this->_db->get('temp_cash_spent', array(
            'date' => array('operator' => '=', 'value' => substr($this->_month, 0, 7)), 
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId()), 
        ));
        if($dbQuery->count()){
            //$logger->info('Found');
            $data = $dbQuery->results();
            $this->_response[0] = $data[0]->amount;
            $this->_response[1] = $data[0]->last_updated;
            return $this->_response;
        }
        else{
            //$logger->info('Not found, updating...');
            $this->updateDb();
            $today = new DateTime();
            $now = $today->format('Y/m/d H:i');
            //$logger->info('Response after update: ' . $this->_cashSpent);
            $this->_response[0] = $this->_cashSpent;
            $this->_response[1] = $now;
            return $this->_response;
        }
    }
}
<?php


class PayValidation
{
    public static function checkResponseCode($code){
        $result = new stdClass();
        if(self::isProcessingSuccessful($code)){
            $result->isSuccess = true;
            $result->message = 'Processing Successful';
            return $result;
        }

        if(self::isSessionPending($code)){
            $result->isSuccess = true;
            $result->message = 'Session is pending';
            return $result;
        }

        if(self::canStatusChange($code)){
            $result->isSuccess = true;
            $result->message = 'Status can Change' ;
            return $result;
        }

        if(self::isRejected($code)){
            $result->isSuccess = false;
            $result->message = 'Rejected';
            return $result;
        }

        if(self::rejectedByExternalInstitution($code)){
            $result->isSuccess = false;
            $result->message = 'Rejected by External Institution';
            return $result;
        }

        if(self::isRejectionCommunicatonError($code)){
            $result->isSuccess = false;
            $result->message = 'Communication Error';
            return $result;
        }
        if(self::isSysError($code)){
            $result->isSuccess = false;
            $result->message = 'System Error';
            return $result;
        }
        if(self::isAsyncError($code)){
            $result->isSuccess = false;
            $result->message = 'Async Error';
            return $result;
        }

        if(self::isSoftDecline($code)){
            $result->isSuccess = false;
            $result->message = 'Soft decline';
            return $result;
        }

        if(self::isRejectedByExternalRiskSystem($code)){
            $result->isSuccess = false;
            $result->message = 'Rejected by external Risk System';
            return $result;
        }

        if(self::isRejectedDueToAddressValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Address Validation Error';
            return $result;
        }

        if(self::isRejectedDueTo3DSecure($code)){
            $result->isSuccess = false;
            $result->message = '3dSecure error';
            return $result;
        }

        if(self::isRejectedDueToBlacklist($code)){
            $result->isSuccess = false;
            $result->message = 'Blacklisted' ;
            return $result;
        }

        if(self::isRejectedDueToRiskValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Rejected by Risk Validation' ;
            return $result;
        }

        if(self::isRejectedDueToConfigurationValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Rejected by Configuration error';
            return $result;
        }

        if(self::isRejectedRegValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Register validation error' ;
            return $result;
        }

        if(self::isRejectedDuetoJobValidaton($code)){
            $result->isSuccess = false;
            $result->message = 'Job Validation Error';
            return $result;
        }

        if(self::isRejectedValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Validation Error';
            return $result;
        }

        if(self::isRejectedFormatValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Format Validation Error | The page must have been reloaded';
            return $result;
        }

        if(self::isRejectedAddressValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Address Validation Error 2';
            return $result;
        }

        if(self::isRejectedContactValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Contact Validation error';
            return $result;
        }

        if(self::isRejectedAccountValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Account Validation Error';
            return $result;
        }

        if(self::isRejectedAmountValidation($code)){
            $result->isSuccess = false;
            $result->message = 'Amount Validation Error';
            return $result;
        }

        if(self::isRejectedRiskManagement($code)){
            $result->isSuccess = false;
            $result->message = 'Rejected by Risk Management';
            return $result;
        }

        if(self::isChargeBack($code)){
            $result->isSuccess = false;
            $result->message = 'is a Chargeback' ;
            return $result;
        }

        $result->isSuccess = false;
        $result->message = 'Validation failed all checks';

        return $result;
    }

    private function isProcessingSuccessful($code){
        return preg_match('/^(000\.000\.|000\.100\.1|000\.[36])/', $code);
    }

    private static function isSessionPending($code){
        return preg_match('/^(000\.200)/', $code);
    }

    private static function canStatusChange($code){
        return preg_match('/^(800\.400\.5|100\.400\.500)/', $code);
    }

    private static function isRejected($code){
        return preg_match(' /^(000\.400\.[1][0-9][1-9]|000\.400\.2)/', $code);
    }

    private static function rejectedByExternalInstitution($code){
        return preg_match('/^(800\.[17]00|800\.800\.[123])/', $code);
    }

    private static function isRejectionCommunicatonError($code){
        return preg_match('/^(900\.[1234]00|000\.400\.030)/', $code);
    }

    private static function isSysError($code){
        return preg_match('/^(800\.[56]|999\.|600\.1|800\.800\.[84])/', $code);
    }

    private static function isAsyncError($code){
        return preg_match('/^(100\.39[765])/', $code);
    }

    private static function isSoftDecline($code){
        return preg_match('/^(300\.100\.100)/', $code);
    }

    private static function isRejectedByExternalRiskSystem($code){
        return preg_match('/^(100\.400\.[0-3]|100\.38|100\.370\.100|100\.370\.11)/', $code);
    }

    private static function isRejectedDueToAddressValidation($code){
        return preg_match('/^(800\.400\.1)/', $code);
    }


    private static function isRejectedDueTo3DSecure($code){
        return preg_match('/^(800\.400\.2|100\.380\.4|100\.390)/', $code);
    }

    private static function isRejectedDueToBlacklist($code){
        return preg_match('/^(100\.100\.701|800\.[32])/', $code);
    }

    private static function isRejectedDueToRiskValidation($code){
        return preg_match('/^(800\.1[123456]0)/', $code);
    }

    private static function isRejectedDueToConfigurationValidation($code){
        return preg_match('/^(600\.[23]|500\.[12]|800\.121)/', $code);
    }

    private static function isRejectedRegValidation($code){
        return preg_match('/^(100\.[13]50)/', $code);
    }

    private static function isRejectedDuetoJobValidaton($code){
        return preg_match('/^(100\.250|100\.360)/', $code);
    }

    private static function isRejectedValidation($code){
        return preg_match('/^(700\.[1345][05]0)/', $code);
    }

    private static function isRejectedFormatValidation($code){
        return preg_match('/^(200\.[123]|100\.[53][07]|800\.900|100\.[69]00\.500)/', $code);
    }

    private static function isRejectedAddressValidation($code){
        return preg_match('/^(100\.800)/', $code);
    }

    private static function isRejectedContactValidation($code){
        return preg_match('/^(100\.[97]00)/', $code);
    }

    private static function isRejectedAccountValidation($code){
        return preg_match('/^(100\.100|100.2[01])/', $code);
    }

    private static function isRejectedAmountValidation($code){
        return preg_match('/^(100\.55)/', $code);
    }

    private static function isRejectedRiskManagement($code){
        return preg_match('/^(100\.380\.[23]|100\.380\.101)/', $code);
    }

    private static function isChargeBack($code){
        return preg_match('/^(000\.100\.2)/', $code);
    }


}
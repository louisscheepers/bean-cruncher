<?php 
class User {
    private $_db,
            $_data,
            $_sessionName,
            $_cookieName,
            $_cookieExpiry,
            $_isLoggedIn;

    public function __construct($user = null){
        $this->_db = DB::getInstance();
        $this->_sessionName = Config::get('session/session_name');
        $this->_cookieName = Config::get('remember/cookie_name');
        $this->_cookieExpiry = Config::get('remember/cookie_expiry');
        if(!$user){
            if(Session::exists($this->_sessionName)){
                $user = Session::get($this->_sessionName);

                if($this->find($user)){
                    $this->_isLoggedIn = true;
                }else{
                    //process logout
                }
            }
        }else{
            $this->find($user);
        }
    }

    public function update($fields = array(), $id = null){

        if(!$id && $this->isLoggedIn()){
            $id = $this->data()->user_id;
        }

        if(!$this->_db->update('user', 'user_id', $id, $fields)){
            throw new Exception('There was a problem updating');  
        }
    }
    public function create($fields = array()){
        if(!$this->_db->insert('user', $fields)){
            throw new Exception('There was a problem creating an account.');
        }
    }

    public function find($user = null){
       
        if($user){
            $field = (is_numeric($user)) ? 'user_id' : 'user_email';
            $data = $this->_db->get('user', array($field, '=', $user));
            
            if($data->count()){
                $this->_data = $data->first();
                return true;
            }
        }

        return false;
    }

    public function login($email = null, $password = null, $remember = false){
        if(!$email && !$password && $this->exists()){
            Session::put($this->_sessionName, $this->data()->user_id);
        }
        else{
            $user = $this->find($email);
            if($user){
                if($this->data()->user_password === Hash::make($password, $this->data()->user_salt))
                {
                    Session::put($this->_sessionName, $this->data()->user_id);

                    $companies = array_values(Companies::getCompanies());
                    
                    $company = $companies[0]['id'];
                    if($company != null){
                        Companies::setActiveCompany($company);
                    }
    
                    if($remember){
                        $hash = Hash::unique();
                        $hashCheck = $this->_db->get('session', array('user_id', '=',$this->data()->user_id));
                        
                        if(!$hashCheck->count()){
                            $this->_db->insert('session', array(
                                'user_id' => $this->data()->user_id,
                                'session_hash' => $hash
                            ));
                        }
                        else{
                            $hash = $hashCheck->first()->session_hash;
                        }
    
                        Cookie::put($this->_cookieName, $hash, $this->_cookieExpiry);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public function exists(){
        return (!empty($this->_data)) ? true : false;
    }
    public function logout(){

        Session::delete('active_role_id');
        Session::delete('user');
        Session::delete('active_tenant_id');
        Session::delete('active_company_id');
        for($i = 0; $i <= 8; $i++){
            Session::delete('dataBox' . $i);
        }
        Session::delete('oauth2state');
        Session::delete('token');

        // $this->_db->delete('users_session',array('user_id', '=', $this->data()->id));
        Session::delete($this->_sessionName);
        Cookie::delete($this->_cookieName);
    }

    public function data(){
        return $this->_data;
    }

    public function isLoggedIn(){
        return $this->_isLoggedIn;
    }

    public function getPackage(){
        return $this->_data->package_id;
    }

    public function hasLoggedIn(){
        return $this->_data->has_logged_in;
    }
    public function setLoggedIn(){
        $this->_db->update('user', 'user_id', $this->data()->user_id, array(
            'has_logged_in' => 1
        ));
    }

    public function hasExpired(){
        $exptime = $this->data()->package_expiry;
        $now = time();
        
       

        if(($exptime - $now) <= 0){
            if($this->getTrail() == TrailTypes::INTRAIL){
                $fields = array('hadTrail' => TrailTypes::HADTRAIL);  
                $id = $this->data()->user_id;
    
                if(!$this->_db->update('user', 'user_id', $id, $fields)){
                    throw new Exception('There was a problem updating the users package');  
                }          
            }
            return true;
        }

        return false;
        // }
    }

    public function updatePackage($package, $trail = false){
        $month = "2629743";
        $minute = "60";
        $now = time();

        
        $fields = array('package_id' => $package, 'package_expiry' => $now + $month);
        if($trail){
            $fields["hadTrail"] = 1;
        }

        $id = $this->data()->user_id;

        if(!$this->_db->update('user', 'user_id', $id, $fields)){
            throw new Exception('There was a problem updating the users package');  
        }
    }

    public function getTrail(){
        return $this->data()->hadTrail;
    }



}

<?php

class DataInventory{
    private $_inventory = array(
                'item' => array(),
                'last_updated'
            ),
            $_modifiedAfter,
            $_apiObject,
            // $_date,
            $_xero,
            $_instance,
            $_api,
            $_db;

    public function __construct($modifiedAfter){
        $this->_db = DB::getInstance();
        $this->_apiId = Companies::getApi();
        $this->_modifiedAfter = $modifiedAfter;
    }

    private function setApiObject(){
        switch ($this->_apiId) {
            //xero
            case 1:
                $this->_xero = Xero::getInstance();
                $this->_instance = $this->_xero->getApiInstance();
                if($this->_modifiedAfter == null){
                    $this->_apiObject = $this->_instance->getItems(Companies::getActiveTenantId());
                }
                else{
                    $dateFormat = str_replace('-', ',', $this->_modifiedAfter);
                    $this->_apiObject = $this->_instance->getItems(Companies::getActiveTenantId(), '', 'updateddateutc >= DateTime('.$dateFormat.')');
                }
            break;

            //sage
            case 2:
                $this->_sage = Sage::getInstance();
                $provider = $this->_sage->getProvider();
                $this->_apiObject = $provider->Items()->get(Session::get('active_tenant_id'));
            break;

            //quickbooks
            case 3:
                $this->_quickbooks = Quickbooks::getInstance();
                $dataService = $this->_quickbooks->getDataService();
                $this->_apiObject = $dataService->FindAll('Item', 1, 500);
            break;
            
        }
        
    }
    
    private function setInventory(){
        $this->setApiObject();

        switch ($this->_apiId) {
            //xero
            case 1:
                $items = $this->_apiObject->getItems();
                // var_dump($items);
                // die();
                for($i = 0; $i < count($items); $i++){
                    $this->_inventory['item'][$i]['name'] = $items[$i]->getName();
                    $this->_inventory['item'][$i]['quantity_on_hand'] = $items[$i]->getQuantityOnHand();
                    $this->_inventory['item'][$i]['sell_price'] = $items[$i]->getSalesDetails()->getUnitPrice();
                    $this->_inventory['item'][$i]['purchase_price'] = $items[$i]->getPurchaseDetails()->getUnitPrice();
                    $this->_inventory['item'][$i]['item_code'] = $items[$i]->getCode();
                }
            break;

            //sage
            case 2:
                $items = $this->_apiObject->results();
                for($i = 0; $i < count($items); $i++){
                    $this->_inventory['item'][$i]['name'] = $items[$i]->Description;
                    $this->_inventory['item'][$i]['quantity_on_hand'] = $items[$i]->QuantityOnHand;
                    $this->_inventory['item'][$i]['sell_price'] = $items[$i]->PriceInclusive;
                    $this->_inventory['item'][$i]['purchase_price'] = null;
                    $this->_inventory['item'][$i]['item_code'] = $items[$i]->ID;
                }
            break;

            //quickbooks
            case 3:
                $items = $this->_apiObject;
                for($i = 0; $i < count($items); $i++){
                    $this->_inventory['item'][$i]['name'] = $items[$i]->Description;
                    $this->_inventory['item'][$i]['quantity_on_hand'] = $items[$i]->QtyOnHand;
                    $this->_inventory['item'][$i]['sell_price'] = $items[$i]->UnitPrice;
                    $this->_inventory['item'][$i]['purchase_price'] = $items[$i]->PurchaseCost;
                    $this->_inventory['item'][$i]['item_code'] = $items[$i]->Name;
                }
            break;
            
        }
    }

    public function updateDb(){
        $this->setInventory();
        
        $today = new DateTime();
        $now = $today->format('Y/m/d H:i');

        for($i = 0; $i < count($this->_inventory['item']); $i ++){
            $dbQuery = $this->_db->get('temp_items', array(
                'item_code' => array('operator' => '=', 'value' => $this->_inventory['item'][$i]['item_code']), 
                'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
            ));
            if($dbQuery->results()){
                $data = $dbQuery->results();
                $id = $data[0]->item_id;
                // echo 'Updating ' . $this->_inventory['item'][$i]['name'] . '...<br>';
                $dbUpdate = $this->_db->update('temp_items', 'item_id', $id, array(
                    'item_name' => $this->_inventory['item'][$i]['name'],
                    'quantity_on_hand' => $this->_inventory['item'][$i]['quantity_on_hand'],
                    'selling_price' => $this->_inventory['item'][$i]['sell_price'],
                    'purchase_price' => $this->_inventory['item'][$i]['purchase_price'],
                    'last_updated' => $now
                ));
            }
            else{
                // echo 'Inserting ' . $this->_inventory['item'][$i]['name'] . '...<br>';
                $dbInsert = $this->_db->insert('temp_items', array(
                    'item_code' => $this->_inventory['item'][$i]['item_code'],
                    'company_id' => Companies::getActiveCompanyId(),
                    'item_name' => $this->_inventory['item'][$i]['name'],
                    'quantity_on_hand' => $this->_inventory['item'][$i]['quantity_on_hand'],
                    'selling_price' => $this->_inventory['item'][$i]['sell_price'],
                    'purchase_price' => $this->_inventory['item'][$i]['purchase_price'],
                    'last_updated' => $now
                ));
            }
        }
    }

    public function getInventory(){
        $updates = array();
        $dbQuery = $this->_db->get('temp_items', array(
            'company_id' => array('operator' => '=', 'value' => Companies::getActiveCompanyId())
        ));
        if($dbQuery->results()){
            for($i = 0; $i < count($dbQuery->results()); $i++){
                $this->_inventory['item'][$i]['name'] = $dbQuery->results()[$i]->item_code;
                $this->_inventory['item'][$i]['quantity_on_hand'] = $dbQuery->results()[$i]->quantity_on_hand;
                $this->_inventory['item'][$i]['sell_price'] = $dbQuery->results()[$i]->selling_price;
                $this->_inventory['item'][$i]['purchase_price'] = $dbQuery->results()[$i]->purchase_price;  
                array_push($updates, $dbQuery->results()[$i]->last_updated);
            }
            if($updates != null){
                $oldestUpdate = min($updates);
            }
            if(count((array)$oldestUpdate) != 1){
                $oldestUpdate = $oldestUpdate[0];
            }
            $this->_inventory['last_updated'] = $oldestUpdate;
        }

        // var_dump($this->_inventory);
        // die();

        return $this->_inventory;
    }

    public function getXeroInventory(){
        $this->setInventory();
        return $this->_inventory;
    }
}
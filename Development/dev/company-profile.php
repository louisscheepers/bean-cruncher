<?php
require_once(__DIR__ . '/core/init.php');

$user = new User();
if($user->isLoggedIn()){
    $_SESSION['active_role_id'] = Companies::getUserCompanyRole();
    $role = Session::get('active_role_id');
}
else{
    Redirect::to('index.php');
}

if(Companies::getActiveCompanyId() == null){
    $companies = Companies::getCompanies();
    if($companies == null){
        Redirect::to('integration.html');
    }
    else{
        Companies::setActiveCompany($comapnies[0]['id']);
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Company Profile</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> 
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
        
        <link href="css/custom.css" rel="stylesheet" />
    </head>
    <body class="content-bg">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
               
            <nav class="sb-topnav navbar navbar-expand nav-mobile-p navbar-dark p-0 my-3 mobile-mx">    
                    <div class="container nav-size">
                        <div class="row d-content no-gutters">
                            <!-- First Colum with nested colums -->
                            <div class="col-3 col-lg-5">
                                <div class="row no-gutters">
                                    <div class="col-6 col-lg-2 align-self-center">
                                        <a type="button" class="btn-lg d-content" href="index.php">
                                            <img class="img-fluid arrow-icon-size pl-1" src="img/arrow-left-icon.png">
                                        </a>
                                    </div>

                                    <div class="col-sm-10 align-self-center d-none d-lg-block">
                                        <div class="dropdown d-none d-md-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                                        <img class="img-fluid search-icon-small" src="img/Group 1511@2x.png">
                                            <input class="search-element" type="text" id="companySearch" autocomplete="off" onkeyup="search()" placeholder="Find company..." title="List of Companies" data-toggle="dropdown">
                                            <i class="fas fa-chevron-down search-icon-dropdown"></i>
                                            <div id="companiesDropDown" class="dropdown-menu radius-0 r-0" aria-labelledby="dropdownMenuButton">
                                            <?php
                                                $companies = Companies::getCompanies();
                                                for($i = 0; $i < count((array)$companies); $i++){
                                                    if($companies[$i]['status'] == 1){
                                                    ?>
                                                    <li>
                                                    <a id="active-company-dropdown" class="dropdown-item" onclick="loadCompany('<?php echo str_replace('\'', '\\\'', $companies[$i]['name']) ?>', <?php echo $companies[$i]['id']; ?>, true)"><?php echo $companies[$i]['name']; ?></a>
                                                    </li>
                                                    <?php
                                                    }
                                                }
                                            ?> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6 d-lg-none align-self-center txt-center-nav">
                                        <div class="dropdown">
                                            <a class="a-icon-sm" href="#" role="button" onclick="mobileSearch()">
                                                <img class="img-fluid search-icon-sm" src="img/Search Icon.png">
                                            </a>
                                            <div id="mbSearch" class="dr-radius dropdown-menu collapse d-lg-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                                                <input class="search-element companyMobileSearch" type="text" id="companySearch" onkeyup="search()" placeholder="Find company..." title="List of Companies">
                                                <div id="companiesDropDown" class="r-0" aria-labelledby="dropdownMenuButton">
                                                <?php
                                                    $companies = Companies::getCompanies();
                                                    for($i = 0; $i < count((array)$companies); $i++){
                                                        if($companies[$i]['status'] == 1){
                                                        ?>
                                                        <li>
                                                            <a id="active-company-dropdown" class="dropdown-item" onclick="loadCompany('<?php echo str_replace('\'', '\\\'', $companies[$i]['name']) ?>', <?php echo $companies[$i]['id']; ?>)"><?php echo $companies[$i]['name']; ?></a>
                                                        </li>
                                                        <?php
                                                        }
                                                    }
                                                ?> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Second Colum with nested colums -->    
                            <div class="col-7 col-lg-2 text-center ml-m10">
                                <h5 class="nav-settings d-content txt-color">
                                    Company Profile
                                </h5>
                            </div>

                            <!-- Third Colum with nested colums -->
                            <div class="col-2 col-lg-5">
                                <div class="row">
                                    <div class="col-lg-1 d-none d-lg-block">
                                        <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0 "></div>
                                    </div>
                                    
                                    <div class="col-lg-9 txt-align-right align-self-center d-none d-lg-block">
                                        <h5 id="companyDropDown" class="nav-settings d-content">
                                            <?php echo Companies::getActiveCompanyName() ?>
                                        </h5>
                                    </div>
                                    <div class="col-12 col-lg-2 align-self-center">
                                        <div id="nav-comp-icon" class="float-right">
                                            <img id="nav-comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">
                                        </div>
                                      </div>
                                </div>
                             </div>
                        </div>
                    </div>
            </nav>
            
                <main>
                    <div class="container my-5 p-0">
                       <div class="row mt-lg-1 m-card-m">
                           <div class="col-12 mt-lg-1">
                               <div class="card mobile-mx">
                                  <div class="card-body profile-card">
                                    <div class="row no-gutters pt-4">
                                        <img src="img/Management roles icon.png" class="img-fluid profile-icon-size d-none d-lg-block"> 
                                        <div class="col-sm-10 txt-inline mt-n1">
                                            <h5 id="companyTitle" class="font-weight-normal align-middle txt-color">
                                                <span id="company-title"><?php echo Companies::getActiveCompanyName() ?> </span>
                                            </h5>
                                            <p class="txt-color font-weight-200 font-size-14">
                                                This information will appear on your company profile
                                            </p>
                                        </div>  
                                    </div>
                                        <hr>
                                        <?php
                                            if($role == 3){
                                                $hidden = 'hidden';
                                                $err = '';
                                            }
                                            else{
                                                $hidden = '';
                                                $err = 'hidden';
                                            }
                                        ?>
                                        <div id="err" <?php echo $err; ?>>
                                            <p class="my-auto txt-color font-weight-200 font-size-14">
                                                You don't have permission to edit this company's information
                                            </p>
                                        </div>
                                        <form method="POST" enctype="multipart/form-data" id="company-settings" <?php echo $hidden; ?> class="row p-5 mb-p-0">
                                            <div class="col-12 col-lg-4 mb-mt-1">
                                                <h5 class="txt-color font-weight-bold">Profile information</h5>
                                                <input name="company-name" id="company-name" type="text" class="form-control mt-6 px-4" placeholder="Name" required> 
                                                <input name="company-email" id="company-email" type="email" class="form-control my-3 px-4" placeholder="Company email">
                                                <button type="submit" name="upload-details" class="btn btn-cross-platform my-3 w-40" accept="image/*">Update</button>
                                                <?php 
                                                    if(isset($_POST['upload-details'])){
                                                        $name = trim($_POST['company-name']);
                                                        $email = $_POST['company-email'];
                                                        if($_FILES['cpfp']['tmp_name']){
                                                            $img_type = exif_imagetype($_FILES['cpfp']['tmp_name']);
                                                            if($img_type > 0 && $img_type <= 3){
                                                                if($img_type == 1){
                                                                    $image = imagecreatefromgif($_FILES['cpfp']['tmp_name']);
                                                                    imagejpeg($image, $_FILES['cpfp']['tmp_name']);
                                                                    uploadFileContents($name, $email);
                                                                }
                                                                else if($img_type == 2){
                                                                    uploadFileContents($name, $email);
                                                                }
                                                                else if($img_type == 3){
                                                                    $image = imagecreatefrompng($_FILES['cpfp']['tmp_name']);
                                                                    $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
                                                                    imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                                                                    imagealphablending($bg, TRUE);
                                                                    imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
                                                                    imagedestroy($image);
                                                                    imagejpeg($bg, $_FILES['cpfp']['tmp_name'], 70);
                                                                    uploadFileContents($name, $email);
                                                                    imagedestroy($bg);
                                                                }
                                                            }
                                                            else{
                                                                echo '<span class="font-size-12 font-weight-200 font-lb-color mx-2">Invalid format</span>';
                                                            }
                                                        }
                                                        else{
                                                            Companies::updateCompany(array('company_name' => $name, 'company_email' => $email));
                                                        }
                                                    }
                                                    function uploadFileContents($name, $email){
                                                        $data = file_get_contents($_FILES['cpfp']['tmp_name']);
                                                        Companies::updateCompany(array('company_brand' => $data, 'company_name' => $name, 'company_email' => $email));
                                                    }
                                                ?>
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div id="companyImage" class="col-12 col-lg-6 mb-mt-1 mb-mb-0">
                                                <h5 class="txt-color font-weight-bold">Upload a New Company Avatar</h5>
                                                <p class="txt-color font-weight-200 font-size-14">You can upload or change your avater here</p>
                                                <div class="row mt-5">
                                                    <div class="col-2 align-self-center">
                                                        <div id="comp-icon">
                                                            <img id="comp-img-up" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">
                                                        </div>
                                                    </div>
                                                    <div class="col-10">
                                                            <div class="input-group">
                                                                <div class="custom-file">
                                                                    <input type="file" onchange="readURL(this)" class="custom-file-input" name="cpfp" id="cpfp">
                                                                    <label id="comp-file" class="custom-file-label field-img-upload font-weight-200" for="cpfp">
                                                                       Choose file
                                                                       <img class="img-fluid dropdown-icon-upload" src="img/dropdown.png">
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        <p class="font-size-12 font-weight-200 font-lb-color my-2">The maximum allowed file size is 200KB.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="loader" class="text-center"><img src="img/BC-preloader-gif.gif" style="height: 40px;"></div>
                                </div>
                           </div>
                       </div>
                    </div>
                </main>
            </div>
        </div>

        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                        <a class="fs-12 font-weight-200 text-white">Designed by </a>
                        <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Company Settings Script -->
        <script src="js/company-profile.js"></script>

        <!-- Simple Search Script -->
        <script src="js/simpleSearch.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    </body>
</html>

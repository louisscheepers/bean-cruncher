<?php
require_once __DIR__ . '/core/init.php';

$user = new User();

if($user->isLoggedIn()){
    if(Input::exists('GET')){
        $checkoutId = Input::get('id');
        $pay = new PeachPay('ZAR', 'DB');
        $pay->setCheckoutId($checkoutId);
        $pay->getPayStatus($pay->getCheckoutId());
        $response = $pay->getPayStatusResponse();
        // var_dump($response->parameterErrors[0]);
        // var_dump($response->result);
        // var_dump($checkoutId);
        // var_dump($response);


        $user = new User();
        $validate = PayValidation::checkResponseCode($response->result->code);

        $db = DB::getInstance();
        $packageTypes = $db->get('package_types', array('package_id', ">", 0));

        if($packageTypes->count()){
            $packageTypes = $packageTypes->results();
        }

        if($response->amount == $packageTypes[2]->amount){
            $package = $packageTypes[2]->package_id;
        }else{
            $package = $packageTypes[3]->package_id;
        }
        
        // var_dump($validate);
        if($validate->isSuccess){
            $customer = new PeachCustomer();
            if(!$customer->exists()){
                $customer->create(array(
                    'reg_id' => $response->registrationId,
                    'user_id' => $user->data()->user_id,
                    'plan' => $package,
                    'term' => 'monthly',
                    'updated' => date('Y-m-d H:i:s'),
                    'created' => date('Y-m-d H:i:s')
                ));

                $customer = new PeachCustomer();
            }else{
                $customer->update($customer->data()->id, array(
                    'reg_id' => $response->registrationId,
                    'updated' => date('Y-m-d H:i:s')
                ));

                $customer = new PeachCustomer();
            }

            $transaction = new PeachTransactions();

            $transaction->create(array(
                'transaction_id' => $response->merchantTransactionId,
                'customer_id' => $customer->data()->id,
                'type' => $response->recurringType,
                'currency' => $response->currency,
                'amount' => $response->amount,
                'status' => 'Success',
                'date' => date('Y-m-d H:i:s')
            ));

            $user->updatePackage($package);
        }

        

        ?>

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- Bootstrap -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
            <!-- Bootstrap -->
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
            <!-- Custom  CSS-->
            <link href="css/style.css" type="text/css" rel="stylesheet">
            <!-- Font Awesome -->
            <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
            <!-- Chartist -->
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
            <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

            <title>BC | Checkout</title>
        </head>
        <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
        <div class="container pt-5" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-6 mt-6">
                <div class="col-11 col-sm-12 col-md-10 col-lg-6 col-xl-6 card rounded-0-1 my-auto custom-card white-boxShadow">
                    <!-- Logo -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-8 col-sm-6 col-md-6 col-lg-6 col-xl-6 logo-size-int">
                            <a href=""><img src="img/logo.png" class="img-fluid" alt="logo"></a>
                        </div>
                    </div>
                    <!-- Heading -->
                    <div class="row d-flex justify-content-center verify-b">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <span><?php echo $validate->message; ?></span>
                        </div>
                    </div>
                    <!-- Lock -->
                    <div class="row d-flex justify-content-center kwl-icon">
                        <div class="col-3 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-0">
                            <a href=""><img src="img/Integration.png" class="img-fluid" alt="logo"></a>
                        </div>
                    </div>
                    <!-- Brand text -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center we-sent-row">
                                <p class="verify-txt-color fs-13">Thank you for using Beancruncher</b></p>
                            </div>
                        </div>
                    </div>

                    <div class="row d-flex justify-content-center int-btn-b">
                        <div class="col-5 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <a href="index.php" class="btn btn-block nav-free-but the-border main-purple py-2 fs-12" >Done</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>

        </body>
        </html>

        <?php

    }else{
       Redirect::to('index.php');
    }
}else{
    Redirect::to('index.php');
}


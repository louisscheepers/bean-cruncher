<?php
require __DIR__ . '/core/init.php';

$resultMsg = '';

$pdfFiles = new FilesystemIterator(__DIR__ . '/tmp/pdf');
$pptxFiles = new FilesystemIterator(__DIR__ . '/tmp/pptx');

try{
    FileCleanup::clean($pdfFiles, "tmp/pdf", 7);
    $resultMsg .= '<br>Successfully removed old PDF documents';
}catch (Exception $e){
    $resultMsg .= '<br>Error while clearing PDFs: ' . $e;
}

try{
    FileCleanup::clean($pptxFiles, "tmp/pptx", 7);
    $resultMsg .= '<br>Successfully removed old PPTX documents';
}catch (Exception $e){
    $resultMsg .= '<br>Error while clearing PPTXs: ' . $e;
}

$email = new Email('webmaster@beancruncher.co.za');
$email::sendCustom('stiaan@cubezoo.joburg', 'File Cleaner Report', $resultMsg);
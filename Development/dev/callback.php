<?php
  ini_set('display_errors', 'On');
  require_once(__DIR__ . '/core/init.php');
  require_once('const/const.php');
  // Storage Classe uses sessions for storing token > extend to your DB of choice
  $storage = new ApiStorage();  

  $user = new User();
  
  ////$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
  if($user->isLoggedIn()){
    ////$logger->debug('callback.php START - User is logged in!');
  }
  else{
    ////$logger->debug('callback.php START - User NOT logged in!');
  }

  $provider = new \League\OAuth2\Client\Provider\GenericProvider([
    'clientId'                =>  __XERO_CLIENT_ID__,   
    'clientSecret'            =>  __XERO_SECRET__,
    'redirectUri'             =>  __REDIRECT_URI__, 
    'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
    'urlAccessToken'          => 'https://identity.xero.com/connect/token',
    'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
  ]);

  $_SESSION['oauth2state'] = $provider->getState();
   
  // If we don't have an authorization code then get one
  if (!isset($_GET['code'])) {
    exit("Something went wrong, no authorization code found");

  // Check given state against previously stored one to mitigate CSRF attack
  } else if (isset($_SESSION['oauth2state'])) {
    if(empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
      echo 'State !== oauth2state<br>';
      echo 'State: ' . $_GET['state'] . '<br>';
      echo 'Oauth2state: ' .  $_GET['oauth2state'] . '<br>';

      // unset($_SESSION['oauth2state']);
      exit('Invalid state');
    }
  } else {
  
    try {
      // Try to get an access token using the authorization code grant.
      $accessToken = $provider->getAccessToken('authorization_code', [
        'code' => $_GET['code']
      ]);
           
      $config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( (string)$accessToken->getToken() );
    
      $config->setHost("https://api.xero.com"); 
      $identityInstance = new XeroAPI\XeroPHP\Api\IdentityApi(
        new GuzzleHttp\Client(),
        $config
      );
       
      $result = $identityInstance->getConnections();

      $tenantIds = array();
      for($i = 0; $i < count($result); $i++){
        $tenantId = strval($result[$i]->getTenantId());
        array_push($tenantIds, $tenantId);
      }

      // Save my tokens, expiration tenant_id
      $storage->setToken(
          $accessToken->getToken(),
          $accessToken->getExpires(),
          $result[0]->getTenantId(),
          $accessToken->getRefreshToken(),
          $accessToken->getValues()["id_token"]
      );

      Session::put('tenants', $tenantIds);

      ////$logger->debug('callback.php END');

      Redirect::to('callbackLink.php');
      exit();
     
    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
      echo "Callback failed";
      exit();
    }
  }
?>
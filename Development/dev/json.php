<?php
ini_set('display_errors', 'On');
require_once('const/const.php');
require_once(__DIR__ . '/core/init.php');
// header('Content-Type: application/json');
$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');

$auth = new Oauth();
$auth->setSessionOauthFromDb();

$fromDate = Input::get('from_date');

// ****
// POST
// ****

if(Input::exists('POST')){
    if(isset($_POST['favourite'])){
        if($_POST['favourite'] == 'fetch'){
            echo json_encode(Favourites::getFavourites());
        }
        if($_POST['favourite'] == 'delete'){
            $names = $_POST['names'];
            for($i = 0; $i < count($names); $i++){
                Favourites::deleteFavourite($names[$i]);
            }

            echo json_encode(true);
        }
    }

    if(isset($_POST['company'])){

        $id = null;
        if(isset($_POST['id'])){
            $id = $_POST['id'];
        }

        //get company data
        if($_POST['company'] == 'fetch'){
            $data = Companies::getActiveCompanyInfo($id);
            $img = $data['company_brand'];
            $data['company_brand'] = '<img id="comp-img" src="data:image/jpeg;base64,'.base64_encode( $img ).'" class="rounded-circle icon-size" alt="Company Brand">';
            $user = new User();
            $user = $user->data();
            $data['username'] = $user->user_name;
            $data['surname'] = $user->user_surname;
            $userimg = $user->user_img;
            $data['userimage'] = '<img id="user-img" src="data:image/jpeg;base64,'.base64_encode( $userimg ).'" class="rounded-circle icon-size" alt="Profile Icon">';
            
            echo json_encode($data);
        }
        //update company data
        else if($_POST['company'] == 'update'){
            $name = $_POST['name'];
            $email = $_POST['email'];
    
            $arr = array(
                'company_name' => $name,
                'company_email' => $email
            );
    
            if($name == null){
                unset($arr['company_name']);
            }
            if($email == null){
                unset($arr['company_email']);
            }
    
            Companies::updateCompany($arr);
    
            echo json_encode(true);
        }
    }
    if(isset($_POST['profile'])){
        //get profile data
        if($_POST['profile'] == 'fetch'){
            $user = new User();
            $data = $user->data();
            $img = $data->user_img;
            $data->user_img = '<img id="user-img" src="data:image/jpeg;base64,'.base64_encode( $img ).'" class="rounded-circle icon-size" alt="Profile Icon">';
            
            echo json_encode($data);
        }
        
        //update profile data
        else if($_POST['profile'] == 'update'){
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $number = $_POST['num'];
            $email = $_POST['email'];
    
            $arr = array(
                'user_name' => $name,
                'user_surname' => $surname,
                'user_number' => $number,
                'user_email' => $email
            );
    
            if($name == null){
                unset($arr['user_name']);
            }
            if($surname == null){
                unset($arr['user_surname']);
            }
            if($number == null){
                unset($arr['user_number']);
            }
            if($email == null){
                unset($arr['user_email']);
            }
    
            $user = new User();
            $user->update($arr);
    
            echo json_encode(true);
        }
        //upload pfp
        else{
            $file = addslashes(file_get_contents($_FILES["image"]["tmp_name"]));

            $user = new User();
            $user->update(
                array(
                    'user_img' => $file
                )
            );

            echo 'Updated profile picture';
        }
    }
}

//REFINEMENT

// ****
// GET
// ****

else if(Input::exists('GET')){
    // OLD CODE START
    
    // get comments array
    if(isset($_GET['gCom'])){
        $module = $_GET['gCom'];
        $response = Comments::getComments($module);
        echo json_encode($response);
    }

    //edit comment id
    if(isset($_GET['eCom'])){
        $id = $_GET['eCom'];
        $comment = $_GET['com'];
        Comments::editComment($id, $comment);
    }

    // set new comment
    if(isset($_GET['sCom'])){
        $module = $_GET['sCom'];
        $comment = $_GET['com'];
        Comments::setComment($module, $comment);
    }

    // delete comment id
    if(isset($_GET['dCom'])){
        $commentId = $_GET['dCom'];
        Comments::deleteComment($commentId);
    }

    // get is updated
    if(isset($_GET['isCompUpdated'])){
        $updated = Companies::getUpdatedModules($_GET['isCompUpdated']);

        echo json_encode($updated);
    }

    //get has user logged
    if(isset($_GET['hasLogged'])){
        echo json_encode($user->hasLoggedIn());
    }

    // get grpah data from db
    if(isset($_GET['q'])){
        if(isset($_GET['from_date'])){
            $fromDate = Input::get('from_date');
            $toDate = Input::get('to_date');
            $action = Input::get('q');
            $box = Input::get('box');
        
            if($fromDate > $toDate){
                $data = PullRange::getData($action, $fromDate, $toDate, true);
            }
            else{
                $data = PullRange::getData($action, $fromDate, $toDate);
            }
    
            if($toDate != null){
                $months = DateLabels::getMonthRange();
                $years = DateLabels::getYearRange();

                for($i = 0; $i < count($months); $i++){
                    $labels[$i] = date('M', mktime(0, 0, 0, $months[$i], 10)) . ' ' . $years[$i];
                }
                if($fromDate > $toDate){
                    $labels = array_reverse($labels);
                }
                $response = array(
                    'labels' => $labels,
                    'data' => $data
                );
            }
            else{
                $response = $data;
            }

            $arr = array($action, $fromDate, $toDate);
            Session::put('dataBox'.$box, $arr);
            echo json_encode($response);
        }

        if($_GET['q'] == 'update'){
            if(isset($_GET['mod']) && $_GET['mod'] == 'all'){

                try{
                    //$logger->info("STARTING COMPANY IMPORT");
                    //$logger->info("Setting session form DB");
                    $oauth = new Oauth();
                    $oauth->setSessionOauthFromDb();
                    //$logger->info("updating modules");
                    Companies::setUpdatedModules(1);
                    $user = new User();
                    $email = $user->data()->user_email;
                    //$logger->info("getting email -> " . $email);
                    //$logger->info("sending email to notify about import");
                    Email::sendPreset($email, 'import');
    
                    $interval = $_GET['int'];
                    //$logger->info("Getting interval | " . $interval . "months");
                    $today = new DateTime('today');
                    $from = new DateTime('first day of -' . ($interval - 1) . ' month');
                    $from = $from->format('Y-m-d');
                    
                    //$logger->info("Updating Chart of Accounts");
                    //Chart of Accounts
                    $coa = new DataChartOfAccounts(null);
                    $coa->updateDb();
                    //$logger->info("Done");
                    //$logger->info("Updating Inventory");
                    //Inventory
                    $inv = new DataInventory($from);
                    $inv->updateDb();
                    
                    //$logger->info("Done");
                    //$logger->info("Updating Invoices");
    
                    //Invoices + Line Items
                    $invoices = new DataInvoices(null);
                    $invoices->updateDb();
    
                    //$logger->info("Done");

                    if(Companies::getApi() == 2 || Companies::getApi() == 3){
                        //$logger->info("Updating Cash Received + spent");
            
                        //Cash Received + Spent
                        for($i = 0 ; $i < 12; $i++){
                            $month = new DateTime('first day of -' . ($i) . ' month');
                            //$logger->info("Getting Cash Recieved for" .  $month->format('Y-m-d'));
                            $cr = new DataCashReceived($month->format('Y-m-d'));
                            $cr->updateDb();
                            //$logger->info("Getting Cash Spent for" .  $month->format('Y-m-d'));
                            $cs = new DataCashSpent($month->format('Y-m-d'));
                            $cs->updateDb();
                        }
                        //$logger->info("Done");
                    }
        
                    if(Companies::getApi() == 3){
                        for($i = 0 ; $i < 12; $i++){
                            $month = new DateTime('first day of -' . ($i) . ' month');

                            //$logger->info("Updating Profit and Loss for " . $month->format('Y-m-d') . " to " . $month->format('Y-m-t'));
                            $pl = new DataProfitAndLoss($month->format('Y-m-d'), $month->format('Y-m-t'), 11);
                            $pl->updateDb();

                            //$logger->info("Updating Balance Sheet for " . $month->format('Y-m-t'));
                            $bs = new DataBalanceSheet($month->format('Y-m-t'), 11);
                            $bs->updateDb();
                        }
                        //$logger->info("Done");
                    }
                    else{
                        //$logger->info("Updating Balancesheet profit and loss");
                        //Balance Sheet, Profit and Loss
                        //$logger->info("Updating for " . $interval . " months");
        
                        if($interval == 12){
                            $firstDayThis = new DateTime('first day of this month');
    
                            //$logger->info('Days in month: ' . $firstDayThis->format('t'));
                            if($firstDayThis->format('t') != '31'){
                                $firstDayThis = new DateTime('first day of last month');
                            }
    
                            //$logger->info('Importing Balance Sheet for ' . $firstDayThis->format('Y-m-d'));
                            $bs = new DataBalanceSheet($firstDayThis->format('Y-m-d'), 11);
                            $bs->updateDb(false);
                            //$logger->info('Done');
    
                            //$logger->info('Importing Profit and loss for ' . $firstDayThis->format('Y-m-d') . ' to ' . $firstDayThis->format('Y-m-t'));
                            $pl = new DataProfitAndLoss($firstDayThis->format('Y-m-d'), $firstDayThis->format('Y-m-t'), 11);
                            $pl->updateDb();
                            //$logger->info('Done');
      
                        }
                        else if($interval == 24){
                            $firstDayThis = new DateTime('first day of this month');
                            $firstDay12 = new DateTime('first day of -12 month');
    
                            //$logger->info('Days in month: ' . $firstDayThis->format('t'));
                            if($firstDayThis->format('t') != '31'){
                                $firstDayThis = new DateTime('first day of last month');
                                $firstDay12 = new DateTime('first day of -13 month');
                            }
                            
    
                            //$logger->info('Importing Balance Sheet for ' . $firstDayThis->format('Y-m-d'));
                            $bs = new DataBalanceSheet($firstDayThis->format('Y-m-d'), 11);
                            $bs->updateDb(false);
        
                            //$logger->info('Importing Balance Sheet for ' . $firstDay12->format('Y-m-d'));
        
                            $bs2 = new DataBalanceSheet($firstDay12->format('Y-m-d'), 11);
                            $bs2->updateDb(false);
                            
    
                            //$logger->info('Importing Profit and loss for ' . $firstDayThis->format('Y-m-d') . ' to ' . $firstDayThis->format('Y-m-t'));
        
                            $pl = new DataProfitAndLoss($firstDayThis->format('Y-m-d'), $firstDayThis->format('Y-m-t'), 11);
                            $pl->updateDb();
        
                            //$logger->info('Importing Profit and loss for ' . $firstDay12->format('Y-m-d') . ' to ' . $firstDay12->format('Y-m-t'));
        
                            $pl2 = new DataProfitAndLoss($firstDay12->format('Y-m-d'), $firstDay12->format('Y-m-t'), 11);
                            $pl2->updateDb();
                        }
                        else{
                            $firstDayThis = new DateTime('first day of this month');
                            $firstDay12 = new DateTime('first day of -12 month');
                            $firstDay24 = new DateTime('first day of -24 month');
                            //$logger->info('Days in month: ' . $firstDayThis->format('t'));
                            if($firstDayThis->format('t') != '31'){
                                $firstDayThis = new DateTime('first day of last month');
                                $firstDay12 = new DateTime('first day of -13 month');
                                $firstDay24 = new DateTime('first day of -25 month');
                            }
    
                            //$logger->info('Importing Balance Sheet for ' . $firstDayThis->format('Y-m-d'));
                            $bs = new DataBalanceSheet($firstDayThis->format('Y-m-d'), 11);
                            $bs->updateDb(false);
                            //$logger->info('Importing Balance Sheet for ' . $firstDay12->format('Y-m-d'));
                            $bs2 = new DataBalanceSheet($firstDay12->format('Y-m-d'), 11);
                            $bs2->updateDb(false);
                            //$logger->info('Importing Balance Sheet for ' . $firstDay24->format('Y-m-d'));
                            $bs3 = new DataBalanceSheet($firstDay24->format('Y-m-d'), 11);
                            $bs3->updateDb(false);
                            //$logger->info('Importing Profit and loss for ' . $firstDayThis->format('Y-m-d') . ' to ' . $firstDayThis->format('Y-m-t'));
                            $pl = new DataProfitAndLoss($firstDayThis->format('Y-m-d'), $firstDayThis->format('Y-m-t'), 11);
                            $pl->updateDb();
                            //$logger->info('Importing Profit and loss for ' . $firstDay12->format('Y-m-d') . ' to ' . $firstDay12->format('Y-m-t'));
                            $pl2 = new DataProfitAndLoss($firstDay12->format('Y-m-d'), $firstDay12->format('Y-m-t'), 11);
                            $pl2->updateDb();
                            //$logger->info('Importing Profit and loss for ' . $firstDay24->format('Y-m-d') . ' to ' . $firstDay24->format('Y-m-t'));
                            $pl3 = new DataProfitAndLoss($firstDay24->format('Y-m-d'), $firstDay24->format('Y-m-t'), 11);
                            $pl3->updateDb();
                        }
                    }
                    
                    //$logger->info("Done");
                    //$logger->info("Sending Email to notify import complete");

                    Companies::setUpdatedModules(2);
                    Email::sendPreset($email, 'importfin');
                    //$logger->info("IMPORT FINISHED");
                    
                    echo json_encode('UPDATED');
                }
                catch(Exception $e){
                    //$logger->error("CAUGHT ERROR: " . $e->getMessage());
                    Companies::setUpdatedModules(3);

                    DB::getInstance()->update('temp_company', Companies::getActiveCompanyId(), 'company_id', array(
                        'update_error' => $e->getMessage()
                    ));

                    echo json_encode('CAUGHT ERROR');
                }
            }
        }
        else if($_GET['q'] == 'delete'){
            if(isset($_GET['box'])){
                $box = $_GET['box'];
                $_SESSION['dataBox'.$box] = null;
                
                if($box == 0){
                    for($i = 1; $i <= 8; $i++){
                        $_SESSION['dataBox' . $i] = null;
                    }
                }



                // $data = Session::get('data');
                // if($box == 0){
                //     for($i = 1; $i <= 8; $i++){
                //         $data[$i] = null;
                //     }
                // }
                // else{
                //     $data[$box] = null;
                // }
                // Session::put('data', $data);
            }
        }


    }

    // update module data db
    if(isset($_GET['uq'])){
        $fromDate = Input::get('from_date');
        $toDate = Input::get('to_date');
        $action = Input::get('uq');
        
        //$logger->debug('Module to update => ' . $action);
        //$logger->debug('from date => ' . $fromDate . ' ' . $toDate);
        $oauth = new Oauth();
        $oauth->setSessionOauthFromDb();

        try{
            $data = UpdateDB::setData($action, $fromDate, $toDate);
            $response = $data;

            if($response == true){
                //$logger->debug('Sending back Json Response');

                echo json_encode($response);
            }
        }
        catch(Exception $e){

            DB::getInstance()->update('temp_company', Companies::getActiveCompanyId(), 'company_id', array(
                'update_error' => $e->getMessage()
            ));

            $logger->error("CAUGHT ERROR: " . $e->getMessage());

            echo json_encode($response);
        }
    }

    //get layout
    if(isset($_GET['gLay'])){
        $name = Input::get('gLay');

        if($name == 'SESSION_LAYOUT'){
            $layout = array();
            for($i = 1; $i <= 8; $i++){
                if(isset($_SESSION['dataBox'.$i])){
                    $layout[$i - 1]['module'] = $_SESSION['dataBox'.$i][0]; 
                    $layout[$i - 1]['from_date'] = $_SESSION['dataBox'.$i][1]; 
                    $layout[$i - 1]['to_date'] = $_SESSION['dataBox'.$i][2]; 
                }
                else{
                    $layout[$i - 1]['module'] = null; 
                    $layout[$i - 1]['from_date'] = null; 
                    $layout[$i - 1]['to_date'] = null; 
                }
                // if(isset($_SESSION['data'][$i])){
                //     $layout[$i - 1]['module'] = $_SESSION['data'][$i][0]; 
                //     $layout[$i - 1]['from_date'] = $_SESSION['data'][$i][1]; 
                //     $layout[$i - 1]['to_date'] = $_SESSION['data'][$i][2]; 
                // }
                // else{
                //     $layout[$i - 1]['module'] = null; 
                //     $layout[$i - 1]['from_date'] = null; 
                //     $layout[$i - 1]['to_date'] = null; 
                // }
            }
            echo json_encode($layout);
        }
        else if($name == 'DEFAULT_SESSION_LAYOUT'){
            $layout = array();
            $layout[0]['module'] = 'cico';
            $layout[0]['from_date'] = null; 
            $layout[0]['to_date'] = null; 
            $layout[1]['module'] = 'avd';
            $layout[1]['from_date'] = null; 
            $layout[1]['to_date'] = null; 
            $layout[2]['module'] = 'bs';
            $layout[2]['from_date'] = null; 
            $layout[2]['to_date'] = null; 
            $layout[3]['module'] = 'ddo';
            $layout[3]['from_date'] = null; 
            $layout[3]['to_date'] = null; 
            $layout[4]['module'] = 'tsp';
            $layout[4]['from_date'] = null; 
            $layout[4]['to_date'] = null; 
            $layout[5]['module'] = 'bsr';
            $layout[5]['from_date'] = null; 
            $layout[5]['to_date'] = null; 
            $layout[6]['module'] = 'im';
            $layout[6]['from_date'] = null; 
            $layout[6]['to_date'] = null; 
            $layout[7]['module'] = 'inas';
            $layout[7]['from_date'] = null; 
            $layout[7]['to_date'] = null; 

            echo json_encode($layout);
        }
        else{
            $response = Favourites::getLayout($name);
            echo json_encode($response);
        }
    }

    //save layout
    if(isset($_GET['sLay'])){
        $layout = Input::get('sLay');
        $name = Input::get('name');

        $array = explode(',', $layout);
        $modules = array();
        $count = 0;
        for($i = 0; $i < count($array); $i++){
            if (($i % 3) == 0){
                //Module
                $modules[$count]['module'] = $array[$i];
            }
            if ((($i % 3) - 1) == 0){
                //From
                $modules[$count]['from_date'] = $array[$i];
            }
            if ((($i % 3) - 2) == 0){
                //To
                $modules[$count]['to_date'] = $array[$i];
                $count++;
            }
        }

        $response = Favourites::setFavourite($name, $modules);
        echo json_encode($response);
    }

    //delete layout
    if(isset($_GET['dLay'])){
        $name = Input::get('dLay');
        Favourites::deleteFavourite($name);
    }

    //get favourites
    if(isset($_GET['fav'])){
        $favourites = Favourites::getFavourites();
        echo json_encode($favourites);
    }

    //get chart of accounts
    if(isset($_GET['coa'])){
        $coa = new DataChartOfAccounts(null);
        $accounts = $coa->getChartOfAccounts();
        echo json_encode($accounts);
    }

    //update an account type
    if(isset($_GET['uCoa'])){
        $id = $_GET['uCoa'];
        $newType = $_GET['type'];

        $coa = new DataChartOfAccounts(null);
        $coa->updateType($id, $newType);
    }

    //get companies
    if(isset($_GET['gComp'])){
        $companies = Companies::getCompanies();
        for($i = 0; $i < count($companies); $i++){
            unset($companies[$i]['brand']);
        }
        echo json_encode($companies);
    }

    //get company type
    if(isset($_GET['cType'])){
        $types = Companies::getTypes();
        echo json_encode($types);
    }

    if(isset($_GET['gaComp'])){
        $info = Companies::getActiveCompanyInfo();
        unset($info['company_brand']);
        $info = json_encode($info);
        echo $info;
    }

    //switch companies
    if(isset($_GET['aComp'])){
        $logger->debug('ID: ' . $id);
        $logger->debug('Setting active company...' . $id);
        $id = $_GET['aComp'];
        Companies::setActiveCompany($id);
        echo json_encode(true);
    }

    //get active role
    if(isset($_GET['aRole'])){
        $id = $_GET['aRole'];
        if($id == 'null')
            $id = null;
        echo json_encode(Companies::getUserCompanyRole($id));
    }
    //get active user
    if(isset($_GET['gaUserId'])){
        $name = $_GET['gaUserId'];
        if($name == 'null')
            $name = null;
        $user = new User($name);
        echo json_encode($user->data()->user_id);
    }

    //delete company
    if(isset($_GET['dComp'])){
        $id = $_GET['dComp'];
        Companies::deleteCompany($id);
        $companies = Companies::getCompanies();
        Companies::setActiveCompany($companies[0]['name']);
        echo json_encode(true);
    }

    //update company
    if(isset($_GET['uComp'])){
        $id = $_GET['uComp'];
        $type = $_GET['type'];
        Companies::updateType($id, $type);
    }

    //get list of users of active company
    if(isset($_GET['gUser'])){
        echo json_encode(array_values(Companies::getCompanyUsers()));
    }

    //get list of users of active company
    if(isset($_GET['cOwner']) == 'package'){
        echo json_encode(Companies::getCompanyOwnerPackage());
    }

    //get list of pending users of active company
    if(isset($_GET['pUser'])){
        echo json_encode(Companies::getPendingCompanyUsers());
    }

    //add new user
    if(isset($_GET['aUser'])){
        $email = $_GET['aUser'];
        echo json_encode(Companies::addNewUser($email, null));
    }

    //update user role 
    if(isset($_GET['uUser'])){
        $email = $_GET['uUser'];
        $newRole = $_GET['role'];
        Companies::updateUserRole($email, $newRole);

        echo json_encode(true);
    }

    //delete user
    if(isset($_GET['dUser'])){
        $email = $_GET['dUser'];
        Companies::deleteUser($email);

        echo json_encode(true);
    }

    //update user status
    if(isset($_GET['sUser'])){
        $email = Input::get('sUser');
        $status = Input::get('status');
        Companies::setUserStatus($email, $status);

        echo json_encode(true);
    }

    // OLD CODE END

    // if($_GET['q'] == 'fetch'){
        
    // }
    // else if($_GET['q'] == 'delete'){

    // }
}
else{
    echo json_encode('No valid input');
}
// if(Input::exists('POST')){
//     if($_POST['q'] == 'fetch'){
        
//     }
//     else if($_POST['q'] == 'update'){

//     }
//     else if($_POST['q'] == 'delete'){

//     }
// }
// if(Input::exists('GET')){
//     if($_GET['q'] == 'fetch'){
        
//     }
//     else if($_GET['q'] == 'update'){

//     }
//     else if($_GET['q'] == 'delete'){

//     }
// }
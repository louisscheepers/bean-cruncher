<?php
require_once(__DIR__ . '/core/init.php');

if(Input::exists()){
    $base64 = Input::get('pdf');
    $filename = Input::get('name');
    $emails = Input::get('emails');
    echo " FILE NAME " . $filename . '<br>';
    $rndname =  uniqid(rand(), true);
    # Decode the Base64 string, making sure that it contains only valid characters
    $bin = base64_decode($base64, true);

    # Perform a basic validation to make sure that the result is a valid PDF file
    # Be aware! The magic number (file signature) is not 100% reliable solution to validate PDF files
    # Moreover, if you get Base64 from an untrusted source, you must sanitize the PDF contents
    if (strpos($bin, '%PDF') !== 0) {
        throw new Exception('Missing the PDF file signature');
    }

    # Write the PDF contents to a local file
    file_put_contents('attachment/pdf/' . $rndname . '.pdf', $bin);

    // $filePath = $_SERVER['DOCUMENT_ROOT'] . '/beancruncher/dev/attachment/pdf/' . $rndname . '.pdf';
    $filePath = 'localhost/beancruncher/dev/attachment/pdf/' . $rndname . '.pdf';

    // Email::sendPreset($emails, 'report', $filePath);

    $emailArr = explode (',', $emails);
    
    for($i = 0; $i < count($emailArr); $i++){
        Email::sendPreset($emailArr[$i], 'report', $filePath);
    }

    echo 'Done';
}
else{
    echo 'No input';
}
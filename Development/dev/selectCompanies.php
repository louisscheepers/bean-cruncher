<?php
require_once __DIR__ . '/core/init.php';

$msg = '';
//$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');


$existingComps = array();
$noComp = true;
$linkCompanies = array();

$user = new User();
if(!$user->isLoggedIn()){
    Redirect::to('index.php');
}

if(!isset($_GET['new'])){
    $sage = new Sage();
    $base64 = $sage->userHasKey();
    //$logger->debug('Not setting new...');
    if(!$base64){
        Redirect::to('loginSage.php');
    }
    else{
        $base64 = $sage->userHasKey()->sage_key;
    
        $sage = new Sage();
        $sage->storeKeySession($base64);
        $provider = $sage->getProvider();
    
        $response = $provider->Companies()->get();
        $sageCompanies = $response->body()->Results;
    
        $companies = array();
    
        if(count($sageCompanies) != 0 && $sageCompanies != null){
            for($i = 0; $i < count($sageCompanies); $i++){
                $name = $sageCompanies[$i]->Name;
                $apiId = $sageCompanies[$i]->ID;
    
                $companies[$i]['company_name'] = $name;
                $companies[$i]['api_id'] = $apiId;
            }
        }
    
        if($companies != null && count($companies) != 0){
            $noComp = false;
        }
    }
}

if(Input::exists() || isset($_GET['new'])){
    //$logger->debug('Input exists');
    
    if(isset($_GET['hash'])){
        // $rawString = Input::get('name') . ':' . Input::get('password');
        $base64 = Input::get('hash');
        //$logger->debug($base64);
        
        $sage = new Sage();
        $sage->storeKeySession($base64);
        $provider = $sage->getProvider();

        $response = $provider->Companies()->get();
        $sageCompanies = $response->body()->Results;

        $companies = array();

        if(count($sageCompanies) != 0 && $sageCompanies != null){
            for($i = 0; $i < count($sageCompanies); $i++){
                $name = $sageCompanies[$i]->Name;
                $apiId = $sageCompanies[$i]->ID;

                $companies[$i]['company_name'] = $name;
                $companies[$i]['api_id'] = $apiId;
            }
        }

        if($companies != null && count($companies) != 0){
            $noComp = false;
        }
    }

    
    if(isset($_POST['linkCompanies'])){
        $sage = new Sage();
        $countCompanies = Input::get('countCompanies');
        $authKey = input::get('authKey');

        $success = array();
        $exists = array();

        for($i = 0; $i < $countCompanies; $i++){
            if(isset($_POST['select-' . $i])){
                $companies[$i]['company_name'] = trim(Input::get('company_name' . $i));
                $companies[$i]['api_id'] = Input::get('api_id' . $i);
                array_push($linkCompanies, $companies[$i]);
                

                $saveCompany = Companies::setCompany(2, $companies[$i]['api_id'], $companies[$i]['company_name']);
                if(!$saveCompany){
                    array_push($existingComps, $companies[$i]['company_name']);
                    array_push($exists, $companies[$i]['company_name']);
                }
                else{
                    // echo 'Saved!<br>';
                    if($sage->storeKeyDb($authKey, Session::get('active_company_id'))){
                        array_push($success, $companies[$i]['company_name']);
                    }
                    else{
                        echo 'Failed to save key';
                    }
                }
            }
        }

        $successMsg = '';
        $existsMsg = '';

        for($i = 0; $i < count($success); $i++){
            if(count($success) - 1 == $i){
                $successMsg .= $success[$i];
            }
            else{
                $successMsg .= $success[$i] . ',';
            }
        }
        for($i = 0; $i < count($exists); $i++){
            if((count($exists) - 1) == $i){
                $existsMsg .= $exists[$i];
            }
            else{
                $existsMsg .= $exists[$i] . ',';
            }
        }

        Redirect::to('integration.php?exists='.$existsMsg.'&success='.$successMsg);
    }
}
else{
    //$logger->debug('Not input exists');
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
        <title>BC | Select companies</title>
    </head>
    <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
    <div class="loader">
        <div class="loading-animation"></div>
    </div>
        <div class="container" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-6 mt-6">
                <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-6 card custom-card rounded-0-1 my-auto white-boxShadow">
                    <a class="pt-3 pl-2" href="javascript:history.back()">
                        <img class="h-20px" src="img/arrow-left-icon.png">
                    </a>
                   <!--BC Logo -->
                    <div class="row d-flex justify-content-center bc-logo">
                        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-0">
                            <img src="img/Logo_BC.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <!-- BC Logo -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-sm-7 col-md-7 col-lg7 col-xl-7 logo-size-pos-sign">
                            <img src="img/logo.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <!-- Sage Logo -->
                    <div class="row d-flex justify-content-center mt-4">
                            <img src="img/sage.png" class="h-30px" alt="logo">
                    </div>
                    <!-- Heading -->
                    <div class="row d-flex justify-content-center mt-3 mb-4">
                        <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center">
                                <?php 
                                if($existingComps != null && count($existingComps) != 0){
                                    echo '';
                                }
                                else{
                                    echo '<p class="verify-txt-color fs-13">Please choose a company to link:</p>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- Form -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-11 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                            <form action="" method="POST" class="needs-validation">
                    <!-- Collapse -->
                                <?php if($msg == ''){ ?>
                                <a <?php if($existingComps != null && count($existingComps) != 0) echo 'hidden' ?> id="collapseCompanies" class="row no-gutters text-decoration-none form-control align-items-center input-group shadow-custom-sm org-name g-field-size fs-14 collapsed" <?php if(!$noComp) echo 'data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapse"' ?>>
                                    <div class="col-10">
                                        <?php 
                                        if($noComp){
                                            echo '<span class="text-color-red">No companies found</span>';
                                        }
                                        else{
                                            echo 'Companies';
                                        }
                                        ?>
                                    </div>
                                    <?php if(!$noComp){ ?>
                                    <div id="arrow-icon" class="col-2 px-8" style="pointer-events: none">
                                        <i class="fas fa-chevron-down"></i>
                                    </div>
                                    <?php } ?>
                                </a>
                                <?php } ?>
                    <!-- Select All -->
                                <div class="collapse multi-collapse" id="collapse">
                                    <div class="input-group shadow-custom-sm org-name g-field-size">
                                        <div class="row w-100 no-gutters">
                                            <div class="col-10">
                                                <div class="input-group-prepend">
                                                    <span class="form-control align-items-center input-group g-field-size fs-14">Select all</span>
                                                </div>
                                            </div>
                                            
                                            <div class="col-2 px-8 py-8">
                                                <label class="container-check">
                                                    <input id="select-all" name="select-all" type="checkbox">
                                                    <span class="checkmark rounded "></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    <!-- Companies -->

                            <?php
                            if(!$noComp){
                                for($i = 0; $i < count($companies); $i++){
                                    echo 
                                    '<div id="collapse" class="collapse multi-collapse">'.
                                        '<div class="input-group shadow-custom-sm org-name g-field-size">'.
                                            '<div class="row w-100 no-gutters">'.
                                                '<div class="col-10">'.
                                                    '<div class="input-group-prepend">'.
                                                        '<span class="form-control align-items-center input-group g-field-size fs-14">' . $companies[$i]['company_name'] . '</span>'.
                                                    '</div>'.
                                                '</div>'.
                                                '<div class="col-2 px-8 py-8">'.
                                                    '<label class="container-check">'.
                                                        '<input id="select-' . $i . '" name="select-' . $i . '" type="checkbox">'.
                                                        '<span class="checkmark rounded"></span>'.
                                                    '</label>'.
                                                '</div>'.
                                            '</div>'.
                                        '</div>'.
                                    '</div>';
                                    echo 
                                    '<input hidden name="api_id' . $i . '" type="text" value="' . $companies[$i]['api_id'] . '">';
                                    echo 
                                    '<input hidden name="company_name' . $i . '" type="text" value="' . $companies[$i]['company_name'] . '">';
                                }
                            }

                            // if($existingComps != null && count($existingComps) != 0){
                            //     for($i = 0; $i < count($existingComps); $i++){
                            //         echo '<p class="text-color-red fs-13 text-center mt-n4">' . $existingComps[$i] . ' already registered to Beancruncher</p>';
                            //     }
                            // }
                            ?>
                            

                    <!-- Link Companies-->
                                <div class="row d-flex mx-auto ">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <input hidden name="countCompanies" type="text" value="<?php if(isset($companies)) echo count($companies) ?>">
                                        <input hidden name="authKey" type="text" value="<?php if(isset($base64)) echo $base64 ?>">
                                        <?php 
                                            if($existingComps != null && count($existingComps) != 0){
                                                echo '<a href="index.php" class="btn btn-block nav-free-but the-border main-purple fs-12 w-50 mx-auto my-3">Done</a>';
                                            }
                                            else{
                                                echo '<input name="linkCompanies" type="submit" class="btn btn-block nav-free-but the-border main-purple fs-12 w-50 mx-auto my-3" value="Link companies">';
                                            }
                                        ?>
                                        
                                    </div>
                                </div>
                                <!-- Sign in again -->
                                <div class="row bluegrey d-flex justify-content-center rem-row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                                        <span class="fs-12"><a href="loginSage.php" class="bluegrey">Sign in with a different <b>Sage</b> account</a></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            
            $('#collapseCompanies').on('click', function (){
                document.getElementById('collapseCompanies').blur();

                // if($('#arrow-icon').html() == '<i class="fas fa-chevron-up"></i>'){
                //     console.log('Icon UP')
                // }
                // if($('#arrow-icon').html() == '<i class="fas fa-chevron-down"></i>'){
                //     console.log('Icon DOWN')
                // }
                // if($('#collapseCompanies').hasClass('collapsing')){
                //     console.log('collapsed')
                // }
                // if($('#collapseCompanies').hasClass('collapsed')){
                //     console.log('collapsed')
                // }
                // if(!$('#collapseCompanies').hasClass('collapsed')){
                //     console.log('NOT collapsed')
                // }

                if($('#arrow-icon').html() == '<i class="fas fa-chevron-up"></i>'){
                    $('#arrow-icon').html('<i class="fas fa-chevron-down"></i>');
                }else{
                    $('#arrow-icon').html('<i class="fas fa-chevron-down"></i>');
                }
            });
            
            document.getElementById('select-all').onclick = function() {
                var checkboxes = document.querySelectorAll('input[type="checkbox"]');
                for (var checkbox of checkboxes) {
                    checkbox.checked = this.checked;
                }
            }
        </script>

        <footer class="py-0 bg-color mf-border">
            <div class="container-fluid">
                <div class="row no-gutters">
                    <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                        <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                    </div>
                    <div class="col-12 col-lg-4 footer-purple my-3 small">
                        <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                        <a class="fs-12 font-weight-200 text-white">Designed by </a>
                        <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                        </p>
                    </div> 
                    <div class="col-lg-4 d-none d-lg-block text-right">
                        <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
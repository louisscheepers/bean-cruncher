<?php
ini_set('display_errors', 'On');
require_once __DIR__ . '/core/init.php';

$msg = '';
//$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');


if(isset($_GET['failed'])){
    if($_GET['failed'] == 'details'){
        $msg = 'Incorrect details, please try again';
    }
    else{
        $msg = 'Failed to log into Sage, please try again';
    }
}

if(Input::exists()){
    if(isset($_POST['linkAccount'])){
        if(isset($_POST['name']) && isset($_POST['password'])){
            $name = $_POST['name'];
            $password = $_POST['password'];

            $sage = new Sage();
            //$logger->debug('Checking Sage details');
            if($sage->validateLogin($name, $password)){
                $hash = base64_encode($name . ':' . $password);
                //$logger->debug('Validated Sage login');
                Redirect::to('selectCompanies.php?new=true&hash='.$hash);
            }
            else{
                //$logger->debug('Invalid Sage login');
                Redirect::to('loginSage.php?failed=details');
            }
        }
    }

}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
        <title>BC | Login Sage</title>
    </head>
    <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
    <div class="loader">
        <div class="loading-animation"></div>
    </div>
        <div class="container" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-6 mt-6">
                <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-6 card custom-card rounded-0-1 my-auto white-boxShadow">
                    <a class="pt-3 pl-2" href="javascript:history.back()">
                        <img class="h-20px" src="img/arrow-left-icon.png">
                    </a>
                   <!--BC Logo -->
                    <div class="row d-flex justify-content-center bc-logo">
                        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-0">
                            <img src="img/Logo_BC.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <!-- BC Logo -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-sm-7 col-md-7 col-lg7 col-xl-7 logo-size-pos-sign">
                            <img src="img/logo.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <!-- Sage Logo -->
                    <div class="row d-flex justify-content-center mt-4">
                            <img src="img/sage.png" class="h-30px" alt="logo">
                    </div>
                    <!-- Heading -->
                    <div class="row d-flex justify-content-center mt-3 mb-4">
                        <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center">
                                <?php 
                                if($msg == ''){
                                    echo '<p class="verify-txt-color fs-13">Please enter your Sage Accounting login details:</p>';
                                }
                                else{
                                    echo '<p class="text-color-red fs-13 text-center">' . $msg . '</p>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- Form -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-11 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                            <form action="" method="POST" class="needs-validation">
                    <!-- Username -->
                                <div class="input-group shadow-custom-sm org-name g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Email icon.svg" class="img-fluid email_icon_width" alt=""/></span>
                                    </div>
                                    <input id="name" name="name" type="text" class="form-control border-left-0 pl-1 rounded-right-border fs-14 my-auto" placeholder="Username" required>
                                </div>
                    <!-- Password -->
                                <div class="input-group shadow-custom-sm g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Lock icon.svg" class="img-fluid email_icon_width pl-1" alt=""/></span>
                                    </div>
                                    <input id="password" name="password" type="password" class="form-control border-left-0 border-right-0 ml-1 pl-1 rounded-right-border fs-14 my-auto" placeholder="Password"  required>
                                    <div id="revealPassword" class="input-group-append">
                                        <span class="input-group-text bg-white rounded-right-border"><span id="eye-icon" class="eye-style"><i class="far fa-eye"></i></span></span>
                                    </div>
                                </div>
                    <!-- Allow BC to store Sage data -->
                                <div class="row d-flex mx-auto mb-3 mt-4">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-sm-left">
                                        <span class="bluegrey fs-12">Allow Beancruncher to securely store your encrypted password</span>
                                    </div>
                                </div>
                    <!-- Link Account-->
                                <div class="row d-flex mx-auto rem-row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <input name="linkAccount" type="submit" class="btn btn-block nav-free-but the-border main-purple fs-12 w-50 mx-auto mb-3" value="Link account">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>
        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        
        <script>
        
            if ( window.history.replaceState ) {
                window.history.replaceState( null, null, window.location.href );
            }
            (function() {
            'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            // form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();

            $('#revealPassword').on('click', function (){
                var inputType = $('#password').attr('type');
                if(inputType === 'password'){
                    $('#password').get(0).type = 'text';
                    $('#eye-icon').html('<i class="far fa-eye-slash"></i>');
                }else{
                    $('#password').get(0).type = 'password';
                    $('#eye-icon').html('<i class="far fa-eye"></i>');
                }
            });
        </script>
    </body>
</html>
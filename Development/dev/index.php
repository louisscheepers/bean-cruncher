<?php
  // ini_set('display_errors', 'On');
  require_once('const/const.php');
  require_once(__DIR__ . '/core/init.php');

  $user_agent = $_SERVER['HTTP_USER_AGENT'];

  //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
  
  $showPackages = false;
  $deletedCompanies = array();
  $inactiveCompanies = array();
  $showInactive = false;
  $expired = false;
  $user = new User();
  if($user->isLoggedIn()){
    //$logger->debug('index - User is logged in');
    if($user->data()->verified == 0){
      Redirect::to('verify.php');
    }

    $dontRedirect = false;
    if(Input::exists('GET')){
      if(Input::get('package') == "expired"){
        $showPackages = true;
        $expired = true;
        $dontRedirect = true;
      }
    }
    
    $companies = Companies::getCompanies();
    $companies = array_values($companies);

    if(Input::exists('GET')){
      if(isset($_GET['deleted']) || isset($_GET['inactive'])){
        if(empty(getInactiveCompanies($companies, 'name')) && empty(getDeletedCompanies($companies, 'name'))){
          Redirect::to('index.php');
        }
        $_SESSION['active_company_id'] = null;
        $showInactive = true;
      }
    }

    $package = Companies::getCompanyOwnerPackage();

    $exportPP = false;
    $exportExcell = false;
    $showFavourites = false;
    $showUsers = false;
    $showEmailReport = false;
    $canAddComments = false;
    if($package > 2){
      $exportPP = true;
      $exportExcell = true;
      $showFavourites = true;
      $showUsers = true;
      $showEmailReport = true;
      $canAddComments = true;
    }

    
    if(!$dontRedirect){
      if(($companies == null || count($companies) == 0)){
        for($i = 1; $i <= 8; $i++){
          Session::delete('boxData'.$i);
        }
        $_SESSION['active_company_id'] = null;
        Redirect::to('integration.php?nocomp=true');
      }
    }
    
    if(Companies::getActiveCompanyId() == null ){
      Companies::setActiveCompany($companies[0]['id']);
    }
    
    $_SESSION['active_role_id'] = Companies::getUserCompanyRole();
    $role = Session::get('active_role_id');
    $role3hide = '';
    if($role == '3'){
      $role3hide = 'hidden';
    }
  }
  else{
    //$logger->debug('index - User is NOT logged in');
    Redirect::to('https://beancruncheranalytics.com');
  }
  
  if(!$dontRedirect){
    if(!isset($_GET['deleted']) || !isset($_GET['inactive'])){

      $deletedCompanies = implode(',',getDeletedCompanies($companies, 'name'));
      $inactiveCompanies = implode(',',getInactiveCompanies($companies, 'name'));

      $deletedCompanyIDs = implode(',',getDeletedCompanies($companies, 'id'));
      $inactiveCompanyIDs = implode(',',getInactiveCompanies($companies, 'id'));

      if($deletedCompanies != '' || $inactiveCompanies != ''){
        Session::delete('data');
        Redirect::to('index.php?deleted='.$deletedCompanies.'&deletedID='.$deletedCompanyIDs.'&inactive='.$inactiveCompanies.'&inactiveID='.$inactiveCompanyIDs);
      }
    }
    if(!isset($_GET['q'])){
      if(Companies::getUpdatedModules() == 0 || Companies::getUpdatedModules() == 1){
        Redirect::to('index.php?q=update');
      }
    } else{
      if(Companies::getUpdatedModules() == 2){
        Redirect::to('index.php');
      }
    }
    if(!isset($_GET['q'])){
      if(!isset($_GET['logged']) && $user->hasLoggedIn() == 0){
        Redirect::to('index.php?logged=0');
    }
  }
  
}

if(Companies::getApi() == 2){
  $sage = new Sage();
  Session::put('sage_auth_key', $sage->getCompanyKey()->sage_key);
}

  $db = DB::getInstance();

  $packageTypes = $db->get('package_types', array('package_id', ">", 0));

  if($packageTypes->count()){
      $packageTypes = $packageTypes->results();
  }

  $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  $years = [date("Y",strtotime("0 year")), date("Y",strtotime("-1 year")), date("Y",strtotime("-2 year")), date("Y",strtotime("-3 year")), date("Y",strtotime("-4 year"))];

  function getInactiveCompanies($companies, $type){
    $inactiveCompanies = array();

    foreach($companies as $company){
      if($company['status'] == 2){

        if($type == 'name'){
          array_push($inactiveCompanies, $company['name']);
        }
        else{
          array_push($inactiveCompanies, $company['id']);
        }

      }
    }

    return $inactiveCompanies;
  }
  function getDeletedCompanies($companies, $type){
    $deletedCompanies = array();

    foreach($companies as $company){
      if($company['status'] == 3){

        if($type == 'name'){
          array_push($deletedCompanies, $company['name']);
        }
        else{
          array_push($deletedCompanies, $company['id']);
        }

      }
    }

    return $deletedCompanies;
  }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Beancruncher</title>
      
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/custom.css" rel="stylesheet" />
        <link href="css/graphs.css" rel="stylesheet" />
        <link href="css/toggle_switch.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
    </head>
    <body class="sb-nav-fixed">

<!--     TOP NAV-->
        <nav class="sb-topnav navbar-expand navbar-dark bg-transparent p-0">
        <div class="container-fluid">
          <div class="row">
          <div class="col-sm-2 col-side-max">
          <div class="mnav-colum-2 mobileNav-bg">
            <div class="dropdown top-side-bg w-side-nav navbar-brand mr-auto mt-mobile">
              <div class="row no-gutters">
                <div class="col-lg-2 my-auto px-0">
                
                    <ul class="navbar-nav ml-md-0">
                      <li class="nav-item dropdown">
                          <a class="nav-link p-0" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <img src="img/burger_menu.png" class="img-fluid burger-menu"> 
                          </a>
                          <div class="dropdown-menu dropdown-menu-left dropdown-position" aria-labelledby="userDropdown">
                            <div class="row txt-color no-gutters">
                                <div class="col-3">
                                <div id="user-icon">
                                  <?php 
                                    $pfp = base64_encode($user->data()->user_img);
                                    if($pfp == null || $pfp == ''){
                                      echo '<img id="user-img" src="img/profile-icon-pink.png" class="rounded-circle icon-size" alt="Profile Icon">';
                                    }
                                    else{
                                      echo '<img id="user-img" src="data:image/jpeg;base64,' . base64_encode($user->data()->user_img) . '" class="rounded-circle icon-size" alt="Profile Icon">';
                                    }
                                  ?>
                                </div>
                                </div>
                                <div class="col-9 my-auto">
                                  <p class="my-auto align-self-end">
                                  <?php echo $user->data()->user_name . ' ' . $user->data()->user_surname; ?>
                                  </p>
                                </div>
                            </div>
                              <div class="dropdown-divider w-15"></div>
                              <a class="dropdown-item txt-color" href="profile.php">Your profile</a>
                              <a class="dropdown-item txt-color" href="company-profile.php">Company profile</a>
                              <?php $hidden = ''; if($showFavourites){ $hidden = "d-none"; }?><span id="favmenupermission" class="<?php echo htmlentities($hidden)?> "><a class="dropdown-item txt-color d-none d-lg-block" href="favourites.php">Favourites</a></span>
                              <!--  -->
                              <a class="dropdown-item txt-color font-weight-bold" href="#" data-toggle="modal" data-target="#packageOption-modal" aria-haspopup="true" aria-expanded="false"><?php echo ($user->getPackage() <= 2) ? "Upgrade Package" : "Change Package"; ?> </a>
                              <!--  -->
                              <a id="logout" name="logout" class="dropdown-item txt-color" href="logout.php">Sign Out</a>
                          </div>
                      </li>
                    </ul>
                </div>
                    <div class="col-sm-10 px-0 ml-m0 d-none d-lg-block">
                      <div class="btn-lg px-0" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <a class="navbar-brand" href="#">Beancruncher</a>
                      </div>
                    </div>
              </div>
            </div>
            </div>
            </div>

        <div class="col-sm-10 col-large-flex mt-m-nav">
<!-- Curved Navigation Bar -->
          <div class="curved-nav nav-spacing nav-radius">
            <div class="row">
            <div class="col-3 col-sm-6 col-lg-6 d-flex">
<!-- First Colum with nested colums -->
              <div class="col-sm-10 align-self-center d-none d-lg-block">
                      <div class="dropdown d-md-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                            <img class="img-fluid search-icon-small" src="img/Group 1511@2x.png">
                            <input class="search-element" type="text" id="companySearch" onkeyup="search()" placeholder="Find company..." title="List of Companies" data-toggle="dropdown" autocomplete="off" >
                            <i class="fas fa-chevron-down search-icon-dropdown"></i>
                            <div id="companiesDropDown" class="dropdown-menu r-0 radius-0" aria-labelledby="dropdownMenuButton">
                              <?php
                                $companies = Companies::getCompanies();
                                $companies = array_values($companies);
                                for($i = 0; $i < count((array)$companies); $i++){
                                  if($companies[$i]['status'] == 1){
                                  ?>
                                  <li>
                                    <a id="active-company-dropdown" class="dropdown-item" onclick="loadCompany('<?php echo str_replace('\'', '\\\'', $companies[$i]['name']) ?>', <?php echo $companies[$i]['id']; ?>, true, <?php echo $companies[$i]['package']; ?>) "><?php echo $companies[$i]['name']; ?></a>
                                  </li>
                                  <?php
                                  }
                                }
                              ?> 
                            </div>
                        </div>
                        <img hidden id="company-loader" src="img/BC-preloader-gif.gif" style="height: 34px;">
              </div>

              <div class="col-lg-2 d-lg-none align-self-center">
                <div class="dropdown">

                  <a class="a-icon-sm" href="#" role="button" onclick="mobileSearchShow()">
                    <img class="img-fluid search-icon-sm" src="img/whiteSearch.png">
                  </a>

                  <div id="mbSearch" class="dr-radius dropdown-menu collapse d-lg-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                    <input class="search-element companyMobileSearch" type="text" id="companySearchM" onkeyup="mobileSearch()" placeholder="Find company..." title="List of Companies">
                    <div id="companiesDropDownM" class="r-0 radius-0" aria-labelledby="dropdownMenuButton">
                      <?php
                        $companies = Companies::getCompanies();
                        $companies = array_values($companies);
                        for($i = 0; $i < count((array)$companies); $i++){
                          if($companies[$i]['status'] == 1){
                          ?>
                          <li>
                            <a id="active-company-dropdown" class="dropdown-item" onclick="mobileSearchShow(); loadCompany('<?php echo str_replace('\'', '\\\'', $companies[$i]['name']) ?>', <?php echo $companies[$i]['id']; ?>, true); mobileSearch();"><?php echo $companies[$i]['name']; ?></a>
                          </li>
                          <?php
                          }
                        }
                      ?> 
                    </div>
                </div>
                </div>
                
              </div>

              <div class="col-sm-2 d-none d-lg-block">
                <div id="nav-loader" class="loader" hidden></div>
                    <span class="small text-muted" hidden></span>
                  <progress value="0" max="100" hidden></progress>
              </div>
            </div>
<!-- Second Colum with nested colums -->
            <div class="col-9 col-sm-6 col-lg-6 d-flex no-gutters">
              <div class="col-9 col-sm-10 col-lg-11 flex-row-reverse align-self-center">
                <ul class="navbar-nav float-right">

                  <?php $hidden = ''; if($showUsers){ $hidden = "d-none"; } ?>
                  <div id="userpermission" class=" <?php echo htmlentities($hidden)?>"><li id="user-management-list-item" class=" nav-item align-self-center d-none d-lg-block top-nav-icon-box tooltip-hover" <?php if($role3hide == 'hidden') { echo 'style="visibility:hidden"'; }?> >
                  <span class="tooltiptext">User Management</span>
                    <a id="user-management" class="btn-lg" href="user-management.php" <?php if($role3hide == 'hidden') { echo 'style="visibility:hidden"'; }?> >
                      <img src="img/admin.png" class="img-fluid nav-icon-size">
                    </a>
                  </li> 
                  </div>

                          <li class="dropdown nav-item align-self-center d-none d-lg-block top-nav-icon-box text-center tooltip-hover">
                              <span id="exportTooltip" class="tooltiptext">Export</span>
                              <a class="btn-lg p-0" href="#" id="dropdownMenuButton" onclick="tooltipExp()" onmouseover="onExpTooltipMouseOver()" onmouseout="onExpTooltipMouseOutHover()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="img/export.png" class="img-fluid nav-icon-size">
                              </a>

                              <div class="dropdown-menu r-0" aria-labelledby="dropdownMenuButton" id="exportSettingDropDown">
                                <?php $hidden = ''; if($exportExcell){ $hidden = "d-none"; }?><a id="exportExcell" class="<?php echo htmlentities($hidden)?> dropdown-item txt-color font-weight-normal font-size-16" href="#" onclick="exportFile('xlsx')">Excel</a>
                                <hr class="<?php echo htmlentities($hidden)?> hr-custom"> 
                                
                                <a class="dropdown-item txt-color font-weight-normal font-size-16" href="#" onclick="exportFile('pdf', true)">PDF</a>
                                
                                <?php $hidden = ''; if($exportPP){ $hidden = "d-none"; }?>
                                  <hr class=" <?php echo htmlentities($hidden)?> hr-custom">
                                <a id="exportPP" class=" <?php echo htmlentities($hidden)?> dropdown-item txt-color font-weight-normal font-size-16" href="#" onclick="exportFile('pptx')">PowerPoint</a>
                              </div>
                            </li>
                            
                            <?php 

                              $hidden = '';
                              if($showFavourites){
                                $hidden = "d-none";
                              }
                            ?>
                            <div id="favspermission" class="<?php echo htmlentities($hidden)?> "><li class="dropdown nav-item align-self-center d-none d-lg-block top-nav-icon-box text-center tooltip-hover">
                              
                              <span id="favouritesTooltip" class="tooltiptext">Favourites</span>
                              <a class="btn-lg p-0" href="#" id="dropdownMenuButton" onclick="tooltipFav()" onmouseover="onFavTooltipMouseOver()" onmouseout="onFavTooltipMouseOutHover()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <img src="img/star.png" class="img-fluid nav-icon-star">
                              </a>
                              <div id="favouritesDropDown" class="dropdown-menu dropdown-mn-mx-w r-0">
                              
                                    <?php
                                      $favourites = Favourites::getFavourites();
                                      for($i = 0; $i < count((array)$favourites); $i++){
                                    ?>
                                      <div id="favourite-<?php echo $favourites[$i] ?>">
                                      <div class="row">
                                        <div class="col-10">
                                          <a class="txt-color font-weight-normal font-size-16" href="#" onclick="setLayout('<?php echo $favourites[$i] ?>', true)">
                                            <span class="float-left my-2" style="width: 100%"><?php echo $favourites[$i] ?></span>
                                          </a>
                                        </div>
                                        <div class="col-2">
                                          <a href="#" class="btn float-right p-0 my-2" onclick="deleteFavourite('<?php echo $favourites[$i]; ?>')">
                                            <i class="far fa-trash-alt"></i>
                                          </a>
                                      </div>
                                    </div><hr class="hr-custom">
                                  </div>
                                    <?php
                                      }
                                    ?>
                                  <a class="dropdown-item txt-color font-weight-bold font-size-16 my-2" href="#" data-toggle="modal" data-target="#favourite-modal">+ Add</a>
                              </div>
                            </li>
                            </div>
                            <?php 
                              $hidden = '';

                              if($showEmailReport){
                                $hidden = 'd-none';
                              }
                              ?>

                          <div id="emailreportpermission" class="<?php echo htmlentities($hidden)?>">
                            <li class="dropdown nav-item align-self-center mr-3 d-none d-lg-block top-nav-icon-box text-center tooltip-hover">
                              <span class="tooltiptext">Email</span>
                              <a class="btn-lg p-0" href="#" data-toggle="modal" data-target="#email-modal" aria-haspopup="true" aria-expanded="false">
                              <img src="img/mail.png" class="img-fluid nav-icon-mail">
                              </a>
                              <div class="dropdown-menu r-0" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item txt-color font-weight-normal font-size-16" href="#" data-toggle="modal" data-target="#email-modal">Email</a>
                              </div>
                            </li>
                          </div>
                            <div class="dropdown d-md-inline-block my-2 my-md-0 mx-3 align-self-center">
                              <h5 class="nav-settings d-content mobile-fs-14 txt-white" id="companyDropDown">
                                <?php echo Companies::getActiveCompanyName() ?>
                              </h5>
                            </div>
                    </ul>
                  </div>

                        <div class="col-3 col-sm-1 align-self-center text-center">
                          <div id="comp-icon">
                            <?php
                              if(Companies::getActiveCompanyBrand() == null){
                                echo '<img id="comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
                              }
                              else{
                                echo '<img id="comp-img" src="data:image/jpeg;base64,' . base64_encode(Companies::getActiveCompanyBrand()) . '" class="rounded-circle icon-size" alt="Profile Icon">';
                              }
                            ?>
                            
                          </div>
                          <!-- Required for default user icon in exports -->
                          <div hidden>
                            <img id="user-img" src="img/person-blue.jpg" class="rounded-circle icon-size" alt="Profile Icon">
                          </div>
                        </div>
                        </div>
                     </div>
                  </div>
                <!-- Curved Navigation Bar -->
              </div>
            </div>
          </div>
          </div>
        </nav>

<!--        END OF TOP NAV    -->
        
<!--        SIDENAV-->
        <div id="layoutSidenav" class="content-bg" onclick="mobileSearchHide()">
            <div id="layoutSidenav_nav" class="content-bg side-nav-position brt-color">
                <nav class="sb-sidenav accordion sb-sidenav-dark pt-side-navMobile" id="sidenavAccordion">
                    <div class="sb-sidenav-menu sidenav-bg">
                        <div class="nav">
                       
                          <div class="pt-3">
                            <a id="overview" class="nav-link collapsed fs-sideNav active" href="#" data-toggle="collapse" data-target="#collapseOverview" aria-expanded="false" aria-controls="collapseLayouts" onmouseover="imgOverview.src='img/overviewIcon.png'" onmouseout="imgOverview.src='img/overviewIconWhite.png'">       
                              <div class="sb-nav-link-icon">
                                <img id="imgOverview" src="img/overviewIconWhite.png" class="img-fluid overview-icon">
                              </div>
                                  Overview
                                  <div class="sb-sidenav-collapse-arrow mr-n2-dropdown">
                                    <img src="img/small_down.png" class="img-fluid dropdown-icon">
                                  </div>
                            </a>
                            <div class="collapse collapse-color" id="collapseOverview" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <p class="nav-link collapse-menu pb-0 pt-4-custom fs-14 grab" id= "cico" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('cico')">Cash IN v Cash OUT</p>
                                    <p class="nav-link collapse-menu pb-0 fs-14 grab" id= "ino" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('ino')">Income v Expenses</p>
                                    <p class="nav-link collapse-menu pb-2 fs-14 grab" id= "avd" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('avd')">Asset v Debt</p>
                                </nav>
                            </div>
                          </div>

                            <div class="pt-3">
                              <a id="finman" class="nav-link collapsed fs-sideNav" href="#" data-toggle="collapse" data-target="#collapseFinancialManagement" aria-expanded="false" aria-controls="collapseLayouts" onmouseover="imgFinMan.src='img/cashManagement.png'" onmouseout="imgFinMan.src='img/cashManagementWhite.png'">
                                     <div class="sb-nav-link-icon">
                                        <img id="imgFinMan" src="img/cashManagementWhite.png" class="img-fluid finManagement-icon">
                                     </div>
                                  Financial Management
                                   <div class="sb-sidenav-collapse-arrow mr-n2-dropdown">
                                    <img src="img/small_down.png" class="img-fluid dropdown-icon">
                                  </div>
                              </a>
                              <div class="collapse collapse-color" id="collapseFinancialManagement" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                  <nav class="sb-sidenav-menu-nested nav">
                                      <p class="nav-link collapse-menu pb-0 pt-4-custom fs-14 grab" id= "pl" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('pl')">Profit and Loss</p>
                                      <p class="nav-link collapse-menu pb-2 fs-14 grab" id= "bs" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('bs')">Balance Sheet</p>
                                  </nav>
                              </div>
                              </div>
                            
                            <div class="pt-3">
                            <a id="customers" class="nav-link collapsed fs-sideNav" href="#" data-toggle="collapse" data-target="#collapseCustomers" aria-expanded="false" aria-controls="collapseLayouts" aria-controls="collapseLayouts" onmouseover="swapIconCustomer.src='img/customers.png'" onmouseout="swapIconCustomer.src='img/customersWhite.png'">
                                <div class="sb-nav-link-icon">
                                  <img id="swapIconCustomer" src="img/customersWhite.png" class="img-fluid customer-icon">
                                </div>
                                Customers
                                 <div class="sb-sidenav-collapse-arrow mr-n2-dropdown">
                                    <img src="img/small_down.png" class="img-fluid dropdown-icon">
                                  </div>
                            </a>
                            <div class="collapse collapse-color" id="collapseCustomers" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <p class="nav-link collapse-menu pb-0 pt-4-custom fs-14 grab" id= "ar" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('ar')">Accounts Receivable</p>
                                    <p class="nav-link collapse-menu pb-0 fs-14 grab" id= "ddo" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('ddo')">Debtors Days Outstanding</p>
                                    <p class="nav-link collapse-menu pb-0 fs-14 grab" id= "tcs" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('tcs')">Top Customers by Balance</p>
                                    <p class="nav-link collapse-menu pb-2 fs-14 grab" id= "ret" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('ret')">Retention</p>
                                </nav>
                            </div>
                            </div>
                            
                            <div class="pt-3">
                            <a id="suppliers" class="nav-link collapsed fs-sideNav" href="#" data-toggle="collapse" data-target="#collapseSuppliers" aria-expanded="false" aria-controls="collapseLayouts" aria-controls="collapseLayouts" onmouseover="swapIconSupplier.src='img/suppliers.png'" onmouseout="swapIconSupplier.src='img/suppliersWhite.png'">
                                <div class="sb-nav-link-icon">
                                  <img id="swapIconSupplier" src="img/suppliersWhite.png" class="img-fluid supplier-icon">
                                </div>
                                Suppliers
                                 <div class="sb-sidenav-collapse-arrow mr-n2-dropdown">
                                    <img src="img/small_down.png" class="img-fluid dropdown-icon">
                                  </div>
                            </a>
                            <div class="collapse collapse-color" id="collapseSuppliers" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                <p class="nav-link collapse-menu pb-0 pt-4-custom fs-14 grab" id= "ap" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('ap')">Accounts Payable</p>
                                <p class="nav-link collapse-menu pb-0 fs-14 grab" id= "cdo" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('cdo')">Creditors days outstanding</p>
                                <p class="nav-link collapse-menu pb-2 fs-14 grab" id= "tsp" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('tsp')">Top Suppliers by Balance</p>
                                </nav>
                            </div>
                            </div>
                            
                            <div class="pt-3">
                            <a id="inventory" class="nav-link collapsed fs-sideNav" href="#" data-toggle="collapse" data-target="#collapseInventory" aria-expanded="false" aria-controls="collapseLayouts" aria-controls="collapseLayouts" onmouseover="swapIconInventory.src='img/inventory.png'" onmouseout="swapIconInventory.src='img/inventoryWhite.png'">
                                <div class="sb-nav-link-icon">
                                  <img id="swapIconInventory" src="img/inventoryWhite.png" class="img-fluid inventory-icon">
                                </div>
                                Inventory
                                 <div class="sb-sidenav-collapse-arrow mr-n2-dropdown">
                                    <img src="img/small_down.png" class="img-fluid dropdown-icon">
                                  </div>
                            </a>
                            <div class="collapse collapse-color" id="collapseInventory" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                <p class="nav-link collapse-menu pb-0 pt-4-custom fs-14 grab" id= "im" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('im')">Inventory Management</p>
                                <p class="nav-link collapse-menu pb-2 fs-14 grab" id= "inas" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('inas')">Inventory Analysis</p>
                                </nav>
                            </div>
                            </div>
                            
                            <div class="pt-3">
                            <a id="analysis" class="nav-link collapsed fs-sideNav" href="#" data-toggle="collapse" data-target="#collapseAnalysis" aria-expanded="false" aria-controls="collapseLayouts" aria-controls="collapseLayouts" onmouseover="swapIconAnalysis.src='img/analysis.png'" onmouseout="swapIconAnalysis.src='img/analysisWhite.png'">
                                <div class="sb-nav-link-icon">
                                  <img id="swapIconAnalysis" src="img/analysisWhite.png" class="img-fluid analysis-icon">
                                </div>
                                Analysis
                                 <div class="sb-sidenav-collapse-arrow mr-n2-dropdown">
                                    <img src="img/small_down.png" class="img-fluid dropdown-icon">
                                  </div>
                            </a>
                            <div class="collapse collapse-color" id="collapseAnalysis" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <p class="nav-link collapse-menu pb-0 pt-4-custom fs-14 grab" id= "plr" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('plr')">Profit and Loss Ratios</p>
                                    <p class="nav-link collapse-menu pb-2 fs-14 grab" id= "bsr" draggable="true" onDragStart="drag(event)" onclick="displayModuleMobile('bsr')">Balance sheet Ratios</p>
                                </nav>
                            </div>
                          </div>

                          <div class="pt-3">
                            <a id="cSettings" class="nav-link collapsed fs-sideNav" href="#" data-toggle="collapse" data-target="#collapseSettings" aria-expanded="false" aria-controls="collapseLayouts" onmouseover="swapIconCompany.src='img/gear.png'" onmouseout="swapIconCompany.src='img/gearWhite.png'">
                                <div class="sb-nav-link-icon">
                                  <img id="swapIconCompany" src="img/gearWhite.png" class="img-fluid companySettings-icon">
                                </div>
                                Company Settings
                                <div class="sb-sidenav-collapse-arrow"></div>
                            </a>
                            <div class="collapse collapse-color" id="collapseSettings" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <p class="nav-link collapse-menu pb-0 fs-14"><a class="collapse-menu" href="company-settings.php">Companies</a></p>
                                    <p id="chart-of-accounts" class="nav-link collapse-menu pb-0 fs-14" <?php echo $role3hide; ?> ><a class="collapse-menu" href="chart-of-accounts.php">Chart Of Accounts</a></p>
                                </nav>
                            </div>
                          </div>
                        </div>
                    </div>
                </nav>
            </div>
<!-- END OF SIDENAV-->
           
          <?php 
            $hidden = '';
            if(isset($_GET['q'])){
              if($_GET['q'] == 'update'){
                if(Companies::getUpdatedModules() == 0){
                $hidden = 'hidden';
                echo 
                '<div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="updatemodal" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content w-fst-popup custom-w text-center mx-auto p-8">
                    
                      <div id="import-modal-body" class="align-self-center modal-body txt-color txt-box-sm">
                        <div class="row justify-content-center mb-4">
                          <img src="img/import.png" class="img-fluid modal-icon-size">
                        </div>
                        <p class="mb-3">
                          <b>' . Companies::getActiveCompanyName() . '</b> needs to be updated. <br>
                          Please select one of the options below:
                        </p>
                        <div class="form-check txt-box-sm">
                          <label class="container-box mb-1 text-left txt-box-sm" for="update-12">
                            Download data for the past <b>12 months?</b>
                            <input class="form-check-input" type="radio" name="update-interval" id="update-12" value=12 checked>
                            <span class="checkmark"></span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="container-box mb-1 text-left txt-box-sm" for="update-24">
                            Download data for the past <b>24 months?</b>
                            <input class="form-check-input" type="radio" name="update-interval" id="update-24" value=24>
                            <span class="checkmark"></span>
                          </label>
                        </div>
                        <div class="form-check mb-3">
                          <label class="container-box mb-1 text-left txt-box-sm" for="update-36">
                            Download data for the past <b>36 months?</b>
                            <input class="form-check-input" type="radio" name="update-interval" id="update-36" value=36>
                            <span class="checkmark"></span>
                          </label>
                        </div>
                        <div id="import-modal-button" class="text-center">
                          <button type="button" class="mt-3 mb-3 btn btn-cross-platform btn-custom-card-w txt-box-sm" onclick="importDataPrompt()">Import Data</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>';

                }
                else if(Companies::getUpdatedModules() == 1){
                  $hidden = 'hidden';
                  echo 
                  '<div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="updatemodal" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content custom-w text-center mx-auto">
                      
                        <div id="import-modal-body" class="modal-body txt-color">
                          <img src="img/BC-preloader-gif.gif" class="img-fluid modal-icon-size mb-4">
                          <br>
                          <b>Data is being imported to dashboard.</b>
                          <p class="mt-3">This might take a few mins.<br> We will notify you when it is completed.</p>
                        </div>
                      </div>
                    </div>';
                    // <button type="button" class="mt-3 mb-3 btn btn-cross-platform w-35" data-dismiss="modal">Continue</button>
                }
              }
            }
            if(isset($_GET['logged'])){
              if($user->hasLoggedIn() == 0){
                echo 
                '<div class="modal fade" id="first-entry-modal" tabindex="-1" role="dialog" aria-labelledby="first-entry-modal" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content custom-w text-center mx-auto p-8">
                      
                      <div id="import-modal-body" class="modal-body">
                        <div class="row justify-content-center">
                          <img src="img/Dashboard view icon.png" class="img-fluid modal-icon-size">
                        </div>
                        <div class="row justify-content-center mt-4">
                          <p class="txt-color font-weight-normal font-size-14">Would you like to see default view?</p>
                        </div>
                      </div>
                      <div class="row d-content mx-2">
                        <button type="button" class="mt-2 btn btn-cross-platform btn-custom-card-w txt-box-sm align-self-center mb-3" onclick="setLayout(\'DEFAULT_SESSION_LAYOUT\')" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-cross-platform btn-custom-card-w txt-box-sm align-self-center" data-dismiss="modal">No</button>
                      </div>
                    </div>
                  </div>
                </div>';
                $user->setLoggedIn();
              }
            }
          ?>
<!--            DASHBOARD CONTENT-->
           
            <div id="layoutSidenav_content" class="content-bg">
                <main>
                    <div class="container-fluid">            
                        <section>
                       <div class="row">
                          <div class="col-12 pl-9">
                            <div id="dashboard-content" class="row py-3" <?php echo $hidden ?>>
                              <div class="page1 col-lg-6 col-12">
                                <div class="row p-custom-cards">
                                    <!-- Box One -->
                                    <div class="col-lg-6 d-none d-lg-block">
                                      <div id="popoverBox1" class="card" data-toggle="popover">

                                      <div class="d-none">

                                          <div hidden id="popoverBoxContent1" title="" class="card-header card-header-curve text-muted border-0 p-0 h-20px">

                                            <div class="row no-gutters align-items-center">

                                              <div class="col-9">
                                                <!-- <label id="labelupdate1" class="font-size-10 mb-1"></label> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <a hidden id="refresh1" class="float-left border-0" onclick="refresh('1')">
                                                      <i class="fas fa-sync-alt popover-load-icon"></i>
                                                    </a> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <button type="button" id="move1" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                                <!-- <a hidden id="pop1" class="float-right border-0" onclick="openModal('1')" data-toggle="modal" data-target="#popupModal1"><img class="img-fluid popover-close-icon" src="img/trigger-modal-2.png"></a> -->
                                              </div>

                                              <div class="col-1">
                                                <!--<a hidden id="clear1" class="float-right border-0" onclick="clearBox('1')" ><img class="img-fluid popover-close-icon" src="img/clear-box-2.png"></a>-->
                                              </div>
                                            
                                            </div> <!-- End of inner row -->
                                          </div>
                                      </div>
                                      <!-- End of dropdown row -->
                                      <div class="card-header card-header-curve border-0 bg-transparent p-0 h-20px z-index-1">
                                        <div id="dropDownBoxMenu1" class="col-2 float-right txt-align-right">
                                          <a hidden id="dropDownBox1" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="border-0 bg-white sosatie-menu-space">
                                            <!--<img class="img-fluid " src="img/sosatieMenu.png">-->
                                            <i class="fas fa-ellipsis-v sosatie-menu-icon"></i>
                                          </a>
                                          <div class="dropdown-menu card-dropdown r-0" aria-labelledby="dropDownBox1">
                                            <div class="row no-gutters">

                                                <div class="col-12 mb-1 small-box-shadow custom-box-size txt-align-center tooltip-hover" >
                                                <span id="calenderTooltip1" class="tooltiptext">Calender</span>
                                                    <a id="dates1" hidden class="p-2 dropdown-toggle border-0 bg-transparent" data-toggle="dropdown" onclick="tooltipCalender()" onmouseover="onCalenderTooltipMouseOver()" onmouseout="onCalenderTooltipMouseOutHover()">
                                                      <img class="img-fluid dropdown-comment-icon" src="img/calendar.png">
                                                    </a>   

                                                    <div id="dateDropDown1" class="dropdown-menu r-0 min-w-dates date-dd">
                                                      <h5 id="fromText1" class="dropdown-header txt-color p-0 date-margins">From: </h5>
                                                      <div class="row">
                                                        <div class="col-6">
                                                          <select id="fromY1" class="form-control custom-select p-1 dropdown-header font-size-12">
                                                            <?php for($i = 0; $i < count($years); $i++){ 
                                                              echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                                            ?>
                                                          </select>
                                                        </div>
                                                        <div class="col-6">
                                                          <select id="fromM1" class="form-control custom-select p-1 dropdown-header font-size-12">
                                                            <?php for($i = 0; $i < count($months); $i++){ 
                                                              echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                                            ?>
                                                          </select>
                                                        </div>
                                                      </div>

                                                      <h5 id="toText1" class="dropdown-header txt-color p-0 date-margins">To: </h5>
                                                      <div class="row">
                                                        <div class="col-6">
                                                          <select id="toY1" class="form-control custom-select p-1 dropdown-header font-size-12">
                                                            <?php for($i = 0; $i < count($years); $i++){ 
                                                              echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                                            ?>
                                                          </select>
                                                        </div>
                                                        <div class="col-6">
                                                          <select id="toM1" class="form-control custom-select p-1 dropdown-header font-size-12">
                                                            <?php for($i = 0; $i < count($months); $i++){ 
                                                              echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                                            ?>
                                                          </select>
                                                        </div>
                                                      </div>

                                                        <div class="row my-2">
                                                          <div class="col-6 my-auto">
                                                            <span hidden id="date-pick-error1" class="small text-secondary"></span>
                                                          </div>
                                                          <div class="col-6">
                                                            <!-- <button type="button" id="datesS1" onclick="setDate('1')" class="mt-3 btn btn-cross-platform float-right mr-3 font-size-12" data-toggle="collapse" data-target="#dateDropDown1">Set Date</button> -->
                                                            <button type="button" id="datesS1" onclick="setDate('1')" class="mt-3 btn btn-cross-platform float-right mr-3 font-size-12">Set Date</button>
                                                          </div>
                                                        </div>
                                                    </div>
                                                    
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openComments('1')">
                                                  <span id="commentTooltip1" class="tooltiptext">Comments</span>
                                                    <a hidden id="com1" class="p-1 border-0 bg-transparent " onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid dropdown-comment-icon" src="img/commentBubble.png">
                                                    </a>
                                                  </div>
                                                  <div id="refreshTag1" class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="refresh('1')">
                                                  <span id="refreshTooltip1" class="tooltiptext">Refresh</span>
                                                    <a id="refresh1" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="fas fa-sync-alt popover-load-icon mt-1 align-middle"></i>
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openModal('1')" data-toggle="modal" data-target="#popupModal1">
                                                  <span id="popupTooltip1" class="tooltiptext">Popup Annexure</span>
                                                    <a hidden id="pop1" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid popup-annexure-icon" src="img/trigger-modal-2.png">
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="scrollToAnnexure('1')">
                                                  <span id="annexTooltip1" class="tooltiptext">Annexure</span>
                                                    <a hidden id="annexBtn1" class="p-1 border-0 h-20px bg-transparent mt-n2" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="far fa-file annexure-icon fs-14 font-weight-200 align-middle"></i>
                                                    </a>
                                                  </div>
                                              </div>
                                            </div>
                                          </div> 
                                            <!-- End of dropdown row -->
                                        </div>

                                          <div class="card-body p-1 m-top-20-sm">
                                            <div class="box1" id="box1" draggable="true" ondrop="drop(event, this)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"></div>
                                            <div hidden id="modal-data1" data-value=""></div>
                                          </div>
                                        
                                        <div class="card-footer card-footer-curve text-muted border-0 bg-white p-0 h-20px">
                                          <label id="labelupdate1" class="fs-9 ml-2 align-middle"></label>
                                          <!-- <button type="button" id="desc1" onclick="setDescription('1')" class="float-left border-0 h-20px bg-white mt-n2"><img src="img/description.png" class="h-20px"></button> -->
                                          <!--<button type="button" hidden id="annexBtn1" class="float-right border-0 h-20px bg-white mt-n2" onclick="scrollToAnnexure('1')" ><i class="far fa-file annexure-icon fs-14 font-weight-200"></i></button>-->
                                          <a hidden id="clear1" class="float-right bg-transparent border-0 h-20px mr-2 mt-n1" onclick="clearBox('1', null, true)" >
                                            <img class="img-fluid popover-close-icon" src="img/clear-box-2.png">
                                          </a>
                                        </div>
                                    </div>
                                    </div>

                            <!-- Box Two -->
                            <div class="col-lg-6 d-none d-lg-block">
                              <div id="popoverBox2" class="card" data-toggle="popover">

                              <div class="d-none">

                                  <div id="popoverBoxContent2" class="card-header card-header-curve text-muted border-0 p-0 h-20px">

                                    <div class="row no-gutters align-items-center">

                                      <div class="col-9">
                                        <!-- <label id="labelupdate2" class="font-size-10 mb-1"></label>-->
                                      </div>

                                      <div class="col-1">
                                        <!--<a  hidden id="refresh2" class="float-left border-0" onclick="refresh('2')"><i class="fas fa-sync-alt popover-load-icon"></i></a>-->
                                      </div>

                                      <div class="col-1">
                                        <!-- <button type="button" id="move2" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                        <!--<a  hidden id="pop2" class="float-right border-0" onclick="openModal('2')" data-toggle="modal" data-target="#popupModal2"><img class="img-fluid popover-close-icon" src="img/trigger-modal-2.png"></a>-->
                                      </div>

                                      <div class="col-1">
                                        <!-- <a  hidden id="clear2" class="float-right border-0" onclick="clearBox('2')" ><img class="img-fluid popover-close-icon" src="img/clear-box-2.png"></a> -->
                                      </div>
                                            
                                    </div> <!-- End of inner row -->
                                  </div>
                                </div>

                                    <!-- End of dropdown row -->

                                <div class="card-header card-header-curve border-0 bg-transparent p-0 h-20px z-index-1">
                                  <div id="dropDownBoxMenu2" class="col-2 float-right txt-align-right">
                                    <a hidden id="dropDownBox2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="border-0 bg-white sosatie-menu-space">
                                      <!--<img class="img-fluid " src="img/sosatieMenu.png">-->
                                      <i class="fas fa-ellipsis-v sosatie-menu-icon"></i>
                                    </a>
                                  <div class="dropdown-menu card-dropdown r-0 min-w-dates" aria-labelledby="dropDownBox2">
                                    <div class="row no-gutters">

                                     <div  class="col-12 mb-1 small-box-shadow custom-box-size txt-align-center tooltip-hover" >
                                     <span id="calenderTooltip2" class="tooltiptext">Calender</span>
                                      <a hidden id="dates2" class=" p-2 dropdown-toggle border-0 bg-transparent " data-toggle="dropdown" onclick="tooltipCalender2()" onmouseover="onCalenderTooltipMouseOver()" onmouseout="onCalenderTooltipMouseOutHover()">
                                        <img class="img-fluid dropdown-comment-icon" src="img/calendar.png">
                                      </a>

                                      <div id="dateDropDown2" class="dropdown-menu r-0 min-w-dates date-dd ">

                                        <h5 id="fromText2" class="dropdown-header txt-color p-0 date-margins">From: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="fromY2" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="fromM2" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>

                                        <h5 id="toText2" class="dropdown-header txt-color p-0 date-margins">To: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="toY2" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="toM2" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>

                                        <div class="row my-2">
                                          <div class="col-6 my-auto">
                                            <span hidden id="date-pick-error2" class="small text-secondary"></span>
                                          </div>
                                          <div class="col-6">
                                            <button type="button" id="datesS2" onclick="setDate('2')" class="mt-3 btn btn-cross-platform float-right mr-3 font-size-12" data-toggle="collapse" data-target="#dateDropDown2">Set Date</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openComments('2')">
                                    <span id="commentTooltip2" class="tooltiptext">Comments</span>
                                                    <a hidden id="com2" class="p-1 border-0 bg-transparent " onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid dropdown-comment-icon" src="img/commentBubble.png">
                                                    </a>
                                                  </div>
                                                  <div id="refreshTag2" class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="refresh('2')">
                                                  <span id="refreshTooltip2" class="tooltiptext">Refresh</span>
                                                    <a id="refresh2" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="fas fa-sync-alt popover-load-icon mt-1 align-middle"></i>
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openModal('2')" data-toggle="modal" data-target="#popupModal2">
                                                  <span id="popupTooltip2" class="tooltiptext">Popup Annexure</span>
                                                    <a hidden id="pop2" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid popup-annexure-icon" src="img/trigger-modal-2.png">
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="scrollToAnnexure('2')">
                                                  <span id="annexTooltip2" class="tooltiptext">Annexure</span>
                                                    <a hidden id="annexBtn2" class="p-1 border-0 h-20px bg-transparent mt-n2" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="far fa-file annexure-icon fs-14 font-weight-200 align-middle"></i>
                                              </a>
                                                  </div>
                                  </div>
                                </div>
                              </div>
                                <!-- End of dropdown row -->
                              </div>

                              <div class="card-body p-1 m-top-20-sm">
                              <div class="box2" id="box2" draggable="true" ondrop="drop(event, this)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"></div>
                                    <div hidden id="modal-data2" data-value=""></div>
                                  </div>

                                  <div class="card-footer card-footer-curve text-muted border-0 bg-white p-0 h-20px">
                                  <label id="labelupdate2" class="fs-9 ml-2 align-middle"></label>
                                    <!-- <button type="button" id="desc1" onclick="setDescription('2')" class="float-left border-0 h-20px bg-white mt-n2"><img src="img/description.png" class="h-20px"></button> -->
                                    <!--<button type="button" hidden id="annexBtn2" class="float-right border-0 h-20px bg-white mt-n2" onclick="scrollToAnnexure('2')" ><i class="far fa-file annexure-icon fs-14 font-weight-200"></i></button>-->
                                    <a hidden id="clear2" class="float-right bg-transparent border-0 h-20px mr-2 mt-n1" onclick="clearBox('2', null, true)" >
                                      <img class="img-fluid popover-close-icon" src="img/clear-box-2.png">
                                    </a>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div class="row p-custom-cards">
                          <!-- Box Three -->
                            <div class="col-lg-12">
                              <div id="popoverBox3" class="card" data-toggle="popover">

                              <div class="d-none">

                                  <div id="popoverBoxContent3" class="card-header card-header-curve text-muted border-0 p-0 h-20px">
                                    <div class="row no-gutters align-items-center">

                                    <div class="col-9">
                                                <!-- <label id="labelupdate1" class="font-size-10 mb-1"></label> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <a hidden id="refresh1" class="float-left border-0" onclick="refresh('1')">
                                                      <i class="fas fa-sync-alt popover-load-icon"></i>
                                                    </a> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <button type="button" id="move1" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                                <!-- <a hidden id="pop1" class="float-right border-0" onclick="openModal('1')" data-toggle="modal" data-target="#popupModal1"><img class="img-fluid popover-close-icon" src="img/trigger-modal-2.png"></a> -->
                                              </div>

                                              <div class="col-1">
                                                <!--<a hidden id="clear1" class="float-right border-0" onclick="clearBox('1')" ><img class="img-fluid popover-close-icon" src="img/clear-box-2.png"></a>-->
                                              </div>

                                    </div> <!-- End of inner row -->
                                  </div>
                              </div>
                              <!-- End of dropdown row -->
                              <div class="card-header card-header-curve border-0 bg-transparent p-0 h-20px z-index-1">
                                <div id="dropDownBoxMenu3" class="col-2 float-right txt-align-right">
                                  <a hidden id="dropDownBox3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="border-0 bg-white sosatie-menu-space">
                                    <!--<img class="img-fluid " src="img/sosatieMenu.png">-->
                                    <i class="fas fa-ellipsis-v sosatie-menu-icon"></i>
                                  </a>
                                  <div class="dropdown-menu card-dropdown sm-box-curve mobile-settings-card small-card-dropdown" aria-labelledby="dropDownBox3">
                                    <div class="row no-gutters">

                                    <div  class="col-12 mb-1 small-box-shadow custom-box-size txt-align-center tooltip-hover">
                                    <span id="calenderTooltip3" class="tooltiptext">Calender</span>
                                      <a hidden id="dates3" class=" p-2 dropdown-toggle border-0 bg-transparent " data-toggle="dropdown" onclick="tooltipCalender3()" onmouseover="onCalenderTooltipMouseOver()" onmouseout="onCalenderTooltipMouseOutHover()">
                                        <img class="img-fluid dropdown-comment-icon" src="img/calendar.png">
                                      </a>

                                      <div id="dateDropDown3" class="dropdown-menu mobile-dates r-0 min-w-dates date-dd">

                                        <h5 id="fromText3" class="dropdown-header txt-color p-0 date-margins">From: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="fromY3" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="fromM3" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>
                                        <h5 id="toText3" class="dropdown-header txt-color p-0 date-margins">To: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="toY3" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="toM3" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>
                                        
                                        <div class="row my-2">
                                          <div class="col-6 my-auto">
                                            <span hidden id="date-pick-error3" class="small text-secondary"></span>
                                          </div>
                                          <div class="col-6">
                                            <button type="button" id="datesS3" onclick="setDate('3')" class="mt-3 btn btn-cross-platform float-right mr-3 font-size-12" data-toggle="collapse" data-target="#dateDropDown3">Set Date</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>   
                                    <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openComments('3')">
                                    <span id="commentTooltip3" class="tooltiptext">Comments</span>
                                                    <a hidden id="com3" class="p-1 border-0 bg-transparent " onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid dropdown-comment-icon" src="img/commentBubble.png">
                                                    </a>
                                                  </div>
                                                  <div id="refreshTag3" class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="refresh('3')">
                                                  <span id="refreshTooltip3" class="tooltiptext">Refresh</span>
                                                    <a id="refresh3" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="fas fa-sync-alt popover-load-icon mt-1 align-middle"></i>
                                                    </a>
                                                  </div>
                                                  <div class="col-12 d-none d-lg-block small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openModal('3')" data-toggle="modal" data-target="#popupModal3">
                                                  <span id="popupTooltip3" class="tooltiptext">Popup Annexure</span>
                                                    <a hidden id="pop3" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid popup-annexure-icon" src="img/trigger-modal-2.png">
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="scrollToAnnexure('3')">
                                                  <span id="annexTooltip3" class="tooltiptext">Annexure</span>
                                                    <a hidden id="annexBtn3" class="p-1 border-0 h-20px bg-transparent mt-n2" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="far fa-file annexure-icon fs-14 font-weight-200 align-middle"></i>
                                              </a>
                                                  </div>    
                                  </div>          
                                </div>
                              </div>
                              <!-- End of dropdown row -->
                              </div>

                              <div class="card-body p-1 m-top-20">
                                    <div class="box3" id="box3" draggable="true" ondrop="drop(event, this)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"></div>
                                    <div hidden id="modal-data3" data-value=""></div>
                                  </div>

                                  <div class="card-footer card-footer-curve text-muted border-0 bg-white p-0 h-20px">
                                  <label id="labelupdate3" class="fs-9 ml-2 align-middle"></label>
                                    <!-- <button type="button" id="desc1" onclick="setDescription('3')" class="float-left border-0 h-20px bg-white mt-n2"><img src="img/description.png" class="h-20px"></button> 
                                    <button type="button" hidden id="annexBtn3" class="float-right border-0 bg-white mt-n2" onclick="scrollToAnnexure('3')" ><i class="far fa-file annexure-icon fs-14 font-weight-200"></i></button>-->
                                    <a hidden id="clear3" class="float-right bg-transparent border-0 h-20px mr-2 mt-n1" onclick="clearBox('3', null, true)" >
                                            <img class="img-fluid popover-close-icon" src="img/clear-box-2.png">
                                          </a>
                                  </div>
                            </div>
                          </div>
                        </div>

                        <!-- Box Four -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block">
                              <div id="popoverBox4" class="card" data-toggle="popover">

                              <div class="d-none">

                                <div id="popoverBoxContent4" class="card-header card-header-curve text-muted border-0 p-0 h-20px">

                                  <div class="row no-gutters align-items-center">

                                  <div class="col-9">
                                                <!-- <label id="labelupdate1" class="font-size-10 mb-1"></label> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <a hidden id="refresh1" class="float-left border-0" onclick="refresh('1')">
                                                      <i class="fas fa-sync-alt popover-load-icon"></i>
                                                    </a> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <button type="button" id="move1" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                                <!-- <a hidden id="pop1" class="float-right border-0" onclick="openModal('1')" data-toggle="modal" data-target="#popupModal1"><img class="img-fluid popover-close-icon" src="img/trigger-modal-2.png"></a> -->
                                              </div>

                                              <div class="col-1">
                                                <!--<a hidden id="clear1" class="float-right border-0" onclick="clearBox('1')" ><img class="img-fluid popover-close-icon" src="img/clear-box-2.png"></a>-->
                                              </div>

                                  </div> <!-- End of inner row -->
                                </div>
                              </div>
                              <!-- End of dropdown row -->
                              <div class="card-header card-header-curve border-0 bg-transparent p-0 h-20px z-index-1">
                                <div id="dropDownBoxMenu4" class="col-2 float-right txt-align-right">
                                  <a hidden id="dropDownBox4" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="border-0 bg-white sosatie-menu-space">
                                     <!--<img class="img-fluid " src="img/sosatieMenu.png">-->
                                     <i class="fas fa-ellipsis-v sosatie-menu-icon"></i>
                                  </a>
                                  <div class="dropdown-menu card-dropdown r-0" aria-labelledby="dropDownBox4">
                                  <div class="row no-gutters">

                                  <div  class="col-12 mb-1 small-box-shadow custom-box-size txt-align-center tooltip-hover">
                                  <span id="calenderTooltip4" class="tooltiptext">Calender</span>
                                    <a hidden id="dates4" class=" p-2 dropdown-toggle border-0 bg-transparent " data-toggle="dropdown" onclick="tooltipCalender4()" onmouseover="onCalenderTooltipMouseOver()" onmouseout="onCalenderTooltipMouseOutHover()">
                                      <img class="img-fluid dropdown-comment-icon" src="img/calendar.png">
                                    </a>

                                      <div id="dateDropDown4" class="dropdown-menu r-0 min-w-dates date-dd">

                                        <h5 id="fromText4" class="dropdown-header txt-color p-0 date-margins">From: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="fromY4" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="fromM4" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>
                                        <h5 id="toText4" class="dropdown-header txt-color p-0 date-margins">To: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="toY4" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="toM4" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>

                                        <div class="row my-2">
                                          <div class="col-6 my-auto">
                                            <span hidden id="date-pick-error4" class="small text-secondary"></span>
                                          </div>
                                          <div class="col-6">
                                            <button type="button" id="datesS4" onclick="setDate('4')" class="mt-3 btn btn-cross-platform float-right mr-3 font-size-12" data-toggle="collapse" data-target="#dateDropDown4">Set Date</button>
                                          </div>
                                        </div>
                                    </div>
                                  </div> 
                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openComments('4')">
                                  <span id="commentTooltip4" class="tooltiptext">Comments</span>
                                                    <a hidden id="com4" class="p-1 border-0 bg-transparent " onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid dropdown-comment-icon" src="img/commentBubble.png">
                                                    </a>
                                                  </div>
                                                  <div id="refreshTag4" class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="refresh('4')">
                                                  <span id="refreshTooltip4" class="tooltiptext">Refresh</span>
                                                    <a id="refresh4" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="fas fa-sync-alt popover-load-icon mt-1 align-middle"></i>
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openModal('4')" data-toggle="modal" data-target="#popupModal4">
                                                  <span id="popupTooltip4" class="tooltiptext">Popup Annexure</span>
                                                    <a hidden id="pop4" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid popup-annexure-icon" src="img/trigger-modal-2.png">
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="scrollToAnnexure('4')">
                                                  <span id="annexTooltip4" class="tooltiptext">Annexure</span>
                                                    <a hidden id="annexBtn4" class="p-1 border-0 h-20px bg-transparent mt-n2" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="far fa-file annexure-icon fs-14 font-weight-200 align-middle"></i>
                                              </a>
                                                  </div>
                                </div>
                              </div>
                            </div>
                              <!-- End of dropdown row -->
                          </div>
                                                
                              <div class="card-body p-1 m-top-20-sm">
                              <div class="box4" id="box4" draggable="true" ondrop="drop(event, this)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"></div>
                              <div hidden id="modal-data4" data-value=""></div>
                            </div>

                            <div class="card-footer card-footer-curve text-muted border-0 bg-white p-0 h-20px">
                            <label id="labelupdate4" class="fs-9 ml-2 align-middle"></label>
                              <!-- <button type="button" id="desc1" onclick="setDescription('4')" class="float-left border-0 h-20px bg-white mt-n2"><img src="img/description.png" class="h-20px"></button> -->
                              <button type="button" hidden id="annexBtn4" class="float-right border-0 bg-white mt-n2" onclick="scrollToAnnexure('4')" ><i class="far fa-file annexure-icon fs-14 font-weight-200"></i></button>
                           <!--<button type="button" hidden id="annexBtn1" class="float-right border-0 h-20px bg-white mt-n2" onclick="scrollToAnnexure('1')" ><i class="far fa-file annexure-icon fs-14 font-weight-200"></i></button>-->
                           <a hidden id="clear4" class="float-right bg-transparent border-0 h-20px mr-2 mt-n1" onclick="clearBox('4', null, true)" >
                                            <img class="img-fluid popover-close-icon" src="img/clear-box-2.png">
                                          </a>
                            </div>
                          </div>
                        </div>

                            <!-- Box Five -->
                            <div class="col-lg-6 d-none d-lg-block">
                              <div id="popoverBox5" class="card" data-toggle="popover">
                              
                              <div class="d-none">

                                  <div id="popoverBoxContent5" class="card-header card-header-curve text-muted border-0 p-0 h-20px">
                                    <div class="row no-gutters align-items-center">

                                    <div class="col-9">
                                                <!-- <label id="labelupdate1" class="font-size-10 mb-1"></label> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <a hidden id="refresh1" class="float-left border-0" onclick="refresh('1')">
                                                      <i class="fas fa-sync-alt popover-load-icon"></i>
                                                    </a> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <button type="button" id="move1" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                                <!-- <a hidden id="pop1" class="float-right border-0" onclick="openModal('1')" data-toggle="modal" data-target="#popupModal1"><img class="img-fluid popover-close-icon" src="img/trigger-modal-2.png"></a> -->
                                              </div>

                                              <div class="col-1">
                                                <!--<a hidden id="clear1" class="float-right border-0" onclick="clearBox('1')" ><img class="img-fluid popover-close-icon" src="img/clear-box-2.png"></a>-->
                                              </div>
                                            
                                      </div> <!-- End of inner row -->
                                    </div>
                                  </div>

                                      <!-- End of dropdown row -->
                              <div class="card-header card-header-curve border-0 bg-transparent p-0 h-20px z-index-1">
                                <div id="dropDownBoxMenu5" class="col-2 float-right txt-align-right">
                                    <a hidden id="dropDownBox5" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="border-0 bg-white sosatie-menu-space">
                                      <!--<img class="img-fluid " src="img/sosatieMenu.png">-->
                                      <i class="fas fa-ellipsis-v sosatie-menu-icon"></i>
                                    </a>
                                <div class="dropdown-menu card-dropdown r-0" aria-labelledby="dropDownBox5">
                                  <div class="row no-gutters">
                                    
                                    <div  class="col-12 mb-1 small-box-shadow custom-box-size txt-align-center tooltip-hover">
                                    <span id="calenderTooltip5" class="tooltiptext">Calender</span>
                                      <a hidden id="dates5" class=" p-2 dropdown-toggle border-0 bg-transparent " data-toggle="dropdown" onclick="tooltipCalender5()" onmouseover="onCalenderTooltipMouseOver()" onmouseout="onCalenderTooltipMouseOutHover()">
                                        <img class="img-fluid dropdown-comment-icon" src="img/calendar.png">
                                      </a>

                                      <div id="dateDropDown5" class="dropdown-menu r-0 min-w-dates date-dd">

                                        <h5 id="fromText5" class="dropdown-header txt-color p-0 date-margins">From: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="fromY5" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="fromM5" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>
                                        <h5 id="toText5" class="dropdown-header txt-color p-0 date-margins">To: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="toY5" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="toM5" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="row my-2">
                                          <div class="col-6 my-auto">
                                            <span hidden id="date-pick-error5" class="small text-secondary"></span>
                                          </div>
                                          <div class="col-6">
                                            <button type="button" id="datesS5" onclick="setDate('5')" class="mt-3 btn btn-cross-platform float-right mr-3 font-size-12" data-toggle="collapse" data-target="#dateDropDown5">Set Date</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div> 
                                    <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openComments('5')">
                                    <span id="commentTooltip5" class="tooltiptext">Comments</span>
                                                    <a hidden id="com5" class="p-1 border-0 bg-transparent " onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid dropdown-comment-icon" src="img/commentBubble.png">
                                                    </a>
                                                  </div>
                                                  <div id="refreshTag5" class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="refresh('5')">
                                                  <span id="refreshTooltip5" class="tooltiptext">Refresh</span>
                                                    <a id="refresh5" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="fas fa-sync-alt popover-load-icon mt-1 align-middle"></i>
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openModal('5')" data-toggle="modal" data-target="#popupModal5">
                                                  <span id="popupTooltip5" class="tooltiptext">Popup Annexure</span>
                                                    <a hidden id="pop5" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid popup-annexure-icon" src="img/trigger-modal-2.png">
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="scrollToAnnexure('5')">
                                                  <span id="annexTooltip5" class="tooltiptext">Annexure</span>
                                                    <a hidden id="annexBtn5" class="p-1 border-0 h-20px bg-transparent mt-n2" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="far fa-file annexure-icon fs-14 font-weight-200 align-middle"></i>
                                              </a>
                                                  </div>   
                                </div>
                              </div>
                            </div>
                              <!-- End of dropdown row -->
                          </div>
                                  
                              <div class="card-body p-1 m-top-20-sm">
                                    <div class="box5" id="box5" draggable="true" ondrop="drop(event, this)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"></div>
                                    <div hidden id="modal-data5" data-value=""></div>
                                  </div>
                                  
                                  <div class="card-footer card-footer-curve text-muted border-0 bg-white p-0 h-20px">
                                  <label id="labelupdate5" class="fs-9 ml-2 align-middle"></label>
                                    <!-- <button type="button" id="desc1" onclick="setDescription('5')" class="float-left border-0 h-20px bg-white mt-n2"><img src="img/description.png" class="h-20px"></button> -->
                                    <!--<button type="button" hidden id="annexBtn1" class="float-right border-0 h-20px bg-white mt-n2" onclick="scrollToAnnexure('1')" ><i class="far fa-file annexure-icon fs-14 font-weight-200"></i></button>-->
                                    <a hidden id="clear5" class="float-right bg-transparent border-0 h-20px mr-2 mt-n1" onclick="clearBox('5', null, true)" >
                                            <img class="img-fluid popover-close-icon" src="img/clear-box-2.png">
                                          </a>
                                  </div>
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="page2 break col-lg-6 col-12 d-none d-lg-block">
                      <!-- Box Six -->
                        <div class="row p-custom-cards">
                            <div class="col-lg-12">
                              <div id="popoverBox6" class="card" data-toggle="popover">
                              
                              <div class="d-none">

                                <div id="popoverBoxContent6" class="card-header card-header-curve text-muted border-0 p-0 h-20px">
                                  <div class="row no-gutters align-items-center">

                                  <div class="col-9">
                                                <!-- <label id="labelupdate1" class="font-size-10 mb-1"></label> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <a hidden id="refresh1" class="float-left border-0" onclick="refresh('1')">
                                                      <i class="fas fa-sync-alt popover-load-icon"></i>
                                                    </a> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <button type="button" id="move1" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                                <!-- <a hidden id="pop1" class="float-right border-0" onclick="openModal('1')" data-toggle="modal" data-target="#popupModal1"><img class="img-fluid popover-close-icon" src="img/trigger-modal-2.png"></a> -->
                                              </div>

                                              <div class="col-1">
                                                <!--<a hidden id="clear1" class="float-right border-0" onclick="clearBox('1')" ><img class="img-fluid popover-close-icon" src="img/clear-box-2.png"></a>-->
                                              </div>

                                  </div> <!-- End of inner row -->
                                </div>
                              </div>
                              <!-- End of dropdown row -->
                              <div class="card-header card-header-curve border-0 bg-transparent p-0 h-20px z-index-1">
                                <div id="dropDownBoxMenu6" class="col-2 float-right txt-align-right">
                                  <a hidden id="dropDownBox6" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="border-0 bg-white sosatie-menu-space">
                                    <!--<img class="img-fluid " src="img/sosatieMenu.png">-->
                                    <i class="fas fa-ellipsis-v sosatie-menu-icon"></i>
                                  </a>
                                  <div class="dropdown-menu card-dropdown small-card-dropdown r-0" aria-labelledby="dropDownBox6">
                                    <div class="row no-gutters">

                                      <div class="col-12 mb-1 small-box-shadow custom-box-size txt-align-center tooltip-hover">
                                      <span id="calenderTooltip6" class="tooltiptext">Calender</span>
                                        <a hidden id="dates6" class=" p-2 dropdown-toggle border-0 bg-transparent " data-toggle="dropdown" onclick="tooltipCalender6()" onmouseover="onCalenderTooltipMouseOver()" onmouseout="onCalenderTooltipMouseOutHover()">
                                          <img class="img-fluid dropdown-comment-icon" src="img/calendar.png">
                                        </a>

                                      <div id="dateDropDown6" class="dropdown-menu r-0 min-w-dates date-dd right-dates">

                                        <h5 id="fromText6" class="dropdown-header txt-color p-0 date-margins">From: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="fromY6" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="fromM6" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>
                                        <h5 id="toText6" class="dropdown-header txt-color p-0 date-margins">To: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="toY6" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="toM6" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>

                                        <div class="row my-2">
                                          <div class="col-6 my-auto">
                                            <span hidden id="date-pick-error6" class="small text-secondary"></span>
                                          </div>
                                          <div class="col-6">
                                            <button type="button" id="datesS6" onclick="setDate('6')" class="mt-3 btn btn-cross-platform float-right mr-3 font-size-12" data-toggle="collapse" data-target="#dateDropDown6">Set Date</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>              
                                    <!-- <button type="button" id="move6" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                    <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openComments('6')">
                                    <span id="commentTooltip6" class="tooltiptext">Comments</span>
                                                    <a hidden id="com6" class="p-1 border-0 bg-transparent" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid dropdown-comment-icon" src="img/commentBubble.png">
                                                    </a>
                                                  </div>
                                                  <div id="refreshTag6" class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="refresh('6')">
                                                  <span id="refreshTooltip6" class="tooltiptext">Refresh</span>
                                                    <a id="refresh6" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="fas fa-sync-alt popover-load-icon mt-1 align-middle"></i>
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openModal('6')" data-toggle="modal" data-target="#popupModal6">
                                                  <span id="popupTooltip6" class="tooltiptext">Popup Annexure</span>
                                                    <a hidden id="pop6" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid popup-annexure-icon" src="img/trigger-modal-2.png">
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="scrollToAnnexure('6')">
                                                  <span id="annexTooltip6" class="tooltiptext">Annexure</span>
                                                    <a hidden id="annexBtn6" class="p-1 border-0 h-20px bg-transparent mt-n2" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="far fa-file annexure-icon fs-14 font-weight-200 align-middle"></i>
                                              </a>
                                                  </div>
                                  </div>
                                </div>
                              </div>
                              <!-- End of dropdown row -->
                            </div>

                              <div class="card-body p-1 m-top-20">
                                    <div class="box6" id="box6" draggable="true" ondrop="drop(event, this)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"></div>
                                    <div hidden id="modal-data6" data-value=""></div>
                                  </div>
                                  
                                  <div class="card-footer card-footer-curve text-muted border-0 bg-white p-0 h-20px">
                                  <label id="labelupdate6" class="fs-9 ml-2 align-middle"></label>
                                    <!-- <button type="button" id="desc1" onclick="setDescription('6')" class="float-left border-0 h-20px bg-white mt-n2"><img src="img/description.png" class="h-20px"></button> -->
                                    <!--<button type="button" hidden id="annexBtn1" class="float-right border-0 h-20px bg-white mt-n2" onclick="scrollToAnnexure('1')" ><i class="far fa-file annexure-icon fs-14 font-weight-200"></i></button>-->
                                    <a hidden id="clear6" class="float-right bg-transparent border-0 h-20px mr-2 mt-n1" onclick="clearBox('6', null, true)" >
                                            <img class="img-fluid popover-close-icon" src="img/clear-box-2.png">
                                          </a>
                                  </div>
                            </div>
                          </div>
                        </div>

                        <!-- Box Seven -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block">
                              <div id="popoverBox7" class="card" data-toggle="popover">
                              
                              <div class="d-none">

                                <div id="popoverBoxContent7" class="card-header card-header-curve text-muted border-0 p-0 h-20px">
                                    <div class="row no-gutters align-items-center">

                                    <div class="col-9">
                                                <!-- <label id="labelupdate1" class="font-size-10 mb-1"></label> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <a hidden id="refresh1" class="float-left border-0" onclick="refresh('1')">
                                                      <i class="fas fa-sync-alt popover-load-icon"></i>
                                                    </a> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <button type="button" id="move1" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                                <!-- <a hidden id="pop1" class="float-right border-0" onclick="openModal('1')" data-toggle="modal" data-target="#popupModal1"><img class="img-fluid popover-close-icon" src="img/trigger-modal-2.png"></a> -->
                                              </div>

                                              <div class="col-1">
                                                <!--<a hidden id="clear1" class="float-right border-0" onclick="clearBox('1')" ><img class="img-fluid popover-close-icon" src="img/clear-box-2.png"></a>-->
                                              </div>

                                      </div> <!-- End of inner row -->
                                    </div>
                                  </div>
                              <!-- End of dropdown row -->
                              <div class="card-header card-header-curve border-0 bg-transparent p-0 h-20px z-index-1">
                                <div id="dropDownBoxMenu7" class="col-2 float-right txt-align-right">
                                  <a hidden id="dropDownBox7" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="border-0 bg-white sosatie-menu-space">
                                    <!--<img class="img-fluid " src="img/sosatieMenu.png">-->
                                    <i class="fas fa-ellipsis-v sosatie-menu-icon"></i>
                                  </a>
                                <div class="dropdown-menu card-dropdown r-0" aria-labelledby="dropDownBox7">
                                  <div class="row no-gutters">

                                    <div class="col-12 mb-1 small-box-shadow custom-box-size txt-align-center tooltip-hover">
                                    <span id="calenderTooltip7" class="tooltiptext">Calender</span>
                                      <a hidden id="dates7" class=" p-2 dropdown-toggle border-0 bg-transparent" data-toggle="dropdown" onclick="tooltipCalender7()" onmouseover="onCalenderTooltipMouseOver()" onmouseout="onCalenderTooltipMouseOutHover()">
                                        <img class="img-fluid dropdown-comment-icon" src="img/calendar.png">
                                      </a>
                                      <div id="dateDropDown7" class="dropdown-menu r-0 min-w-dates date-dd">
                                        <h5 id="fromText7" class="dropdown-header txt-color p-0 date-margins">From: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="fromY7" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="fromM7" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>
                                        <h5 id="toText7" class="dropdown-header txt-color p-0 date-margins">To: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="toY7" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="toM7" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>

                                        <div class="row my-2">
                                          <div class="col-6 my-auto">
                                            <span hidden id="date-pick-error7" class="small text-secondary"></span>
                                          </div>
                                          <div class="col-6">
                                            <button type="button" id="datesS7" onclick="setDate('7')" class="mt-3 btn btn-cross-platform float-right mr-3 font-size-12" data-toggle="collapse" data-target="#dateDropDown7">Set Date</button>
                                          </div>
                                        </div>
                                    </div>
                                  </div>              
                                  <!-- <button type="button" id="move7" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openComments('7')">
                                  <span id="commentTooltip7" class="tooltiptext">Comments</span>
                                                    <a hidden id="com7" class="p-1 border-0 bg-transparent " onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid dropdown-comment-icon" src="img/commentBubble.png">
                                                    </a>
                                                  </div>
                                                  <div id="refreshTag7" class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="refresh('7')">
                                                  <span id="refreshTooltip7" class="tooltiptext">Refresh</span>
                                                    <a id="refresh7" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="fas fa-sync-alt popover-load-icon mt-1 align-middle"></i>
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openModal('7')" data-toggle="modal" data-target="#popupModal7">
                                                  <span id="popupTooltip7" class="tooltiptext">Popup Annexure</span>
                                                    <a hidden id="pop7" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid popup-annexure-icon" src="img/trigger-modal-2.png">
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="scrollToAnnexure('7')">
                                                  <span id="annexTooltip7" class="tooltiptext">Annexure</span>
                                                    <a hidden id="annexBtn7" class="p-1 border-0 h-20px bg-transparent mt-n2" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="far fa-file annexure-icon fs-14 font-weight-200 align-middle"></i>
                                              </a>
                                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- End of dropdown row -->
                          </div>

                              <div class="card-body p-1 m-top-20">
                                    <div class="box7" id="box7" draggable="true" ondrop="drop(event, this)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"></div>
                                    <div hidden id="modal-data7" data-value=""></div>
                                  </div>

                                  <div class="card-footer card-footer-curve text-muted border-0 bg-white p-0 h-20px">
                                  <label id="labelupdate7" class="fs-9 ml-2 align-middle"></label>
                                    <!-- <button type="button" id="desc1" onclick="setDescription('7')" class="float-left border-0 h-20px bg-white mt-n2"><img src="img/description.png" class="h-20px"></button> -->
                                    <!--<button type="button" hidden id="annexBtn1" class="float-right border-0 h-20px bg-white mt-n2" onclick="scrollToAnnexure('1')" ><i class="far fa-file annexure-icon fs-14 font-weight-200"></i></button>-->
                                    <a hidden id="clear7" class="float-right bg-transparent border-0 h-20px mr-2 mt-n1" onclick="clearBox('7', null, true)" >
                                            <img class="img-fluid popover-close-icon" src="img/clear-box-2.png">
                                          </a>
                                  </div>
                            </div>
                            </div>

                            <!-- Box Eight -->
                            <div class="col-lg-6 d-none d-lg-block">
                              <div id="popoverBox8" class="card" data-toggle="popover">

                              <div class="d-none">

                                <div id="popoverBoxContent8" class="card-header card-header-curve text-muted border-0 p-0 h-20px">
                                  <div class="row no-gutters align-items-center">

                                  <div class="col-9">
                                                <!-- <label id="labelupdate1" class="font-size-10 mb-1"></label> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <a hidden id="refresh1" class="float-left border-0" onclick="refresh('1')">
                                                      <i class="fas fa-sync-alt popover-load-icon"></i>
                                                    </a> -->
                                              </div>

                                              <div class="col-1">
                                                <!-- <button type="button" id="move1" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                                <!-- <a hidden id="pop1" class="float-right border-0" onclick="openModal('1')" data-toggle="modal" data-target="#popupModal1"><img class="img-fluid popover-close-icon" src="img/trigger-modal-2.png"></a> -->
                                              </div>

                                              <div class="col-1">
                                                <!--<a hidden id="clear1" class="float-right border-0" onclick="clearBox('1')" ><img class="img-fluid popover-close-icon" src="img/clear-box-2.png"></a>-->
                                              </div>

                                    </div> <!-- End of inner row -->
                                    </div>
                                    </div>
                                    <!-- End of dropdown row -->
                                  <div class="card-header card-header-curve border-0 bg-transparent p-0 h-20px z-index-1">
                                    <div id="dropDownBoxMenu8" class="col-2 float-right txt-align-right">
                                      <a hidden id="dropDownBox8" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="border-0 bg-white sosatie-menu-space">
                                        <!--<img class="img-fluid " src="img/sosatieMenu.png">-->
                                        <i class="fas fa-ellipsis-v sosatie-menu-icon"></i>
                                      </a>
                                    <div class="dropdown-menu card-dropdown r-0" aria-labelledby="dropDownBox8">
                                      <div class="row no-gutters">

                                      <div class="col-12 mb-1 small-box-shadow custom-box-size txt-align-center tooltip-hover">
                                      <span id="calenderTooltip8" class="tooltiptext">Calender</span>
                                        <a  hidden id="dates8" class=" p-2 dropdown-toggle border-0 bg-transparent" data-toggle="dropdown" onclick="tooltipCalender8()" onmouseover="onCalenderTooltipMouseOver()" onmouseout="onCalenderTooltipMouseOutHover()">
                                          <img class="img-fluid dropdown-comment-icon" src="img/calendar.png">
                                        </a>
                                      <div id="dateDropDown8" class="dropdown-menu r-0 min-w-dates date-dd right-dates">
                                        <h5 id="fromText8" class="dropdown-header p-0 txt-color">From: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="fromY8" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="fromM8" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){
                                                  echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>
                                        <h5 id="toText8" class="dropdown-header txt-color p-0 date-margins">To: </h5>
                                        <div class="row">
                                          <div class="col-6">
                                            <select id="toY8" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($years); $i++){ 
                                                echo '<option value="'.$years[$i].'">'.$years[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                          <div class="col-6">
                                            <select id="toM8" class="form-control custom-select p-1 dropdown-header font-size-12">
                                              <?php for($i = 0; $i < count($months); $i++){ 
                                                  echo '<option value="'.($i+1).'">'.$months[$i].'</option>'; }
                                              ?>
                                            </select>
                                          </div>
                                        </div>

                                        <div class="row my-2">
                                          <div class="col-6 my-auto">
                                            <span hidden id="date-pick-error8" class="small text-secondary"></span>
                                          </div>
                                          <div class="col-6">
                                            <button type="button" id="datesS8" onclick="setDate('8')" class="mt-3 btn btn-cross-platform float-right mr-3 font-size-12" data-toggle="collapse" data-target="#dateDropDown8">Set Date</button>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                  <!-- <button type="button" id="move8" class="float-right border-0 bg-white" draggable="true" ondrop="drop(event)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"><img src="img/move-box-2.png"></button> -->
                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openComments('8')">
                                  <span id="commentTooltip8" class="tooltiptext">Comments</span>
                                                    <a hidden id="com8" class="p-1 border-0 bg-transparent " onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid dropdown-comment-icon" src="img/commentBubble.png">
                                                    </a>
                                                  </div>
                                                  <div id="refreshTag8" class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="refresh('8')">
                                                  <span id="refreshTooltip8" class="tooltiptext">Refresh</span>
                                                    <a id="refresh8" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="fas fa-sync-alt popover-load-icon mt-1 align-middle"></i>
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="openModal('8')" data-toggle="modal" data-target="#popupModal8">
                                                  <span id="popupTooltip8" class="tooltiptext">Popup Annexure</span>
                                                    <a hidden id="pop8" class="p-1 bg-transparent border-0" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <img class="img-fluid popup-annexure-icon" src="img/trigger-modal-2.png">
                                                    </a>
                                                  </div>
                                                  <div class="col-12 small-box-shadow custom-box-size txt-align-center tooltip-hover" onclick="scrollToAnnexure('8')">
                                                  <span id="annexTooltip8" class="tooltiptext">Annexure</span>
                                                    <a hidden id="annexBtn8" class="p-1 border-0 h-20px bg-transparent mt-n2" onmouseover="onMenuTooltipMouseOver()" onmouseout="onMenuTooltipMouseOutHover()">
                                                      <i class="far fa-file annexure-icon fs-14 font-weight-200 align-middle"></i>
                                              </a>
                                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- End of dropdown row -->
                          </div>
                                    <div class="card-body p-1 m-top-20">
                                    <div class="box8" id="box8" draggable="true" ondrop="drop(event, this)" ondragover="allowDrop(event)" ondragstart="dragBox(event)"></div>
                                    <div hidden id="modal-data8" data-value=""></div>
                                  </div>

                                    <div class="card-footer card-footer-curve text-muted border-0 bg-white p-0 h-20px">
                                    <label id="labelupdate8" class="fs-9 ml-2 align-middle"></label>
                                      <!-- <button type="button" id="desc1" onclick="setDescription('8')" class="float-left border-0 h-20px bg-white mt-n2"><img src="img/description.png" class="h-20px"></button> -->
                                      <!--<button type="button" hidden id="annexBtn1" class="float-right border-0 h-20px bg-white mt-n2" onclick="scrollToAnnexure('1')" ><i class="far fa-file annexure-icon fs-14 font-weight-200"></i></button>-->
                                      <a hidden id="clear8" class="float-right bg-transparent border-0 h-20px mr-2 mt-n1" onclick="clearBox('8', null, true)" >
                                            <img class="img-fluid popover-close-icon" src="img/clear-box-2.png">
                                          </a>
                                    </div>
                              </div>
                            </div>
                          </div>
                        </div>     
                      </div>
                    </div>
                  </div>
                </section>

                        <!-- Popup Module -->
                        <?php 
                        for($i = 1; $i <= 8; $i++){
                          echo 
                          '<div id="popupModal' . $i . '" class="modal fade" role="dialog">' .
                            '<div class="modal-dialog modal-dialog-centered">' .
                              '<div class="modal-content w-75 mx-auto">' .
                                '<div class="modal-header txt-color mb-m3" id="modal-header' . $i . '">' .
                                  '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>' . 
                                '</div>' .
                                '<div class="modal-body" id="modal-body' . $i . '">' .
                                '</div>' .
                              '</div>' .
                            '</div>' .
                          '</div>';
                        }
                        ?>


                        <!-- BC Package Option Modal -->
                        <div class="modal fade" id="packageOption-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="packageOption" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered modal-px-5 py-3" role="document">
                            <div class="modal-content mx-5 w-100 text-center">
                              <div class="modal-header modal-header-border mb-5">
                              <div class=" p-0 w-100">
                                <div class="row w-100 no-gutters">
                                  <div class="col-12">

                                    <?php
                                        if(!$expired){
                                    ?>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    <?php
                                        }
                                    ?>

                                    </div> 
                                  </div>  
                                  <div class="row w-100 no-gutters">
                                  <div class="col-12">

                                  <?php

                                    $packageTitle = "Our Pricing Plans";
                                    if($user->getTrail() == TrailTypes::HADTRAIL && $user->hasExpired()){
                                      $packageTitle = "Your trial has expired";
                                    }

                                    
                                  ?>
                                    <h3 class="mb-4 mt-2 fw-400 txt-color"><?php echo htmlentities($packageTitle); ?></h3> 
                                  </div> 
                                  </div>  
                                  </div>  
                              </div>
                              <div class="modal-body py-5 my-3 modal-p-1">
                                <div class="container">
                                  <div class="row align-items-center justify-content-center">
                                    <div class="col-sm-9 col-md-4 col-lg-4 mb-md-4 mb-lg-0 mb-5 mt-sm-3 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                                      <div class="card modal-p-4 align-items-center shadow-sm" style="border: 0;">
                                      <div class="col-4 p-0 mt-n5 ">
                                      <img alt="BeanCruncher" class="img-fluid icon-position" src="img/Slices/Pricing/Free%20trial.png">
                                      </div>
                                        <div class="text-center pt-4">
                                          <h4 class="main-purple pb-md-0" style="font-size: 1.2rem;"><span><?php echo htmlentities(strtoupper($packageTypes[1]->description)) ?> </span></h4>
                                          <div class="d-flex justify-content-center">
                                            <span class="h5 mb-0 mr-0 social-color" style="margin-top: 2px;">R</span>
                                            <span class="display-4 mb-0 social-color"><?php echo htmlentities($packageTypes[1]->amount) ?></span>
                                          </div>
                                          <p class="light-purple mt-1 fs-12 not-bold">per month</p>
                                          <p class="main-purple bolder mb-2 mt-4 fs-15" style="font-size: 16px; font-weight: 600;padding-top: 3px;padding-bottom: 1.2px;">Sign up for free!</p>
                                        </div>
                                        <p class="text-center not-bold light-purple fw-200 fs-14 ">Experience the full power of data visualization with Beancruncher.</p>
                                        <hr class="orange">   
                                        <div class="text-center p-1">
                                          <p class="light-purple not-bold pb-lg-4 fw-200 fs-14 d-content">
                                          <br><br><br><br>
                                              One company <br>
                                              Modules on dashboard <br>
                                              Date ranges <br>
                                              Chart of Account features <br>
                                              Export to PDF <br><br><br><br><br>
                                          </p>
                                        </div>
                                        <hr class="orange" style="margin-top: 1.5rem !important;margin-bottom: 0.82rem !important;">
                                        <hr class="m-0" style="padding-bottom: 0.1rem !important;">
                                        <!-- <div class="col-12 text-center py-payment"> -->
                                        <div class="col-12 text-center pt-3 pb-2">
                                        <a class="btn btn-orange mt-4 mb-2" href="<?php echo htmlentities("packages.php?package=" . $packageTypes[1]->package_id) . "&trail=0";  ?>">Start</a>
                                        <br>
                                        <a class="fs-12 btn btn-trans mb-3">_</a>
                                      </div>
                                      </div>
                                    </div>
                                    <div class="col-sm-9 col-md-4 col-lg-4 mb-md-4 mb-lg-0 mb-5 mt-5 mt-sm-3 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                                      <div class="card modal-p-4 align-items-center shadow-sm" style="border: 0;">
                                      <div class="col-4 mt-n5 px-0 ">
                                      <img alt="BeanCruncher" class="img-fluid icon-position" src="img/Slices/Pricing/Single%20Entity.png">
                                      </div>
                                        <div class="text-center pt-4">
                                          <h4 class="main-purple pb-md-2" style="font-size: 1.2rem;"><span><?php echo htmlentities(strtoupper($packageTypes[2]->description)) ?> </span></h4>
                                          <div class="d-flex justify-content-center">
                                            <span class="h5 mb-0 mr-0 price-pink " style="margin-top: 2px;">R</span>
                                            <span class="display-4 mb-0 price-pink"><?php echo htmlentities($packageTypes[2]->amount) ?></span>
                                          </div>
                                          <div>
                                            <p class="light-purple mt-1 fs-12 not-bold pb-md-2">per month</p>
                                          </div>
                                        </div>
                                        <p class="text-center not-bold light-purple mt-md-4 pt-2 fw-200 fs-14">For a single company that requires profesional data visualization.</p>
                                        <hr class="price-pink">    
                                        <div class="text-center p-1">
                                          <p class="light-purple not-bold pb-lg-4 fw-200 fs-14 d-content">
                                              One company <br>
                                              Unlimited favourites <br>
                                              Modules on dashboard <br>
                                              Date ranges <br>
                                              Chart of Account features <br>
                                              Profile image <br>
                                              Company profile image <br>
                                              User management <br>
                                              Export to XLS <br>
                                              Export to PDF <br>
                                              Export to PPTX <br>
                                              Email dashboard reports <br>
                                              Add comments
                                          </p>
                                        </div>
                                        <hr class="m-0 pb-1">
                                        <hr class="price-pink"> 
                                        <div class="col-12 text-center pt-3 pb-2">
                                        <a class="btn btn-pink mt-4 mb-2" href="<?php echo htmlentities("packages.php?package=" . $packageTypes[2]->package_id) . "&trail=0";  ?>">Buy</a>
                                        <br>
                                        <?php 

                                          if($user->getTrail() == TrailTypes::NOTRAIL){
                                            ?>
                                              
                                                <a href="<?php echo htmlentities("packages.php?package=" . $packageTypes[2]->package_id) . "&trail=1";  ?>" class="fs-12 btn btn-pink-outlined mb-3">Free trial</a>
                                            <?php
                                          } else{
                                            ?>
                                            <a class="fs-12 btn btn-trans mb-3">_</a>
                                            <?php
                                          }
                                        ?>
                                        
                                      </div>
                                      </div>
                                    </div>
                                    <div class="col-sm-9 col-md-4 col-lg-4 mb-md-4 mb-lg-0 mb-5 mt-5 mt-sm-3 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                                      <div class="card modal-p-4 align-items-center shadow-sm" style="border: 0;">
                                      <div class="col-4 mt-n5 px-0 ">
                                      <img alt="BeanCruncher" class="img-fluid icon-position" src="img/Slices/Pricing/Multiple%20Entities.png">
                                      </div>
                                        <div class="text-center pt-4">
                                          <h4 class="main-purple pb-md-2" style="font-size: 1.2rem;"><span><?php echo htmlentities(strtoupper($packageTypes[3]->description)) ?> </span></h4>
                                          <div class="d-flex justify-content-center">
                                            <span class="h5 mb-0 mr-0 hasty-purple" style="margin-top: 1px;">R</span>
                                            <span class="display-4 mb-0 hasty-purple"><?php echo htmlentities($packageTypes[3]->amount) ?></span>
                                          </div>
                                          <div>
                                            <p class="light-purple mt-1 fs-12 not-bold pb-md-2">per month</p>
                                          </div>
                                        </div>
                                        <p class="text-center not-bold light-purple mt-md-4 pt-2 fw-200 fs-14">For linking of multiple companies that requires profesional data visualization</p>
                                        <hr class="hasty">    
                                        <div class="text-center p-1">
                                          <p class="light-purple not-bold pb-lg-4 fw-200 fs-14 d-content">
                                              Unlimited companies <br>
                                              Unlimited favourites <br>
                                              Modules on dashboard <br>
                                              Date ranges <br>
                                              Chart of Account features <br>
                                              Profile image <br>
                                              Company profile image <br>
                                              User management <br>
                                              Export to XLS <br>
                                              Export to PDF <br>
                                              Export to PPTX <br>
                                              Email dashboard reports <br>
                                              Add comments
                                          </p>
                                        </div>
                                        <hr class="m-0 pb-1">
                                        <hr class="hasty">
                                        <div class="col-12 text-center pt-3 pb-2">
                                        <a class="btn btn-purple mt-4 mb-2" href="<?php  echo htmlentities("packages.php?package=" . $packageTypes[3]->package_id) . "&trail=0";  ?>">Buy </a>
                                        <br>
                                        <?php 

                                        if($user->getTrail() == TrailTypes::NOTRAIL){
                                          ?>
                                        <a href="<?php echo htmlentities("packages.php?package=" . $packageTypes[3]->package_id) . "&trail=1";  ?>" class="fs-12 btn btn-purple-outlined mb-3">Free trial</a>

                                        <?php
                                          } else{
                                            ?>
                                            <a class="fs-12 btn btn-trans mb-3">_</a>
                                            <?php
                                          }
                                        ?>
                                        
                                      </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> 
                        <!-- END of BC Package Option Modal -->

                        <!-- Deactivated and Deleted companies Modal -->
                        <div class="modal fade" id="inactiveCompany-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="packageOption" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content w-35 text-center mx-auto">
                              <div class="modal-body">
                              <div class="container">
                                <div class="row justify-content-center mt-5">
                                  <img src="img/Password Access icon.png" class="img-fluid modal-icon-size">
                                </div>
                                <div class="row justify-content-center mt-6">
                                  <h6 class="txt-color font-weight-bold font-size-16">Restricted access</h6>
                                </div>
                                <p class="txt-color font-weight-normal font-size-14">
                                  <?php

                                    if(isset($_GET['deleted'])){

                                      $companies = explode(',',$_GET['deleted']);
                                      $ids = explode(',',$_GET['deletedID']);

                                      if($companies[0] != ''){
                                        for($i = 0; $i < count($companies); $i++){
                                          echo 'You have been removed from <b>' . $companies[$i] . '</b>.<br>';
                                          Companies::deleteUser($user->data()->user_email, $ids[$i]);
                                        }
                                      }
                                    }

                                    if(isset($_GET['inactive'])){

                                      $companies = explode(',',$_GET['inactive']);
                                      $ids = explode(',',$_GET['inactiveID']);

                                      if($companies[0] != ''){
                                        for($i = 0; $i < count($companies); $i++){
                                          echo 'Access from <b>' . $companies[$i] . '</b> has been restricted.<br>';
                                          Companies::setUserStatus($user->data()->user_email, 0, $ids[$i]);
                                        }
                                      }
                                    }

                                  ?>
                                </p>
                                <div class="row justify-content-center mt-3 mx-2 mb-lg-5">
                                  <div class="col-12">
                                    <a href="index.php" class="mt-3 btn btn-cross-platform w-35" type="submit">Continue</a>
                                  </div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- END of Deactivated and Deleted companies Modal -->

                        <!-- Comment Modal -->
                        <div id="commentsModal" class="modal fade" role="dialog">
                          <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content custom-w text-center mx-auto">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>

                              <div class="modal-body comment-card-mobile" id="modal-body">
                                <div class="container">
                                    <div class="row justify-content-center">
                                      <img src="img/Comments icon.png" class="img-fluid modal-icon-sm-size">
                                    </div>
                                    <div class="row justify-content-center my-4">
                                        <p id="comment-modal-header" class="txt-color font-weight-normal font-size-16"></p>
                                    </div>
                                    <div id="commentSection" class="mx-h-comments style-comments" style="overflow-x: hidden; overflow-y: auto;"></div>
                                    <div class="form-group">
                                      <img id="cancelEdit" onclick="cancelEdit()" src="img/clear-box-2.png" style="width:11px; height:11px;" class="float-right mx-1 my-2" hidden>
                                      <textarea class="form-control mh-9 min-h-9 p-10" id="commentForm" rows="7" placeholder="Leave a comment" onfocus="focusComment()" onblur="blurComment()"></textarea>
                                      <hr class="comment-textarea">
                                      <span class="font-size-10 text-muted float-right txt-color-light-grey">Max 250 characters</span>
                                    </div>
                                    <span id="comment-error" class="text-center color-red small" hidden>Comment cannot exceed 250 characters</div>
                                    <button type="button" id="setCommentBtn" onclick="setComment()" class="my-4 btn btn-cross-platform w-35">Submit</button>
                          
                                  </div>
                            </div>
                          </div>
                        </div>
                        </div>
                    
                        <!-- Annexures -->
                        <div id="annex-group">
                            <div id="annex1" class="my-4 bg-white"></div>
                            <div id="annex2" class="break my-4 bg-white"></div>
                            <div id="annex3" class="break my-4 bg-white annex-mobile"></div>
                            <div id="annex4" class="break my-4 bg-white"></div>
                            <div id="annex5" class="break my-4 bg-white"></div>
                            <div id="annex6" class="break my-4 bg-white"></div>
                            <div id="annex7" class="break my-4 bg-white"></div>
                            <div id="annex8" class="break my-4 bg-white"></div>
                        </div>
                        

                        <div id="pdfDash">
                            <div class="row"> 
                              <div class="col-12"> 
                                <h1 hidden="true" id="pdfDashTitle" class="pdf-h p-1 mt-3 txt-color">Dashboard Summary</h1>
                              </div>
                            </div>

                            <div class="row mt-3 pdf-sum-w">
                              <div id="pdfbox1"></div>
                              <div id="pdfbox2"></div>
                              <div id="pdfbox3"></div>
                              <div id="pdfbox4"></div>
                              <div id="pdfbox5"></div>
                              <div id="pdfbox6"></div>
                              <div id="pdfbox7"></div>
                              <div id="pdfbox8"></div>
                            </div>
                        </div>

                        <div id="pdfAnnexure">
                          <?php 
                            for($i = 1; $i <= 8; $i++){
                          ?>
                            <div class="row">
                                <div class="col-12">
                                    <div id="pdfAnnbox<?php echo $i; ?>">
                                    </div>
                                </div>
                            </div>
                          <?php 
                            }
                          ?>
                        <section class="collapse bg-light" id="sectionbox1">
                            
                        </section>

                    </div>
                </main>
                
            </div>
<!--            END OF DASHBOARD CONTENT-->
        </div>
        
        
        <!-- Modal --> 

    <div class="modal fade" id="favourite-modal" tabindex="-1" role="dialog" aria-labelledby="favourite-modal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content w-35 text-center mx-auto">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <div class="container">
            <div class="row justify-content-center">
              <img src="img/Report Icon.png" class="img-fluid modal-icon-size">
            </div>
            <div class="row justify-content-center my-2">
                <p class="txt-color font-weight-normal font-size-16">Add report selection to favourites:</p>
            </div>
            <div class="row justify-content-center mx-2 mb-lg-5">
               <div class="col-12">
                  <input id="favouriteName" class="w-100 form-control mb-2" type="text" name="" value="" placeholder=" Title">
                  <div id="favourite-error" class="my-3 color-red font-weight-normal font-size-16" hidden>Name already taken</div>
                  <input id="btnAddFavourite" class="btn btn-cross-platform w-35 mt-2" type="submit" value="Add" onclick="saveFavourite()">
                </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="export-modal" tabindex="-1" role="dialog" aria-labelledby="export-modal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content w-25 text-center mx-auto">
          <div class="modal-header">
          </div>
          <div class="modal-body">
           <div class="container">
            <div class="row justify-content-center">
              <img src="img/BC-preloader-gif.gif" class="img-fluid modal-icon-size">
            </div>
            <div class="row justify-content-center my-2">
              <p class="txt-color font-weight-normal font-size-16">Please wait for the export...</p>
              <div id="nav-loader" class="row" hidden></div>
                <div class="col-12">
                  <div id="nav-progress" hidden></div>
                </div>
                <div class="col-12">
                  <span id="nav-progress-text" class="small text-muted" hidden></span>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="email-modal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content w-35 text-center mx-auto">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <div class="container">
            <div class="row justify-content-center">
              <img src="img/Add email icon.png" class="img-fluid modal-icon-size">
            </div>
            <div class="row justify-content-center mt-6">
                <h6 class="txt-color font-weight-bold font-size-16">Add Email Address</h6>
            </div>
            <div class="row justify-content-center">
                <p class="txt-color font-weight-normal font-size-14">Please use a comma to separate multiple emails.</p>
            </div>
            <div class="row justify-content-center mt-3 mx-2 mb-lg-5">
               <div class="col-12">
                 <div class="input-group w-100 form-control">
                    <img src="img/Mail icon.png" class="img-fluid mail-icon-size align-self-center">
                    <input id="addresses" class="w-50 form-control tranparent-form-control" type="text" name="" value="" placeholder="Enter email address">
                  </div>
                  <br>
                  <input id="btnSendEmail" class="mt-3 btn btn-cross-platform w-35" type="submit" value="Send" onclick="prepareEmail()">
                </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div> 

    <div class="modal fade" id="email-sent-modal" tabindex="-1" role="dialog" aria-labelledby="email-sent-modal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content w-35 text-center mx-auto">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <div class="container">
            <div class="row justify-content-center">
            <img src="img/Email sent icon.png" class="img-fluid modal-icon-size">
            </div>
            <div class="row justify-content-center">
                <p class="txt-color font-weight-normal font-size-16">Email sent</p>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    
    <!-- Dashboard & Boxes Scripts -->
    <script src="js/dashboard.js"></script>
    <script src="js/boxes.js"></script>
    <script src="js/comments.js"></script>  

    <!-- Simple Search Script -->
    <script src="js/simpleSearch.js"></script>

    <!-- Box Popover Script -->
    <script src="js/box-popover.js"></script>

    <!-- Boxes -->
    <script src="js/SmallBox.js"></script>
    <script src="js/MedBox.js"></script>
    <script src="js/LargeBox.js"></script>

    <!-- Modal -->
    <script src="js/Modal.js"></script>

    <!-- Descriptions -->
    <script src="js/Description.js"></script>

    <!-- Graphs -->
    <script src="js/Graphs.js"></script>
    <script src="js/BeanChart.js"></script>

    <!-- Annexure -->
    <script src="js/Annexure.js"></script>  

    <!-- Exports -->
    <script src="js/export.js"></script>  
    
    <!-- PDF -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

    <!--chart js-->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.4.0/dist/chartjs-plugin-datalabels.min.js" crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>

    <!--dependency for Kendo UI API-->
    <script src="js/jszip.min.js"></script>
    <script src="js/kendo.all.min.js"></script>
      <!-- Pako enables compression on pdfs -->
    <script src="https://kendo.cdn.telerik.com/2021.1.119/js/pako_deflate.min.js"></script>

    <!--PDF LIB-->
    <script src="https://cdn.jsdelivr.net/npm/pdf-lib/dist/pdf-lib.js"></script>
      

    <?php 
      if($showPackages){
        ?>
        <script type="text/javascript">
            $("#packageOption-modal").modal("show");
          </script>
        <?php
      }

      if($showInactive){
        ?>
        <script type="text/javascript">
            $("#inactiveCompany-modal").modal("show");
          </script>
        <?php
      }
    ?>

<style>
    .k-pdf-export .col-5 {
        flex: 0 0 100% !important;
        max-width: 100% !important;
        margin: 50px;
    }

    .k-pdf-export .bg-white{
       background-color: transparent !important;
    }

    .k-pdf-export {
        margin: 50px 75px !important;
        height: 1403px !important;
    }

    .k-pdf-export .k-mx-auto {
        margin: auto;
    }

    .k-pdf-export .k-hide {
        display: none;
    }

    .k-pdf-export .k-show .d-none{
        display: block;
    }

    /* .k-pdf-export .txt-color{
	    color: #008666 !important;
    } */

</style>


<script type="text/javascript">
  
  $(document).ready(function(){
      checkPermissions(<?php echo htmlentities($package); ?>);
  });
</script>

    </body>
</html>

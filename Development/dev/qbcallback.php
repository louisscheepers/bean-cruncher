<?php 
    ini_set('display_errors', 'On');
    require_once(__DIR__ . '/core/init.php');
    require_once('const/const.php');
    // use QuickBooksOnline\API\DataService\DataService;

    session_start();

    function processCode(){
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
        //$logger->debug('qbcallback.php START - User is logged in!');

        //$logger->info('Getting SDK instance (data service) from QB class');
        $quickbooks = new Quickbooks(true);
        $dataService = $quickbooks->getDataService();

        $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();

        //$logger->info('Query string : ' . $_SERVER['QUERY_STRING']);
        $parseUrl = parseAuthRedirectUrl($_SERVER['QUERY_STRING']);

        //$logger->info('Generating access token');
        $accessToken = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($parseUrl['code'], $parseUrl['realmId']);

        //$logger->info('Updating OAuth2Token');
        $dataService->updateOAuth2Token($accessToken);

        //$logger->info('Adding access token to session');
        $_SESSION['sessionAccessToken'] = $accessToken;
    }

    function parseAuthRedirectUrl($url)
    {
        //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs'); 
        //$logger->info('Parsing URL parameters');
        parse_str($url,$qsArray);
        return array(
            'code' => $qsArray['code'],
            'realmId' => $qsArray['realmId']
        );
    }

    $result = processCode();

    //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs'); 
    //$logger->info('Reached end of callback, redirecting to qbcallbackLink.php');
    Redirect::to('qbcallbackLink.php');



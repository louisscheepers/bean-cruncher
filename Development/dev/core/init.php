<?php

date_default_timezone_set('Africa/Johannesburg');

$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => 'dev.beancruncher.co.za',
        'username' => 'beancruncher',
        'password' => 'Timetochangethegame2020',
        'db' => 'bc_dev_testing'
    ),
    'remember' => array(
        'cookie_name' => 'hash',
        'cookie_expiry' => 604800
    ),
    'session' => array(
        'session_name' => 'user',
        'token_name' => 'token'
    )
);

spl_autoload_register(function($class){
    clearstatcache();
    // $file = 'classes/' . $class . '.php';
    $file = dirname(__DIR__, 1) . '/classes/' . $class . '.php';
    // var_dump($file);
    if(file_exists($file)){
        require_once $file;
    }
});

require_once dirname(__DIR__, 1) . '/functions/sanitize.php';
require_once dirname(__DIR__, 1) . '/vendor/autoload.php';
session_start();

if(Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name'))){
    $hash = Cookie::get(Config::get('remember/cookie_name'));
    $hashCheck = DB::getInstance()->get('session', array('session_hash', '=', $hash));

    if($hashCheck->count()){
        $user = new User($hashCheck->first()->user_id);
        $user->login();
    }
}


$user = new User();

if(strpos(Redirect::getCurrentUri(), 'integration') !== false){
    if($user->isLoggedIn()){
        if(!Session::exists('oauth2')){
            // echo 'Api session missing getting new one';
            setActiveCompanyOnLogin();
            $auth = new Oauth();
            $auth->setSessionOauthFromDb();
        }else if(!Session::exists('active_company_id')){
            setActiveCompanyOnLogin();
        }
    }
    
    
}


if($user->isLoggedIn()){
    if(!strpos($_SERVER['REQUEST_URI'] ,"register.php") && !strpos($_SERVER['REQUEST_URI'] ,"verify") && !strpos($_SERVER['REQUEST_URI'] ,"reset-password") && !strpos($_SERVER['REQUEST_URI'] ,"recovery") &&!strpos($_SERVER['REQUEST_URI'] ,"index.php?package=expired") && !strpos($_SERVER['REQUEST_URI'] ,"logout.php")&& !strpos($_SERVER['REQUEST_URI'] ,"index.php?q=update") && !strpos($_SERVER['REQUEST_URI'] ,"packages.php") && !strpos($_SERVER['REQUEST_URI'] ,"checkout.php") && !strpos($_SERVER['REQUEST_URI'] ,"payment.php") && !strpos($_SERVER['REQUEST_URI'] ,"checkout.php") && !strpos($_SERVER['REQUEST_URI'] ,"test.php")){
        if($user->getPackage() < 2 || $user->hasExpired()){
             Redirect::to("index.php?package=expired");
        }

    }
}

function setActiveCompanyOnLogin(){
    if(Companies::getActiveCompanyId() == null){
        $companies = Companies::getCompanies();
        if($companies == null || count($companies) == 0){
            
        }else{
            Companies::setActiveCompany($companies[0]['name']);
        }

    }
}


?>
<?php
    require_once('const/const.php');
    require_once(__DIR__ . '/core/init.php');

    $now = new DateTime();
    $today = $now->format('Y-m-d');
    $companyName = Companies::getActiveCompanyName();

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $companyName . ' - ' . $today . '.csv');
    
    $output = fopen('php://output', 'w');

    $data = Session::get('data');

    for($box = 1; $box <= 8; $box++){
        if($data[$box] != null){

            $rows = null;
            $mod = $data[$box][0];
            $fromDate = $data[$box][1];
            $toDate = $data[$box][2];
            $modData = PullRange::getData($mod, $fromDate, $toDate);
            unset($modData['last_updated']);

            switch ($mod) {
                case 'cico':

                    fputcsv($output, array('Cash In v Cash Out'), ';');
                    fputcsv($output, array('', 'Cash In', 'Cash Out'), ';');

                    for($i = 0; $i < count($modData['cash_in']); $i++){
                        if($i == 0){
                            $dt = new DateTime($fromDate);
                            $date = $dt->format('Y-m');
                        }
                        else{
                            $dt->modify('+1 month');
                            $date = $dt->format('Y-m');
                        }
                        $rows[$i][0] = $date;
                        $rows[$i][1] = $modData['cash_in'][$i];
                        $rows[$i][2] = $modData['cash_out'][$i];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'ino':

                    fputcsv($output, array('Income v Expenses'), ';');
                    fputcsv($output, array('', 'Income', 'Expenses'), ';');

                    for($i = 0; $i < count($modData['income']); $i++){
                        if($i == 0){
                            $dt = new DateTime($fromDate);
                            $date = $dt->format('Y-m');
                        }
                        else{
                            $dt->modify('+1 month');
                            $date = $dt->format('Y-m');
                        }
                        $rows[$i][0] = $date;
                        $rows[$i][1] = $modData['income'][$i];
                        $rows[$i][2] = $modData['expenses'][$i];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'avd':

                    fputcsv($output, array('Assets v Debt'), ';');
                    fputcsv($output, array('', 'Assets', 'Debt'), ';');
                    
                    $rows[0] = $fromDate;
                    $rows[1] = $modData['assets'];
                    $rows[2] = $modData['debt'];

                    fputcsv($output, $rows, ';');

                break;
                case 'pl':

                    fputcsv($output, array('Profit & Loss as seen on ' . $fromDate), ';');
                    fputcsv($output, array('Account', 'Amount'), ';');
                    
                    $rowCount = 0;
                    $rows[$rowCount][0] = 'Sales';
                    $rowCount++;
                    for($i = 0; $i < count($modData['sales']); $i++){
                        $rows[$rowCount][0] = $modData['sales'][$i]['account'];
                        $rows[$rowCount][1] = $modData['sales'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Sales';
                    $rows[$rowCount][1] = $modData['total_sales'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Cost of Sales';
                    $rowCount++;
                    for($i = 0; $i < count($modData['cost_of_sales']); $i++){
                        $rows[$rowCount][0] = $modData['cost_of_sales'][$i]['account'];
                        $rows[$rowCount][1] = $modData['cost_of_sales'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Cost of Sales';
                    $rows[$rowCount][1] = $modData['total_cost_of_sales'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Gross Profit';
                    $rows[$rowCount][1] = $modData['gross_profit'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Other Income';
                    $rowCount++;
                    for($i = 0; $i < count($modData['other_income']); $i++){
                        $rows[$rowCount][0] = $modData['other_income'][$i]['account'];
                        $rows[$rowCount][1] = $modData['other_income'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Other Income';
                    $rows[$rowCount][1] = $modData['total_other_income'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Expenses';
                    $rowCount++;
                    for($i = 0; $i < count($modData['expenses']); $i++){
                        $rows[$rowCount][0] = $modData['expenses'][$i]['account'];
                        $rows[$rowCount][1] = $modData['expenses'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Expenses';
                    $rows[$rowCount][1] = $modData['total_expenses'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Net Profit Before Tax';
                    $rows[$rowCount][1] = $modData['total_expenses'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Tax';
                    $rowCount++;
                    for($i = 0; $i < count($modData['tax']); $i++){
                        $rows[$rowCount][0] = $modData['tax'][$i]['account'];
                        $rows[$rowCount][1] = $modData['tax'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Tax';
                    $rows[$rowCount][1] = $modData['total_tax'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Net Profit After Tax';
                    $rows[$rowCount][1] = $modData['net_profit_after_tax'][0];;
                    $rowCount++;

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'bs':

                    fputcsv($output, array('Balance Sheet as seen on ' . $fromDate), ';');
                    fputcsv($output, array('Account', 'Amount'), ';');

                    $rowCount = 0;
                    $rows[$rowCount][0] = 'Current Assets';
                    $rowCount++;
                    for($i = 0; $i < count($modData['current_assets']); $i++){
                        $rows[$rowCount][0] = $modData['current_assets'][$i]['account'];
                        $rows[$rowCount][1] = $modData['current_assets'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Current Assets';
                    $rows[$rowCount][1] = $modData['total_current_assets'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Non-Current Assets';
                    $rowCount++;
                    for($i = 0; $i < count($modData['non_current_assets']); $i++){
                        $rows[$rowCount][0] = $modData['non_current_assets'][$i]['account'];
                        $rows[$rowCount][1] = $modData['non_current_assets'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Non-Current Assets';
                    $rows[$rowCount][1] = $modData['total_non_current_assets'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Total Assets';
                    $rows[$rowCount][1] = $modData['total_assets'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Current Liabilities';
                    $rowCount++;
                    for($i = 0; $i < count($modData['current_liabilities']); $i++){
                        $rows[$rowCount][0] = $modData['current_liabilities'][$i]['account'];
                        $rows[$rowCount][1] = $modData['current_liabilities'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Current Liabilities';
                    $rows[$rowCount][1] = $modData['total_current_liabilities'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Non-Current Liabilities';
                    $rowCount++;
                    for($i = 0; $i < count($modData['non_current_liabilities']); $i++){
                        $rows[$rowCount][0] = $modData['non_current_liabilities'][$i]['account'];
                        $rows[$rowCount][1] = $modData['non_current_liabilities'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Non-Current Liabilities';
                    $rows[$rowCount][1] = $modData['total_non_current_liabilities'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Equity';
                    $rowCount++;
                    for($i = 0; $i < count($modData['equity']); $i++){
                        $rows[$rowCount][0] = $modData['equity'][$i]['account'];
                        $rows[$rowCount][1] = $modData['equity'][$i]['amount'][0];
                        $rowCount++;
                    }
                    $rows[$rowCount][0] = 'Total Equity';
                    $rows[$rowCount][1] = $modData['total_equity'][0];;
                    $rowCount++;
                    $rows[$rowCount][0] = 'Total Equity and Liabilities';
                    $rows[$rowCount][1] = $modData['total_equity_and_liabilities'][0];;
                    $rowCount++;

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'cfs':
                    $module = 'Cash Flow Statement';
                break;
                case 'ar':

                    fputcsv($output, array('Accounts Receivable on ' . $fromDate), ';');     
                    fputcsv($output, array('Account', '0-30d', '30-60d', '60-90d', '90-120d', '120d+', 'Total'), ';');                   
                    
                    for($i = 0; $i < count($modData['accounts']); $i++){
                        $rows[$i][0] = $modData['accounts'][$i]['name'];
                        $rows[$i][1] = $modData['accounts'][$i]['0-30'];
                        $rows[$i][2] = $modData['accounts'][$i]['30-60'];
                        $rows[$i][3] = $modData['accounts'][$i]['60-90'];
                        $rows[$i][4] = $modData['accounts'][$i]['90-120'];
                        $rows[$i][5] = $modData['accounts'][$i]['120+'];
                        $rows[$i][6] = $modData['accounts'][$i]['total'];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'ddo':

                    fputcsv($output, array('Debtor Days Outstanding on ' . $fromDate), ';');     
                    fputcsv($output, array('0-30d', '30-60d', '60-90d', '90-120d', '120d+', 'Total'), ';');  

                    $_0_30 = 0;
                    $_30_60 = 0;
                    $_60_90 = 0;
                    $_90_120 = 0;
                    $_120 = 0;
                    $_total = 0;

                    for($i = 0; $i < count($modData['accounts']); $i++){
                        $_0_30 += $modData['accounts'][$i]['0-30'];
                        $_30_60 += $modData['accounts'][$i]['30-60'];
                        $_60_90 += $modData['accounts'][$i]['60-90'];
                        $_90_120 += $modData['accounts'][$i]['90-120'];
                        $_120 += $modData['accounts'][$i]['120+'];
                        $_total += $modData['accounts'][$i]['total'];
                    }

                    $rows = [$_0_30, $_30_60, $_60_90, $_90_120, $_120, $_total];
                    fputcsv($output, $rows, ';');

                break;
                case 'tcs':

                    fputcsv($output, array('Top Customers by Balance Outstanding on ' . $fromDate), ';');     
                    fputcsv($output, array('Account', '0-30d', '30-60d', '60-90d', '90-120d', '120d+', 'Total'), ';');                   
                    
                    for($i = 0; $i < count($modData['accounts']); $i++){
                        $rows[$i][0] = $modData['accounts'][$i]['name'];
                        $rows[$i][1] = $modData['accounts'][$i]['0-30'];
                        $rows[$i][2] = $modData['accounts'][$i]['30-60'];
                        $rows[$i][3] = $modData['accounts'][$i]['60-90'];
                        $rows[$i][4] = $modData['accounts'][$i]['90-120'];
                        $rows[$i][5] = $modData['accounts'][$i]['120+'];
                        $rows[$i][6] = $modData['accounts'][$i]['total'];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'ret':

                    fputcsv($output, array('Retention'), ';');

                    for($i = 0; $i < count($modData['retention']); $i++){
                        if($i == 0){
                            $dt = new DateTime($fromDate);
                            $date = $dt->format('Y-m');
                        }
                        else{
                            $dt->modify('+1 month');
                            $date = $dt->format('Y-m');
                        }
                        $rows[$i][0] = $date;
                        $rows[$i][1] = $modData['retention'][$i];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'ap':

                    fputcsv($output, array('Accounts Payable on ' . $fromDate), ';');     
                    fputcsv($output, array('Account', '0-30d', '30-60d', '60-90d', '90-120d', '120d+', 'Total'), ';');                   
                    
                    for($i = 0; $i < count($modData['accounts']); $i++){
                        $rows[$i][0] = $modData['accounts'][$i]['name'];
                        $rows[$i][1] = $modData['accounts'][$i]['0-30'];
                        $rows[$i][2] = $modData['accounts'][$i]['30-60'];
                        $rows[$i][3] = $modData['accounts'][$i]['60-90'];
                        $rows[$i][4] = $modData['accounts'][$i]['90-120'];
                        $rows[$i][5] = $modData['accounts'][$i]['120+'];
                        $rows[$i][6] = $modData['accounts'][$i]['total'];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'cdo':

                    fputcsv($output, array('Creditor Days Outstanding on ' . $fromDate), ';');     
                    fputcsv($output, array('0-30d', '30-60d', '60-90d', '90-120d', '120d+', 'Total'), ';');  

                    $_0_30 = 0;
                    $_30_60 = 0;
                    $_60_90 = 0;
                    $_90_120 = 0;
                    $_120 = 0;
                    $_total = 0;

                    for($i = 0; $i < count($modData['accounts']); $i++){
                        $_0_30 += $modData['accounts'][$i]['0-30'];
                        $_30_60 += $modData['accounts'][$i]['30-60'];
                        $_60_90 += $modData['accounts'][$i]['60-90'];
                        $_90_120 += $modData['accounts'][$i]['90-120'];
                        $_120 += $modData['accounts'][$i]['120+'];
                        $_total += $modData['accounts'][$i]['total'];
                    }

                    $rows = [$_0_30, $_30_60, $_60_90, $_90_120, $_120, $_total];
                    fputcsv($output, $rows, ';');

                break;
                case 'tsp':

                    fputcsv($output, array('Top Suppliers by Balance Outstanding on ' . $fromDate), ';');     
                    fputcsv($output, array('Account', '0-30d', '30-60d', '60-90d', '90-120d', '120d+', 'Total'), ';');                   
                    
                    for($i = 0; $i < count($modData['accounts']); $i++){
                        $rows[$i][0] = $modData['accounts'][$i]['name'];
                        $rows[$i][1] = $modData['accounts'][$i]['0-30'];
                        $rows[$i][2] = $modData['accounts'][$i]['30-60'];
                        $rows[$i][3] = $modData['accounts'][$i]['60-90'];
                        $rows[$i][4] = $modData['accounts'][$i]['90-120'];
                        $rows[$i][5] = $modData['accounts'][$i]['120+'];
                        $rows[$i][6] = $modData['accounts'][$i]['total'];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'im':

                    fputcsv($output, array('Inventory on ' . $fromDate), ';');
                    fputcsv($output, array('Item', 'Quantity', 'Purchase Price', 'Sell Price'), ';');

                    for($i = 0; $i < count($modData['item']); $i++){
                        if($modData['item'][$i]['quantity_on_hand'] != null){
                            $rows[$i][0] = $modData['item'][$i]['name'];
                            $rows[$i][1] = $modData['item'][$i]['quantity_on_hand'];
                            $rows[$i][2] = $modData['item'][$i]['purchase_price'];
                            $rows[$i][3] = $modData['item'][$i]['sell_price'];
                        }
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'inas':
                    
                    fputcsv($output, array('Item Sales on ' . $fromDate), ';');
                    fputcsv($output, array('Item', 'Sales'), ';');

                    for($i = 0; $i < count($modData); $i++){
                        $rows[$i][0] = $modData[$i]['item_code'];
                        $rows[$i][1] = $modData[$i]['total_sales'];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'inaq':

                    fputcsv($output, array('Top Inventory (By Quantity) on ' . $fromDate), ';');
                    fputcsv($output, array('Item', 'Quantity'), ';');

                    for($i = 0; $i < count($modData['item']); $i++){
                        if($modData['item'][$i]['quantity_on_hand'] != null){
                            $rows[$i][0] = $modData['item'][$i]['name'];
                            $rows[$i][1] = $modData['item'][$i]['quantity_on_hand'];
                        }
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'plr':

                    fputcsv($output, array('Profit & Loss Ratios'), ';');
                    fputcsv($output, array('', 'Gross Margin', 'Operating margin', 'Net Margin'), ';');

                    for($i = 0; $i < count($modData['gross_margin']); $i++){
                        if($i == 0){
                            $dt = new DateTime($fromDate);
                            $date = $dt->format('Y-m');
                        }
                        else{
                            $dt->modify('+1 month');
                            $date = $dt->format('Y-m');
                        }
                        $rows[$i][0] = $date;
                        $rows[$i][1] = $modData['gross_margin'][$i];
                        $rows[$i][2] = $modData['operating_margin'][$i];
                        $rows[$i][3] = $modData['net_margin'][$i];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
                case 'bsr':

                    fputcsv($output, array('Balance Sheet Ratios'), ';');
                    fputcsv($output, array('', 'Current Ratio', 'Quick Ratio', 'Debt to Equity'), ';');

                    for($i = 0; $i < count($modData['current_ratio']); $i++){
                        if($i == 0){
                            $dt = new DateTime($fromDate);
                            $date = $dt->format('Y-m');
                        }
                        else{
                            $dt->modify('+1 month');
                            $date = $dt->format('Y-m');
                        }
                        $rows[$i][0] = $date;
                        $rows[$i][1] = $modData['current_ratio'][$i];
                        $rows[$i][2] = $modData['quick_ratio'][$i];
                        $rows[$i][3] = $modData['debt_to_equity'][$i];
                    }

                    foreach($rows as $row){
                        fputcsv($output, (array)$row, ';');
                    }

                break;
            }
            fputcsv($output, array(''), ';');
        }
    }
    
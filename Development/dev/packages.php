<?php
require __DIR__ . '/core/init.php';

if(Input::exists('GET')){
    $package = Input::get("package");
    $freetrail = Input::get("trail");

    $user = new User();

    $freetrail = ($user->getTrail() == TrailTypes::NOTRAIL && strcmp($freetrail, '1') == 0) ? true : false;

    if($user->isLoggedIn()){
        $db = DB::getInstance();

        $db->get('package_types', array('package_id', ">", 0));
        if($package < 3){
            $user->updatePackage($package, $freetrail);
            Redirect::to('index.php');
        }else{

            if($freetrail){
                $user->updatePackage($package, $freetrail);

                Redirect::to('index.php');
            }else{
                $packageTypes = $db->get('package_types', array('package_id', ">", 0));

                if($packageTypes->count()){
                    $packageTypes = $packageTypes->results();
                }
    
                $amount = 0;
    
                foreach($packageTypes as $pkg){
                    if($package == $pkg->package_id){
                        $amount = $pkg->amount;
                    }
                }
                
                Redirect::to('checkout.php?package=' . $package . "&amount=" . $amount);
            }
            
        }


    }else{
        Redirect::to("index.php");
    }
}else{
    Redirect::to("index.php");
}



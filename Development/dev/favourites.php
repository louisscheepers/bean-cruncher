<?php
  ini_set('display_errors', 'On');
  require_once('const/const.php');
  require_once(__DIR__ . '/core/init.php');
  $user = new User();
  if($user->isLoggedIn()){
    $role = Session::get('active_role_id');
  }
  else{
    Redirect::to('index.php');
  }
  
  if(Companies::getActiveCompanyId() == null){
    $companies = Companies::getCompanies();
    if($companies == null){
        Redirect::to('integration.php');
    }
    else{
        Companies::setActiveCompany($comapnies[0]['id']);
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Favourites</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
        
        <link href="css/custom.css" rel="stylesheet" />
    </head>
    <body class="content-bg">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
               
            <nav class="sb-topnav navbar navbar-expand navbar-dark p-0 my-3 mobile-mx">    
                    <div class="container nav-size">
                        <div class="row d-content no-gutters">
                            <!-- First Colum with nested colums -->
                            <div class="col-4 col-lg-5">
                                <div class="row">
                                <div class="col-4 col-lg-2 align-self-center">
                                        <a type="button" class="btn-lg d-content" href="index.php">
                                            <img class="img-fluid arrow-icon-size pl-1" src="img/arrow-left-icon.png">
                                        </a>
                                    </div>

                                    <div class="col-sm-10 align-self-center d-none d-lg-block">
                                      <div class="dropdown d-none d-md-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                                      <img class="img-fluid search-icon-small" src="img/Group 1511@2x.png">
                                        <input class="search-element" type="text" id="companySearch" autocomplete="off" onkeyup="search()" placeholder="Find company..." title="List of Companies" data-toggle="dropdown">
                                        <i class="fas fa-chevron-down search-icon-dropdown"></i>
                                        <div id="companiesDropDown" class="dropdown-menu r-0 radius-0" aria-labelledby="dropdownMenuButton">
                                          <?php
                                            $companies = Companies::getCompanies();
                                            for($i = 0; $i < count((array)$companies); $i++){
                                              if($companies[$i]['status'] == 1){
                                              ?>
                                              <li>
                                                <a id="active-company-dropdown" class="dropdown-item" onclick="loadCompany('<?php str_replace('\'', '\\\'', $companies[$i]['name']) ?>', <?php echo $companies[$i]['id']; ?>)"><?php echo $companies[$i]['name']; ?></a>
                                              </li>
                                              <?php
                                              }
                                            }
                                          ?> 
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-6 d-lg-none align-self-center">
                                        <div class="dropdown">
                                            <a class="a-icon-sm" href="#" role="button" onclick="mobileSearch()">
                                                <img class="img-fluid search-icon-sm" src="img/Search Icon.png">
                                            </a>
                                            <div id="mbSearch" class="dr-radius dropdown-menu collapse d-lg-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                                                <input class="search-element companyMobileSearch" type="text" id="companySearch" onkeyup="search()" placeholder="Find company..." title="List of Companies">
                                                <div id="companiesDropDown" class="r-0" aria-labelledby="dropdownMenuButton">
                                                <?php
                                                    $companies = Companies::getCompanies();
                                                    for($i = 0; $i < count((array)$companies); $i++){
                                                    if($companies[$i]['status'] == 1){
                                                    ?>
                                                    <li>
                                                        <a id="active-company-dropdown" class="dropdown-item" onclick="loadCompany('<?php echo str_replace('\'', '\\\'', $companies[$i]['name']) ?>')"><?php echo $companies[$i]['name']; ?></a>
                                                    </li>
                                                    <?php
                                                    }
                                                    }
                                                ?> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Second Colum with nested colums -->    
                            <div class="col-4 col-lg-2 text-center">
                                <h5 class="nav-settings d-content txt-color">
                                Favourites
                                </h5>
                            </div>

                            <!-- Third Colum with nested colums -->
                            <div class="col-4 col-lg-5">
                                <div class="row">
                                    <div class="col-lg-1 d-none d-lg-block">
                                        <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0 "></div>
                                    </div>
                                    
                                    <div class="col-lg-9 txt-align-right align-self-center d-none d-lg-block">
                                        <h5 class="nav-settings d-content">
                                            <div id="comp-name"><?php echo Companies::getActiveCompanyName() ?></div>
                                        </h5>
                                    </div>
                                    <div class="col-12 col-lg-2 align-self-center">
                                        <div id="comp-icon" class="float-right">
                                            <?php
                                              if(Companies::getActiveCompanyBrand() == null){
                                                  echo '<img id="comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
                                              }
                                              else{
                                                  echo '<img id="comp-img" src="data:image/jpeg;base64,' . base64_encode(Companies::getActiveCompanyBrand()) . '" class="rounded-circle icon-size" alt="Profile Icon">';
                                              }
                                            ?>
                                        </div>
                                      </div>
                                </div>
                             </div>
                        </div>
                    </div>
                </nav>
              <main>
                    <div class="container my-5 p-0">
                        <div class="row mt-lg-1">
                            <div class="col-12 mt-lg-1">
                               <div class="card mobile-mx">
                                  <div class="card-body profile-card">
                                        <div class="row no-gutters blue-card mb-4">
                                          <div class="col-6 col-lg-9 align-self-center">
                                            <div class="row no-gutters">
                                              <div class="col-sm-1 d-none d-lg-block">
                                                <img src="img/star-white.png" class="img-fluid profile-icon-size mb-small">
                                              </div>
                                              <div class="col-sm-5 ml-m20">
                                                <h5 class="font-weight-light align-middle txt-color-w m-0 ">Favourites</h5>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-6 col-lg-3 align-self-center">
                                           <button class="btn float-right filter-icon  font-weight-200" onclick="deleteFavourites()">
                                            <i class="fas fa-times mx-3"></i> Remove selected
                                            </button>
                                          </div>
                                        </div>
                                        <div class="p-4 mt-n2">
                                          <h5 class="txt-color font-weight-bold">Saved Dashboard layouts:</h5>
                                          <div id="favourites"></div>
                                          <div id="loader" class="text-center"><img src="img/BC-preloader-gif.gif" style="height: 40px;"></div>
                                        </div>
                                  </div>
                                </div>
                           </div>
                       </div>
                    </div>
                </main>
            </div>
        </div>

        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Company Settings Script -->
        <script src="js/favourites.js"></script>

         <!-- Simple Search Script -->
        <script src="js/simpleSearch.js"></script>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    </body>
</html>

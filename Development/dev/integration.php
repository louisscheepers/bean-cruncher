<?php
  ini_set('display_errors', 'On');
require_once __DIR__ . '/core/init.php';

//$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
$user = new User();
if($user->isLoggedIn()){
    //$logger->debug('Integration - User is logged in');
}
else{
    //$logger->debug('Integration - NOT LOGGED IN!');
}

  $user = new User();
  if(!$user->isLoggedIn()){
    Redirect::to('index.php');
  }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

        <title>BC | Integration</title>

    </head>
    <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
        <div class="container" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-6 mt-6">
                <div class="col-11 col-sm-12 col-md-10 col-lg-6 col-xl-6 px-0 card rounded-0-1 my-auto custom-card white-boxShadow">
                    <a class="pt-3 pl-24px" href="javascript:history.back()">
                        <img class="h-20px" src="img/arrow-left-icon.png">
                    </a>
                <!-- Logo -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-8 col-sm-6 col-md-6 col-lg-6 col-xl-6 logo-size-int">
                            <img src="img/logo.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                <!-- Heading -->
                    <div class="row d-flex justify-content-center mb-2">    
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <span>Integration</span>
                        </div>
                    </div>
                <!-- Lock -->
                    <div class="row d-flex justify-content-center my-4">
                        <div class="col-3 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-0">
                            <img src="img/Integration.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                <!-- Brand text -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 px-3 mb-3">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0 text-center">
                                <p class="verify-txt-color fs-13">Link your cloud accounting software.<br/> <b>Please select your accounting provider.</b></p>
                            </div>
                        </div>
                    </div>
                    <?php
                        // if(isset($_GET['inactive'])){
                        //     $inactive = $_GET['inactive'];
                        //     $by = $_GET['by'];

                        //     echo '<div class="row d-flex tx-center justify-content-center cont-b mt-2">';
                        //     echo '<div class="col-10 col-sm-8 col-md-8 col-lg-8 col-xl-8 px-0 container rounded-0-5 bg-light w-75 my-4">';
                        //     echo 'You have been deactivated from ' . $inactive . ' by ' . $by . '.';
                        //     echo '<br><br>To continue using Beancruncher, please link a company.';
                        //     echo '</div>';
                        //     echo '</div>';
                        // }

                        if(isset($_GET['nocomp'])){
                            echo '<div class="row my-lg-2"><div class="col-sm-12 text-color-blue text-center small">Please link a company to continue</div></div>';
                        }

                        if(isset($_GET['exists']) && $_GET['exists'] != '' || isset($_GET['success']) && $_GET['success'] != ''){
                            echo '<div class="row d-flex tx-center justify-content-center cont-b mt-2">';
                            echo '<div class="col-10 col-sm-8 col-md-8 col-lg-8 col-xl-8 px-0 container rounded-0-5 bg-light w-75 my-4">';
                        }

                        if(isset($_GET['success']) && $_GET['success'] != ''){
                            $successComps = explode(',', $_GET['success']);
                            echo '<div class="row text-center my-3">';
                            echo '<div class="col-sm-12 text-color-green small text-center">';
                            if(count($successComps) == 1){
                                if($successComps != ''){
                                    echo '<p class="text-center text-color-green px-2"><b>' . $successComps[0] . '</b> successfully registered!</p>';
                                }
                            }
                            else{
                                echo '<p class="text-left text-color-green px-3 mb-1">The following companies has been successfully registered: </p>';
                                for($i = 0; $i < count($successComps); $i++){
                                    echo '<b class="float-left mx-3">• ' . $successComps[$i] . '</b><br>';
                                }
                            }
                            echo '</div></div>';
                        }

                        if(isset($_GET['exists']) && $_GET['exists'] != ''){
                            $registeredComps = explode(',', $_GET['exists']);
                            echo '<div class="row text-center my-3">';
                            echo '<div class="col-sm-12 text-color-green small text-center">';
                            // echo '<div class="row my-lg-2 text-center">';
                            // echo '<div class="col-sm-12 text-color-green small text-center mb-3">';
                            if(count($registeredComps) == 1){
                                echo '<p class="text-center text-color-green px-2"><b>' . $registeredComps[0] . '</b> has already been registered to Beancruncher by another user.</p>';
                            }
                            else{
                                echo '<p class="text-left text-color-green px-3 mb-1">The following companies are already registered to Beancruncher: </p>';
                                for($i = 0; $i < count($registeredComps); $i++){
                                    echo '<b class="float-left mx-3">• ' . $registeredComps[$i] . '</b><br>';
                                }
                            }
                            echo '</div></div>';
                        }

                        if(isset($_GET['exists']) && $_GET['exists'] != '' || isset($_GET['success']) && $_GET['success'] != ''){
                            echo '</div></div>';
                        }
                    ?>
                <!-- Xero -->
                    <div class="row d-flex tx-center justify-content-center cont-b mt-2">
                        <div class="col-10 col-sm-8 col-md-8 col-lg-8 col-xl-8 px-0">
                            <a href="xeroconnect.php" class="justify-content-center my-auto">
                                <div class="container shadow-custom-sm x-h">
                                    <img src="img/xero-2.png" class="img-fluid brand-height-xero mt-1" alt="Xero">
                                </div>
                            </a>
                        </div>
                    </div>
                <!-- Sage -->
                    <div class="row d-flex tx-center justify-content-center cont-b">
                        <div class="col-10 col-sm-8 col-md-8 col-lg-8 col-xl-8 px-0">
                            <a href="selectCompanies.php" class="justify-content-center">
                                <div class="container shadow-custom-sm s-h">
                                    <img src="img/sage-2.png" class="img-fluid brand-height-sage pt-3" alt="Sage">
                                </div>
                            </a>
                        </div>
                    </div>
                <!-- Quickbooks -->
                    <div class="row d-flex tx-center justify-content-center pb-3 we-sent-row">
                        <div class="col-10 col-sm-8 col-md-8 col-lg-8 col-xl-8 px-0">
                            <a href="qbconnect.php" class="justify-content-center my-auto">
                            <!-- <a href="#" onclick="oauth.loginPopup()" class="justify-content-center my-auto"> -->
                            <div class="container shadow-custom-sm q-h">
                                    <img src="img/quickbooks-2.png" class="img-fluid brand-height-q mt-1 integration-p" alt="Quickbooks">
                                </div>
                            </a>
                        </div>
                        <?php
                            // $displayString = isset($accessTokenJson) ? $accessTokenJson : "No Access Token Generated Yet";
                            // echo json_encode($displayString, JSON_PRETTY_PRINT); 
                        ?>
                    </div>
                <!-- Next -->
                    <?php 
                    if(isset($_GET['exists'])){
                        $comps = Companies::getCompanies();
                        if(count($comps) > 0){
                            echo 
                            '<div class="row d-flex justify-content-center int-btn-b">' .
                                '<div class="col-5 col-sm-4 col-md-4 lg-4 col-xl-4">' .
                                    '<input type="button" class="btn btn-block nav-free-but the-border main-purple pb-2 fs-12" value="Done" onclick="window.location=\'index.php\';">' .
                                '</div>' .
                            '</div>';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer> 
        



        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    </body>
</html>
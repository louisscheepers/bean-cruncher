<?php
    ini_set('display_errors', 'On');
    require_once('const/const.php');
    require_once(__DIR__ . '/core/init.php');
    //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');

    //$logger->info('qbconnect - Getting QB instance');
    $quickbooks = new Quickbooks(true);
    //$logger->info('qbconnect - Getting Data Sevice');
    $dataService = $quickbooks->getDataService();

    //$logger->info('qbconnect - Getting OAuth2LoginHelper');
    $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
    //$logger->info('qbconnect - Getting getAuthorizationCodeURL');
    $authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

    $_SESSION['authUrl'] = $authUrl;

    //$logger->info('qbconnect - Going to Auth URL');
    Redirect::to($authUrl);

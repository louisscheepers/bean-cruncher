<?php
require_once __DIR__ . '/core/init.php';

    $matched = true;
    $user;
    $salt;

    if(Input::exists('GET')){
        $user = new User($_GET['email']);
        
        $dbHash = $user->data()->user_salt;
        $getHash = $_GET['hash'];
        $email = $_GET['email'];


        if($dbHash != $getHash){
            // echo 'Invalid hash, redirect to recovery.php';
            Redirect::to('recovery.php');
        }
        else{
            $salt = $getHash;
        }
    }
    else{
        // echo 'No get,  redirect to recovery.php<br><br>';
        Redirect::to('recovery.php');
    }

    if(Input::exists('POST')){
        if(isset($_POST['reset'])){
            if(isset($_POST['password']) && isset($_POST['password-again'])){
                $validate = new Validate();
                $validation = $validate->check($_POST, array(
                    'password' => array(
                        'required' => true,
                        'min' => 6,
                    ),
                    'password-again' => array(
                        'required' => true,
                        'matches' => 'password'
                    )
                ));
            
                if($validate->passed()){
                    // echo 'Updating password';
                    $id = $user->data()->user_id;
                    $user->update(array('user_password' => Hash::make($_POST['password'], $salt)), $id);
                    Redirect::to('login.php');
                }
                else{
                    for($i = 0; $i < count($validate->errors()); $i++){
                        if($validate->errors()[$i] == 'password must match password-again'){
                            $matched = false;
                        }
                    }
                }
            }
            else{
                echo 'Enter new password and confirm password.';
            }
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <!-- Custom  CSS-->
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- Chartist -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">

        <title>BC | Reset Password</title>
    </head>
    <body id="grad" style="overflow-x: auto; min-width: 100%; background-color: #ECF3F8;">
        <div class="container" style="min-height: 100vh;">
            <div class="row d-flex justify-content-center mb-5 mt-6">
                <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-6 card rounded-0-1 my-auto custom-card white-boxShadow">
                <!-- Logo -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-8 col-sm-8 col-md-6 col-lg-6 col-xl-6 logo-size-int-2">
                            <img src="img/logo.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                <!-- Heading -->
                    <div class="row d-flex justify-content-center recovery-2">    
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <span>Set password</span>
                        </div>
                    </div>
                <!-- Lock -->
                    <div class="row d-flex justify-content-center kwl-icon recovery-2">
                        <div class="col-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 px-0">
                            <img src="img/Verify icon.png" class="img-fluid" alt="logo">
                        </div>
                    </div>
                    <?php
                        if(!$matched){
                            echo '<p class="text-color-red fs-13 text-center mb-4">Passwords don\'t match</p>';
                        }
                    ?>
                <!-- Form -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-11 col-sm-9 col-md-7 col-lg-9 col-xl-9">
                            <form action="" method="POST" id="needs-validation">
                <!-- Password -->
                                <div class="input-group shadow-custom-sm org-name g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Lock icon.svg" class="img-fluid email_icon_width pl-1" alt=""/></span>
                                    </div>
                                    <input name="password" id="password" type="password" class="form-control p-0 border-left-0 pl-0 rounded-right-border field-text-color fs-13 g-field-size" placeholder="New Password" required>
                                    <div id="revealPassword" class="input-group-append">
                                        <span class="input-group-text bg-white rounded-right-border"><span id="eye-icon" class="eye-style"><i class="far fa-eye"></i></span></span>
                                    </div>
                                </div>
                    <!-- Password -->
                                <div class="input-group shadow-custom-sm org-name-login g-field-size">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white rounded-left-border"><img src="img/Lock icon.svg" class="img-fluid email_icon_width pl-1" alt=""/></span>
                                    </div>
                                    <input name="password-again" id="password-again" type="password" class="form-control p-0 border-left-0 pl-0 rounded-right-border field-text-color fs-13 g-field-size" placeholder="Confirm password" required>
                                    <div id="revealPasswordAgain" class="input-group-append">
                                        <span class="input-group-text bg-white rounded-right-border"><span id="eye-icon1" class="eye-style"><i class="far fa-eye"></i></span></span>
                                    </div>
                                </div>
                <!-- Next -->
                                <div class="row d-flex justify-content-center set-b">
                                    <div class="col-8 col-sm-8 col-md-7 col-lg-5 col-xl-5">
                                        <input type="submit" name="reset" class="btn btn-block nav-free-but the-border main-purple fs-12" value="Reset">
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="py-0 bg-color mf-border">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>
        
        <script>
            (function() {
            'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            // form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();

            $('#revealPassword').on('click', function (){
                var inputType = $('#password').attr('type');
                if(inputType === 'password'){
                    $('#password').get(0).type = 'text';
                    $('#eye-icon').html('<i class="far fa-eye-slash"></i>');
                }else{
                    $('#password').get(0).type = 'password';
                    $('#eye-icon').html('<i class="far fa-eye"></i>');
                }
            });

            $('#revealPasswordAgain').on('click', function (){
                var inputType = $('#password-again').attr('type');
                if(inputType === 'password'){
                    $('#password-again').get(0).type = 'text';
                    $('#eye-icon1').html('<i class="far fa-eye-slash"></i>');
                }else{
                    $('#password-again').get(0).type = 'password';
                    $('#eye-icon1').html('<i class="far fa-eye"></i>');
                }
            });
        </script>


        
        <!-- Chartist -->
        <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    </body>
</html>
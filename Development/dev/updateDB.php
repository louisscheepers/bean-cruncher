<?php 
ini_set('display_errors', 'On');
require_once (__DIR__ . '/core/init.php');

$email = new Email('webmaster@beancruncher.co.za');
$email::sendCustom('stiaan@cubezoo.joburg', 'Database Update Started', 'Database update has been initiated, you will be notified when it is completed.');

$db = DB::getInstance();
$resultMsg = '<div class="row text-right float-right w-100">';

$companyQuery = $db->get('temp_company', array(
    'company_id' => array('operator' => '>', 'value' => 0)
));

if($companyQuery->count()){
    $allCompanies = $companyQuery->results();
    
    foreach($allCompanies as $company){
        
        Companies::setActiveCompany($company->company_id);
        $oauth = new Oauth();
        $oauth->setSessionOauthFromDb();
        // Companies::setUpdatedModules(1);

        $resultMsg .= printLine('<b>Starting update: ' . $company->company_name . ' with ID: ' . $company->company_id . '</b>');

        // Chart of Accounts
        try {
            $resultMsg .= printLine('Updating Chart of Accounts');

            $coa = new DataChartOfAccounts(null);
            $coa->updateDb();

            $resultMsg .= printLine('Success');
        }
        catch(Exception $e){
            $resultMsg .= printLine('<span style="color: red">Failed: ' . $e->getMessage() . '</span>');
        }

        // Inventory
        try {
            $resultMsg .= printLine('Updating Inventory');

            $inv = new DataInventory(null);
            $inv->updateDb();

            $resultMsg .= printLine('Success');
        }
        catch(Exception $e){
            $resultMsg .= printLine('Failed: ' . $e->getMessage());
        }

        // Invoices + Line Items
        try {
            $resultMsg .= printLine('Updating Invoices');

            $inv = new DataInvoices(null);
            $inv->updateDb();

            $resultMsg .= printLine('Success');
        }
        catch(Exception $e){
            $resultMsg .= printLine('Failed: ' . $e->getMessage());
        }

        // Cash In v Cash Out
        if($company->api_id == 2 || $company->api_id == 3){
            try {
                $resultMsg .= printLine('Updating Cash In v Cash Out');

                $month = new DateTime('first day of this month');
                $lastMonth = new DateTime('first day of last month');

                $resultMsg .= printLine('Cash Received for ' . $month->format('Y-m-d'));
                $cr1 = new DataCashReceived($month->format('Y-m-d'));
                $cr1->updateDb();
                $resultMsg .= printLine('Cash Spent for ' . $month->format('Y-m-d'));
                $cs1 = new DataCashSpent($month->format('Y-m-d'));
                $cs1->updateDb();

                $resultMsg .= printLine('Cash Received for ' . $lastMonth->format('Y-m-d'));
                $cr2 = new DataCashReceived($lastMonth->format('Y-m-d'));
                $cr2->updateDb();
                $resultMsg .= printLine('Cash Spent for ' . $lastMonth->format('Y-m-d'));
                $cs2 = new DataCashSpent($lastMonth->format('Y-m-d'));
                $cs2->updateDb();

                $resultMsg .= printLine('Success');
            }
            catch(Exception $e){
                $resultMsg .= printLine('Failed: ' . $e->getMessage());
            }
        }
        else{
            $resultMsg .= printLine('Skipping Cash In v Cash Out for this API');
        }

        // Profit & Loss and Balance Sheet
        if($company->api_id == 3){
            try {
                for($i = 0 ; $i < 12; $i++){
                    $month = new DateTime('first day of -' . ($i) . ' month');
                    $resultMsg .= printLine('Updating Profit and Loss for ' . $month->format('Y-m-t'));
        
                    $pl = new DataProfitAndLoss($month->format('Y-m-d'), $month->format('Y-m-t'), 11);
                    $pl->updateDb();

                    $resultMsg .= printLine('Updating Balance Sheet for ' . $month->format('Y-m-t'), 11);
                    $bs = new DataBalanceSheet($month->format('Y-m-t'), 11);
                    $bs->updateDb();
                }
                $resultMsg .= printLine('Success');
            }
            catch(Exception $e){
                $resultMsg .= printLine('Failed: ' . $e->getMessage());
            }
        }
        else if($company->api_id == 1 || $company->api_id == 2){
            try {
                $firstDayThis = new DateTime('first day of this month');
                if($firstDayThis->format('t') != '31'){
                    $firstDayThis = new DateTime('first day of last month');
                }
                
                $resultMsg .= printLine('Updating Balance Sheet for ' . $firstDayThis->format('Y-m-t'));
                $bs = new DataBalanceSheet($firstDayThis->format('Y-m-d'), 11);
                $bs->updateDb(false);
                $resultMsg .= printLine('Success');

                $resultMsg .= printLine('Updating Profit & Loss for ' . $firstDayThis->format('Y-m-t'));
                $pl = new DataProfitAndLoss($firstDayThis->format('Y-m-d'), $firstDayThis->format('Y-m-t'), 11);
                $pl->updateDb();
                $resultMsg .= printLine('Success');
            }
            catch(Exception $e){
                $resultMsg .= printLine('Failed: ' . $e->getMessage());
            }
        }
    }
}

$resultMsg .= '</div>';

function printLine($message){
    $now = new DateTime();
    $today = $now->format('Y-m-d H:i:s');

    //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
    //$logger->info($message);

    $processedMessage = '[' . $today . ']: ' . $message . '<br>';
    return $processedMessage;
}

$email::sendCustom('stiaan@cubezoo.joburg', 'Database Update Report', $resultMsg);
<?php
    ini_set('display_errors', 'On');
    require_once(__DIR__ . '/core/init.php');
    require_once('const/const.php');

    //$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
    //$logger->info('Getting QB class instance');

    $quickbooks = new Quickbooks(true);
    //$logger->info('Getting data service');
    $dataService = $quickbooks->getDataService();
    
    //$logger->info('Setting access token');
    $qbAuth = $_SESSION['sessionAccessToken'];

    // echo 'qbAuth: <br>';
    // var_dump($qbAuth);
    
    // echo 'SESSION: sessionAccessToken: <br>';
    // var_dump($_SESSION['sessionAccessToken']);
    
    // die();

    //$logger->info('Updating oauth2token');
    $dataService->updateOAuth2Token($qbAuth);
    
    //$logger->info('Getting company info');
    $companyInfo = $dataService->getCompanyInfo();

    // var_dump($dataService->getCompanyInfo());
    // var_dump($dataService);
    // var_dump($companyInfo->CompanyName);
    // die();

    $insertCompany = Companies::setCompany(3, $qbAuth->getRealmID(), $companyInfo->CompanyName);
    if($insertCompany){
        //$logger->info('Company inserted into DB');

        $accessToken = $qbAuth->getAccessToken();
        $refreshToken = $qbAuth->getRefreshToken();

        $insertIntegration = $quickbooks->insertOauth($accessToken, $refreshToken, $qbAuth->getRealmID());
        if($insertIntegration){
            //$logger->info('Integration inserted into DB');
        }
    }
    

    // echo '<br>Access token key: ' . $accessToken->accessTokenKey;
    // if($insert){
    Redirect::to('index.php');
    // }
    // else{
    //     echo 'Something went wrong, check logs';
    // }

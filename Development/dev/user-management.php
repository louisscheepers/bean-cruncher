<?php
    ini_set('display_errors', 'On');
    require_once('const/const.php');
    require_once(__DIR__ . '/core/init.php');

    $user = new User();
    if($user->isLoggedIn()){
        Companies::setActiveCompany(Companies::getActiveCompanyId());
        $role = Companies::getUserCompanyRole(Companies::getActiveCompanyId());
        if($role == 3){
            Redirect::to('index.php');
        }
    }
    else{
      Redirect::to('index.php');
    }
    if(Companies::getActiveCompanyId() == null){
        $companies = Companies::getCompanies();
        if($companies == null){
            Redirect::to('integration.php');
        }
        else{
            Companies::setActiveCompany($comapnies[0]['id']);
        }
    }
    
    $previous = "javascript:history.go(-1)";
    if(isset($_SERVER['HTTP_REFERER'])) {
        $previous = $_SERVER['HTTP_REFERER'];
    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=1200" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>User Management</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/custom.css" rel="stylesheet" />
        <link href="css/toggle_switch.css" rel="stylesheet" />
        <link rel="shortcut icon" type="image/png" href="img/Slices/new/pinkFav.png">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="content-bg">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
               
            <nav class="sb-topnav navbar navbar-expand navbar-dark p-0 my-3">    
                    <div class="container nav-size">
                        <div class="row d-content no-gutters">
                            <!-- First Colum with nested colums -->
                            <div class="col-sm-5">
                                <div class="row">
                                <div class="col-sm-2 align-self-center">
                                        <a type="button" class="btn-lg d-content" href="javascript:history.back()">
                                            <img class="img-fluid arrow-icon-size pl-1" src="img/arrow-left-icon.png">
                                        </a>
                                    </div>

                                    <div class="col-sm-10 align-self-center">
                                    <div class="dropdown d-none d-md-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                                    <img class="img-fluid search-icon-small" src="img/Group 1511@2x.png">
                            <input class="search-element" type="text" id="companySearch" autocomplete="off" onkeyup="search()" placeholder="Find company..." title="List of Companies" data-toggle="dropdown">
                            <i class="fas fa-chevron-down search-icon-dropdown"></i>
                            <div id="companiesDropDown" class="dropdown-menu radius-0 r-0" aria-labelledby="dropdownMenuButton">
                              <?php
                                $companies = Companies::getCompanies();

                                for($i = 0; $i < count((array)$companies); $i++){
                                  if($companies[$i]['status'] == 1 && $companies[$i]['role'] != 3){
                                  ?>
                                  <li>
                                    <a id="active-company-dropdown" class="dropdown-item" onclick="loadCompany('<?php echo str_replace('\'', '\\\'', $companies[$i]['name']) ?>', <?php echo $companies[$i]['id']; ?>)"><?php echo $companies[$i]['name']; ?></a>
                                  </li>
                                  <?php
                                  }
                                }
                              ?> 
                            </div>
                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Second Colum with nested colums -->    
                            <div class="col-sm-2">
                                <h5 class="nav-settings d-content txt-color">
                                    User Management
                                </h5>
                            </div>

                            <!-- Third Colum with nested colums -->
                            <div class="col-sm-5">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0 "></div>
                                    </div>
                                    
                                    <div class="col-sm-9 txt-align-right align-self-center">
                                        <div class="dropdown d-none d-md-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
                                        <h5 class="nav-settings d-content mobile-fs-14 txt-white" id="companyDropDown">
                                            <?php echo Companies::getActiveCompanyName() ?>
                                        </h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 align-self-center">
                                        <div id="comp-icon">
                                            <?php
                                              if(Companies::getActiveCompanyBrand() == null){
                                                  echo '<img id="comp-img" src="img/default-company-icon.jpg" class="rounded-circle icon-size" alt="Profile Icon">';
                                              }
                                              else{
                                                  echo '<img id="comp-img" src="data:image/jpeg;base64,' . base64_encode(Companies::getActiveCompanyBrand()) . '" class="rounded-circle icon-size" alt="Profile Icon">';
                                              }
                                            ?>
                                        </div>
                                      </div>
                                </div>
                             </div>
                        </div>
                    </div>
            </nav>
            
        <main>
            <div class="container my-5 p-0">
                <div class="row mt-lg-1">
                    <div class="col-12 mt-lg-1">
                        <div class="card">
                            <div class="card-body profile-card">
                                <div class="row">
                                        <div class="col-sm-1 align-self-center">
                                            <div class="text-right">
                                                <img src="img/companySettings.png" class="img-fluid company-icon-size">
                                            </div>
                                        </div>

                                        <div class="col-sm-7 my-6 align-self-center">
                                            <div class="txt-inline">
                                                <h5 class="font-weight-normal align-middle txt-color mt-m5">
                                                    <span id="companyTitle"> <?php echo Companies::getActiveCompanyName() ?> </span>
                                                </h5>
                                            </div>
                                        </div>

                                        <div class="col-sm-4 my-auto">
                                            <!-- Add User Icon  -->
                                            <div class="row">
                                                <div class="col-sm-9"></div>
                                                <div class="col-sm-1">
                                                <a data-toggle="collapse" href="#addUser" aria-expanded="false" aria-controls="addUser">
                                                <img src="img/Add User icon.png" class="profile-icon-size">
                                            </a>
                                                </div>
                                        </div>
                                    </div>
                                    <!-- Collapse Content -->
                                    <div class="container collapse px-6 " id="addUser">
                                        <form id="userForm" name="userFrom" action="" method="POST">
                                            <div class="row my-3">
                                                <div class="col-sm-2">
                                                <h5 class="font-weight-normal txt-color">Add User</h5>
                                                </div>
                                            </div>
                                            <!-- Email -->
                                            <div class="row no-gutters">
                                                <div class="col-sm-4 my-auto">
                                                    <input type="text" name="email" id="email" class="form-control b-0" placeholder="Email">
                                                </div>
                                                <div class="col-sm-4 my-auto">
                                                    <div id="invite-loader"><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>
                                                    <p id="error-text" class="my-auto small text-muted"></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- Add button -->
                                                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                                                <div class="col-sm-3 my-3">
                                                    <!-- <button type="button" class="btn btn-cross-platform my-3 px-4" onclick="addUser()" data-toggle="collapse" href="#addUser" aria-expanded="false" aria-controls="addUser">Send Invite</button> -->
                                                    <button type="button" class="btn btn-cross-platform my-3 px-4" onclick="addUser()" href="#addUser" aria-controls="addUser">Send Invite</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row sm-blue-card font-weight-bold txt-color-w mb-3 mx-4">
                                    
                                    <div class="col-2 align-self-center">
                                        Name
                                    </div>
                                    <div class="col-3 text-center align-self-center">
                                        Email
                                    </div>
                                    <div class="col-2 text-center align-self-center">
                                        Joined
                                    </div>
                                    <div class="col-1 text-center align-self-center">
                                        Active
                                    </div>
                                    <div class="col-3 text-center align-self-center">
                                        Role
                                    </div>
                                    <div class="col-1 text-right align-self-center">
                                        Delete
                                    </div>
                                </div>

                                <!-- User List -->
                                <div id="ownerlist" class="mx-4 px-8 font-size-14 txt-color"></div>
                                <div id="adminlist" class="mx-4 px-8 font-size-14 txt-color"></div>
                                <div id="stafflist" class="mx-4 px-8 font-size-14 txt-color"></div>
                                <div id="users-loader" class="text-center my-auto" hidden><img src="img/BC-preloader-gif.gif" style="height: 25px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="pending-container"></div>
        </main>
        
        <!-- Delete User Modal -->
        <div class="modal fade" id="confirm-delete" role="dialog">
          <div class="modal-dialog modal-dialog-centered w-25">
            <div class="modal-content txt-color">
            <div class="modal-header">
            <button type="button" class="close z-index-1" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
              <div class="modal-body">
                <div class="container">
                  <div class="row justify-content-center">
                    <img src="img/user-icon.png" class="img-fluid modal-icon-size">
                  </div>
                  <h5 class="my-4 modal-title text-center font-weight-bold font-size-16">Delete User</h5>
                  
                  <div class="row mb-4">
                    <div class="col-6 text-right">
                      <div id="confirm-btn"></div>
                    </div>
                    <div class="col-6 text-left">
                      <button type="button" class="mt-3 btn btn-cross-platform btn-w align-self-center" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Added User Modal -->
        <div id="userAddedModal" class="modal fade" style="padding-top: 265px;" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content px-35px py-50px" style="border-radius: 1em;">
                    <div class="col-12 mb-3">
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="col-12 mb-2">
                        <span class="fs-21 font-weight-bold">New user</span>
                    </div>
                    <div class="col-12 mb-5">
                        <span id="modalMsg" class="fs-16">
                        </span>
                    </div>
                    <div class="col-12">
                        <button type="button" class="btn bg-light-green text-black w-300px fs-18 " data-dismiss="modal">Done</button>
                    </div>
                </div>
            </div>
        </div>

        <footer class="py-0 bg-color mf-border fixed-bottom">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-4 align-self-center mb-footer-center">
                    <h5 class="text-white mb-0 mt-fc">Beancruncher</h5>
                </div>
                <div class="col-12 col-lg-4 footer-purple my-3 small">
                    <p class="m-0 text-center fs-12 font-weight-200 text-white">Copyright©2021 |
                    <a class="fs-12 font-weight-200 text-white">Designed by </a>
                    <a class="fs-12 font-weight-200 text-white" href="http://cubezoo.co.za/" target="#blank">CubeZoo</a>
                    </p>
                </div> 
                <div class="col-lg-4 d-none d-lg-block text-right">
                    <img class="img-fluid fw-icon mr-n4" src="img/dashFooter.png">
                </div>
            </div>
        </div>
    </footer>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        
        <!-- User Management Script -->
        <script src="js/user-management.js"></script>

        <!-- Simple Search Script -->
        <script src="js/simpleSearch.js"></script>

    </body>
</html>

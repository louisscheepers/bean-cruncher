let data = null;

function onDragStart(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}
function onDragOver(ev) {
    ev.preventDefault();
}
function drop(ev) {
    ev.preventDefault();
    data = ev.dataTransfer.getData("text");
    let boxid = ev.target.id;

    //change data on drop
    if(boxid === "box1" || boxid === "box8")
    {
        bigBox(ev, data, boxid);
    }
    else if(boxid === "box2" || boxid === "box5")
    {
        medBox(ev, data, boxid);
    }
    else if(boxid === "box3" || boxid === "box4" || boxid === "box6" || boxid === "box7")
    {
        smallBox(ev, data, boxid);
    }
}
function dropBox(ev) {
    drop(ev);
    //document.getElementById(ev.target.id/*current drop on box*/ */).value = data;    //set current box to value of dataID
}
function bigBox(ev, da, box)
{
    let par = document.getElementById(box);
    // if(box === "box1")
    // {
    //     par = document.getElementById(box);
    // }
    // else if(box === "box8")
    // {
    //     par = document.getElementById(box);
    // }
   // par.innerHTML = "Fill: ".bold() + da + " Dropzone: ".bold() + ev.target.id;
    par.innerHTML = '<div class="ct-chart chartlg ct-golden-section" id="ch' + box + '"></div>';
    buildChartL(box);
    ev.currentTarget.style.backgroundColor = "#FFF";
    //AJAX request + fill
}
function medBox(ev, da, box)
{
    let par = document.getElementById(box);
    // if(box === "box2")
    // {
    //     par = document.getElementById(box);
    // }
    // else if(box === "box5")
    // {
    //     par = document.getElementById(box);
    // }
    //par.innerHTML = "Fill: ".bold() + da + " Dropzone: ".bold() + ev.target.id;
    let n1 = Math.floor(Math.random() * 200);
    let n2 = Math.floor(Math.random() * 200);

    par.innerHTML = 
    '<div class="row smBox">'
    +   '<div class="col-6 no-gutters">'
    +       '<div class="ct-chart chartmd ct-golden-section" id="ch' + box + '"></div>'
    +   '</div>'
    +   '<div class="col-6 no-gutters">'
    +       '<div class="row smBox"><div class="col-3 no-gutters">'
    +'<i class="fa fa-arrow-up green arrowBoxM" aria-hidden="true"></i>'
    +'</svg></div><div class="col-3 no-gutters"><span class="mBoxText green"><b>'+n1+'K</b></span></div>'
    +'<div class="col-3 no-gutters">'
    +'<i class="fa fa-arrow-down red arrowBoxM" aria-hidden="true"></i>'
    +'</svg></div><div class="col-3 no-gutters"><span class="mBoxText red"><b>'+n2+'K</b></span></div></div>'
    +   '</div>'
    +'</div>'
    buildChartM(box);
    ev.currentTarget.style.backgroundColor = "#FFF";
    //AJAX request + fill
}
function smallBox(ev, da, box)
{
    let par = document.getElementById(box);
    // if(box === "box3")
    // {
    //     par = document.getElementById(box);
    // }
    // else if(box === "box4")
    // {
    //     par = document.getElementById(box);
    // }
    // else if(box === "box6")
    // {
    //     par = document.getElementById(box);
    // }
    // else if(box === "box7")
    // {
    //     par = document.getElementById(box);
    // }
    let n1 = Math.floor(Math.random() * 200);
    let n2 = Math.floor(Math.random() * 200);

    par.innerHTML = par.style.display = 
    '<div class="row smBox"><div class="col-3 no-gutters">'
    +'<i class="fa fa-arrow-up green arrowBoxS" aria-hidden="true"></i>'
    +'</svg></div><div class="col-3 no-gutters"><span class="sBoxText green"><b>'+n1+'K</b></span></div>'
    +'<div class="col-3 no-gutters">'
    +'<i class="fa fa-arrow-down red arrowBoxS" aria-hidden="true"></i>'
    +'</svg></div><div class="col-3 no-gutters"><span class="sBoxText red"><b>'+n2+'K</b></span></div></div>';
    ev.currentTarget.style.backgroundColor = "#FFF";
    //AJAX request + fill
}

function buildChartL(id){
    //initialize chart
    let v1 = Math.random() * 100;
    let v2 = Math.random() * 100;
    let v3 = Math.random() * 100;
    let v4 = Math.random() * 100;
    let v5 = Math.random() * 100;
    let v6 = Math.random() * 100;

    let vv1 = Math.random() * 100;
    let vv2 = Math.random() * 100;
    let vv3 = Math.random() * 100;
    let vv4 = Math.random() * 100;
    let vv5 = Math.random() * 100;
    let vv6 = Math.random() * 100;

    let chart = new Chartist.Line('#ch'+id, {
        labels:['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
        series:[[v1, v2, v3, v4, v5, v6],
                [vv1, vv2, vv3, vv4, vv5, vv6]]
    }, {
        low: 0,
        high: 100,
        showArea: true,
        showPoint: false,
        fullWidth: true
        }
    )
    //draw lines
    chart.on('draw', function(data){
        if(data.type == 'line' || data.type == 'area')
        {
            data.element.animate({
                d:{
                    begin: 0 * data.index,
                    dur: 2000,
                    from: data.path.clone().scale(1,0).translate(0, 
                        data.chartRect.height()).stringify(), 
                    to: data.path.clone().stringify(), 
                    easing: Chartist.Svg.Easing.easeOutQuint 
                }
            });
        }
    });
}
function buildChartM(id){
    //initialize chart
    let v1 = Math.random() * 100;
    let v2 = Math.random() * 100;
    let v3 = Math.random() * 100;
    let v4 = Math.random() * 100;
    let v5 = Math.random() * 100;
    let v6 = Math.random() * 100;

    let vv1 = Math.random() * 100;
    let vv2 = Math.random() * 100;
    let vv3 = Math.random() * 100;
    let vv4 = Math.random() * 100;
    let vv5 = Math.random() * 100;
    let vv6 = Math.random() * 100;

    let chart = new Chartist.Line('#ch'+id, {
        labels:['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
        series:[[v1, v2, v3, v4, v5, v6],
                [vv1, vv2, vv3, vv4, vv5, vv6]]
    }, {
        low: 0,
        high: 100,
        showArea: true,
        showPoint: false,
        fullWidth: true
        }
    )
    //draw lines
    chart.on('draw', function(data){
        if(data.type == 'line' || data.type == 'area')
        {
            data.element.animate({
                d:{
                    begin: 0 * data.index,
                    dur: 2000,
                    from: data.path.clone().scale(1,0).translate(0, 
                        data.chartRect.height()).stringify(), 
                    to: data.path.clone().stringify(), 
                    easing: Chartist.Svg.Easing.easeOutQuint 
                }
            });
        }
    });
}
<?php

?>
<!-- Main Container -->
<div class="container bg-white shadow-black-lg max-w-90per w-90per text-center min-h-680px mt-5 fs-24 rounded mb-5 pb-10px">
    <!-- Add User -->
    <!-- Add User Icon  -->
    <div class="row">
        <div class="container mt-4 mr-1 float-right">
            <a data-toggle="collapse" href="#addUser" aria-expanded="false" aria-controls="addUser">
                <img src="img/add-user.png" class="float-right mt-2 mb-5">
            </a>
        </div>
    </div>
    <!-- Collapse Content -->
    <div class="container collapse" id="addUser">
        <form id="userForm" action="" method="POST">
            <div class="row mt-3 mb-5">
                <div class="col-12">
                    <img src="img/user-1.png" class="h-48px float-left ml-4">
                    <span class="float-left fs-28 ml-3 mt-5px font-weight-bold"> Add User <span>
                </div>
            </div>
            <div class="row">
                <!-- Name -->
                <div class="col-12 col-lg-6">
                    <div class="shadow-black-sm input-group mb-3 rounded-0-5">
                        <div class="input-group-prepend mr-0">
                            <span class="input-group-text h-43px b-0 bg-white" id="name-form">
                                <img src="img/user.svg" class="img-responsive bg-white" alt=""/>
                            </span>
                        </div>
                        <input type="text" name="name" id="name" class="form-control h-43px b-0" placeholder="Name">
                    </div>  
                </div>
                <!-- Surname -->
                <div class="col-12 col-lg-6">
                    <div class="shadow-black-sm input-group mb-3 rounded-0-5">
                        <div class="input-group-prepend mr-0">
                            <span class="input-group-text h-43px b-0 bg-white" id="basic-addon1">
                                <img src="img/user.svg" class="img-responsive bg-white" alt=""/>
                            </span>
                        </div>
                        <input type="text" name="surname" id="surname" class="form-control h-43px b-0" placeholder="Surname">
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- Cell Number -->
                <div class="col-12 col-lg-6">
                    <div class="shadow-black-sm input-group mb-3 rounded-0-5">
                        <div class="input-group-prepend mr-0">
                            <span class="input-group-text h-43px b-0 bg-white" id="basic-addon1">
                                <img src="img/cell.png" class="img-responsive bg-white" alt=""/>
                            </span>
                        </div>
                        <input type="text" name="number" id="number" class="form-control h-43px b-0" placeholder="Cell number">
                    </div>
                </div>
                <!-- Desired Password -->
                <div class="col-12 col-lg-6">
                    <div class="shadow-black-sm input-group rounded-0-5">
                        <div class="input-group-prepend mr-0">
                            <span class="input-group-text h-43px b-0 bg-white" id="basic-addon1">
                                <img src="img/password.svg" class="img-responsive bg-white" alt=""/>
                            </span>
                        </div>
                        <input type="password" name="password" id="password" class="form-control h-43px b-0" placeholder="Password">
                    </div>
                </div>
            </div>  
            <div class="row mb-80px">
                <div class="col-12 col-lg-2">
                    <!-- Add button -->
                    <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                    <div class="col-12 p-0">
                        <input type="submit" class="btn bg-light-green text-black b-0 h-42px fs-18 w-100per mt-5" value="Add">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- Add User Modal -->
    <div id="newUserAdded" class="modal fade" style="padding-top: 265px;" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content px-35px py-50px" style="border-radius: 1em;">
                <div class="col-12 mb-3">
                    <img src="img/user-1.png" class="h-90px">
                </div>
                <div class="col-12 mb-2">
                    <span class="fs-21 font-weight-bold">New user</span>
                </div>
                <div class="col-12 mb-5">
                    <span id="modalMsg" class="fs-16">
                      
                            
                       </span>
                </div>
                <div class="col-12">
                    <button type="button" class="btn bg-light-green text-black w-300px fs-18 " data-dismiss="modal">Done</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Title Row -->
    <div class="row mb-5 d-none d-sm-flex">
        <div class="sm-0 col-1">

        </div>
        <div class="col-2">
            <h5 id="#nameHeader" class="font-weight-bold fs-26 sm-0">Name</h5>
        </div>
        <div class="col-2">
            <h5 class="font-weight-bold ml-4 fs-26 sm-0">Active</h5>
        </div>
        <div class="col-2">
            <h5 class="font-weight-bold  fs-26 sm-0">Date created</h5>
        </div>
        <div class="col-2">
            <h5 class="font-weight-bold fs-26 sm-0">Password</h5>
        </div>
        <div class="col-2">
            <h5 class="font-weight-bold fs-26 sm-0">Delete user</h5>
        </div>
    </div>
    <div id="userlist" class="userlist"></div>
    

    <?php
    
    ?>

<script>
$( document ).ready(function() {
    load();
 
});

function load(){
    $.getJSON('load.php?q=users', function(result){
        
        var html = '';
        var x = 0;
        $.each(result, function(key, user){
            if(x > 4){
                 x=0;
            }
             x++;
             
            html += '<div class="row mb-5 d-none d-sm-flex">' +
                '<div class="col-1 p-15px d-none d-sm-block">' + 
                    '<img src="img/user-' + x + '.png" class="h-48px">'+
               ' </div>'+
            '<div class="col-2 data-row rounded-left p-10px">'+
                '<div class="mt-10px d-none d-sm-block">' +
                user.name + ' ' + user.surname +
                '</div>'+ '<div class="mt-10px d-sm-none d-block">' +
                user.name + '</div>' +
           ' </div>' +
            '<div class="col-2 data-row p-15px">'+
                '<label class="switch mt-2">' +
                    '<input type="checkbox" ' + user.checked + '>' +
                    '<span class="slider round h-30px w-80px"></span>' +
               ' </label>' +
           ' </div>' +
            '<div class="col-2 data-row p-10px">' +
                '<div class="mt-10px">' +
                user.joined +
                '</div>' +
            '</div>'+
           ' <div class="col-2 data-row p-10px">'+
                '<div class="mt-15px font-weight-bold d-none d-sm-block">********<a href="" class="float-right mt-n1 mr-5 text-black font-weight-normal">Edit</a></div>' +
            '</div>' +
            '<div class="col-2 data-row p-10px">' +
                '<button type="button"  onclick="deleteUser(' + user.id + ')" class="btn btn-img mt-1"><img src="img/delete.png" ></button>'+
            '</div>' +
            '</div>' + 
            //Mobile view
            '<div class="wrapper d-sm-none card my-3 h-50px bg-light-grey b-0 w-100per">' +
                '<button class="btn btn-link" data-toggle="collapse" data-target="#collapse' + user.id + '" aria-expanded="false" aria-controls="collapse' + user.id + '">' +
                    '<div class="row">' +
                        '<div class="col-1">' +
                            '<img src="img/user-' + x + '.png" class="h-30px m-0">' +
                        '</div>' +
                        '<div class="col-9 text-center text-black">' +
                            user.name + ' ' + user.surname +
                        '</div>' +
                        '<div class="col-1" id="user1">' +
                            '<i class="fa fa-caret-down"></i>' +
                        '</div>' +
                    '</div>' +
                '</button>' +
                '<div class="collapse" id="collapse' + user.id + '">' +
                    '<div class="row">' +
                        '<div class="col-3 text-center font-weight-bold pr-0">Active</div>' +
                        '<div class="col-3 text-center font-weight-bold sm-0">Joined</div>' +
                        '<div class="col-3 text-center font-weight-bold sm-0">Password</div>' +
                        '<div class="col-3 text-center font-weight-bold sm-0">Delete</div>' +
                    '</div>' + 
                    '<div class="row">' +
                        '<div class="col-3 text-center mt-2">' +
                            '<label class="switch">' +
                                '<input type="checkbox" ' + user.checked + '>' +
                                '<span class="slider-sm round w-40px h-20px"></span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="col-3 text-center mt-2 fs-12 p-0">' + user.joined + '</div>' +
                        '<div class="col-3 text-center mt-2 fs-12 ">' +
                            '<a href="" class="text-black">Edit</a>' +
                        '</div>' +
                        '<div class="col-3 text-center mt-2">' +
                            '<button type="button" onclick="deleteUser(' + user.id + ')" class="btn btn-img mt-n2"><img src="img/delete.png" class=" w-10px h-15px"></button>' +
                        '</div>' +
                    '</div>' + 
                '</div>' +
            '</div>'
        });

        $('.userlist').html(html).slidedown;

        $('.collapse')
        .on('shown.bs.collapse', function(){
            $(this)
                .parent()
                .find(".fa-caret-down")
                .removeClass("fa-caret-down")
                .addClass("fa-caret-up")
        })
        .on('hidden.bs.collapse', function(){
            $(this)
                .parent()
                .find(".fa-caret-up")
                .removeClass("fa-caret-up")
                .addClass("fa-caret-down")
        })
    });
}

function deleteUser(id){
   $.get('load.php?q=delete&id=' + id, function(response){
        load();
   });
}
// Variable to hold request
var request;

// Bind to the submit event of our form
$("#userForm").submit(function(event){

    // Prevent default posting of form - put here to work in case of errors
    event.preventDefault();

    // Abort any pending request
    if (request) {
        request.abort();
    }
    // setup some local variables
    var $form = $(this);

    // Let's select and cache all the fields
    var $inputs = $form.find("input, select, button, textarea");

    // Serialize the data in the form
    var serializedData = $form.serialize();

    // Let's disable the inputs for the duration of the Ajax request.
    // Note: we disable elements AFTER the form data has been serialized.
    // Disabled form elements will not be serialized.
    $inputs.prop("disabled", true);

    // Fire off the request to /form.php
    request = $.ajax({
        url: "add-users.php",
        type: "post",
        data: serializedData
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        load();
        $('#modalMsg').append($('#name').val() + ' has been added to the user list');
        console.log("POST SUCCESS SHOW MODAL");
        $('#addUser').collapse('hide');
        $('#newUserAdded').modal('show');
        
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs
        $inputs.prop("disabled", false);
    });

});

</script>